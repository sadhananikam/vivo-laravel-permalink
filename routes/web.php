<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    //return view('welcome');
//    return view('index');
//});

/* All mobile routes */
Route::get('/m', function () {
    return \File::get(public_path() . '/m-partials/home.html');
});

Route::get('/m/home', function () {
   return \File::get(public_path() . '/m-partials/home.html');
});

Route::get('/m/about-us', function () {
    return \File::get(public_path() . '/m-partials/about.html');
});

Route::get('/m/brands', function () {
   return \File::get(public_path() . '/m-partials/brands.html');
});

Route::get('/m/contact-us', function () {
   return \File::get(public_path() . '/m-partials/contact.html');
});

Route::get('/m/privacy', function () {
   return \File::get(public_path() . '/m-partials/privacypolicy.html');
});

Route::get('/m/terms', function () {
   return \File::get(public_path() . '/m-partials/terms.html');
});

Route::get('/m/lookbook', function () {
   return \File::get(public_path() . '/m-partials/lookbook.html');
});

Route::get('/m/jewellery-education', function () {
   return \File::get(public_path() . '/m-partials/jewelleryeducation.html');
});

Route::get('/m/find-your-ring-size', function () {
   return \File::get(public_path() . '/m-partials/ringguide.html');
});

Route::get('/m/faq/{id?}', function () {
   return \File::get(public_path() . '/m-partials/faq.html');
});

Route::get('/m/partner-with-us', function () {
   return \File::get(public_path() . '/m-partials/partner.html');
});

Route::get('/m/affiliate', function () {
   return \File::get(public_path() . '/m-partials/affiliate.html');
});

/*Route::get('/m/lookbook/{urlslug}.html', function ($urlslug) {
    $canurl = "https://www.vivocarat.com/lookbook/".$urlslug.".html";
    $data = \File::get(public_path() . '/m-partials/lookbookArticle.html');
    $data = str_replace('$canurl', $canurl, $data);
    return $data;
    //return \File::get(public_path() . '/m-partials/lookbookArticle.html');
});*/
Route::get('/m/lookbook/{urlslug}.html', 'Allroutes@mobilelookbookarticle');

/*Route::get('/m/brands/{store}', function ($store) {
    $canurl = "https://www.vivocarat.com/brands/".$store;
    $data = \File::get(public_path() . '/m-partials/store.html');
    $data = str_replace('$canurl', $canurl, $data);
    return $data;
   //return \File::get(public_path() . '/m-partials/store.html');
});*/
Route::get('/m/brands/{store}', 'Allroutes@mobilestorepage');

/*Route::get('/m/brands/{store}/{category}.html', function ($store,$category) {
    $canurl = "https://www.vivocarat.com/brands/".$store."/".$category.".html";
    $data = \File::get(public_path() . '/m-partials/splist.html');
    $data = str_replace('$canurl', $canurl, $data);
    return $data;
//   return \File::get(public_path() . '/m-partials/splist.html');
});*/
Route::get('/m/brands/{store}/{category}.html', 'Allroutes@mobilestorecategorypage');

/*Route::get('/m/jewellery/{urlslug}.html', function ($urlslug) {
    $canurl = "https://www.vivocarat.com/jewellery/".$urlslug.".html";
    $data = \File::get(public_path() . '/m-partials/list.html');
    $data = str_replace('$canurl', $canurl, $data);
    return $data;
//   return \File::get(public_path() . '/m-partials/list.html');
});*/
Route::get('/m/jewellery/{urlslug}.html', 'Allroutes@mobilelistpage');

Route::get('/m/checkout', function () {
   return \File::get(public_path() . '/m-partials/checkout.html');
});

Route::get('/m/login', function () {
   return \File::get(public_path() . '/m-partials/login.html');
});

Route::get('/m/account', function () {
   return \File::get(public_path() . '/m-partials/account.html');
});

Route::get('/m/address', function () {
   return \File::get(public_path() . '/m-partials/address.html');
});

Route::get('/m/orders', function () {
   return \File::get(public_path() . '/m-partials/orders.html');
});

Route::get('/m/edit-account', function () {
   return \File::get(public_path() . '/m-partials/editaccount.html');
});

Route::get('/m/payment-success-{id}.html', function () {
   return \File::get(public_path() . '/m-partials/paymentsuccess.html');
});

Route::get('/m/reset-password-{token}.html', function () {
   return \File::get(public_path() . '/m-partials/resetpassword.html');
});

/*Route::get('/m/search/{tag}.html', function ($tag) {
    $canurl = "https://www.vivocarat.com/search/".$tag.".html";
    $data = \File::get(public_path() . '/m-partials/search.html');
    $data = str_replace('$canurl', $canurl, $data);
    return $data;
//   return \File::get(public_path() . '/m-partials/search.html');
});*/
Route::get('/m/search/{tag}.html', 'Allroutes@mobilesearchpage');

/*Route::get('/m/product/{urlslug}.html', function ($urlslug) {
    $canurl = "https://www.vivocarat.com/product/".$urlslug.".html";
    $data = \File::get(public_path() . '/m-partials/product.html');
    $data = str_replace('$canurl', $canurl, $data);
    return $data;
//   return \File::get(public_path() . '/m-partials/product.html');
});*/
Route::get('/m/product/{urlslug}.html', 'Allroutes@mobileproductpage');

Route::get('/m/compare', function () {
   return \File::get(public_path() . '/m-partials/compare.html');
});

Route::get('/m/return', function () {
   return \File::get(public_path() . '/m-partials/returns.html');
});

Route::get('/m/wishlist', function () {
   return \File::get(public_path() . '/m-partials/wishlist.html');
});

Route::get('/m/404', function () {
   return \File::get(public_path() . '/m-partials/404.html');
});

Route::get('/m/seller-locate', function () {
   return \File::get(public_path() . '/m-partials/sellerlocate.html');
});

Route::get('/m/diwali-offer', function () {
   return \File::get(public_path() . '/m-partials/diwalioffer.html');
});



/* All desktop routes */
Route::get('/', function () {
   return \File::get(public_path() . '/default.php');
});
Route::get('/home', function () {
   return \File::get(public_path() . '/default.php');
});

Route::get('/about-us', function () {
    return \File::get(public_path() . '/p-about.php');
});

Route::get('/account', function () {
   return \File::get(public_path() . '/p-account.php');
});

Route::get('/address', function () {
   return \File::get(public_path() . '/p-address.php');
});

Route::get('/affiliate', function () {
   return \File::get(public_path() . '/p-affiliate.php');
});

Route::get('/brands', function () {
   return \File::get(public_path() . '/p-brands.php');
});

Route::get('/checkout', function () {
   return \File::get(public_path() . '/p-checkout.php');
});

Route::get('/compare', function () {
   return \File::get(public_path() . '/p-compare.php');
});

Route::get('/contact-us', function () {
   return \File::get(public_path() . '/p-contact.php');
});

Route::get('/edit-account', function () {
   return \File::get(public_path() . '/p-editaccount.php');
});

Route::get('/faq/{id?}', function () {
   return \File::get(public_path() . '/p-faq.php');
});

Route::get('/jewellery-education', function () {
   return \File::get(public_path() . '/p-jewelleryeducation.php');
});

/*Route::get('/jewellery/{urlslug}.html', function ($urlslug) {
    $canurl = "https://www.vivocarat.com/jewellery/".$urlslug.".html";
    $moburl = "https://www.vivocarat.com/m/jewellery/".$urlslug.".html";
    $data = \File::get(public_path() . '/p-list.php');
    $data = str_replace('$canurl', $canurl, $data);
    $data = str_replace('$moburl', $moburl, $data);
    return $data;
//   return \File::get(public_path() . '/m-partials/list.html');
});*/

Route::get('/jewellery/{urlslug}.html', 'Allroutes@listpage');

Route::get('/login', function () {
   return \File::get(public_path() . '/p-login.php');
});

Route::get('/lookbook', function () {
   return \File::get(public_path() . '/p-lookbook.php');
});

/*Route::get('/lookbook/{urlslug}.html', function ($urlslug) {
    $canurl = "https://www.vivocarat.com/lookbook/".$urlslug.".html";
    $moburl = "https://www.vivocarat.com/m/lookbook/".$urlslug.".html";
    $data = \File::get(public_path() . '/p-lookbookArticle.php');
    $data = str_replace('$canurl', $canurl, $data);
    $data = str_replace('$moburl', $moburl, $data);
    return $data;
});*/

Route::get('/lookbook/{urlslug}.html', 'Allroutes@lookbookarticle');

Route::get('/moissanite', function () {
   return \File::get(public_path() . '/p-moissanite.php');
});

Route::get('/orders', function () {
   return \File::get(public_path() . '/p-orders.php');
});

Route::get('/partner-with-us', function () {
   return \File::get(public_path() . '/p-partner.php');
});

Route::get('/payment-success-{id}.html', function ($id) {
    $canurl = "https://www.vivocarat.com/payment-success-".$id.".html";
    $moburl = "https://www.vivocarat.com/m/payment-success-".$id.".html";
    $data = \File::get(public_path() . '/p-paymentsuccess.php');
    $data = str_replace('$canurl', $canurl, $data);
    $data = str_replace('$moburl', $moburl, $data);
    return $data;
//   return \File::get(public_path() . '/p-paymentsuccess.php');
});

Route::get('/privacy', function () {
   return \File::get(public_path() . '/p-privacypolicy.php');
});

/*Route::get('/product/{urlslug}.html', function ($urlslug) {
    $canurl = "https://www.vivocarat.com/product/".$urlslug.".html";
    $moburl = "https://www.vivocarat.com/m/product/".$urlslug.".html";
    $data = \File::get(public_path() . '/p-product.php');
    $data = str_replace('$canurl', $canurl, $data);
    $data = str_replace('$moburl', $moburl, $data);
    return $data;
});*/

Route::get('/product/{urlslug}.html', 'Allroutes@productpage');

Route::get('/reset-password-{token}.html', function ($token) {
    $canurl = "https://www.vivocarat.com/reset-password-".$token.".html";
    $moburl = "https://www.vivocarat.com/m/reset-password-".$token.".html";
    $data = \File::get(public_path() . '/p-resetpassword.php');
    $data = str_replace('$canurl', $canurl, $data);
    $data = str_replace('$moburl', $moburl, $data);
    return $data;
    
//   return \File::get(public_path() . '/p-resetpassword.php');
});

Route::get('/return', function () {
   return \File::get(public_path() . '/p-returns.php');
});

Route::get('/find-your-ring-size', function () {
   return \File::get(public_path() . '/p-ringguide.php');
});

/*Route::get('/search/{tag}.html', function ($tag) {
    $canurl = "https://www.vivocarat.com/search/".$tag.".html";
    $moburl = "https://www.vivocarat.com/m/search/".$tag.".html";
    $data = \File::get(public_path() . '/p-search.php');
    $data = str_replace('$canurl', $canurl, $data);
    $data = str_replace('$moburl', $moburl, $data);
    return $data;
});*/

Route::get('/search/{tag}.html', 'Allroutes@searchpage');

Route::get('/sitemap', function () {
   return \File::get(public_path() . '/p-sitemap.php');
});

/*Route::get('/brands/{store}', function ($store) {
    $canurl = "https://www.vivocarat.com/brands/".$store;
    $moburl = "https://www.vivocarat.com/m/brands/".$store;
    $data = \File::get(public_path() . '/p-store.php');
    $data = str_replace('$canurl', $canurl, $data);
    $data = str_replace('$moburl', $moburl, $data);
    return $data;
});*/

Route::get('/brands/{store}', 'Allroutes@storepage');

/*Route::get('/brands/{store}/{category}.html', function ($store,$category) {
    $canurl = "https://www.vivocarat.com/brands/".$store."/".$category.".html";
    $moburl = "https://www.vivocarat.com/m/brands/".$store."/".$category.".html";
    $data = \File::get(public_path() . '/p-splist.php');
    $data = str_replace('$canurl', $canurl, $data);
    $data = str_replace('$moburl', $moburl, $data);
    return $data;
});*/

Route::get('/brands/{store}/{category}.html', 'Allroutes@storecategorypage');

Route::get('/terms', function () {
   return \File::get(public_path() . '/p-terms.php');
});

Route::get('/wishlist', function () {
   return \File::get(public_path() . '/p-wishlist.php');
});

Route::get('/seller-locate', function () {
   return \File::get(public_path() . '/p-sellerlocate.php');
});

Route::get('/diwali-offer', function () {
   return \File::get(public_path() . '/p-diwaliOffer.php');
});



Route::post('/api/v1/newsletter','Home@newsletter');
Route::get('/api/v1/getbanners','Home@getbanners');

Route::get('/api/v1/getlookbook/{id?}','Home@getlookbook');

Route::get('/api/v1/getLookbookDetail','Home@getLookbookDetail');
Route::get('/api/v1/getLookbookDetailByUrlslug','Home@getLookbookDetailByUrlslug');
Route::get('/api/v1/getLookbookList','Home@getLookbookList');
Route::get('/api/v1/getRelatedBlog','Home@getRelatedBlog');
Route::get('/api/v1/getSimilarProducts','Home@getSimilarProducts');
Route::get('/api/v1/getfeaturedproducts','Home@getfeaturedproducts');

Route::post('/api/v1/contactus','Home@saveContactusform');
Route::post('/api/v1/partner','Home@savePartnerform');
Route::get('/api/v1/getsellerlocation','Home@getsellerlocation');
Route::post('/api/v1/getSellerDetailBySearch','Home@getSellerDetailBySearch');
Route::post('/api/v1/saveSellerLocationEnquiry','Home@saveSellerLocationEnquiry');
Route::post('/api/v1/sendSMS','Home@sendSMS');
Route::post('/api/v1/sendSMSToCustomerNotPurchasedCron','Home@sendSMSToCustomerNotPurchasedCron');

Route::get('/api/v1/testemail','Home@testemail');

Route::get('/api/v1/getStoreCategories','Store@getStoreCategories');
Route::get('/api/v1/getJewellerInformationByName','Store@getJewellerInformationByName');
Route::get('/api/v1/getStoreProducts','Store@getStoreProducts');
Route::get('/api/v1/getStoreProductsCount','Store@getStoreProductsCount');
Route::get('/api/v1/filterStorePrice','Store@filterStorePrice');
Route::get('/api/v1/filterStorePriceCount','Store@filterStorePriceCount');
Route::get('/api/v1/sortStoreList','Store@sortStoreList');
Route::get('/api/v1/sortStoreListCount','Store@sortStoreListCount');

Route::get('/api/v1/getProducts','Category@getProducts');
Route::get('/api/v1/getProductsCount','Category@getProductsCount');
Route::get('/api/v1/filterPrice','Category@filterPrice');
Route::get('/api/v1/filterPriceCount','Category@filterPriceCount');
Route::get('/api/v1/sortList','Category@sortList');
Route::get('/api/v1/sortListCount','Category@sortListCount');
Route::post('/api/v1/savetocart','Category@savetocart');
Route::get('/api/v1/getAllJewellers','Category@getAllJewellers');
Route::get('/api/v1/getcategorydescription','Category@getcategorydescription');

Route::get('/api/v1/getProductsTag','Search@getProductsTag');
Route::get('/api/v1/getProductsTagCount','Search@getProductsTagCount');
Route::get('/api/v1/filterPriceTag','Search@filterPriceTag');
Route::get('/api/v1/filterPriceTagCount','Search@filterPriceTagCount');
Route::get('/api/v1/sortListTag','Search@sortListTag');
Route::get('/api/v1/sortListTagCount','Search@sortListTagCount');
Route::get('/api/v1/gettagdescription','Search@gettagdescription');

Route::get('/api/v1/getProductDetail','Product@getProductDetail');
Route::get('/api/v1/getProductDetailByUrlslug','Product@getProductDetailByUrlslug');
Route::get('/api/v1/getJewellerInformation','Product@getJewellerInformation');
Route::get('/api/v1/getSimilarJewellerProducts','Product@getSimilarJewellerProducts');
Route::get('/api/v1/getSimilarProductsByCat','Product@getSimilarProductsByCat');
Route::get('/api/v1/getProductVariant','Product@getProductVariant');
Route::get('/api/v1/setAlert','Product@setAlert');
Route::post('/api/v1/sendPriceDropAlert','Product@sendPriceDropAlert');
Route::get('/api/v1/getReviewList','Product@getReviewList');
Route::get('/api/v1/getCumulativeReview','Product@getCumulativeReview');
Route::post('/api/v1/addReview','Product@addReview');
Route::get('/api/v1/getWatchlist','Product@getWatchlist');
Route::get('/api/v1/addToWatchlist','Product@addToWatchlist');
Route::get('/api/v1/removeFromWatchlist','Product@removeFromWatchlist');
Route::get('/api/v1/getProductSize','Product@getProductSize');
Route::post('/api/v1/uploadimages','Product@uploadimages');
Route::post('/api/v1/saveCustomizeformData','Product@saveCustomizeformData');
Route::get('/api/v1/getJewellerRating','Product@getJewellerRating');
Route::post('/api/v1/addProductReview','Product@addProductReview');
Route::get('/api/v1/getProductReviewList','Product@getProductReviewList');
Route::get('/api/v1/getProductCumulativeReview','Product@getProductCumulativeReview');

Route::get('/api/v1/session','Customer@session');
Route::post('/api/v1/login','Customer@login');
Route::post('/api/v1/signUp','Customer@signUp');
Route::get('/api/v1/logout','Customer@logout');
Route::post('/api/v1/forgotpassword','Customer@forgotpassword');
Route::get('/api/v1/getAccountDetails','Customer@getAccountDetails');
Route::post('/api/v1/updateAccountDetails','Customer@updateAccountDetails');
Route::get('/api/v1/checkUrl','Customer@checkUrl');
Route::post('/api/v1/resetPassword','Customer@resetPassword');
Route::post('/api/v1/oauth_login','Customer@oauth_login');
Route::post('/api/v1/oauth_login_google','Customer@oauth_login_google');
Route::post('/api/v1/oauth_login_googleplus','Customer@oauth_login_googleplus');
Route::post('/api/v1/updateAccountName','Customer@updateAccountName');
Route::post('/api/v1/updateAccountEmail','Customer@updateAccountEmail');
Route::post('/api/v1/updateAccountPhone','Customer@updateAccountPhone');
Route::post('/api/v1/updateAccountDOB','Customer@updateAccountDOB');
Route::post('/api/v1/updateAccountAnniversary','Customer@updateAccountAnniversary');
Route::post('/api/v1/updatePassword','Customer@updatePassword');
Route::get('/api/v1/getAddressDetails','Customer@getAddressDetails');
Route::post('/api/v1/addAddressFromAccount','Customer@addAddressFromAccount');

Route::post('/api/v1/addOrder','Checkout@addOrder');
Route::get('/api/v1/getAddress','Checkout@getAddress');
Route::post('/api/v1/addNewAddress','Checkout@addNewAddress');
Route::get('/api/v1/getCoupons','Checkout@getCoupons');
Route::get('/api/v1/getOrderDetails','Checkout@getOrderDetails');
Route::get('/api/v1/confirmOrder','Checkout@confirmOrder');
Route::post('/api/v1/removeFromCart','Checkout@removeFromCart');
Route::get('/api/v1/getAddressFromAccount','Checkout@getAddressFromAccount');

Route::get('/api/v1/getOrderHistory','Order@getOrderHistory');
Route::get('/api/v1/cancelOrder','Order@cancelOrder');
Route::post('/api/v1/removeProductFromOrder','Order@removeProductFromOrder');
Route::get('/api/v1/returnOrder','Order@returnOrder');
Route::get('/api/v1/changeItemOrder','Order@changeItemOrder');

Route::get('/api/v1/getVivoHeader','Mobile@getVivoHeader');

Route::get('/api/v1/sitemap','Sitemap@getSitemap');

// Catch all undefined routes. Always gotta stay at the bottom since order of routes matters.
Route::any('/m/{undefinedRoute}', function ($undefinedRoute) {
    //return view('index');
    return \File::get(public_path() . '/m-partials/404.html');
})->where('undefinedRoute', '(.*)');

Route::any('{undefinedRoute}', function ($undefinedRoute) {
    //return view('index');
    return \File::get(public_path() . '/p-404.php');
})->where('undefinedRoute', '(.*)');