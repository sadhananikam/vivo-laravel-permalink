<!DOCTYPE html>
<html lang="en" data-ng-app="vivo">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="http://www.vivocarat.com" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="noindex,nofollow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />

    <link href="css/style.css" rel="stylesheet" media="all" />
    
    <link rel="stylesheet" href="css/kendo.common-material.min.css" />
    <link rel="stylesheet" href="css/kendo.material.min.css" />
    <link rel="stylesheet" href="css/arthref.css">
    <link href="css/form.css" rel="stylesheet" media="all" />
    <link href="css/megamenu.css" rel="stylesheet" media="all" />
    <link href="css/magnific-popup.css" rel="stylesheet" media="all" />
    <link href="css/etalage.css" rel="stylesheet" media="all" />
    <link href="css/megamenu.css" rel="stylesheet" media="all" />
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    <link rel="stylesheet" href="css/daterangepicker.css" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/fwslider.css">
    
    <script>
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        if(isMobile.any())
        {
            document.location = "m-index.html#/"+getUrlParameter('store');
        }
    </script>    
    
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>

<body style="overflow-x: hidden;">

<style>
    .box-shadow-outside {
        -webkit-box-shadow: 0px 4px 11px 2px rgba(198, 198, 188, 1);
        -moz-box-shadow: 0px 4px 11px 2px rgba(198, 198, 188, 1);
        box-shadow: 0px 4px 11px 2px rgba(198, 198, 188, 1);
    }
    /*Border bottom shadow CSS*/
    
    .border-bottom-shadow {
        -webkit-box-shadow: 0px 3px 0px rgba(225, 225, 225, 0.74);
        -moz-box-shadow: 0px 3px 0px rgba(225, 225, 225, 0.74);
        box-shadow: 0px 3px 0px rgba(225, 225, 225, 0.74);
    }
    /*END of border bottom shadow CSS*/
    /*Carousel slides CSS*/
    
    .inner_content2 {
        position: relative;
    }
    
    .btn-book {
        padding: 5px;
        background-color: white;
        border: 1px solid #E62739;
        color: #E62739 !important;
        border-radius: 1px;
    }
    
    .btn-book:hover {
        padding: 5px;
        background-color: #E62739;
        border: 1px solid #E62739;
        color: white !important;
        border-radius: 1px;
    }
    
    .grid-price-after1 {
        font-weight: bold;
        color: black !important;
        font-family: 'muller-bold';
        font-size: 15px;
        padding-top: 5px;
        text-align: center;
    }
    /*END of Carousel slide CSS*/
    /*Arrow nav tabs CSS*/
    
    .arrow {
        position: relative;
        color: white !important;
        margin-bottom: 5px;
    }
    
    .red-gradient {
        height: 20px;
        padding: 10px;
        width: 200px;
        color: #000000;
        background: #e62739;
        /* Old browsers */
    }
    
    .red {
        color: white !important;
        background: #e62739;
        padding: 8px 159px 8px 25px;
        font-weight: bold;
    }
    
    .red:hover {
        color: white !important;
        background: #e62739;
        padding: 8px 159px 8px 25px;
    }
    /*Arrow tip css*/
    
    #arrow4:after {
        content: '';
        height: 0;
        display: block;
        border-color: transparent transparent transparent #e62739;
        border-width: 15px;
        border-style: solid;
        position: absolute;
        top: -7px;
        left: 248px;
    }
    /*END of Arrow nav tabs CSS*/
    /*Yellow colour gradient*/
    
    .yellowGradient {
        background: #f7f5e9;
        /* Old Browsers */
        background: -moz-linear-gradient(top, #f7f5e9 0%, #f0ecd5 100%);
        /* FF3.6+ */
        background: -webkit-gradient(left top, left bottom, color-stop(0%, #f7f5e9), color-stop(100%, #f0ecd5));
        /* Chrome, Safari4+ */
        background: -webkit-linear-gradient(top, #f7f5e9 0%, #f0ecd5 100%);
        /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #f7f5e9 0%, #f0ecd5 100%);
        /* Opera 11.10+ */
        background: -ms-linear-gradient(top, #f7f5e9 0%, #f0ecd5 100%);
        /* IE 10+ */
        background: linear-gradient(to bottom, #f7f5e9 0%, #f0ecd5 100%);
        /* W3C */
        filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#f7f5e9 ', endColorstr='#f0ecd5 ', GradientType=0);
        /* IE6-9 */
    }
    /*    END of Yellow colour gradient*/
</style>
<vivo-header></vivo-header>

<div data-ng-controller='storev1Ctrl'>

<div class="container" style="padding-bottom:60px;">

 

    <div class="row">
        <img src="images/banners/VivoCarat-jewellery-holi-festival-banner-web-1.jpg">
    </div>

    <div class="row" style="background-color: #F1F1F1;padding-bottom: 50px;">
        <div class="col-lg-12" style="padding: 0px;">
            <div class="row" style="padding-top: 30px;padding-left: 68px;padding-right: 68px;">

                <div class="col-lg-12 box-shadow-outside yellowGradient" style="border-top:solid 1px #FFFEF9;padding: 10px 0px 10px 20px;z-index:1;">
                    <h4 style="font-family: 'leela';font-size:16px;font-weight:bold;color:#E62739 !important;">COLLECTION</h4>

                </div>

            </div>

            <div class="row">
                <div class="col-lg-12" style="padding:0px;">
                    <div class="row box-shadow-outside" style="margin-top: -30px;margin-left: 30px;background-color: white;margin-right: 30px;">
                        <div class="col-lg-9 border-bottom-shadow" style="padding: 75px 0px 0px 0px;border-bottom: 1px solid #e1e1e1;">

                            <!--                            <p class="text-center" style="padding-top:30px;color:#E7283A !important;padding-left: 50px;font-family: 'vivo-header-gabriol';font-size:45px;">Featured Products</p>-->


                            <div class="" style="padding: 0px !important">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">


                                        </div>
                                    </div>


                                    <div id="carousel-example-store" class="carousel slide hidden-xs" data-ride="carousel" style="position:relative;z-index:1">



                                        <a data-ng-init="store1='images/icons/icons/round arrow left.png'" data-ng-mouseout="store1='images/icons/icons/round arrow left.png'" data-ng-mouseover="store1='images/icons/icons/round arrow left hover.png'" class="left carousel-control  hidden-xs" style="background-image:none;z-index:2;width: 28px;" data-target="#carousel-example-store" role="button" data-slide="prev">
                                            <img style="padding-top: 140px;margin-left: 7px;" ng-src="{{store1}}">
                                        </a>
                                        <a data-ng-init="store2='images/icons/icons/round arrow right.png'" data-ng-mouseout="store2='images/icons/icons/round arrow right.png'" data-ng-mouseover="store2='images/icons/icons/round arrow right hover.png'" style="background-image:none;z-index:2;width: 28px;" class="right carousel-control hidden-xs" data-target="#carousel-example-store" role="button" data-slide="next">
                                            <img style="padding-top: 146px;margin-right: -12px;" ng-src="{{store2}}">
                                        </a>


                                        <!-- Wrapper for slides -->
                                        <div class="height carousel-inner" style="padding-bottom: 55px;">

                                            <div class="item active">

                                                <div class="row">

                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[0].id}}/{{list[0].slug}}">
                                                            <div class="inner_content2 clearfix">


                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[0].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">
                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--            <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[0].title}}</span></div>-->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[0].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[0])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[1].id}}/{{list[1].slug}}">
                                                            <div class="inner_content2 clearfix">



                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[1].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">
                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[1].title}}</span></div>-->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[1].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[1])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[2].id}}/{{list[2].slug}}">
                                                            <div class="inner_content2 clearfix">









                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[2].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">
                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[2].title}}</span></div>-->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[2].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[2])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <a href="#p/{{list[3].id}}/{{list[3].slug}}">
                                                            <div class="inner_content2 clearfix">







                                                                <div class="">
                                                                    <img src="images/products-v2/{{list[3].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">

                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[3].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[3].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[3])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>


                                                </div>


                                            </div>

                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[4].id}}/{{list[4].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[4].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[4].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[4].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[4])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[5].id}}/{{list[5].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[5].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[5].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[5].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[5])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[6].id}}/{{list[6].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[6].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[6].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[6].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[6])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <a href="#p/{{list[7].id}}/{{list[7].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="">
                                                                    <img src="images/products-v2/{{list[7].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[7].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[7].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[7])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[8].id}}/{{list[8].slug}}">
                                                            <div class="inner_content2 clearfix">





                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[8].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[8].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[8].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[8])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[9].id}}/{{list[9].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[9].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[9].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[9].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[9])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[10].id}}/{{list[10].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[10].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">
                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[10].title}}</span></div>-->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[10].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[10])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <a href="#p/{{list[11].id}}/{{list[11].slug}}">
                                                            <div class="inner_content2 clearfix">





                                                                <div class="">
                                                                    <img src="images/products-v2/{{list[11].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">



                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--            <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[11].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[11].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[11])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>






                        </div>
                        <!--END of col-lg-9 div-->
                        <div class="col-lg-3 border-bottom-shadow" style="padding: 0px;padding-bottom: 134px;">
                            <div class="row" style="padding-right: 37px;padding-left: 20px;">
                                <div class="col-lg-12 text-center box-shadow-outside" style="background-color:#f6f3e4;padding:20px 0px 20px 0px;border-top:solid 1px #fffefb;margin-top: 95px;">
                                    <h4 class="text-center" style="font-family: 'leela';font-size:16px;font-weight:bold;text-transform:uppercase;color:#E62739 !important;">Jewel</h4>
                                    <p style="font-family: 'leela';font-size:13px;">Have a look at some</p>
                                    <p style="font-family: 'leela';font-size:13px;"> of our jewels.</p>
                                    <a class="btn btn-vivo button-de" href="#i/" role="button" style="border-radius: 0;" align="right">VIEW MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row box-shadow-outside" style="background-color: white;margin-left: 30px;margin-right: 30px;">
                        <div class="col-lg-3" style="padding: 0px;padding-bottom: 134px;">
                            <div class="row" style="padding-right: 37px;padding-left: 20px;">
                                <div class="col-lg-12 text-center box-shadow-outside" style="background-color:#f6f3e4;padding:20px 0px 20px 0px;border-top:solid 1px #fffefb;margin-top: 50px;">
                                    <h4 class="text-center" style="font-family: 'leela';font-size:16px;font-weight:bold;text-transform:uppercase;color:#E62739 !important;">Jewel</h4>
                                    <p style="font-family: 'leela';font-size:13px;">Have a look at some</p>
                                    <p style="font-family: 'leela';font-size:13px;"> of our jewels.</p>
                                    <a class="btn btn-vivo button-de" href="#i/" role="button" style="border-radius: 0;" align="right">VIEW MORE</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-9" style="padding: 50px 0px 0px 0px;border-bottom: 1px solid #e1e1e1;">




                            <div class="" style="padding-right: 20px;">
                                <div class="row">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">


                                        </div>
                                    </div>


                                    <div id="carousel-example-store1" class="carousel slide hidden-xs" data-ride="carousel" style="position:relative;z-index:1">



                                        <a data-ng-init="look1='images/icons/icons/round arrow left.png'" data-ng-mouseout="look1='images/icons/icons/round arrow left.png'" data-ng-mouseover="look1='images/icons/icons/round arrow left hover.png'" class="left carousel-control  hidden-xs" style="background-image:none;z-index:2;width: 31px;" data-target="#carousel-example-store1" role="button" data-slide="prev">
                                            <img style="padding-top: 140px;margin-left: -25px;" ng-src="{{look1}}">
                                        </a>
                                        <a data-ng-init="look2='images/icons/icons/round arrow right.png'" data-ng-mouseout="look2='images/icons/icons/round arrow right.png'" data-ng-mouseover="look2='images/icons/icons/round arrow right hover.png'" style="background-image:none;z-index:2;width: 28px;" class="right carousel-control hidden-xs" data-target="#carousel-example-store1" role="button" data-slide="next">
                                            <img style="padding-top: 140px;margin-right: -17px;" ng-src="{{look2}}">
                                        </a>


                                        <!-- Wrapper for slides -->
                                        <div class="height carousel-inner" style="padding-bottom: 55px;">

                                            <div class="item active">

                                                <div class="row">

                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[0].id}}/{{list[0].slug}}">
                                                            <div class="inner_content2 clearfix">


                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[0].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">
                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--            <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[0].title}}</span></div>-->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[0].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[0])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[1].id}}/{{list[1].slug}}">
                                                            <div class="inner_content2 clearfix">



                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[1].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">
                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[1].title}}</span></div>-->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[1].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[1])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[2].id}}/{{list[2].slug}}">
                                                            <div class="inner_content2 clearfix">









                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[2].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">
                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[2].title}}</span></div>-->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[2].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[2])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <a href="#p/{{list[3].id}}/{{list[3].slug}}">
                                                            <div class="inner_content2 clearfix">







                                                                <div class="">
                                                                    <img src="images/products-v2/{{list[3].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">

                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[3].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[3].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[3])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>


                                                </div>


                                            </div>

                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[4].id}}/{{list[4].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[4].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[4].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[4].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[4])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[5].id}}/{{list[5].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[5].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[5].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[5].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[5])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[6].id}}/{{list[6].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[6].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[6].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[6].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[6])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <a href="#p/{{list[7].id}}/{{list[7].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="">
                                                                    <img src="images/products-v2/{{list[7].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[7].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[7].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[7])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[8].id}}/{{list[8].slug}}">
                                                            <div class="inner_content2 clearfix">





                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[8].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[8].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[8].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[8])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[9].id}}/{{list[9].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[9].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">


                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[9].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[9].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[9])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3" style="">
                                                        <a href="#p/{{list[10].id}}/{{list[10].slug}}">
                                                            <div class="inner_content2 clearfix">






                                                                <div class="product_image">
                                                                    <img src="images/products-v2/{{list[10].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">
                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--                                                                <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[10].title}}</span></div>-->
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[10].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[10])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <a href="#p/{{list[11].id}}/{{list[11].slug}}">
                                                            <div class="inner_content2 clearfix">





                                                                <div class="">
                                                                    <img src="images/products-v2/{{list[11].VC_SKU}}-1.jpg" alt="" />
                                                                </div>




                                                                <div class="row" style="margin-bottom: 10px">
                                                                    <div class="col-sm-12 no-padding">



                                                                        <div style="margin-top: 10px;" class="row">
                                                                            <!--            <div class="col-sm-12 text-center"><span class="truncate" style="font-family: 'muller'; font-size:15px;color: #676767 !important;">{{list[11].title}}</span></div>-->
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12 grid-price-after1">RS. {{list[11].price_after_discount }}</div>
                                                                        </div>

                                                                        <div class="col-sm-4" style="padding: 15px 0px 0px 48px;width: 20%;">
                                                                            <a class="btn btn-book" style="z-index: 0;" data-ng-click="buyNow(list[11])"> BUY NOW </a> </div>
                                                                    </div>


                                                                </div>
                                                        </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>






                        </div>
                        <!--END of col-lg-9 div-->

                    </div>

                    <div class="row" style="margin-right:30px;margin-left:30px;padding-top: 30px;">
                        <div class="col-lg-6" style="padding-right: 4px;">
                            <div class="row" style="padding-left: 20px;">

                                <div class="col-lg-12 box-shadow-outside yellowGradient" style="border-top:solid 1px #FFFEF9;padding: 10px 0px 10px 20px;z-index:1;">
                                    <h4 style="font-family: 'leela';font-size:16px;font-weight:bold;color:#E62739 !important;">ABOUT US</h4>

                                </div>

                            </div>



                        </div>



                        <div class="col-lg-6">
                            <div class="row" style="padding-right: 20px;margin-left: 77px;">

                                <div class="col-lg-12 box-shadow-outside yellowGradient" style="border-top:solid 1px #FFFEF9;padding: 10px 0px 10px 20px;z-index:1;">
                                    <h4 style="font-family: 'leela';font-size:16px;font-weight:bold;color:#E62739 !important;">OUR COMMITMENT</h4>

                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 box-shadow-outside" style="background-color: #ffffff;margin-left: 30px;margin-top: -34px;padding: 0px 34px 25px 0px;">
                            <p style="font-family: 'leela';font-size:13px;padding: 73px 0px 10px 34px;">This section tells us about jewellers' details.We're situated in NYC place.</p>
                            <a class="btn btn-vivo button-de pull-right" href="#i/" role="button" style="border-radius: 0;" align="right">VIEW MORE</a>
                        </div>

                        <!--                        <div class="col-lg-1" style="padding:0px;"></div>-->
                        <div class="col-lg-5 box-shadow-outside" style="background-color: #ffffff;margin-left: 34px;margin-top: -34px;padding:0px;">
                            <div class="row" style="padding: 68px 33px 20px 29px;">
                                <img src="images/icons/trusted icons 2.jpg">
                            </div>

                            <div class="row" style="font-family: 'leela';font-size:13px;text-transform:uppercase;">
                                <div class="col-lg-3" style="padding: 0px 0px 0px 29px;">
                                    <p>Trusted Jewellers</p>
                                </div>
                                <div class="col-lg-3" style="padding: 0px 0px 0px 29px;">
                                    <p>Certified & Hallmarked</p>
                                </div>
                                <div class="col-lg-3" style="padding: 0px 0px 0px 36px;">
                                    <p>Free Shipping</p>
                                </div>
                                <div class="col-lg-3" style="padding: 0px 0px 0px 29px;">
                                    <p>Easy Return Policy</p>
                                </div>
                            </div>

                            <div class="row" style="padding-right: 34px;padding-bottom: 25px;">
                                <a class="btn btn-vivo button-de pull-right" href="#i/about" role="button" style="border-radius: 0;" align="right">VIEW MORE</a>
                            </div>

                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


</div>

</div>

</div>
<!--END of controller div-->
<vivo-footer></vivo-footer>

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>
<script src="js/megamenu.js"></script>
<script src="js/slides.min.jquery.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/socialShare.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>
<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/daterangepicker.js"></script>
<script src="js/jquery.simplyscroll.js"></script>
<script src="js/fwslider.js"></script> 

<!--  start angularjs modules  -->
<!--
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoStore.js"></script>
-->
<!-- end angularjs modules -->
    
<script src="app/data.js"></script>
<script src="app/directives.js"></script>
    
<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
    
<script src="device-router.js"></script>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

 </body>
</html>