<!DOCTYPE html>
<html lang="en" data-ng-app="vivoLookbook">
<head>
    <title>$title</title>
    <meta name="keywords" content="$meta_keywords" >
    <meta name="description" content="$meta_description" />
    <meta name="robots" content="$meta_robots" >
    <meta property="og:url" content="$og_url" />  
    <meta property="og:description" content="$og_description" />
    <meta property="og:title" content="$og_title" />
    <meta property="og:type" content="$og_type" />
    <meta property="og:image" content="$og_image" />
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="$canurl">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="$moburl">
    <link rel="alternate" media="handheld" href="$moburl" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <style>
        .bottom:after {
            background: -moz-linear-gradient(left, rgba(208, 208, 208, 1) 1%, rgba(240, 240, 240, 1) 90%, rgba(255, 255, 255, 0) 100%);
            background: -webkit-linear-gradient(left, rgba(208, 208, 208, 1) 1%, rgba(240, 240, 240, 1) 90%, rgba(255, 255, 255, 0) 100%);
            background: linear-gradient(to right, rgba(208, 208, 208, 1) 1%, rgba(240, 240, 240, 1) 90%, rgba(255, 255, 255, 0) 100%);
            filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#d0d0d0 ', endColorstr='#00ffffff', GradientType=1);
            height: 1px;
            width: 100%;
        }
    </style>
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
    <body data-ng-controller="lookbookArticleCtrl" ng-cloak>
        <vivo-header></vivo-header>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li><a href="/lookbook" target="_self" class="theme-text-special">LOOKBOOK</a></li>
                            <li>$articletodisplay</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="exclude m-t-0 m-b-25">
                <div class="row">
                    <div class="col-md-6 no-pad-l">
                        <div class="row">
                            <div class="col-xs-12">
                                <span>{{list.category}}</span>
                                <span class="pad-l-40">
                                    {{list.created_at | dateToISO | date:'dd MMMM'}}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <a data-ng-href="{{prevUrl}}" target="_self">Previous</a>&nbsp;&nbsp;|&nbsp;
                        <a data-ng-href="{{nextUrl}}" target="_self">Next</a>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-md-12">
                    <h1 class="h2 exclude m-b-20 m-t-0">{{list.title}}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <span data-ng-bind-html='list.lb_short_description' data-ng-if="!readMore">
                    </span>
                    <span data-ng-if="readMore" data-ng-bind-html='list.lb_long_description'> </span>&nbsp;<span data-ng-click='toggleReadMore()'>
                    <a class="vivocarat-theme-colour bold italic">
                        {{readMore?'read less':'..read more'}}
                    </a>
                    </span>
                </div>
            </div>
            <section>
                <div class="row">
                    <div class="col-md-6">
                        <img data-ng-src="/images/lookbook/blogs/{{list.banner_img}}.jpg" alt="{{list.banner_img}}">
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="hr exclude m-0"><span>SIMILAR LOOKS</span></h3>
                            </div>
                        </div>
                        <div class="row pad-t-30 pad-b-30 bottom">
                            <div class="col-md-5 pad-b-30">
                                <a data-ng-href="/product/{{slist[0].urlslug}}.html" target="_self">
                                    <div class="product_image text-center pad-b-30">
                                        <img data-ng-src="/images/products-v2/{{slist[0].VC_SKU}}-1.jpg" alt="{{slist[0].title}}">
                                    </div>
                                    <div class="text-center" ng-if="slist[0].category != 'Rings' && slist[0].category != 'Bangles' && slist[0].category != 'Bracelets'">
                                        <a class="btn btn-book" data-ng-click="buyNow(slist[0])"> Buy Now </a>
                                    </div>
                                    <div class="text-center" ng-if="slist[0].category == 'Rings' || slist[0].category == 'Bangles' || slist[0].category == 'Bracelets'">
                                        <a class="btn btn-book" data-ng-href="/product/{{slist[0].urlslug}}.html" target="_self"> Buy Now </a>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-5 pad-b-30">
                                <a data-ng-href="/product/{{slist[1].urlslug}}.html" target="_self">
                                    <div class="text-center pad-b-30">
                                        <img data-ng-src="/images/products-v2/{{slist[1].VC_SKU}}-1.jpg" alt="{{slist[1].title}}">
                                    </div>
                                    <div class="text-center" ng-if="slist[1].category != 'Rings' && slist[1].category != 'Bangles' && slist[1].category != 'Bracelets'">
                                        <a class="btn btn-book" data-ng-click="buyNow(slist[1])"> Buy Now </a>
                                    </div>
                                    <div class="text-center" ng-if="slist[1].category == 'Rings' || slist[1].category == 'Bangles' || slist[1].category == 'Bracelets'">
                                        <a class="btn btn-book" data-ng-href="/product/{{slist[1].urlslug}}.html" target="_self"> Buy Now </a>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row pad-t-15">
                            <div class="col-md-5" style="padding-top: 36px;">
                                <a data-ng-href="/product/{{slist[2].urlslug}}.html" target="_self">
                                    <div class="product_image text-center pad-b-30">
                                        <img data-ng-src="/images/products-v2/{{slist[2].VC_SKU}}-1.jpg" alt="{{slist[2].title}}">
                                    </div>
                                    <div class="text-center" ng-if="slist[2].category != 'Rings' && slist[2].category != 'Bangles' && slist[2].category != 'Bracelets'">
                                        <a class="btn btn-book" data-ng-click="buyNow(slist[2])"> Buy Now </a>
                                    </div>
                                    <div class="text-center" ng-if="slist[2].category == 'Rings' || slist[2].category == 'Bangles' || slist[2].category == 'Bracelets'">
                                        <a class="btn btn-book" data-ng-href="/product/{{slist[2].urlslug}}.html" target="_self"> Buy Now </a>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-5" style="padding-top: 36px;">
                                <a data-ng-href="/product/{{slist[3].urlslug}}.html" target="_self">
                                    <div class="text-center pad-b-30">
                                        <img data-ng-src="/images/products-v2/{{slist[3].VC_SKU}}-1.jpg" alt="{{slist[3].title}}">
                                    </div>
                                    <div class="text-center" ng-if="slist[3].category != 'Rings' && slist[3].category != 'Bangles' && slist[3].category != 'Bracelets'">
                                        <a class="btn btn-book" data-ng-click="buyNow(slist[3])"> Buy Now </a>
                                    </div>
                                    <div class="text-center" ng-if="slist[3].category == 'Rings' || slist[3].category == 'Bangles' || slist[3].category == 'Bracelets'">
                                        <a class="btn btn-book" data-ng-href="/product/{{slist[3].urlslug}}.html" target="_self"> Buy Now </a>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>                
                <div class="row m-b-30">
                    <div class="col-xs-12">
                        <h2 class="hr exclude m-0">
                            <span>RELATED STORIES</span>
                        </h2>
                    </div>
                </div>
                <div class="row pad-b-15">
                    <a data-ng-href="/lookbook/{{related[0].urlslug}}.html" target="_self">
                        <div class="col-md-4">
                            <div class="row b1-lgray-d5">
                                <div class="col-md-12 no-pad-lr">
                                    <h3 class="title-background exclude text-limit" title="{{related[0].title}}">
                                        {{related[0].title}}
                                    </h3>
                                </div>
                            </div>
                            <div class="row b1-lgray-d5">
                                <div class="col-md-12 no-pad-lr">
                                    <img data-ng-src="/images/lookbook/blogs/{{related[0].banner_img}}.jpg" alt="{{related[0].banner_img}}">
                                </div>
                            </div>
                            <div class="row related-footer">
                                <div class="col-md-8">
                                    {{related[0].category}}
                                </div>
                                <div class="col-md-4 text-right">
                                    {{related[0].created_at | dateToISO | date:'dd MMMM'}}
                                </div>
                            </div>
                        </div>
                    </a>
                    <a data-ng-href="/lookbook/{{related[1].urlslug}}.html" target="_self">
                        <div class="col-md-4">
                            <div class="row b1-lgray-d5">
                                <div class="col-md-12 no-pad-lr">
                                    <h3 class="title-background exclude text-limit" title="{{related[1].title}}">
                                        {{related[1].title}}
                                    </h3>
                                </div>
                            </div>
                            <div class="row b1-lgray-d5">
                                <div class="col-md-12 no-pad-lr">
                                    <img data-ng-src="/images/lookbook/blogs/{{related[1].banner_img}}.jpg" alt="{{related[1].banner_img}}">
                                </div>
                            </div>
                            <div class="row related-footer">
                                <div class="col-md-8">
                                    {{related[1].category}}
                                </div>
                                <div class="col-md-4 text-right">
                                    {{related[1].created_at | dateToISO | date:'dd MMMM'}}
                                </div>
                            </div>
                        </div>
                    </a>
                    <a data-ng-href="/lookbook/{{related[2].urlslug}}.html" target="_self">
                        <div class="col-md-4">
                            <div class="row b1-lgray-d5">
                                <div class="col-md-12 no-pad-lr">
                                    <h3 class="title-background exclude text-limit" title="{{related[2].title}}">
                                        {{related[2].title}}
                                    </h3>
                                </div>
                            </div>
                            <div class="row b1-lgray-d5">
                                <div class="col-md-12 no-pad-lr">
                                    <img data-ng-src="/images/lookbook/blogs/{{related[2].banner_img}}.jpg" alt="{{related[2].banner_img}}">
                                </div>
                            </div>
                            <div class="row related-footer">
                                <div class="col-md-8">
                                    {{related[2].category}}
                                </div>
                                <div class="col-md-4 text-right">
                                    {{related[2].created_at | dateToISO | date:'dd MMMM'}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </section>
        </div>
<vivo-footer></vivo-footer>
<script src="/js/jquery.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/css3-mediaqueries.js"></script>
<script src="/js/megamenu.js"></script>
<script src="/js/slides.min.jquery.js"></script>
<script src="/js/jquery.jscrollpane.min.js"></script>
<script src="/js/jquery.easydropdown.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/custom.js"></script>
<script src="/js/angular.min.js"></script>
<script src="/js/angular-ui-router.min.js"></script>
<script src="/js/angular-animate.min.js"></script>
<script src="/js/angular-sanitize.js"></script>
<script src="/js/satellizer.min.js"></script>
<script src="/js/angular.rangeSlider.js"></script>
<script src="/js/select.js"></script>
<script src="/js/toaster.js"></script>
<script src="/js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="/js/taggedInfiniteScroll.js"></script>
<script src="/js/jquery.easing.min.js"></script>
<script src="/js/angular-google-plus.min.js"></script>
<script src="/js/jquery.etalage.min.js"></script>
<script src="/js/jquery.simplyscroll.js"></script>
<!--  start angularjs modules  -->
<script src="/app/modules/vivoCommon.js"></script>
<script src="/app/modules/vivoLookbook.js"></script>
<!-- end angularjs modules -->
<script src="/app/data.js"></script>
<!-- Start include Controller for angular -->
<script src="/app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
<script src="/device-router.js"></script>
<!-- Facebook Pixel Code -->
<script>
    !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->        
<!-- onesignal start   -->
<link rel="manifest" href="/manifest.json">
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<script>
var OneSignal = window.OneSignal || [];
OneSignal.push(["init", {
    appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
    autoRegister: true,
    notifyButton: {
        enable: false /* Set to false to hide */
    }
}]);
</script>
<!-- onesignal end   -->    
 </body>
</html>