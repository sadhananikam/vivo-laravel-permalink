<!DOCTYPE html>
<html lang="en" data-ng-app="vivoOrders">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta name="keywords" content="" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/orders" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="My Orders | VivoCarat.com" />
    <meta property="og:description" content="VivoCarat is an Online Jewellery store for gold & diamond rings, Earrings, Pendants with latest designs from trusted brands at lowest prices." />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>My Orders | VivoCarat.com</title>
    <meta name="keywords" content="orders,account,jewellery, online jewellery, jewelry, engagement rings, wedding rings, diamond rings, rings, diamond earrings, gold earrings" />
    <meta name="description" content="VivoCarat is an Online Jewellery store for gold & diamond rings, Earrings, Pendants with latest designs from trusted brands at lowest prices." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/orders">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/orders">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/orders" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <style>
        /*Arrow nav tabs CSS*/
        .arrow {
            position: relative;
            color: white !important;
            margin-bottom: 5px;
        }     
        .red-gradient {
            height: 20px;
            padding: 10px;
            width: 200px;
            color: #000000;
            background: #e62739;
        }        
        .red {
            color: white !important;
            background: #e62739;
            padding: 8px 62px 8px 25px;
            font-weight: bold;
        }     
        .red:hover {
            color: white !important;
            background: #e62739;
            padding: 8px 62px 8px 25px;
        }
        /*Arrow tip css*/
        #arrow4:after {
            content: '';
            height: 0;
            display: block;
            border-color: transparent transparent transparent #e62739;
            border-width: 17px;
            border-style: solid;
            position: absolute;
            top: -7px;
            left: 174px;
        }
        /*END of Arrow nav tabs CSS*/
    </style>    
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='ordersCtrl'>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li><a href="/account" target="_self" class="theme-text-special">MY ACCOUNT</a>
                                <li>MY ORDERS</li>
                        </ul>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-md-3">
                    <ul class="account-nav">
                        <li>
                            <a class="inactive-tab-text-colour" href="/wishlist" target="_self">Wishlist</a>
                        </li>
                        <li class="current">
                            <a href="/orders" target="_self">Order history</a>
                        </li>
                        <li>
                            <a class="inactive-tab-text-colour" href="/account" target="_self">Account details</a>
                        </li>
                        <li>
                            <a class="inactive-tab-text-colour" href="/address" target="_self">Address</a>
                        </li>
                    </ul>
                </div>
                <div data-ng-hide="!authenticated || isLoaded">
                    <div class="col-md-9" align="center">
                        <div class="row  text-center home-panel-row">
                            <div class="well w-80pc block-center pad-20 m-tb-30">Please wait...</div>
                        </div>
                    </div>
                </div>
                <div data-ng-show="!isauthenticated || isLoaded" class="register_account col-md-9" style="margin-top: -2px;">
                    <div class="well not-loggedin-pad" data-ng-show="!authenticated">
                        <div class="well w-80pc block-center pad-20 m-tb-30">
                            <h4 class="title t13">
                                Not Logged In
                            </h4>
                            <p class="t13 m-b-20">
                                Kindly <a class="link-hover" href="/login" target="_self">login</a> to see your orders.
                            </p>
                        </div>
                    </div>
                    <div class="wrap" data-ng-show="authenticated && orderHistoryList.length<1">
                        <h4 class="title">Order list is empty</h4>
                        <p>You have no items in your shopping cart.
                            <br>Click<a class="link-hover" href="/" target="_self"> here</a> to continue shopping
                        </p>
                    </div>
                    <div data-ng-hide="!authenticated" class="col-md-9 no-pad" style="margin-top: -17px;">
                        <div class="row home-panel-row">
                            <div data-ng-repeat="o in orderHistoryList" class="b1-b-eee pad-b-15 m-t-10 m-b-30">
                                <h4 class="panel-title exclude m-b-15">
                                    <div class="row">
                                        <div class="col-xs-12 t13 bold lgray-79 pad-lr-0 pad-b-5 uppercase">
                                            Order Id: {{o.id}}
                                        </div>
                                        <div class="col-xs-12 t13 bold lgray-79 pad-lr-0 pad-tb-5">
                                            Placed on {{o.created_at}}
                                        </div>
                                        <div class="col-xs-12 t13 bold lgray-79 pad-lr-0 pad-tb-5 uppercase">
                                            Total amount Rs: {{o.total | INR}}
                                        </div>
                                    </div>
                                </h4>
                                <div class="col-xs-12 vivo-panel order-status-structure" data-ng-repeat="c in o.products">
                                    <div class="row m-b-15">
                                        <div class="col-xs-2 pad-lr-0">
                                            <img data-ng-src="/images/products-v2/{{c.item.VC_SKU}}-1.jpg" class="w-full b1-eee" alt="{{c.item.title}}">
                                        </div>
                                        <div class="col-xs-10">
                                            <div class="row">
                                                <div class="col-xs-4 text-left">
                                                    <span class="t13 uppercase black" style="letter-spacing:0px;">
                                                        <a class="lgray-79" data-ng-href="/product/{{c.item.urlslug}}.html" target="_self">  
                                                            {{c.item.title}}
                                                        </a>
                                                        </span>
                                                </div>
                                                <div class="col-xs-4 text-center">
                                                    <span class="lgray-79 t13">
                                                        {{o.status}}
                                                    </span>
                                                </div>                                           
                                                <div class="col-xs-4 t15">
                                                    <a class="btn btn-xs btn-style uppercase br-0" data-ng-click="returnOrder(o.id)" data-ng-if="o.status=='DELIVERED'">
                                                        RETURN
                                                    </a>
                                                    <a class="btn btn-xs btn-style uppercase br-0" data-ng-click="cancelOrder(o.id)" data-ng-if="o.status=='NOT_CONFIRMED'">
                                                        Cancel
                                                    </a>
                                                    <a class="btn btn-xs btn-style uppercase br-0" data-ng-click="buyNow(o)" data-ng-if="o.status=='NOT_CONFIRMED'">
                                                        Buy Now
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 text-left m-t-20">
                                                    <div class="row">
                                                        <div class="col-xs-2 pad-lr-0">
                                                            <p class="t13 lgray-79 exclude">
                                                                Qty:{{c.quantity}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div data-ng-show="c.item.ring_size != null && c.item.ring_size != '' && c.item.ring_size != 0" class="row">
                                                        <div class="col-xs-2 pad-lr-0">
                                                            <p class="t13 lgray-79 exclude">
                                                                Size:{{c.item.ring_size}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div data-ng-show="c.item.bangle_size != null && c.item.bangle_size != '' && c.item.bangle_size != 0" class="row">
                                                        <div class="col-xs-2 pad-lr-0">
                                                            <p class="t13 lgray-79 exclude">
                                                                Size:{{c.item.bangle_size}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div data-ng-show="c.item.bracelet_size != null && c.item.bracelet_size != '' && c.item.bracelet_size != 0" class="row">
                                                        <div class="col-xs-2 pad-lr-0">
                                                            <p class="t13 lgray-79 exclude">
                                                                Size:{{c.item.bracelet_size}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="process pad-b-30">
                                    <div class="process-row">
                                        <div class="col-md-12 no-pad" style="background-color:#F8F8F8;border:solid 1px #E5E5E5;">
                                            <div class="row">
                                                <div class="process-step col-xs-2">
                                                    <p class="feedback-style m-b-15 exclude m-lr-0 exclude">ORDERED</p>
                                                    <a data-ng-class="o.status=='NOT_CONFIRMED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
                                                        <img class="exclude" src="/images/orders/icons/grey-circle.png" data-ng-if="o.status!='CONFIRMED'" alt="not confirmed">
                                                        <img class="exclude" src="/images/orders/icons/green-circle.png" data-ng-if="o.status=='CONFIRMED'" alt="confirmed">
                                                    </a>
                                                </div>
                                                <div class="process-step col-xs-2">
                                                    <p class="feedback-style m-b-15 exclude m-lr-0 exclude">CONFIRMED</p>
                                                    <a data-ng-class="o.status=='CONFIRMED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
                                                        <img class="exclude" src="/images/orders/icons/grey-circle.png" data-ng-if="o.status!='CONFIRMED'" alt="not confirmed">
                                                        <img class="exclude" src="/images/orders/icons/green-circle.png" data-ng-if="o.status=='CONFIRMED'" alt="confirmed">
                                                    </a>
                                                </div>
                                                <div class="process-step col-xs-2">
                                                    <p class="feedback-style m-b-15 exclude m-lr-0">PROCESSING</p>
                                                    <a data-ng-class="o.status=='PROCESSING'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
                                                        <img class="exclude" src="/images/orders/icons/grey-circle.png" data-ng-if="o.status!='PROCESSING'" alt="process in queue">
                                                        <img src="/images/orders/icons/green-circle.png" data-ng-if="o.status=='PROCESSING'" alt="processing">
                                                    </a>
                                                </div>
                                                <div class="process-step col-xs-2">
                                                    <p class="feedback-style m-b-15 exclude m-lr-0">SHIPPING</p>
                                                    <a data-ng-class="o.status=='DISPATCHED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
                                                        <img class="exclude" src="/images/orders/icons/grey-circle.png" data-ng-if="o.status!='DISPATCHED'" alt="waiting for dispatch">
                                                        <img class="exclude" src="/images/orders/icons/green-circle.png" data-ng-if="o.status=='DISPATCHED'" alt="dispatched">
                                                    </a>
                                                </div>
                                                <div class="process-step col-xs-2">
                                                    <p class="feedback-style m-b-15 exclude m-lr-0">DELIVERED</p>
                                                    <a data-ng-class="o.status=='DELIVERED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
                                                        <img class="exclude" src="/images/orders/icons/grey-circle.png" data-ng-if="o.status!='DELIVERED'" alt="not delievered">
                                                        <img class="exclude" src="/images/orders/icons/green-circle.png" data-ng-if="o.status=='DELIVERED'" alt="delievred">
                                                    </a>
                                                </div>
                                                <div class="process-step col-xs-2 pad-lr-0">
                                                    <p class="feedback-style m-b-15 exclude m-lr-0">THANK YOU</p>
                                                    <a data-ng-class="o.status=='RECEIVED'?'btn-success btn-circle':'btn-default btn-circle'" disabled="disabled">
                                                        <img class="exclude" src="/images/orders/icons/thank.png" alt="thank you">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row need-help-structure m-b-50 text-center">
                <div class="col-md-3 b1-r-af">
                    <p class="exclude m-tb-5 bold">need help?</p>
                </div>
                <div class="col-md-3 b1-r-af lgray-79 bold t15">
                    <img class="pad-r-5 exclude t-middle" src="/images/orders/icons/email.png" alt="email us">hello@vivocarat.com
                </div>
                <div class="col-md-3 b1-r-af lgray-79 bold t15">
                    <img class="pad-r-5 exclude t-middle" src="/images/orders/icons/phone.png" alt="email us"> +91 9167645314
                </div>
                <div class="col-md-3 lgray-79 bold t15">
                    <img class="pad-r-5 exclude t-middle" src="/images/orders/icons/chat.png" alt="email us">
                    <span onclick="maximizetawk()">Chat</span>
                </div>
            </div>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <div class="modal fade" id="returnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reason for Return</h4>
                </div>
                <div class="modal-body">
                    <textarea style="width:100%" data-ng-model="reason"></textarea>
                </div>
                <div class="modal-footer">
                    <a data-ng-click="forgotPassword()" class="btn btn-default btn-sx" data-dismiss="modal">Cancel</a>
                    <a data-ng-click="returnOrderConfirm(rid,reason)" data-ng-click="forgotPassword()" class="btn btn-vivo btn-sm"> Return Order</a>
                </div>
            </div>
        </div>
    </div>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoOrders.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>