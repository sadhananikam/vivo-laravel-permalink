<!DOCTYPE html>
<html lang="en" data-ng-app="vivoRingGuide">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/find-your-ring-size" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Find your ring size - Ring Size Guide | VivoCarat" />
    <meta property="og:description" content="Find your Ring Size accurately by using the VivoCarat Ring Size. Easy to use guide is for India ring sizes." />
    <meta property="og:image" content="https://www.vivocarat.com/images/ringguide/ring_sizer_3.jpg" />
    <title>Find your ring size - Ring Size Guide | VivoCarat</title>
    <meta name="keywords" content="ring sizing guide,ring sizing chart,find ring size" />
    <meta name="description" content="Find your Ring Size accurately by using the VivoCarat Ring Size. Easy to use guide is for India ring sizes." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/find-your-ring-size">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/find-your-ring-size">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/find-your-ring-size" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <style>
        /*Table CSS*/
        .CSSTableGenerator {
            margin: 0px;
            padding: 0px;
            width: 100%;
            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;
            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }       
        .CSSTableGenerator table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            height: 100%;
            margin: 0px;
            padding: 0px;
            display: table;
        }        
        .CSSTableGenerator tr:last-child td:last-child {
            -moz-border-radius-bottomright: 0px;
            -webkit-border-bottom-right-radius: 0px;
            border-bottom-right-radius: 0px;
        }       
        .CSSTableGenerator table tr:first-child td:first-child {
            -moz-border-radius-topleft: 0px;
            -webkit-border-top-left-radius: 0px;
            border-top-left-radius: 0px;
        }       
        .CSSTableGenerator table tr:first-child td:last-child {
            -moz-border-radius-topright: 0px;
            -webkit-border-top-right-radius: 0px;
            border-top-right-radius: 0px;
        }        
        .CSSTableGenerator tr:last-child td:first-child {
            -moz-border-radius-bottomleft: 0px;
            -webkit-border-bottom-left-radius: 0px;
            border-bottom-left-radius: 0px;
        }       
        .CSSTableGenerator tr:nth-child(odd) {
            background-color: #fafafa;
        }        
        .CSSTableGenerator tr:nth-child(even) {
            background-color: #ffffff;
        }      
        .CSSTableGenerator td {
            vertical-align: middle;
            border: 1px solid #e3e3e3;
            border-width: 0px 1px 1px 0px;
            text-align: left;
            padding: 3px 15px;
            font-size: 13px;
            font-family: 'leela';
            font-weight: normal;
            color: #797979;
        }     
        .CSSTableGenerator tr:last-child td {
            border-width: 0px 1px 0px 0px;
        }       
        .CSSTableGenerator tr td:last-child {
            border-width: 0px 0px 1px 0px;
        }      
        .CSSTableGenerator tr:last-child td:last-child {
            border-width: 0px 0px 0px 0px;
        }      
        .CSSTableGenerator tr:first-child td {
            background: -o-linear-gradient(bottom, #e62737 5%, #e62737 100%);
            background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #e62737), color-stop(1, #e62737));
            background: -moz-linear-gradient( center top, #e62737 5%, #e62737 100%);
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#e62737", endColorstr="#e62737");
            background: -o-linear-gradient(top, #e62737, e62737);
            background-color: #e62737;
            border: 0px solid #fafafa;
            text-align: center;
            border-width: 0px 0px 1px 1px;
            font-size: 16px;
            font-family: 'leela';
            font-weight: bold;
            color: #ffffff;
        }   
        .CSSTableGenerator tr:first-child:hover td {
            background: -o-linear-gradient(bottom, #e62737 5%, #e62737 100%);
            background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #e62737), color-stop(1, #e62737));
            background: -moz-linear-gradient( center top, #e62737 5%, #e62737 100%);
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr="#e62737", endColorstr="#e62737");
            background: -o-linear-gradient(top, #e62737, e62737);
            background-color: #e62737;
        }  
        .CSSTableGenerator tr:first-child td:first-child {
            border-width: 0px 0px 1px 0px;
        } 
        .CSSTableGenerator tr:first-child td:last-child {
            border-width: 0px 0px 1px 1px;
        }
        /*END of table CSS*/
    </style>    
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='ringGuideCtrl'>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>RING SIZER</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="exclude m-t-0 m-b-50">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>RING SIZER</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 theme-text t13">
                        <p>
                            This method illustrates the steps which will be helpful in determining your ring size.
                            <br>Please make sure you compare your measurement with the chart below.
                        </p>
                    </div>
                </div>
                <div class="w-80pc block-center">
                    <div class="row m-tb-30">
                        <div class="col-md-6">
                            <img src="/images/ringguide/step1.png" alt="step 1">
                            <div class="row pad-t-10">
                                <div class="col-md-12">
                                    <img src="/images/ringguide/ring_sizer_1.jpg" alt="Take a piece of paper or thread">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img src="/images/ringguide/step2.png" alt="step 2">
                            <div class="row pad-t-10">
                                <div class="col-md-12">
                                    <img src="/images/ringguide/ring_sizer_2.jpg" alt="wrap paper/thread around finger">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row m-tb-30">
                        <div class="col-md-6">
                            <img src="/images/ringguide/step3.png" alt="step 3">
                            <div class="row pad-t-10">
                                <div class="col-md-12">
                                    <img src="/images/ringguide/ring_sizer_3.jpg" alt="mark spot where paper/thread meets">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img src="/images/ringguide/step4.png" alt="step 4">
                            <div class="row pad-t-10">
                                <div class="col-lg-12">
                                    <img src="/images/ringguide/ring_sizer_4.jpg" alt="measure length with ruler">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 subheader text-center pad-b-20 pad-t-50">
                        <p>
                            Check your measurement with the chart below to determine your ring size.
                        </p>
                    </div>
                </div>
                <div class="row CSSTableGenerator">
                    <div class="w-60pc block-center">
                        <div class="col-md-6">
                            <table class="w-full" style="border: 1px solid #E3E3E3;">
                                <tbody>
                                    <tr>
                                        <td style="color: #FFF5E9 !important;">
                                            Ring Size (Indian)
                                        </td>
                                        <td colspan="2" style="color: #FFF5E9 !important;">
                                            Circumference
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td class="bold subheader">
                                            Inches
                                        </td>
                                        <td class="bold subheader">
                                            MM
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            1
                                        </td>
                                        <td>
                                            1.61
                                        </td>
                                        <td>
                                            40.8
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            1.66
                                        </td>
                                        <td>
                                            42.1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3
                                        </td>
                                        <td>
                                            1.69
                                        </td>
                                        <td>
                                            43
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            4
                                        </td>
                                        <td>
                                            1.73
                                        </td>
                                        <td>
                                            44
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            5
                                        </td>
                                        <td>
                                            1.77
                                        </td>
                                        <td>
                                            44.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            6
                                        </td>
                                        <td>
                                            1.81
                                        </td>
                                        <td>
                                            45.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            7
                                        </td>
                                        <td>
                                            1.86
                                        </td>
                                        <td>
                                            47.1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            8
                                        </td>
                                        <td>
                                            1.89
                                        </td>
                                        <td>
                                            48.1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            9
                                        </td>
                                        <td>
                                            1.93
                                        </td>
                                        <td>
                                            49
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            1.97
                                        </td>
                                        <td>
                                            50
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            11
                                        </td>
                                        <td>
                                            2
                                        </td>
                                        <td>
                                            50.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            12
                                        </td>
                                        <td>
                                            2.04
                                        </td>
                                        <td>
                                            51.8
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            13
                                        </td>
                                        <td>
                                            2.08
                                        </td>
                                        <td>
                                            52.8
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            14
                                        </td>
                                        <td>
                                            2.13
                                        </td>
                                        <td>
                                            54
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            15
                                        </td>
                                        <td>
                                            2.16
                                        </td>
                                        <td>
                                            55
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--2nd table-->
                        <div class="col-md-6">
                            <table class="w-full" style="border: 1px solid #E3E3E3;">
                                <tbody>
                                    <tr>
                                        <td style="color: #FFF5E9 !important;">
                                            Ring Size (Indian)
                                        </td>
                                        <td colspan="2" style="color: #FFF5E9 !important;">
                                            Circumference
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td class="bold subheader">
                                            Inches
                                        </td>
                                        <td class="bold subheader">
                                            MM
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            16
                                        </td>
                                        <td>
                                            2.2
                                        </td>
                                        <td>
                                            55.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            17
                                        </td>
                                        <td>
                                            2.24
                                        </td>
                                        <td>
                                            56.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            18
                                        </td>
                                        <td>
                                            2.28
                                        </td>
                                        <td>
                                            57.8
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            19
                                        </td>
                                        <td>
                                            2.33
                                        </td>
                                        <td>
                                            59.1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            20
                                        </td>
                                        <td>
                                            2.36
                                        </td>
                                        <td>
                                            60
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            21
                                        </td>
                                        <td>
                                            2.4
                                        </td>
                                        <td>
                                            60.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            22
                                        </td>
                                        <td>
                                            2.44
                                        </td>
                                        <td>
                                            61.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            23
                                        </td>
                                        <td>
                                            2.47
                                        </td>
                                        <td>
                                            62.8
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            24
                                        </td>
                                        <td>
                                            2.51
                                        </td>
                                        <td>
                                            63.8
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            25
                                        </td>
                                        <td>
                                            2.55
                                        </td>
                                        <td>
                                            64.7
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            26
                                        </td>
                                        <td>
                                            2.6
                                        </td>
                                        <td>
                                            66
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            27
                                        </td>
                                        <td>
                                            2.63
                                        </td>
                                        <td>
                                            66.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            28
                                        </td>
                                        <td>
                                            2.67
                                        </td>
                                        <td>
                                            67.9
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            29
                                        </td>
                                        <td>
                                            2.72
                                        </td>
                                        <td>
                                            69.1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            30
                                        </td>
                                        <td>
                                            2.76
                                        </td>
                                        <td>
                                            70.1
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--End of container-->
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoRingGuide.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>