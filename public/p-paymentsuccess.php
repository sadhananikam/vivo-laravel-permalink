<!DOCTYPE html>
<html lang="en" data-ng-app="vivoPaymentSuccess">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta name="keywords" content="" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="$canurl">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="$moburl">
    <link rel="alternate" media="handheld" href="$moburl" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='paymentSuccessCtrl'>
        <div class="container pad-b-50">
            <div class="row">
                <div class="col-md-12 payment-success-text">
                    YOUR ORDER HAS BEEN PLACED. THANK YOU FOR YOUR ORDER.
                </div>
            </div>
            <div class="row m-t-30">
                <div class="col-md-12 success-paragraph">Vivocarat team will shortly send you an email to {{email}} with all the above details.</div>
                <div class="col-md-12 success-paragraph">You will also receive a confirmation call on {{ship.phone}} from the team to verify your order.</div>
                <div class="col-md-12 success-paragraph">In case you are unreachable, please call us on +91 9167645314 to confirm your order.</div>
            </div>
            <div class="row" style="">
                <div class="col-md-6 b1-r-eee pad-l-0">
                    <div class="row">
                        <div class="col-md-12 product-summary-heading">ORDER SUMMARY</div>
                    </div>
                    <div class="row pad-t-15">
                        <div class="col-md-4">
                            <p class="bold lgray-79">Order No.</p>
                        </div>
                        <div class="col-md-1">
                            <p class="credsfont">:</p>
                        </div>
                        <div class="col-md-7">
                            <p class="credsfont">{{order.id}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p class="bold lgray-79">Ordered On</p>
                        </div>
                        <div class="col-md-1">
                            <p class="credsfont">:</p>
                        </div>
                        <div class="col-md-7">
                            <p class="credsfont">{{order.created_at}}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p class="bold lgray-79">Payment Mode</p>
                        </div>
                        <div class="col-md-1">
                            <p class="credsfont">:</p>
                        </div>
                        <div class="col-md-7">
                            <p class="credsfont">{{order.isCod=='0'?'Online':'Cash on delivery'}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12 product-summary-heading">
                            PRODUCT SUMMARY
                        </div>
                    </div>
                    <div data-ng-repeat="p in products" class="b1-b-eee">
                        <div class="row pad-t-15">
                            <div class="col-md-4">
                                <p class="bold lgray-79">Product Id</p>
                            </div>
                            <div class="col-md-1">
                                <p class="credsfont">:</p>
                            </div>
                            <div class="col-md-7">
                                <p class="credsfont">{{p.item.id}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <p class="bold lgray-79">Product Name</p>
                            </div>
                            <div class="col-md-1">
                                <p class="credfont">:</p>
                            </div>
                            <div class="col-md-7">
                                <p class="credsfont">{{p.item.title}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <p class="bold lgray-79">Product Quantity</p>
                            </div>
                            <div class="col-md-1">
                                <p class="credsfont">:</p>
                            </div>
                            <div class="col-md-7">
                                <p class="credsfont">{{p.quantity}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row pad-t-15">
                        <div class="col-md-4">
                            <p class="bold lgray-79">Grand Total</p>
                        </div>
                        <div class="col-md-1">
                            <p class="credsfont">:</p>
                        </div>
                        <div class="col-md-7">
                            <p class="credsfont">Rs.{{order.total | INR}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row detas b1-b-eee">
                <div class="col-lg-12 m-b-30 pad-t-30">
                    <h3 class="customer-details-heading">Customer Details</h3>
                </div>
                <div class="row theme-text t13">
                    <div class="row pad-b-10">
                        <div class="col-md-3 bold lgray-79">Name</div>
                        <div class="col-md-1 credsfont">:</div>
                        <div class="col-md-8 credsfont">{{name}}</div>
                    </div>
                    <div class="row pad-b-10">
                        <div class="col-md-3 bold lgray-79">Email</div>
                        <div class="col-md-1 credsfont">:</div>
                        <div class="col-md-8 credsfont">{{email}}</div>
                    </div>
                    <div class="row pad-b-10">
                        <div class="col-md-3 bold lgray-79">Phone</div>
                        <div class="col-md-1 credsfont">:</div>
                        <div class="col-md-8 credsfont">{{phone}}</div>
                    </div>
                    <div class="row pad-b-10">
                        <div class="col-md-3 bold lgray-79">Billing Address</div>
                        <div class="col-md-1 credsfont">:</div>
                        <div class="col-md-8 credsfont">
                            {{order.first_line}},{{order.city}}, {{order.state}} - {{order.pin}}, {{order.country}}
                        </div>
                    </div>
                    <div class="row pad-b-10">
                        <div class="col-md-3 bold lgray-79">Shipping Address</div>
                        <div class="col-md-1 credsfont">:</div>
                        <div class="col-md-8 credsfont">
                            {{ship.first_line}},{{ship.city}}, {{ship.state}} - {{ship.pin}}, {{ship.country}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pull-right m-t-30">
                <div class="col-md-12">
                    <a href="/" target="_self" class="btn big-button"> 
            Home 
        </a>
                </div>
            </div>
            <div class="row pull-right m-t-30">
                <div class="col-md-12">
                    <a href="/orders" target="_self" class="btn big-button"> 
            My orders 
        </a>
                </div>
            </div>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoPaymentSuccess.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>