<!DOCTYPE html>
<html lang="en" data-ng-app="vivoJewelleryEducation">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/jewellery-education" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Jewellery Education | VivoCarat" />
    <meta property="og:description" content="Learn more about the jewellery that you buy. 4 C's of a diamond, diamond cuts, gold colour. This is your jewellery guide." />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>Jewellery Education | VivoCarat</title>
    <meta name="keywords" content="jewelry education,jewellery guide" />
    <meta name="description" content="Learn more about the jewellery that you buy. 4 C's of a diamond, diamond cuts, gold colour. This is your jewellery guide." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/jewellery-education">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery-education">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery-education" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body style="overflow-x: hidden;" ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='jewelleryEducationCtrl'>
        <!-- Designed by PR -->
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12 pad-lr-0">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>JEWELLERY EDUCATION</li>
                        </ul>
                    </div>
                </div>
            </section>
            <article class="j_edu">
                <!-- Article Image -->
                <div class="row">
                    <img src="/images/jewellery-education/4c-diamonds-1.jpg" alt="4c of diamonds" class="w-full">
                </div>
                <section class="pad-t-50">
                    <div class="row m-offset-15">
                        <div class="col-md-6 col-sm-12">
                            <div class="wrapper bg-white">
                                <h2 class="theme_bg_header_gradient t24">CARAT</h2>
                                <img src="/images/jewellery-education/carat.jpg" alt="Jewellery Carat">
                                <p>
                                    Carat represents the weight of the diamond and is abbreviated as CT. Not to be mistaken by the quality of a diamond, the Carat unit merely denotes its size by weight. It is also one of the easiest measurement to determine amongst the 4C’s. One carat is one fifth of a gram. Large diamonds are rare to find and because of this the price increases with the increase in its carat. So two one-carat diamonds are less expensive than one two-carat diamond.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="wrapper bg-white">
                                <h2 class="theme_bg_header_gradient t24">COLOR</h2>
                                <img src="/images/jewellery-education/colour.jpg" alt="Diamond Color">
                                <p>
                                    Diamonds are measured by the lack of their color and its denoted by their respective grades. The grades have been devised by The Gemological Institute of America and they follow certain guidelines. The grading starts from letter D and stretches upto letter Z. The highest grades are D, E, F and are absolutely colorless and rarest. G through J comes next and are nearly colorless. K, L, M grades are noticeable color and are faint yellow. N through Z are further shades of yellow; from very light yellow to light yellow.
                                </p>
                            </div>
                        </div>
                    </div>
                </section>  
                <section class="pad-t-50 pad-b-0">
                    <div class="row m-offset-15">
                        <div class="col-md-6 col-sm-12">
                            <div class="wrapper bg-white">
                                <h2 class="theme_bg_header_gradient t24">CLARITY</h2>
                                <img src="/images/jewellery-education/clarity.jpg" alt="Jewellery Clarity">
                                <p>
                                    Diamonds are bound to have some imperfections. Clarity is the measure of tiny little imperfections. The internal imperfections are called inclusions while the external ones are called blemishes. In short, the clearer the diamond, the better it is. Clarity grades FL and IF denotes Flawless and Internally Flawless which are very rare. VVS1 and VVS2 is Very Very Slightly Included where imperfections are very difficult to see under 10x magnification. Imperfections are vaguely detectable under 10x magnification in VS1 and VS2, Very Slightly Included. SI1 and SI2 are Slightly Included where imperfections are easily detectable under 10x magnification.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="wrapper bg-white">
                                <h2 class="theme_bg_header_gradient t24">CUT</h2>
                                <img src="/images/jewellery-education/cut.jpg" alt="Diamond Cuts">
                                <p>
                                    Diamond’s cut grade indicates its light performance. It’s one of the most characteristic feature of a diamond. Also because all the other C’s are influenced by nature while cut is determined by it’s makers. Cut grades are Ideal/Excellent, Very Good, Good, Fair and Poor; in the decreasing order of their light performance. Alternatively, the more a diamond reflects the light, the higher is its quality. The important thing to note here is that the cut of a diamond is different from the its shape. Shape is the external appearance of the diamond, while the cut is more indicative of its reflective qualities.
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </article>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoJewelleryEducation.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>