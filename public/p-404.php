<!DOCTYPE html>
<html lang="en" data-ng-app="vivoPageNotFound">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta name="keywords" content="" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>Sorry, Page you are looking for is not Available | VivoCarat</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />
    <!-- SEO-->
    <meta name="robots" content="noindex,nofollow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/404">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/404">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/404" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body style="overflow-x: hidden;" data-ng-controller='pagenotfoundCtrl' ng-cloak>
<vivo-header></vivo-header>    
<div>
<div class="container m-tb-50">
 <div class="login">
  <div class="wrap">
   <div class="page-not-found">
    <h1>404</h1>
    <p class="normal-text">
        Oops! The Web address you entered is no longer available.
        <br>
        Go back to
    </p>
    <p class="normal-text">
     <a class="btn btn-buy-now" href="/" target="_self">
         <img class="exclude" src="/images/icons/favico.png">www.vivocarat.com
     </a>
    </p>
   </div>
  </div>
 </div>
</div> 
</div>
 <vivo-footer></vivo-footer>       
<script src="/js/jquery.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/css3-mediaqueries.js"></script>
<script src="/js/megamenu.js"></script>
<script src="/js/slides.min.jquery.js"></script>
<script src="/js/jquery.jscrollpane.min.js"></script>
<script src="/js/jquery.easydropdown.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/custom.js"></script>
<script src="/js/angular.min.js"></script>
<script src="/js/angular-ui-router.min.js"></script>
<script src="/js/angular-animate.min.js"></script>
<script src="/js/angular-sanitize.js"></script>
<script src="/js/satellizer.min.js"></script>
<script src="/js/angular.rangeSlider.js"></script>
<script src="/js/select.js"></script>
<script src="/js/toaster.js"></script>
<script src="/js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="/js/taggedInfiniteScroll.js"></script>
<script src="/js/jquery.easing.min.js"></script>
<script src="/js/angular-google-plus.min.js"></script>
<script src="/js/jquery.etalage.min.js"></script>
<script src="/js/jquery.simplyscroll.js"></script>
<!--  start angularjs modules  -->
<script src="/app/modules/vivoCommon.js"></script>
<script src="/app/modules/404.js"></script>
<!-- end angularjs modules -->
<script src="/app/data.js"></script>
<!-- Start include Controller for angular -->
<script src="/app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->
<script src="/device-router.js"></script>
<!-- Facebook Pixel Code -->
<script>
    !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->    
<!-- onesignal start   -->
<link rel="manifest" href="/manifest.json">
<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
      autoRegister: true,
      notifyButton: {
        enable: false /* Set to false to hide */
      }
    }]);
</script>
<!-- onesignal end   -->    
 </body>
</html>