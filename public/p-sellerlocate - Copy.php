<!DOCTYPE html>
<html lang="en" data-ng-app="vivoSellerlocate">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/seller-locate" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Partner Stores | VivoCarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>Partner Stores | VivoCarat</title>
    <meta name="keywords" content="partner,online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/pseller-locate">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/seller-locate">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/seller-locate" />
    <link href="css/style.css" rel="stylesheet" media="all">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/megamenu.css" rel="stylesheet" media="all">
    <link href="css/etalage.css" rel="stylesheet" media="all">
    <link href="css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="css/kendo.common-material.min.css" rel="stylesheet">
    <link href="css/kendo.material.min.css" rel="stylesheet">
    <!-- FontAwesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
</head>
<body>
    <vivo-header></vivo-header>
    <div data-ng-controller='sellerlocateCtrl' ng-cloak>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>VIVO PARTNER STORES</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="exclude m-t-0 m-b-30">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr uppercase">
                            <span>Vivo Partner Stores</span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p class="pad-b-0">
                            VivoCarat Partner Store is a unique blend of digital technology and the knowledge of the experienced jeweller. We love our customers before and also after a purchase. With this intention, we have created In-Store Experience Centres inside our Partner Retail Stores. You can visit these stores and check out the best designs on the VivoCarat platform. You can complete the purchase in the store or at the comfort of your home. Not just that, you can also avail after-sales services from the partner stores.
                            <br><br> Happy Shopping.
                            <br><br> If you have any suggestions for improving our offerings please write to us at hello@vivocarat.com
                        </p>
                    </div>
                </div>
                <div class="row bg-skin-fair m-tb-25 m-lr-15 text-center b1-lgray bs-simple">
                    <div class="col-md-4">
                        <div class="icon-home sprite-plocate block-center m-t-10"></div>
                        <h3 class="exclude m-tb-10 capitalize">Trusted Jewellers offering 10000+ designs</h3>
                        <p class="unbold exclude m-tb-10">
                            We have partnered with Trusted Retail Jewellery Stores where you can access VivoCarat's exhaustive and trending design collection from the In-Store Experience kiosk's.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-ring-customize sprite-plocate block-center m-t-10"></div>
                        <h3 class="exclude m-tb-10 capitalize">Repair/Polish your jewellery</h3>
                        <p class="exclude m-tb-10">
                            You can visit the partner stores for post sales services to always keep your jewellery shining and in perfect condition.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <div class="icon-mesurement sprite-plocate block-center m-t-10"></div>
                        <h3 class="exclude m-tb-10 capitalize">Ring/Bangle/Bracelet sizing assistance</h3>
                        <p class="exclude m-tb-10">
                            Want to be sure about your ring/bangle size? Or need any assistance then get in touch with a VivoCarat Partner Store
                        </p>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr uppercase">
                            <span>Find Us</span>
                        </h2>
                    </div>
                </div>
                <div class="row m-tb-15 pad-lr-15">
                    <div class="col-md-6 col-sm-12 no-pad-lr h-465">
                        <div class="row pad-b-15 pad-t-30 bg-lgray-f1">
                                <div class="col-xs-12 pad-lr-30">
                                    <form>
                                        <div class="partner_locate_search_form">
                                            <span>
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </span>
                                            <input class="exclude" type="text" placeholder="Enter your city" data-ng-model="searchtext">
                                            <span class="submit">
                                            <button data-ng-click="getSellerDetailBySearch(searchtext)">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <div class="partner_locate">
                            <div class="row partner_loc pad-tb-0 pad-lr-30 b1-b-e8" data-ng-repeat="s in sellers track by $index">
                                <div class="col-md-12 no-pad-lr">
                                    <h3 class="exclude m-tb-15">{{s.name}}</h3>
                                    <address class="m-tb-15">
                                        {{s.address}} {{s.pincode}}
                                        <br>
                                        Phone: {{s.phone}}
                                    </address>
                                </div>
                            </div>
                            <div id="sellernotfound">
                                <div class="row">
                                    <div class="col-xs-12 pad-25">
                                        <p>
                                            Sorry! We are not available in this city at present. But we will be there soon.<br> Please provide your details below and we will inform you when we get a Partner Store in your city.
                                        </p>
                                    </div>
                                </div>
                                <div class="default_form">
                                    <form name="sellerenquiryForm" ng-submit="saveSellerLocationEnquiry()" autocomplete="off" novalidate>
                                        <div class="row">
                                            <div class="col-md-3">
                                                Name
                                            </div>
                                            <div class="col-md-1">
                                                :
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'" data-ng-model="sellerenquiry.pname" name="pname" id="pname" data-ng-required="true" data-ng-pattern="/^[a-zA-Z\s]+$/">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-4 theme-text" data-ng-show="sellerenquiryForm.pname.$dirty && sellerenquiryForm.pname.$error.required">Name is required</div>
                                                <div class="col-md-8 col-md-offset-4 theme-text" data-ng-show="sellerenquiryForm.pname.$dirty && sellerenquiryForm.pname.$error.pattern">Name can contain only alphabets</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                City/Location
                                            </div>
                                            <div class="col-md-1">
                                                :
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" placeholder="Enter City" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter City'" data-ng-model="sellerenquiry.plocation" name="plocation" id="plocation" data-ng-required="true" data-ng-pattern="/^[a-zA-Z\s]+$/">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-4 theme-text" data-ng-show="sellerenquiryForm.plocation.$dirty && sellerenquiryForm.plocation.$error.required">City/Location is required</div>
                                                <div class="col-md-8 col-md-offset-4 theme-text" data-ng-show="sellerenquiryForm.plocation.$dirty && sellerenquiryForm.plocation.$error.pattern">City/Location can contain only alphabets</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                Email
                                            </div>
                                            <div class="col-md-1">
                                                :
                                            </div>
                                            <div class="col-md-8">
                                                <input type="email" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email'" data-ng-model="sellerenquiry.pemail" name="pemail" id="pemail" data-ng-required="true">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-4 theme-text" data-ng-show="sellerenquiryForm.pemail.$dirty && sellerenquiryForm.pemail.$error.required">Email is required</div>
                                                <div class="col-md-8 col-md-offset-4 theme-text" data-ng-show="sellerenquiryForm.pemail.$dirty && sellerenquiryForm.pemail.$error.email">Invalid Email</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                Phone
                                            </div>
                                            <div class="col-md-1">
                                                :
                                            </div>
                                            <div class="col-md-8">
                                                <input type="text" placeholder="+91 Enter Phone" onfocus="this.placeholder = ''" onblur="this.placeholder = '+91 Enter Phone'" data-ng-model="sellerenquiry.pphone" name="pphone" id="pphone" data-ng-required="true" data-ng-pattern="/^[0-9]{10,10}$/">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-4 theme-text" data-ng-show="sellerenquiryForm.pphone.$dirty && sellerenquiryForm.pphone.$error.required">Phone is required</div>
                                                <div class="col-md-8 col-md-offset-4 theme-text grey-colour" data-ng-show="sellerenquiryForm.pphone.$dirty && sellerenquiryForm.pphone.$error.pattern">Enter correct phone number</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 pad-t-15">
                                                <button type="submit" class="btn browse-more-button pull-right" ng-disabled="!sellerenquiryForm.$valid">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 no-pad-lr">
                        <!--<iframe style="width: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.0342944589356!2d72.8632563144684!3d19.106151387070582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c8478e27b5d5%3A0xf3d09043b2998e9e!2sSwaroop+Centre!5e0!3m2!1sen!2sin!4v1482234650022" width="1111" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                        <div id="map" class="w-full h-465"></div>
                        <!-- Old Size: style="width: 1111px; height: 370px;" -->
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr uppercase">
                            <span>Ask for VivoCarat</span>
                        </h2>
                    </div>
                </div>
                <div class="row m-tb-15">
                    <div class="bg-e m-lr-15 float-left">
                        <div class="col-lg-3 col-md-4 pad-lr-0">
                            <img src="/images/partner-locate/partner-locate.png" alt="Partner Locate">
                        </div>
                        <div class="col-lg-9 col-md-8 pad-lr-20 pad-tb-30">
                            <p>
                                We have partnered with Trusted Jewellery Retail Stores where you can access VivoCarat's exhaustive and trending designs from the In-Store Experience Centre Kiosks. There are always special discounts running at these stores so ask for VivoCarat when you see our banner outside a showroom.
                                <br><br>
                                How to recognize a VivoCarat Partner Store? Every Partner Store will be VivoCarat Branded which will be visible all around the store. You will also find a VivoCarat Kiosk in few of the stores. You can browse and buy products on these Digital Kiosk's and choose to pay online or at the store. The order will be securely delivered to your doorsteps by our trusted logistics partners.
                                <br><br>
                                Why should I buy from a Partner Store? Buying from a Partner Store is same as buying with us online. You will get all the benefits and will be eligible for all our after-sales services. The partner stores are there just to making the jewellery buying experience smooth for you. If you want to find out your ring size, try a few designs, or just talk about jewellery; our Partner Stores will be more than happy to help you.
                                <br><br>
                                If you have any more questions, please feel free to Call Us at +91 9503781870 or email us at hello@vivocarat.com.
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="js/jquery.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/css3-mediaqueries.js"></script>
    <script src="js/megamenu.js"></script>
    <script src="js/slides.min.jquery.js"></script>
    <script src="js/jquery.jscrollpane.min.js"></script>
    <script src="js/jquery.easydropdown.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/angular-ui-router.min.js"></script>
    <script src="js/angular-animate.min.js"></script>
    <script src="js/angular-sanitize.js"></script>
    <script src="js/satellizer.min.js"></script>
    <script src="js/angular.rangeSlider.js"></script>
    <script src="js/select.js"></script>
    <script src="js/toaster.js"></script>
    <script src="js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="js/taggedInfiniteScroll.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/angular-google-plus.min.js"></script>
    <script src="js/jquery.etalage.min.js"></script>
    <script src="js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="app/modules/vivoCommon.js"></script>
    <script src="app/modules/vivoSellerlocate.js"></script>
    <!-- end angularjs modules -->
    <script src="app/data.js"></script>
    <!--<script src="app/directives.js"></script>-->
    <!-- Start include Controller for angular -->
    <script src="app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-67690535-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATiG7u40IAJS0xYnaZJg6d_vSwc26iIac&extension=.js"></script>
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>