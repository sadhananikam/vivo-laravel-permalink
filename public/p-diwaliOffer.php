<!DOCTYPE html>
<html lang="en" data-ng-app="vivoHome">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/diwali-offer" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Vivo Diwali Sale | Free 10 gram Silver Coin" />
    <meta property="og:description" content="Buy Gold & Diamond Rings, Earrings, Pendants, Nosepins and Goldcoins at upto 40% off this Diwali. Also get a 10 gram Silver Coin with every purchase above Rs.15000" />
    <meta property="og:image" content="https://www.vivocarat.com/images/diwali_promo.png" />
    <title>Vivo Diwali Sale | Free 10 gram Silver Coin</title>
    <meta name="keywords" content="diwali,goldcoins,rings,jewellery,jewelry,diwali-sale,gold,diamond" />
    <meta name="description" content="Buy Gold & Diamond Rings, Earrings, Pendants, Nosepins and Goldcoins at upto 40% off this Diwali. Also get a 10 gram Silver Coin with every purchase above Rs.15000" />
    <link rel="canonical" href="https://www.vivocarat.com/diwali-offer">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/diwali-offer">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/diwali-offer" />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <meta name="google-site-verification" content="jcP_vd70IOn4T8kX7Tix5aBBzNiuck3vVZT_y8gX5Vo" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='homeCtrl'>
        <div id="carousel-example-generic" style="position:relative;z-index:1">
            <img class="ban-img img-responsive" data-ng-src="/images/home/carousel banners/Diwali Offer-VivoCarat-n.jpg" style="height:calc(100vh - 173px) !important;" alt="Diwali Offer on VivoCarat">
        </div>
        <div class="container">
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>CATEGORIES</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pad-r-0">
                        <a href="/jewellery/diamond-earrings.html" target="_self">
                            <img src="images/diwali-offer/ER.png" alt="Diwali Offer - Ear Rings for Women" />
                        </a>
                    </div>
                    <div class="col-md-6 pad-l-0">
                        <a href="/jewellery/diamond-rings.html" target="_self">
                            <img src="images/diwali-offer/ring.png" alt="Diwali Offer - Rings for Women" />
                        </a>
                        <a href="/jewellery/diamond-pendants.html" target="_self">
                            <img src="images/diwali-offer/pendent.png" alt="Diwali Offer - Pendants" />
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pad-r-0">
                        <a href="/jewellery/diamond-rings.html" target="_self">
                            <img src="images/diwali-offer/Rose-gold.png" alt="Diwali Offer - Rose Gold Jewellery" />
                        </a>
                        <a href="/jewellery/diamond-nosepins.html" target="_self">
                            <img src="images/diwali-offer/nose-pins.png" alt="Diwali Offer - Nose Pins" />
                        </a>                        
                    </div>
                    <div class="col-md-6 pad-l-0">
                        <a href="/search/religious.html" target="_self">
                            <img src="images/diwali-offer/diwali-products.png" alt="Diwali Offer - Jewellery Products" />
                        </a>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="hr h2"><span>Terms & Condition of VIVO Diwali Sale</span></h1>
                        <ul class="exclude m-l-15">
                            <li>This offer is valid on purchase of any jewellery from VivoCarat.com except GoldCoins.</li>
                            <li>This offer is valid from 9th September to 22nd October 2017.</li>
                            <li>This offer cannot be clubbed with any other vouchers or discount coupons.</li>
                            <li>For a purchase of Rs.15000 and above, a 10 grams Silver coin of 995 purity will be given as free and will be dispatched along with jewellery.</li>
                            <li>The Silver Coin will be automatically added to the cart and no Coupon Code is required.</li>
                            <li>If the product is returned customer will have to return the free gift as well.</li>
                            <li>This offer cannot be encashed under any circumstance.</li>
                            <li>Any conditions which are not explicitly covered would be at the sole discretion of VivoCarat. The decision of the VivoCarat in this regard will be final and the company has the right to change terms and conditions at any time.</li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
        <!--End of container div-->
    </div>
    <vivo-footer></vivo-footer>
    
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoDiwaliOffer.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
        <!--Script for making the image scroller clickable for navigating to Jeweller's page-->
    <script>
        $(document).ready(function() {
            $("#scroller").simplyScroll();
        });
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->
</body>
</html>