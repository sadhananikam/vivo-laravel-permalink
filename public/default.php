<!DOCTYPE html>
<html lang="en" data-ng-app="vivoHome">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Online Jewellery Shopping | Buy Gold & Diamond Rings, Earrings, Bracelets at Lowest Prices from Trusted Brands | VivoCarat.com" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>Online Jewellery Shopping | Buy Gold & Diamond Rings, Earrings, Bracelets at Lowest Prices from Trusted Brands | VivoCarat.com</title>
    <meta name="keywords" content="online jewellery shopping, diamond jewellery, gold jewellery, fashion Jewellery, rings, earrings, pendant, engagement rings, online jewellery shopping india, jewellery design" />
    <meta name="description" content="Buy Gold & Diamond Rings,Earrings, Bracelets with latest designs from trusted brands at lowest prices.✓100% Certified ✓Lifetime exchange ✓COD ✓Free Shipping" />
    <link rel="canonical" href="https://www.vivocarat.com">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/home">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/home" />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <meta name="google-site-verification" content="jcP_vd70IOn4T8kX7Tix5aBBzNiuck3vVZT_y8gX5Vo" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location="/m/home"+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m/home"}}
        else{document.location="/m/home"}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='homeCtrl'>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" style="position:relative;z-index:1">
            <!-- Indicators -->
            <ol class="carousel-indicators" style="margin-bottom: 0;">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                <li data-target="#carousel-example-generic" data-slide-to="4"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div data-ng-repeat="b in banners" class="item" data-ng-class="$index==0?'active':''">
                    <a data-ng-if="b.is_link==1" data-ng-href="{{b.href}}" target="{{b.target}}">
                        <img class="ban-img img-responsive" data-ng-src="{{b.img_url}}" alt="{{b.alt}}">
                    </a>
                    <img data-ng-if="b.is_link==0" class="ban-img img-responsive" data-ng-src="{{b.img_url}}" style="height:calc(100vh - 173px) !important;" alt="{{b.alt}}">
                </div>
            </div>
            <!-- Controls -->
            <div class="left carousel-control banner-arrow-structure" data-target="#carousel-example-generic" role="button" data-slide="prev">
                <div class="left-scroll center-block"></div>
            </div>
            <div class="right carousel-control banner-arrow-structure" data-target="#carousel-example-generic" role="button" data-slide="next">
                <div class="right-scroll center-block"></div>
            </div>
        </div>
        <div class="container">
            <div class="pad-lr-15">
                <div class="container infyscroll scroller-parent-container">
                    <div id="scroller" class="img-responsive" style="max-width:none;">
                        <img class="first pointer" id="incocu" src="/images/header/brands logos dropdown/Incocu Jewellers hover.png" alt="Incocu Jewellers logo">
                        <img class="pointer" id="shankaram" src="/images/header/brands logos dropdown/Shankaram Jewellers hover.png" alt="Shankaram Jewellers logo">
                        <img class="pointer" id="lagu" src="/images/header/brands logos dropdown/Lagu Bandhu hover.png" alt="Lagu Bandhu logo">
                        <img class="pointer" id="arkina" src="/images/header/brands logos dropdown/Arkina-Diamonds hover.png" alt="Arkina-Diamonds logo">
                        <img class="pointer" id="ornomart" src="/images/header/brands logos dropdown/OrnoMart hover.png" alt="OrnoMart logo">
                        <img class="pointer" id="kundan" src="/images/header/brands logos dropdown/Kundan Jewellers hover.png" alt="Kundan Jewellers logo">
                        <img class="pointer" id="mayura" src="/images/header/brands logos dropdown/Mayura Jewellers hover.png" alt="Mayura Jewellers logo">
                        <img class="pointer" id="megha" src="/images/header/brands logos dropdown/Megha Jewellers hover.png" alt="Megha Jewellers logo">
                        <img class="pointer" id="regaalia" src="/images/header/brands logos dropdown/Regaalia Jewels hover.png" alt="Regaalia Jewels logo">
                        <img class="pointer" id="glitter" src="/images/header/brands logos dropdown/Glitter Jewels hover.png" alt="Glitter Jewels logo">
                        <img class="pointer" id="pp" src="/images/header/brands logos dropdown/PP-Gold hover.png" alt="Parshwa Padmavati Gold(PPG) logo">
                        <img class="pointer" id="karatcraft" src="/images/header/brands logos dropdown/KaratCraft hover.png" alt="KaratCraft logo">
                        <img class="pointer" id="zkd" src="/images/header/brands logos dropdown/ZKD-Jewels hover.png" alt="Zaveri Kapoorchand Dalichand & Sons (ZKD)-Jewels logo">
                        <img class="pointer" id="iskiuski" src="/images/header/brands logos dropdown/IskiUski hover.png" alt="IskiUski logo">
                        <img class="pointer" id="myrah" src="/images/header/brands logos dropdown/Myrah-Silver-Works hover.png" alt="Myrah-Silver-Works logo">
                        <img class="pointer" id="charu" src="/images/header/brands logos dropdown/Charu-Jewels hover.png" alt="Charu-Jewels logo">
                        <img class="pointer" id="tsara" src="/images/header/brands logos dropdown/tsara hover.png" alt="Tsara-Jewellery logo">
                        <img class="pointer" id="sarvada" src="/images/header/brands logos dropdown/Sarvada-Jewels hover.png" alt="Sarvada-Jewels logo">
                    </div>
                </div>
            </div>
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>CATEGORIES</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pad-r-0">
                        <a href="/jewellery/diamond-earrings.html" target="_self">
                            <img src="images/home/categories/ER.jpg" alt="Ear Rings for Women" />
                        </a>
                    </div>
                    <div class="col-md-6 pad-l-0">
                        <a href="/jewellery/diamond-rings.html" target="_self">
                            <img src="images/home/categories/ring.jpg" alt="Rings for Women" />
                        </a>
                        <a href="/jewellery/diamond-pendants.html" target="_self">
                            <img src="images/home/categories/pendent.jpg" alt="Pendants" />
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pad-r-0">
                        <a href="/jewellery/diamond-rings.html" target="_self">
                            <img src="images/home/categories/Rose-gold.jpg" alt="Rose Gold Jewellery" />
                        </a>
                        <a href="/jewellery/diamond-nosepins.html" target="_self">
                            <img src="images/home/categories/nose-pins.jpg" alt="Nose Pins" />
                        </a>                        
                    </div>
                    <div class="col-md-6 pad-l-0">
                        <a href="/search/religious.html" target="_self">
                            <img src="images/home/categories/new-arrival.jpg" alt="Jewellery Products" />
                        </a>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-md-11">
                        <h2 class="hr"><span>FEATURED PRODUCTS</span></h2>
                    </div>
                    <div class="col-md-1 controls pull-right">
                        <a data-ng-init="look1='/images/home/icons/home icons.png'" data-ng-mouseout="look1='/images/home/icons/home icons.png'" data-ng-mouseover="look1='/images/home/icons/home icons.png'" class="left carousel-control featured-products-arrow-background" data-target="#carousel-example-feat" data-slide="prev">
                            <div class="left-arrow" data-ng-src="/images/home/icons/home icons.png" src="/images/home/icons/home icons.png"></div>
                        </a>
                        <a data-ng-init="look2='/images/home/icons/home icons.png'" data-ng-mouseout="look2='/images/home/icons/home icons.png'" data-ng-mouseover="look2='/images/home/icons/home icons.png'" class="right carousel-control featured-products-arrow-background" data-target="#carousel-example-feat" data-slide="next">
                            <div class="right-arrow featured-products-right-arrow-position" data-ng-src="/images/home/icons/home icons.png" src="/images/home/icons/home icons.png"></div>
                        </a>
                    </div>
                </div>
                <vivo-featured-product-carousel></vivo-featured-product-carousel>
            </section>
            <section class="exclude m-0">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>OUR CATALOGUE</span></h2>
                    </div>
                </div>
                <div class="row m-t-15">
                    <div class="col-md-6 no-pad-r">
                        <img src="/images/home/catalogue/engagement.jpg" alt="VivoCarat catalogue">
                    </div>
                    <div class="col-md-6 no-pad-l">
                        <div class="row">
                            <div class="col-md-6 no-pad-lr">
                                <a href="/jewellery/Gold-All.html" target="_self">
                                    <img src="/images/home/catalogue/gold.jpg" alt="Gold catalogue">
                                </a>
                            </div>
                            <div class="col-md-6 no-pad-lr">
                                <a href="/jewellery/diamond-All.html" target="_self">
                                    <img src="/images/home/catalogue/diamond.jpg" alt="Diamond catalogue">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="row m-t-30">
                <div class="col-xs-12">
                    <h2 class="hr"><span>LOOKBOOK</span></h2>
                </div>
            </div>
        </div>
        <!--End of container div-->
        <div class="row">
            <div class="col-md-12 bg-lgray-f7 pad-tb-40 m-t-15">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <p class="exclude m-b-20">
                                Stay tuned to stay updated! Treat us as your personal stylist and let us take you to the world of fad and fancy.
                            </p>
                            <a data-ng-href="/lookbook/{{title[0].urlslug}}.html" target="_self" class="m-t-15">
                                <div class="row b1-lgray-d5">
                                    <div class="col-md-12 no-pad-lr">
                                        <h3 class="title-background exclude text-limit" title="{{title[0].title}}">
                                            {{title[0].title}}
                                        </h3>
                                    </div>
                                </div>
                                <div class="row b1-lgray-d5">
                                    <div class="col-md-12 no-pad-lr">
                                        <img data-ng-src="/images/lookbook/blogs/{{title[0].banner_img}}.jpg" alt="{{title[0].banner_img}}">
                                    </div>
                                </div>
                            </a>
                            <div class="row lookbook-footer">
                                <div class="col-md-8">
                                    {{title[0].category}}
                                </div>
                                <div data-ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
                                    {{title[0].created_at}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <a data-ng-href="/lookbook/{{title[1].urlslug}}.html" target="_self" class="m-t-15">
                                <div class="row b1-lgray-d5">
                                    <div class="col-md-12 no-pad-lr">
                                        <h3 class="title-background exclude text-limit" title="{{title[1].title}}">
                                            {{title[1].title}}
                                        </h3>
                                    </div>
                                </div>
                                <div class="row b1-lgray-d5">
                                    <div class="col-md-12 no-pad-lr">
                                        <img data-ng-src="/images/lookbook/blogs/{{title[1].banner_img}}.jpg" alt="{{title[1].banner_img}}">
                                    </div>
                                </div>
                            </a>
                            <div class="row lookbook-footer">
                                <div class="col-md-8">
                                    {{title[1].category}}
                                </div>
                                <div data-ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
                                    {{title[1].created_at}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 pad-t-30 text-center">
                                    <a class="btn btn-vivo" href="/lookbook" target="_self"> 
                                        Browse more styles 
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p class="exclude m-b-20">
                                Our lookbook features trendy posts to enlighten you with the latest who’s who of fashion, jewellery, celebrity style and much more.
                            </p>
                            <a data-ng-href="/lookbook/{{title[2].urlslug}}.html" target="_self" class="m-t-15">
                                <div class="row b1-lgray-d5">
                                    <div class="col-md-12 no-pad-lr">
                                        <h3 class="title-background exclude text-limit" title="{{title[2].title}}">
                                            {{title[2].title}}
                                        </h3>
                                    </div>
                                </div>
                                <div class="row b1-lgray-d5">
                                    <div class="col-md-12 no-pad-lr">
                                        <img data-ng-src="/images/lookbook/blogs/{{title[2].banner_img}}.jpg" alt="{{title[2].banner_img}}">
                                    </div>
                                </div>
                            </a>
                            <div class="row lookbook-footer">
                                <div class="col-md-8">
                                    {{title[2].category}}
                                </div>
                                <div data-ng-bind="formatDate(date) |  date:'dd MMMM'" class="col-md-4 text-right">
                                    {{title[2].created_at}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END of container div-->
            </div>
        </div>
        <div class="container">
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>TESTIMONIAL</span></h2>
                    </div>
                </div>
                <div class="row testimonial-side-space m-t-15">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12 text-center testimonial-structure-1">
                                <img class="img-circle testimonial-image-structure" src="/images/home/testimonial/benson.png" alt="attestant Benson">
                                <p class="testimonial-text">
                                    Beautiful collection of jewellery. The delivery packaging is the best amongst all ecomm sites. One happy customer.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12 text-center testimonial-structure-2">
                                <img class="img-circle testimonial-image-structure" src="/images/home/testimonial/ajinkya.png" alt="attestant Ajinkya">
                                <p class="testimonial-text">
                                    Had to get an anniversary gift for my wife. VivoCarat delivered the necklace in 5 days. Earlier than what was promised. Strongly recommend it.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12 text-center testimonial-structure-3">
                                <img class="img-circle testimonial-image-structure" src="/images/home/testimonial/sarika.png" alt="attestant Sarika">
                                <p class="testimonial-text">
                                    VivoCarat sells the best jewellery available online. Smooth ordering and express delivery. Loved it.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr exclude m-t-0"><span>OUR COMMITMENT</span></h2>
                    </div>
                </div>
                <div class="row m-t-15">
                    <div class="col-md-3 text-center no-pad-lr" data-ng-init="abouthover1='/images/home/our commitment/our commitment.png'" data-ng-mouseout="abouthover1='/images/home/our commitment/our commitment.png'" data-ng-mouseover="abouthover1='/images/home/our commitment/our commitment.png'">
                        <a href="/about-us" target="_self">
                            <div class="trusted-jewellers" data-ng-src="{{abouthover1}}" title="Trusted Jewellers"></div>
                        </a>
                        <h4>Trusted Jewellers</h4>
                    </div>
                    <div class="col-md-3 text-center no-pad-lr" data-ng-init="abouthover2='/images/home/our commitment/our commitment.png'" data-ng-mouseout="abouthover2='/images/home/our commitment/our commitment.png'" data-ng-mouseover="abouthover2='/images/home/our commitment/our commitment.png'">
                        <a href="/about-us" target="_self">
                            <div class="commitment-certified" data-ng-src="{{abouthover2}}" title="Certified & Hallmarked"></div>
                        </a>
                        <h4>Certified &amp; Hallmarked</h4>
                    </div>
                    <div class="col-md-3 text-center no-pad-lr" data-ng-init="abouthover3='/images/home/our commitment/our commitment.png'" data-ng-mouseout="abouthover3='/images/home/our commitment/our commitment.png'" data-ng-mouseover="abouthover3='/images/home/our commitment/our commitment.png'">
                        <a href="/about-us" target="_self">
                            <div class="commitment-free-shipping" data-ng-src="{{abouthover3}}" title="Free Shipping"></div>
                        </a>
                        <h4>Free Shipping</h4>
                    </div>
                    <div class="col-md-3 text-center no-pad-lr" data-ng-init="abouthover4='/images/home/our commitment/our commitment.png'" data-ng-mouseout="abouthover4='/images/home/our commitment/our commitment.png'" data-ng-mouseover="abouthover4='/images/home/our commitment/our commitment.png'">
                        <a href="/about-us" target="_self">
                            <div class="commitment-easy-return" data-ng-src="{{abouthover4}}" title="Easy Return Policy"></div>
                        </a>
                        <h4>Easy Return Policy</h4>
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="hr"><span>VIVOCARAT IN NEWS</span></h2>
                </div>
            </div>
        </div>
        <!--End of container-->
        <div class="row bg-f2f0f1 m-t-15">
            <div class="container">
                <ul class="media-ul">
                    <li class="media-li">
                        <a href="http://bwdisrupt.businessworld.in/article/VivoCarat-com-an-Online-Jewellery-Marketplace-Secures-50K-in-Seed-Funding/20-12-2016-110077/" target="_blank" rel="noopener noreferrer">
                            <img src="/images/home/media/business-standard-logo.png" alt="business-standard-logo">
                        </a>
                    </li>
                    <li class="media-li">
                        <a href="http://www.dealstreetasia.com/stories/india-dealbook-vivocarat-tiyo-events-high-raise-funding-60935/" target="_blank" rel="noopener noreferrer">
                            <img src="/images/home/media/deal-street-asia-logo.png" alt="deal-street-asia-logo">
                        </a>
                    </li>
                    <li class="media-li">
                        <a href="http://techcircle.vccircle.com/2016/12/19/online-jewellery-marketplace-vivocarat-raises-seed-funding/" target="_blank" rel="noopener noreferrer">
                            <img src="/images/home/media/vcccircle logo.png" alt="vcccircle logo">
                        </a>
                    </li>
                    <li class="media-li">
                        <a href="http://techstory.in/jewellery-vivocarat-funding-1912/" target="_blank" rel="noopener noreferrer">
                            <img src="/images/home/media/techstory logo.png" alt="techstory logo">
                        </a>
                    </li>
                    <li class="media-li">
                        <a href="http://economictimes.indiatimes.com/small-biz/money/vivocarat-com-raises-50000-in-seed-funding/articleshow/56061909.cms" target="_blank" rel="noopener noreferrer">
                            <img src="/images/home/media/ettech-logo.png" alt="ettech-logo">
                        </a>
                    </li>
                    <li class="media-li">
                        <a href="http://www.moneycontrol.com/news/sme/online-jewellery-store-vivocaratcom-raises-3650kseed-funding_8135561.html" target="_blank" rel="noopener noreferrer">
                            <img src="/images/home/media/money-control-logo.png" alt="money-control-logo">
                        </a>
                    </li>
                    <li class="media-li">
                        <a href="https://www.techinasia.com/4-rising-startups-in-india-dec-19-2016" target="_blank" rel="noopener noreferrer">
                            <img src="/images/home/media/techinasia-logo.png" alt="techinasia-logo">
                        </a>
                    </li>
                </ul>
            </div>
            <!--End of container div-->
        </div>
        <div class="container">
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>FOLLOW US ON</span></h2>
                    </div>
                </div>
                <div class="row m-t-15">
                    <div class="col-md-2 text-center">
                        <a href="https://twitter.com/vivocarat" title="Twitter" target="_blank" rel="noopener noreferrer">
                            <div class='twitter'></div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="https://www.facebook.com/VivoCarat/" title="Facebook" target="_blank" rel="noopener noreferrer">
                            <div class='facebook'></div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="https://www.pinterest.com/vivocarat/" title="Pinterest" target="_blank" rel="noopener noreferrer">
                            <div class='pinterest'></div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="https://www.instagram.com/vivocarat/" title="Instagram" target="_blank" rel="noopener noreferrer">
                            <div class='insta'></div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="https://plus.google.com/+vivocarat" title="Google +" target="_blank" rel="noopener noreferrer">
                            <div class='googleplus'></div>
                        </a>
                    </div>
                    <div class="col-md-2 text-center">
                        <a href="https://www.linkedin.com/company/vivocarat" title="Linkedin" target="_blank" rel="noopener noreferrer">
                            <div class='linkedin'></div>
                        </a>
                    </div>
                </div>
            </section>
        </div>
        <!--End of container-->
    </div>
    <vivo-footer></vivo-footer>
    <div class="row m-tb-40">
        <div class="container">
            <div class="col-xs-12">
                <h3>VivoCarat - Online Jewellery Store</h3>
                <p>
                    VivoCarat is an online jewellery store that features the best designs from trusted brands at affordable prices. We take pride in showcasing our vivid, spectacular and assorted diamond, gold and gemstone jewellery collection. Each piece is distinctive and flawless in itself since it has been passed under the eye of strict professionals resulting in unparalleled craftsmanship. Every piece of ring, earring, pendant, couple band, bangle, bracelet, engagement ring, gold coin on the portal is Customisable according to your demands.
                </p>
                <h3>Why buy Jewellery Online?</h3>
                <p>
                    The number of designs and options available online on our store is so extensive that you will be spoilt for choice. Unlike buying in a retail store where you have to make a decision in a short span of time, here you can have the luxury of browsing anytime during the day and shortlist products before making that final call. You can also filter jewellery based on collections, diamond type, gemstone type, the weight of gold, price and much more which make the selection process easier.
                </p>
                <h3>Trusted Brands</h3>
                <p>
                    VivoCarat has tied hands with some of the best and most trusted jewellery brands in the country who have a legacy and vast experience in selling jewellery. Our core values are transparency, affordability and top quality which is reflected by every seller on the platform.
                </p>
                <h3>Certified and Hallmarked Jewellery</h3>
                <p>
                    Every product sold on VivoCarat is BIS hallmarked and the diamonds are certified from internationally recognized institutes like IGI, SGL, BIS. Jewellery is not just a piece of ornament but also an investment and buying only certified goods safeguards you and your purchase.
                </p>
                <h3>VivoCarat Partner Stores</h3>
                <p>
                    There are times when just having an online store isn't enough. Like you want to buy a ring or a bangle but don't know which size fits you. For all such cases, you can visit one of our Partner Stores to try real products and understand more about that piece of jewellery that you might keep close to you forever. Few of our Partner Stores have In-Store Experience Centres where you can browse and buy from our catalog by paying in the store or online.
                </p>
            </div>
        </div>
    </div>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoHome.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
        <!--Script for making the image scroller clickable for navigating to Jeweller's page-->
    <script>
        $(document).ready(function() {
            $("#scroller").simplyScroll();
            $('#incocu').click(function() {
                window.location.href = "/brands/Incocu Jewellers"
            });
            $('#glitter').click(function() {
                window.location.href = "/brands/Glitter Jewels"
            });
            $('#kundan').click(function() {
                window.location.href = "/brands/Kundan Jewellers"
            });
            $('#lagu').click(function() {
                window.location.href = "/brands/Lagu Bandhu"
            });
            $('#mayura').click(function() {
                window.location.href = "/brands/Mayura Jewellers"
            });
            $('#megha').click(function() {
                window.location.href = "/brands/Megha Jewellers"
            });
            $('#shankaram').click(function() {
                window.location.href = "/brands/Shankaram Jewellers"
            });
            $('#arkina').click(function() {
                window.location.href = "/brands/Arkina-Diamonds"
            });
            $('#ornomart').click(function() {
                window.location.href = "/brands/OrnoMart"
            });
            $('#regaalia').click(function() {
                window.location.href = "/brands/Regaalia Jewels"
            });
            $('#pp').click(function() {
                window.location.href = "/brands/PP-Gold"
            });
            $('#karatcraft').click(function() {
                window.location.href = "/brands/KaratCraft"
            });
            $('#zkd').click(function() {
                window.location.href = "/brands/ZKD-Jewels"
            });
            $('#iskiuski').click(function() {
                window.location.href = "/brands/IskiUski"
            });
            $('#myrah').click(function() {
                window.location.href = "/brands/Myrah-Silver-Works"
            });

            $('#charu').click(function() {
                window.location.href = "/brands/Charu-Jewels"
            });
            $('#sarvada').click(function() {
                window.location.href = "/brands/Sarvada-Jewels"
            });
            $('#tsara').click(function() {
                window.location.href = "/brands/Tsara-Jewellery"
            });
        });
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->
</body>
</html>