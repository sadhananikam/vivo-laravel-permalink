<!DOCTYPE html>
<html lang="en" data-ng-app="vivoLogin">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta name="keywords" content="" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com/login" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>Login/Register | VivoCarat</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/login">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/login">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/login" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <style>
        .register_account form input[type="numeric"]:hover,
        .register_account form select:hover {
            box-shadow: 0 0 4px #aaa;
            -webkit-box-shadow: 0 0 4px #aaa;
            -moz-box-shadow: 0 0 4px #aaa;
            -o-box-shadow: 0 0 4px #aaa;
        }      
        .register_account form input[type="text"]:invalid {
            color: red !important;
        }        
        .register_account form input[type="text"]:valid {
            color: green !important;
        }       
        .register_account form input[type="email"]:invalid {
            color: red !important;
        }       
        .register_account form input[type="email"]:valid {
            color: green !important;
        }       
        .register_account form input[type="numeric"]:invalid {
            color: red !important;
        }      
        .register_account form input[type="numeric"]:valid {
            color: green !important;
        }
       /*CSS for materializing input fields*/   
        * {
            box-sizing: border-box;
        }
        /* form starting stylings ------------------------------- */     
        .group {
            position: relative;
        }
        input {
            font-family: 'leela';
            font-size: 18px;
            padding: 10px 10px 10px 5px;
            display: block;
            width: 300px;
            border: none;
            border-bottom: 1px solid #888888;
            box-shadow: none;
        } 
        input:focus {
            outline: none;
        }
        label {
            color: #797979 !important;
            font-size: 15px;
            font-weight: normal;
            font-family: 'leela';
            position: absolute;
            pointer-events: none;
            left: 5px;
            top: -10px;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }
        /* active state */
        input:focus~label,
        input:valid~label {
            top: -20px;
            font-size: 14px;
            color: #E62739;
        }
        /* BOTTOM BARS ================================= */
        .bar {
            position: relative;
            display: block;
            width: 100%;
        }  
        .bar:before,
        .bar:after {
            content: '';
            height: 2px;
            width: 0;
            bottom: 1px;
            position: absolute;
            background: #E62739;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }        
        .bar:before {
            left: 50%;
        }        
        .bar:after {
            right: 50%;
        }
        /* active state */      
        input:focus~.bar:before,
        input:focus~.bar:after {
            width: 50%;
        }
        /* HIGHLIGHTER ================================== */ 
        .highlight {
            position: absolute;
            height: 60%;
            width: 100px;
            top: 25%;
            left: 0;
            pointer-events: none;
            opacity: 0.5;
        }
        /* active state */     
        input:focus~.highlight {
            -webkit-animation: inputHighlighter 0.3s ease;
            -moz-animation: inputHighlighter 0.3s ease;
            animation: inputHighlighter 0.3s ease;
        }
        /* ANIMATIONS ================ */
        @-webkit-keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }
        @-moz-keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        } 
        @keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }
        /*END of Material CSS design*/
    </style>
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='loginCtrl'>
        <div class="container">
            <div id="fb-root"></div>
            <div class="row pad-t-20">
                <div class="col-sm-6 b1-r-eee">
                    <div class="vivo-login-panel">
                        <h4 class="text-center gothbody t15">New Customer</h4>
                        <div class="register_account">
                            <div class="wrap">
                                <form name="form">
                                    <div class="row text-center">
                                        <div class="col-lg-12 placeholdlight pad-2">
                                            <div class="group m-b-15 pad-b-10">
                                                <input class="exclude" type="text" data-ng-model="customer_new.name" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Name
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="group m-b-15 pad-b-10">
                                                <input class="exclude" type="text" data-ng-model="customer_new.phone" maxlength="13" pattern="(\+?\d[- .]*){7,13}" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Phone Number
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="group m-b-15 pad-b-10">
                                                <input class="exclude" type="email" data-ng-model="customer_new.email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Email
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="group m-b-15">
                                                <input class="exclude" type="password" data-ng-class="isPasswordError?'error-text':'correct-text'" name="password" data-ng-model="customer_new.password" required>{{message}}
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Password
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="row theme-text lgray-79 t15" align="left">
                                                <p>
                                                    By clicking 'Create Account' you agree to the
                                                    <a class="underline lgray-79" href="/terms" target="_blank">Terms &amp; Conditions</a>.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div>
                                    <input class="always-bold pad-10 t18 w-75pc block-center btn btn-vivo-bold bold-400 uppercase" type="submit" name="Submit" data-ng-click="signup()" value="Create an Account">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 pad-b-30">
                    <div class="vivo-login-panel">
                        <h4 class="text-center gothbody t15">Registered Customers</h4>
                        <div class="register_account">
                            <div class="wrap">
                                <form>
                                    <div class="row text-center">
                                        <div class="col-lg-12 placeholdlight pad-2">
                                            <div class="group m-b-15 pad-b-10">
                                                <input class="exclude" id="modlgn_username" data-ng-model="customer.email" type="text" name="email" size="18" autocomplete="off" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Email
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="group m-b-15">
                                                <input class="exclude" type="password" id="modlgn_passwd" name="password" data-ng-model="customer.password" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Password
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="underline" align="left">
                                                <a class="theme-text lgray-79 t13 pointer" data-toggle="modal" data-target="#myModal">Forgot Your Password ? </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="m-tb-20">
                                    <input class="always-bold pad-10 t18 w-75pc block-center btn btn-vivo-bold bold-400 uppercase" type="submit" name="Submit" data-ng-click="loginUser(customer)" value="Login">
                                </div>
                                <div class="row m-t-20 text-center bold t15">OR</div>
                                <div class="row">
                                    <div ng-click="authenticate('facebook')" class="col-lg-6 col-md-12 m-tb-20">
                                        <div class="btn facebook-login-button row">
                                            <div class="col-md-8 login-with-text">
                                                Login with
                                            </div>
                                            <div class="col-md-4 no-pad-lr">
                                                <img class="exclude" src="/images/login/icons/facebook login.png" alt="Facebook login">
                                            </div>
                                        </div>
                                    </div>
                                    <div ng-click="authenticate('google')" class="col-lg-6 col-md-12 m-tb-20">
                                        <div class="btn google-plus-login-button row">
                                            <div class="col-md-8 login-with-text">
                                                Login with
                                            </div>
                                            <div class="col-md-4 no-pad-lr">
                                                <img class="exclude" src="/images/login/icons/google login.png" alt="Google login">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <fb:login-button scope="public_profile,email" show-faces="true" max-rows="1" size="large"></fb:login-button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel" style="font-family: 'leela';">Enter your email</h4>
                            </div>
                            <div class="modal-body">
                                <input type="email" data-ng-model="femail" required>
                            </div>
                            <div class="modal-footer">
                                <a data-ng-click="forgotPassword()" class="btn btn-vivo vivo-text-hover pointer"> Submit</a>
                                <a class="btn btn-default btn-sx" data-dismiss="modal" style="font-family: 'leela';">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoLogin.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>