<!DOCTYPE html>
<html lang="en" data-ng-app="vivoAddress">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta name="keywords" content="" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/address" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="My Address | VivoCarat.com" />
    <meta property="og:description" content="VivoCarat is an Online Jewellery store for gold & diamond rings, Earrings, Pendants with latest designs from trusted brands at lowest prices." />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>My Address | VivoCarat.com</title>
    <meta name="keywords" content="address,account,jewellery, online jewellery, jewelry, engagement rings, wedding rings, diamond rings, rings, diamond earrings, gold earrings" />
    <meta name="description" content="VivoCarat is an Online Jewellery store for gold & diamond rings, Earrings, Pendants with latest designs from trusted brands at lowest prices." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/address">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/address">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/address" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/jquery-ui.css">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='addressCtrl'>
        <div class="container theme-text">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li><a href="/account" target="_self" class="theme-text-special">MY ACCOUNT</a></li>
                            <li>MY ADDRESS</li>
                        </ul>
                    </div>
                </div>
            </section>
            <div data-ng-hide="!authenticated || isLoaded">
                <div class="well w-75pc pad-10 m-t-20 block-center text-center home-panel-row">
                    <div class="row well">Please wait...</div>
                </div>
            </div>
            <div data-ng-show="!authenticated" class="well w-75pc m-t-20 pad-10 block-center  register_account">
                <div class="not-loggedin-pad">
                    <h4 class="normal-text">
                        Not Logged In
                    </h4>
                    <p class="normal-text margin-bottom-20px">
                        Kindly <a class="link-hover" href="/login" target="_self">login</a> to see your account.
                    </p>
                </div>
            </div>
            <section class="exclude m-t-0 m-b-30" data-ng-show="isauthenticated || isLoaded">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="account-nav">
                            <li>
                                <a class="inactive-tab-text-colour" href="/wishlist" target="_self">Wishlist</a>
                            </li>
                            <li>
                                <a class="inactive-tab-text-colour" href="/orders" target="_self">Order history</a>
                            </li>
                            <li>
                                <a class="inactive-tab-text-colour" href="/account" target="_self">Account details</a>
                            </li>
                            <li class="current">
                                <a href="/address" target="_self">Address</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9 no-pad-lr" ng-show="isVisible">
                        <div class="row m-b-30">
                            <div class="col-md-6">
                                <button class="btn-vivo-bold w-80pc pad-15 t18" id="addaddress">Add new address</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 no-pad-lr" ng-show="!isVisible">
                        <div class="row m-b-30">
                            <div class="col-md-3 pad-tb-5">
                                <b>Name</b>
                                <span class="pull-right">:</span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="uname" ng-model="uname">
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-md-3 pad-tb-5">
                                <b>Phone</b>
                                <span class="pull-right">:</span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="uphone" ng-model="uphone">
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-md-3 pad-tb-5">
                                <b>Address</b>
                                <span class="pull-right">:</span>
                            </div>
                            <div class="col-md-6">
                                <textarea ame="uaddress" ng-model="uaddress"></textarea>
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-md-3 pad-tb-5">
                                <b>Pincode</b>
                                <span class="pull-right">:</span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="upincode" ng-model="upincode">
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-md-3 pad-tb-5">
                                <b>City</b>
                                <span class="pull-right">:</span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="ucity" ng-model="ucity">
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-md-3 pad-tb-5">
                                <b>State</b>
                                <span class="pull-right">:</span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="ustate" ng-model="ustate">
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-md-3 pad-tb-5">
                                <b>Country</b>
                                <span class="pull-right">:</span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="ucountry" ng-model="ucountry">
                            </div>
                        </div>
                        <div class="row m-b-30">
                            <div class="col-md-3 pad-tb-5">
                            </div>
                            <div class="col-md-6">
                                <button class="btn-vivo" id="saveaddress">SAVE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoAddress.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->
</body>
</html>