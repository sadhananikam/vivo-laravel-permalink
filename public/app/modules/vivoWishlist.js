var wishlistapp = angular.module("vivoWishlist", ['vivoCommon']);

wishlistapp.controller('wishlistCtrl',['$scope','$rootScope','$http','$timeout','API_URL', function ($scope, $rootScope, $http, $timeout,API_URL) {
           
    $scope.removeFromWatchlist = function (p) {

        if (!$rootScope.authenticated) {

            //Please login
            $("#login").modal('show');

            $timeout(function () {
                $("#login").modal('hide');
            }, 2000);

        } else {

            $http({
                method: 'GET',
                url : API_URL + 'removeFromWatchlist',
                params : {uid : $rootScope.uid,
                          pid:p}
            }).then(function successCallback(response){
                //Product removed from watchlist
                $("#removewatch").modal('show');

                $timeout(function () {
                    $("#removewatch").modal('hide');
                }, 2000);

                $scope.getWatchlist();
            },function errorCallback(response){
                console.log(response.data);
                //Oops! We are facing some error removing products from your watchlist. Just give us some time and we will be up and running.
                $("#removewatcherr").modal('show');

                $timeout(function () {
                    $("#removewatcherr").modal('hide');
                }, 2000);
            });
        }

    }
    $scope.isLoaded = false;

    $scope.getWatchlist = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getWatchlist',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            $scope.watchlist = response.data;
            $scope.isLoaded = true;
        },function errorCallback(response){
            console.log(response.data);
            //Oops! We are facing some error loading products from your watchlist. Just give us some time and we will be up and running.
            $("#loadwatcherr").modal('show');

            $timeout(function () {
                $("#loadwatcherr").modal('hide');
            }, 2000);
        });
        
    }
    
    $scope.addToCartFromWishlist = function (p) {
            if (p.category == 'Bracelets') {
                if (p.bracelet_size == null) {

                    //Please select a braclet size
                    $("#BraceletSize").modal('show');

                    $timeout(function () {
                        $("#BraceletSize").modal('hide');
                    }, 2000);
                } else {

                    $rootScope.addCartConfirmed(p);
                    $scope.removeFromWatchlist(p.id);
                }

            } else if (p.category == 'Rings') {
                if (p.ring_size == null) {

                    //Please select a ring size
                    $("#RingSize").modal('show');

                    $timeout(function () {
                        $("#RingSize").modal('hide');
                    }, 2000);
                } else {

                    $rootScope.addCartConfirmed(p);
                    $scope.removeFromWatchlist(p.id);
                }

            } else if (p.category == 'Bangles') {
                if (p.bangle_size == null) {

                    //Please select a bangle size
                    $("#BangleSize").modal('show');

                    $timeout(function () {
                        $("#BangleSize").modal('hide');
                    }, 2000);
                } else {

                    $rootScope.addCartConfirmed(p);
                    $scope.removeFromWatchlist(p.id);
                }

            } else {

                $rootScope.addCartConfirmed(p);
                $scope.removeFromWatchlist(p.id);
            }
        }

    $rootScope.$on('loadWatchlistEvent', function () {
        $scope.getWatchlist();
    });

}]);