var loginapp = angular.module("vivoLogin", ['vivoCommon']);
loginapp.controller('loginCtrl',['$scope','$rootScope','$location','$http','Data','$window','sAuth','GooglePlus','$timeout','$auth','API_URL', function ($scope, $rootScope, $location, $http, Data,  $window, sAuth, GooglePlus, $timeout, $auth, API_URL) {
    
    $scope.customer_new = {
        name: null,
        email: null,
        phone: null,
        password: null
    }
    
    $scope.customer = {
        email: null,
        password: null
    }

    $('#modlgn_passwd').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $scope.loginUser($scope.customer);
        }
    });
    $scope.loginGP = function () {
        GooglePlus.login().then(function (authResult) {
            console.log(authResult);

            GooglePlus.getUser().then(function (user) {
                $rootScope.doLogin(c);
            });
        }, function (err) {
            console.log(err);
        });
    };
    
    $scope.loginUser = function (c) {
        
        if ($scope.customer.email == null || $scope.customer.email == "" || $scope.customer.password == null || $scope.customer.password == "") {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } else {        
            $rootScope.doLogin(c);
        }
    }
    
    $scope.authenticate = function (provider) {
        $auth.authenticate(provider).then(function (data) {

                if (provider == 'facebook') {
                    $http.get('https://graph.facebook.com/v2.5/me?fields=id,email,first_name,last_name,link,name&access_token=' + data.access_token).success(function (data) {
                        
						waitingDialog.show();
						$http({
							method: 'POST',
							url : API_URL + 'oauth_login',
							params: {
								customer: data,
								items: JSON.stringify($rootScope.cart),
                                login_url: window.location.href
							}
						}).then(function successCallback(response){							
							var results = response.data;
							$rootScope.response = results.message;
							$('#loggedIn').modal('show');
							$timeout(function () {
								$('#loggedIn').modal('hide');

							}, 2000);
							$rootScope.from = getUrlParameter('from');
							if (results.status == "success") {
								if(results.items != null)
								{
									var pr = JSON.parse(results.items);

									var k = 1;
									for (var i = 0; i < pr.length; i++) {
										var p_id = pr[i].product_id;
										var ring_size = pr[i].ring_size;
										var bangle_size = pr[i].bangle_size;
										var bracelet_size = pr[i].bracelet_size;
										var quantity = pr[i].quantity;   

										$http({
											method: 'GET',
											url : API_URL + 'getProductDetail',
											params : {id:p_id}
										}).then(function successCallback(response){
											var p = response.data[0];
											p.ring_size = pr[k-1].ring_size;
											p.bangle_size = pr[k-1].bangle_size;
											p.bracelet_size = pr[k-1].bracelet_size;
											p.quantity = pr[k-1].quantity;

											$rootScope.showmodal = false;
											$rootScope.addToCart(p);
											$rootScope.showmodal = true;

											if(k == pr.length){
												$rootScope.quantity = null;
												if ($rootScope.from == 'c') {
													window.location.href = "/checkout";

												} else {
													window.location.href = "/";

												}
												location.reload(true);
											}
											k = k+1;

										},function errorCallback(response){
											console.log(response.data);
										});

									}
								}
								else
								{
									 if ($rootScope.from == 'c') {
										window.location.href = "/checkout";

									} else {
										window.location.href = "/";

									}
									location.reload(true);
								}

							}	
							else
							{
								window.location.href = "/";
								location.reload(true);
							}
						},function errorCallback(response){
							console.log(response.data);
							waitingDialog.hide();
						});

                    }).error(function (data) {


                    });
                }


                if (provider == 'google') {
					waitingDialog.show();
					$http({
						method: 'POST',
						url : API_URL + 'oauth_login_googleplus',
						params: {
							customer: data.config.data,
							items: JSON.stringify($rootScope.cart),
                            login_url: window.location.href
						}
					}).then(function successCallback(response){
						
						var results = response.data;
						$rootScope.response = results.message;
						$('#loggedIn').modal('show');
						$timeout(function () {
							$('#loggedIn').modal('hide');

						}, 2000);
						$rootScope.from = getUrlParameter('from');
						if (results.status == "success") {
							if(results.items != null)
							{
								var pr = JSON.parse(results.items);

								var k = 1;
								for (var i = 0; i < pr.length; i++) {
									var p_id = pr[i].product_id;
									var ring_size = pr[i].ring_size;
									var bangle_size = pr[i].bangle_size;
									var bracelet_size = pr[i].bracelet_size;
									var quantity = pr[i].quantity;       

									$http({
										method: 'GET',
										url : API_URL + 'getProductDetail',
										params : {id:p_id}
									}).then(function successCallback(response){
										var p = response.data[0];
										p.ring_size = pr[k-1].ring_size;
										p.bangle_size = pr[k-1].bangle_size;
										p.bracelet_size = pr[k-1].bracelet_size;
										p.quantity = pr[k-1].quantity;

										$rootScope.showmodal = false;
										$rootScope.addToCart(p);
										$rootScope.showmodal = true;

										if(k == pr.length){
											$rootScope.quantity = null;
											if ($rootScope.from == 'c') {
												window.location.href = "/checkout";

											} else {
												window.location.href = "/";

											}
											location.reload(true);
										}
										k = k+1;

									},function errorCallback(response){
										console.log(response.data);
									});

								}
							}
							else
							{
								 if ($rootScope.from == 'c') {
									window.location.href = "/checkout";

								} else {
									window.location.href = "/";

								}
								location.reload(true);
							}

						}	
						else
						{
							window.location.href = "/";
							location.reload(true);
						}
						
					},function errorCallback(response){
						console.log(response.data);
						waitingDialog.hide();
					});

                }

            })
            .catch(function (error) {
                if (error.error) {
                    // Popup error - invalid redirect_uri, pressed cancel button, etc.
                    console.log(error.error);
                } else if (error.data) {
                    // HTTP response error from server
                    console.log(error.data.message, error.status);
                } else {
                    console.log(error);
                }
            });
        
    };

    $scope.from = getUrlParameter('from');
    $scope.isPasswordError = false;
    $scope.$watch('customer_new.password', function () {
        if ($scope.customer_new.password !== undefined && $scope.customer_new.password !== null && $scope.customer_new.password.length < 8) {
            $scope.message = "Password must be atleast 8 characters";
            $scope.isPasswordError = true;
        } else {
            $scope.isPasswordError = false;
            $scope.message = "";
        }
    }, true);
    $scope.signup = function () {
        if ($scope.customer_new.name == null || $scope.customer_new.name == null || $scope.customer_new.phone == null || $scope.customer_new.phone == null || $scope.customer_new.email == null || $scope.customer_new.email == "" || $scope.customer_new.password == null || $scope.customer_new.password == "") {
            //Please verify the details entered.
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } else {

            $rootScope.signUp($scope.customer_new);
            if ($scope.from == 'c') {
                window.location.href = "/checkout";
            }
        }

    }
    
    $scope.forgotPassword = function () {
        
        if ($scope.femail == null || $scope.femail == "") {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } 
        else 
        {
            waitingDialog.show();
            
            var url = API_URL + 'forgotpassword';
            $http({
                method: 'POST',
                url: url, 
                params: {
                        email: $scope.femail   
                    }
            })
            .then(function successCallback(response){
                waitingDialog.hide();
                
                if (response.data.trim() == "ND") {
                    //Oops we do not have this email id registered with us.
                    $("#noemail").modal('show');

                    $timeout(function () {
                        $("#noemail").modal('hide');
                    }, 2000);

                } else {
                    $("#myModal").modal('hide');
                    //An email has been sent to you to reset the password.
                    $("#pwdreset").modal('show');

                    $timeout(function () {
                        $("#pwdreset").modal('hide');
                    }, 2000);

                }            
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
           });

        }
    }
    
}]);