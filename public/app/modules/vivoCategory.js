var categoryapp=angular.module("vivoCategory",['vivoCommon']);categoryapp.filter('unsafe',function($sce){return function(val){return $sce.trustAsHtml(val)}});categoryapp.controller('categoryCtrl',['$scope','$rootScope','$location','$http','Data','$window','API_URL','$compile',function($scope,$rootScope,$location,$http,Data,$window,API_URL,$compile){var params=window.location.pathname.split('/').slice(1);var urlslug=params[1];urlslug=urlslug.replace('.html','');var params1=urlslug.split('-');$scope.allN=0;$scope.filterN=0;$scope.sortN=0;$scope.category=params1[0];$scope.subtype=params1[1];$scope.getProductsCount=function(){$http({method:'GET',url:API_URL+'getProductsCount',params:{c:$scope.category,type:$scope.subtype}}).then(function successCallback(response){$scope.count=response.data[0].count;$scope.allN=Math.floor($scope.count/8)+1;$scope.allarr=Array.apply(null,{length:$scope.allN}).map(Number.call,Number);$scope.disabled=!1;$scope.loadMore()},function errorCallback(response){console.log(response.data)})}
$scope.filterPriceCount=function(){$http({method:'GET',url:API_URL+'filterPriceCount',params:{c:$scope.category,isGold:$scope.isGold,isWhiteGold:$scope.isWhiteGold,isPlatinum:$scope.isPlatinum,isRoseGold:$scope.isRoseGold,isSilver:$scope.isSilver,is22:$scope.is22,is18:$scope.is18,is14:$scope.is14,isOther:$scope.isOther,isCasual:$scope.isCasual,isBridal:$scope.isBridal,isFashion:$scope.isFashion,jewellers:JSON.stringify($scope.jewellers),type:$scope.subtype,s:$scope.demo1.min,e:$scope.demo1.max}}).then(function successCallback(response){$scope.count=response.data[0].count;$scope.filterN=Math.floor($scope.count/8)+1;$scope.filterarr=Array.apply(null,{length:$scope.filterN}).map(Number.call,Number);$scope.disabled=!1;$scope.loadMore()},function errorCallback(response){console.log(response.data)})}
$scope.sortListCount=function(){$http({method:'GET',url:API_URL+'sortListCount',params:{p:$scope.sortType,c:$scope.category,isGold:$scope.isGold,isWhiteGold:$scope.isWhiteGold,isPlatinum:$scope.isPlatinum,isRoseGold:$scope.isRoseGold,isSilver:$scope.isSilver,is22:$scope.is22,is18:$scope.is18,is14:$scope.is14,isOther:$scope.isOther,isCasual:$scope.isCasual,isBridal:$scope.isBridal,isFashion:$scope.isFashion,jewellers:JSON.stringify($scope.jewellers),type:$scope.subtype,s:$scope.demo1.min,e:$scope.demo1.max}}).then(function successCallback(response){$scope.count=response.data[0].count;$scope.sortN=Math.floor($scope.count/8)+1;$scope.sortarr=Array.apply(null,{length:$scope.sortN}).map(Number.call,Number);$scope.disabled=!1;$scope.loadMore()},function errorCallback(response){console.log(response.data)})}
$scope.share=function(p){$('.share-icon').socialShare({social:'facebook,google,twitter',shareUrl:'www.vivocarat.com/%23/p/'+p.id})}
$scope.demo1={min:20,max:500000};$scope.getAllJewellers=function(){$http({method:'GET',url:API_URL+'getAllJewellers'}).then(function successCallback(response){var result=response.data;var jsonObj=[];for(var k=0;k<result.length;k++){var j_id=result[k].id;var j_name=result[k].name;var j_dispname=j_name;if(j_name==='Lagu Bandhu')
{j_dispname='Lagu Bandhu Jewellers'}
if(j_name==='PP-Gold')
{j_dispname='PP Gold'}
if(j_name==='Arkina-Diamonds')
{j_dispname='Arkina Diamonds'}
if(j_name==='ZKD-Jewels')
{j_dispname='ZKD Jewels'}
if(j_name==='Myrah-Silver-Works')
{j_dispname='Myrah Silver Works'}
if(j_name==='Charu-Jewels')
{j_dispname='Charu Jewels'}
var item={}
item["jname"]=j_dispname;item["jvalue"]=j_name;jsonObj.push(item)}
$scope.selectOptions={placeholder:"Select Jewellers..",dataTextField:"jname",dataValueField:"jvalue",valuePrimitive:!0,autoBind:!1,dataSource:{type:"jsonp",serverFiltering:!0,data:jsonObj}}},function errorCallback(response){console.log(response.data)})}
$scope.getAllJewellers();$scope.resetFilter=function(){$('#singleSelect option[value=""]').attr('selected','selected');$scope.selectedIds=[];$scope.isFirst=1;$scope.jewellers=[];$scope.isGold=!1;$scope.isWhiteGold=!1;$scope.isPlatinum=!1;$scope.isRoseGold=!1;$scope.isSilver=!1;$scope.is22=!1;$scope.is18=!1;$scope.is14=!1;$scope.isOther=!1;$scope.isCasual=!1;$scope.isBridal=!1;$scope.isFashion=!1;$scope.demo1={min:20,max:500000};$scope.lastpage=0;$scope.resultList=[];$scope.getProductsCount()}
$scope.resetFilter();$scope.openFilter=function(){$("#filterArea").slideDown("slow",function(){})}
$scope.closeFilter=function(){$("#filterArea").slideUp('fast')}
$scope.$watch('selectedFilter',function(){if($scope.selectedFilter!=null||$scope.selectedFilter==""){if($scope.selectedFilter=="Price H to L"){$scope.sortType="price_after_discount desc"}else if($scope.selectedFilter=="Price L to H"){$scope.sortType="price_after_discount asc"}else if($scope.selectedFilter=="Name"){$scope.sortType="title desc"}else if($scope.selectedFilter==""){$scope.sortType="id desc"}
$scope.isFirst=3;$scope.lastpage=0;$scope.resultList=[];$scope.sortListCount()}},!0);$scope.banImg=$scope.category+"_"+$scope.subtype;$scope.list=[];$scope.isLoaded=!1;if($scope.category.match("less_")){$scope.resultfor="All"}
else if($scope.category.match("greater_")){$scope.resultfor="All"}
else if($scope.category.match("purity_")){$scope.resultfor="All"}
else if($scope.category.match("wt_")){$scope.resultfor="All"}
else if($scope.category.match("weight_lt_")){$scope.resultfor="All"}
else if($scope.category.match("weight_gt_")){$scope.resultfor="All"}
else if($scope.category.match("gender_")){$scope.resultfor="All"}
else{$scope.resultfor=$scope.category}
var el=angular.element(document.querySelector('#loadMore'));el.attr('tagged-infinite-scroll','loadMore()');$compile(el)($scope);$scope.lastpage=0;$scope.resultList=[];$scope.isFirst=1;$scope.defaulttag={page_title:"VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store",meta_keywords:"online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery",meta_robots:"index,follow",og_title:"Vivocarat",og_type:"website"};$scope.getcategorydescription=function(){$http({method:'GET',url:API_URL+'getcategorydescription',params:{parent_category:$scope.category,category:$scope.subtype}}).then(function successCallback(response){if(response.data.page_title!==undefined){$scope.catdesc=response.data}
else{$scope.catdesc=$scope.defaulttag}},function errorCallback(response){console.log(response.data)})}
$scope.getcategorydescription();$scope.loadMore=function(){$scope.fetching=!0;if($scope.isFirst==1&&$scope.disabled==!1){if($.inArray($scope.lastpage,$scope.allarr)>-1&&typeof $scope.allarr!=='undefined'&&$scope.allarr.length>0)
{var pos=$.inArray($scope.lastpage,$scope.allarr);$scope.allarr.splice(pos,1);$http({method:'GET',url:API_URL+'getProducts',params:{c:$scope.category,type:$scope.subtype,page:$scope.lastpage}}).then(function successCallback(response){$scope.fetching=!1;if(response.data.length){$scope.resultList=$scope.resultList.concat(response.data);$scope.lastpage+=1;$scope.isLoaded=!0}else{$scope.disabled=!0;$scope.isLoaded=!0}},function errorCallback(response){console.log(response.data)})}}else if($scope.isFirst==2&&$scope.disabled==!1){if($.inArray($scope.lastpage,$scope.filterarr)>-1&&typeof $scope.filterarr!=='undefined'&&$scope.filterarr.length>0)
{var pos=$.inArray($scope.lastpage,$scope.filterarr);$scope.filterarr.splice(pos,1);$http({method:'GET',url:API_URL+'filterPrice',params:{page:$scope.lastpage,c:$scope.category,isGold:$scope.isGold,isWhiteGold:$scope.isWhiteGold,isPlatinum:$scope.isPlatinum,isRoseGold:$scope.isRoseGold,isSilver:$scope.isSilver,is22:$scope.is22,is18:$scope.is18,is14:$scope.is14,isOther:$scope.isOther,isCasual:$scope.isCasual,isBridal:$scope.isBridal,isFashion:$scope.isFashion,jewellers:JSON.stringify($scope.jewellers),type:$scope.subtype,s:$scope.demo1.min,e:$scope.demo1.max}}).then(function successCallback(response){$scope.fetching=!1;if(response.data.length){$scope.resultList=$scope.resultList.concat(response.data);$scope.lastpage+=1;$scope.isLoaded=!0}else{$scope.disabled=!0}},function errorCallback(response){console.log(response.data)})}}else if($scope.isFirst==3&&$scope.disabled==!1){if($.inArray($scope.lastpage,$scope.sortarr)>-1&&typeof $scope.sortarr!=='undefined'&&$scope.sortarr.length>0)
{var pos=$.inArray($scope.lastpage,$scope.sortarr);$scope.sortarr.splice(pos,1);$http({method:'GET',url:API_URL+'sortList',params:{p:$scope.sortType,page:$scope.lastpage,c:$scope.category,isGold:$scope.isGold,isWhiteGold:$scope.isWhiteGold,isPlatinum:$scope.isPlatinum,isRoseGold:$scope.isRoseGold,isSilver:$scope.isSilver,is22:$scope.is22,is18:$scope.is18,is14:$scope.is14,isOther:$scope.isOther,isCasual:$scope.isCasual,isBridal:$scope.isBridal,isFashion:$scope.isFashion,jewellers:JSON.stringify($scope.jewellers),type:$scope.subtype,s:$scope.demo1.min,e:$scope.demo1.max}}).then(function successCallback(response){$scope.fetching=!1;if(response.data.length){$scope.resultList=$scope.resultList.concat(response.data);$scope.lastpage+=1;$scope.isLoaded=!0}else{$scope.disabled=!0}},function errorCallback(response){console.log(response.data)})}}};$scope.filter=function(){$scope.isFirst=2;$scope.lastpage=0;$scope.resultList=[];$scope.filterPriceCount()}
$scope.addProductReview = function (title,review) {
        //alert($scope.reviewscore);
        if ($rootScope.authenticated) {
            $scope.review.pid = $scope.productId;
            $scope.review.uid = $rootScope.uid;
            $scope.review.name = $rootScope.name;
            $scope.review.score = $scope.reviewscore;
            $scope.review.title = title;
            $scope.review.review = review;

            if ($scope.review.score <= 0) {

                $("#reviewError").modal('show');

                    $timeout(function () {
                        $("#reviewError").modal('hide');
                    }, 2000);
            }
            else
            { 
                if ($scope.review.title == null || $scope.review.title == ""){
                    $scope.review.title = null;
                }
                
                if($scope.review.review == null || $scope.review.review == ""){
                    $scope.review.review = null;
                }
                    
                $http({
                    method: 'POST',
                    url : API_URL + 'addProductReview',
                    params : {
                    review:JSON.stringify($scope.review)
                        }
                }).then(function successCallback(response){
                    $scope.getProductReviewList();
                    $scope.getJewellerRating();
                    //Review added successfully
                   
                   $scope.reviewscore = 0;
                   $scope.reviewtitle = '';
                   $scope.reviewmsg = '';
                    $("#myCarousel").carousel("prev");
                    $("#myCarouselT").carousel("prev");
                    $("#reviewSuccess").modal('show');

                    $timeout(function () {
                        $("#reviewSuccess").modal('hide');
                    }, 2000);
                },function errorCallback(response){
                    console.log(response.data);
                    //Review error
                    $("#reviewError").modal('show');

                    $timeout(function () {
                        $("#reviewError").modal('hide');
                    }, 2000);
                });
                
            }

        } else {
            //Please login to add review
            $("#login").modal('show');

            $timeout(function () {
                $("#login").modal('hide');
            }, 2000);
        }
    }
}])