var resetpassapp = angular.module("vivoResetpassword", ['vivoCommon']);

resetpassapp.controller('resetPasswordCtrl',['$scope','$rootScope', '$controller','$location','$http','Data','$window', '$timeout','API_URL', function ($scope, $rootScope, $controller, $location, $http, Data, $window, $timeout, API_URL) {
      
    $scope.validToken = true;
    
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[0];
    urlslug = urlslug.replace('.html','');
    $scope.token = urlslug.split('-').slice(1);
    
    $http({
        method: 'GET',
        url : API_URL + 'checkUrl',
        params : {t:$scope.token}
    }).then(function successCallback(response){
        if (response.data.trim() == "invalid") {
            //The url is invalid.
            $("#urlinvalid").modal('show');

            $timeout(function () {
                $("#urlinvalid").modal('hide');
            }, 2000);
            
            window.location.href = "/";

        } else {
            var result = response.data.trim().split('-');
            $scope.uid = result[0];
            $scope.name = result[1];
        }

    },function errorCallback(response){
        console.log(response.data);
    });
    

    $scope.resetPassword = function (pass) {
        waitingDialog.show();
        var url = API_URL + 'resetPassword';
        $http({
            method: 'POST',
            url: url, 
            params:{
                    uid: $scope.uid,
                    password2: $scope.signup.password2
                },
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){
            waitingDialog.hide();
            
            //Password changed successfully. Please Login with your new password.
            $("#pwdchanged").modal('show');

            $timeout(function () {
                $("#pwdchanged").modal('hide');
            }, 2000);

            window.location.href = "/login"
            
        },function errorCallback(response){
            console.log(response.data);
            waitingDialog.hide();
        });
    }
    
}]);