var storeapp = angular.module("vivoStore", ['vivoCommon']);

storeapp.controller('storeCtrl',['$scope','$rootScope','$location','$http','Data','$window','API_URL', function ($scope, $rootScope, $location, $http, Data, $window,API_URL) {
    $scope.isLoaded = false;
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[1];
    $scope.store = decodeURIComponent(urlslug);
    
    $http({
        method: 'GET',
        url : API_URL + 'getStoreCategories',
        params : {store:$scope.store}
    }).then(function successCallback(response){
        $scope.categoriesList = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $http({
        method: 'GET',
        url : API_URL + 'getJewellerInformationByName',
        params : {store:$scope.store}
    }).then(function successCallback(response){
        $scope.isLoaded = true;
        $scope.jeweller = response.data[0];
    },function errorCallback(response){
        console.log(response.data);
    });
    
}]);