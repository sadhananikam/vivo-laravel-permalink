var accountapp = angular.module("vivoAccount", ['vivoCommon']);
accountapp.controller('accountCtrl',['$scope','$rootScope','$http','$timeout','API_URL', function ($scope, $rootScope, $http, $timeout, API_URL) {
    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else 
        {
            return false;
        }
    }
    function validatePhone(txtPhone) {
        var a = txtPhone;
        var filter = /^[0-9-+]+$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }
    function validateName(txtName) {
        var a = txtName;
        var filter = /^[a-zA-Z\s]+$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }
    $scope.showchangepass = true;
    $scope.toggleChangePassword = function() {
        $scope.showchangepass = $scope.showchangepass === false ? true: false;
    };
    $scope.uname = '';
    $scope.uemail = '';
    $scope.uphone = '';
    $scope.udob = '';
    $scope.uanniversary = '';
    $scope.uoldpassword = '';
    $scope.unewpassword = '';
    $scope.unewrenterpassword = '';
    $scope.showuname = false;
    $scope.showuemail = false;
    $scope.showuphone = false;
    $scope.showudob = false;
    $scope.showuanniversary = false;
    $scope.saveuname = false;
    $scope.saveuemail = false;
    $scope.saveuphone = false;
    $scope.saveudob = false;
    $scope.saveuanniversary = false;
    $scope.showname = true;
    $scope.showemail = true;
    $scope.showphone = true;
    $scope.showdob = true;
    $scope.showanniversary = true;
    $scope.edituname = true;
    $scope.edituemail = true;
    $scope.edituphone = true;
    $scope.editudob = true;
    $scope.edituanniversary = true;
    $("#uname").click(function(){
        $timeout(function () {
            $scope.edituname = false;
            $scope.showname = false;
            $scope.showuname = true;
            $scope.saveuname = true;
        }, 1);
    });
    $("#saveuname").click(function(){
        $timeout(function () {
            if($scope.uname == '')
            {
                $rootScope.responseCUMsg = "Your name is empty";
                $('#contactusMsg').modal('show');
                $timeout(function () {
                    $('#contactusMsg').modal('hide');
                }, 2000);
            }
            else
            {
                if (!validateName($scope.uname)) 
                {
                    $rootScope.responseCUMsg = "Enter correct name";
                    $('#contactusMsg').modal('show');
                    $timeout(function () {
                        $('#contactusMsg').modal('hide');
                    }, 2000);
                }
                else
                {
                    waitingDialog.show();
                    var url = API_URL + 'updateAccountName';
                    $http({
                        method: 'POST',
                        url: url, 
                        params: {
                                uid:$rootScope.uid,
                                name: $scope.uname   
                            }
                    })
                    .then(function successCallback(response){
                        waitingDialog.hide();
                        $scope.showname = true;
                        $scope.showuname = false;
                        $scope.edituname = true;
                        $scope.saveuname = false;            
                    },function errorCallback(response){
                        console.log(response.data);
                        waitingDialog.hide();
                    });
                }
            }
        }, 1);
    });
    $("#uemail").click(function(){
        $timeout(function () {
            $scope.showemail = false;
            $scope.showuemail = true;
            $scope.edituemail = false;
            $scope.saveuemail = true;
        }, 1);
    });
    $("#saveuemail").click(function(){
        $timeout(function () {
            if($scope.uemail == '')
            {
                $rootScope.responseCUMsg = "Your email is empty";
                $('#contactusMsg').modal('show');
                $timeout(function () {
                    $('#contactusMsg').modal('hide');
                }, 2000);
            }
            else
            {
                if (!validateEmail($scope.uemail)) 
                {
                    $rootScope.responseCUMsg = "Enter correct email";
                    $('#contactusMsg').modal('show');
                    $timeout(function () {
                        $('#contactusMsg').modal('hide');
                    }, 2000);
                }
                else
                {
                    waitingDialog.show();
                    var url = API_URL + 'updateAccountEmail';
                    $http({
                        method: 'POST',
                        url: url, 
                        params: {
                                uid:$rootScope.uid,
                                email: $scope.uemail   
                            }
                    })
                    .then(function successCallback(response){
                        waitingDialog.hide();
                        $scope.showemail = true;
                        $scope.showuemail = false;
                        $scope.edituemail = true;
                        $scope.saveuemail = false;           
                    },function errorCallback(response){
                        console.log(response.data);
                        waitingDialog.hide();
                    });  
                }
            }
        }, 1);
    });
    $("#uphone").click(function(){
        $timeout(function () {
            $scope.showphone = false;
            $scope.showuphone = true;
            $scope.edituphone = false;
            $scope.saveuphone = true;
        }, 1);
    });    
    $("#saveuphone").click(function(){
        $timeout(function () {
            if($scope.uphone == '')
            {
                $rootScope.responseCUMsg = "Your phone number is empty";
                $('#contactusMsg').modal('show');
                $timeout(function () {
                    $('#contactusMsg').modal('hide');
                }, 2000);
            }
            else
            {
                if (!validatePhone($scope.uphone)) 
                {
                    $rootScope.responseCUMsg = "Enter correct phone number";
                    $('#contactusMsg').modal('show');
                    $timeout(function () {
                        $('#contactusMsg').modal('hide');
                    }, 2000);
                }
                else
                {
                    waitingDialog.show();            
                    var url = API_URL + 'updateAccountPhone';
                    $http({
                        method: 'POST',
                        url: url, 
                        params: {
                                uid:$rootScope.uid,
                                phone: $scope.uphone   
                            }
                    })
                    .then(function successCallback(response){
                        waitingDialog.hide();
                        $scope.showphone = true;
                        $scope.showuphone = false;
                        $scope.edituphone = true;
                        $scope.saveuphone = false;          
                    },function errorCallback(response){
                        console.log(response.data);
                        waitingDialog.hide();
                    }); 
                }
            }
        }, 1);
    });  
    $("#udob").click(function(){
        $timeout(function () {
            $scope.showdob = false;
            $scope.showudob = true;
            $scope.editudob = false;
            $scope.saveudob = true;
        }, 1);
    }); 
    $("#saveudob").click(function(){
        $timeout(function () {
            var currentdate = $.datepicker.formatDate('yy-mm-dd', new Date());
            
            if($scope.udob == '' || $scope.udob == null)
            {
                $rootScope.responseCUMsg = "Your birth date is empty";
                $('#contactusMsg').modal('show');
                $timeout(function () {
                    $('#contactusMsg').modal('hide');
                }, 2000);
            }
            else
            {
                if( (new Date($scope.udob).getTime() > new Date(currentdate).getTime()))
                {
                    $("#dateerr").modal('show');
                    $timeout(function () {
                        $("#dateerr").modal('hide');
                    }, 2000);
                }
                else
                {
                    waitingDialog.show();            
                    var url = API_URL + 'updateAccountDOB';
                    $http({
                        method: 'POST',
                        url: url, 
                        params: {
                                uid:$rootScope.uid,
                                dob: $scope.udob   
                            }
                    })
                    .then(function successCallback(response){
                        waitingDialog.hide();
                        $scope.showdob = true;
                        $scope.showudob = false;
                        $scope.editudob = true;
                        $scope.saveudob = false;         
                    },function errorCallback(response){
                        console.log(response.data);
                        waitingDialog.hide();
                    }); 
                }
            }
        }, 1);
    });    
    $("#uanniversary").click(function(){
        $timeout(function () {
            $scope.showanniversary = false;
            $scope.showuanniversary = true;
            $scope.edituanniversary = false;
            $scope.saveuanniversary = true;
        }, 1);
    });  
    $("#saveuanniversary").click(function(){
        $timeout(function () {
            var currentdate = $.datepicker.formatDate('yy-mm-dd', new Date());
            console.log($scope.uanniversary);
            if($scope.uanniversary == '' || $scope.uanniversary == null)
            {
                $rootScope.responseCUMsg = "Your anniversary date is empty";
                $('#contactusMsg').modal('show');
                $timeout(function () {
                    $('#contactusMsg').modal('hide');
                }, 2000);
            }
            else
            {
                if( (new Date($scope.uanniversary).getTime() > new Date(currentdate).getTime()))
                {
                    $("#dateerr").modal('show');
                    $timeout(function () {
                        $("#dateerr").modal('hide');
                    }, 2000);
                }
                else
                {
                    waitingDialog.show();            
                    var url = API_URL + 'updateAccountAnniversary';
                    $http({
                        method: 'POST',
                        url: url, 
                        params: {
                                uid:$rootScope.uid,
                                anniversary: $scope.uanniversary   
                            }
                    })
                    .then(function successCallback(response){
                        waitingDialog.hide();
                        $scope.showanniversary = true;
                        $scope.showuanniversary = false;
                        $scope.edituanniversary = true;
                        $scope.saveuanniversary = false;         
                    },function errorCallback(response){
                        console.log(response.data);
                        waitingDialog.hide();
                    }); 
                }
            }
        }, 1);
    });   
    $("#savenewpassword").click(function(){
        if($scope.uoldpassword == '' || $scope.unewpassword == '' || $scope.unewrenterpassword == '')
        {
            $("#verifydetails").modal('show');
            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);
        }
        else
        {
            if($scope.unewpassword !== $scope.unewrenterpassword)
            {
                $("#passworderr").modal('show');
                $timeout(function () {
                    $("#passworderr").modal('hide');
                }, 2000);
            }
            else
            {
                waitingDialog.show();
                var url = API_URL + 'updatePassword';
                $http({
                    method: 'POST',
                    url: url, 
                    params: {
                            uid:$rootScope.uid,
                            oldpassword: $scope.uoldpassword,
                            newpassword: $scope.unewpassword
                        }
                })
                .then(function successCallback(response){
                    console.log(response.data);
                    waitingDialog.hide();
                    if(response.data == 'error')
                    {
                        $rootScope.responseCUMsg = "Old password is wrong";
                        $('#contactusMsg').modal('show');
                        $timeout(function () {
                            $('#contactusMsg').modal('hide');
                        }, 2000);
                    }
                    else if(response.data == 'success')
                    {
                        $scope.toggleChangePassword();
                        $("#pwdchanged").modal('show');
                        $timeout(function () {
                            $("#pwdchanged").modal('hide');
                        }, 2000);
                        $scope.uoldpassword = '';
                        $scope.unewpassword = '';
                        $scope.unewrenterpassword = '';
                    }
                },function errorCallback(response){
                    console.log(response.data);
                    waitingDialog.hide();
                }); 
            }
        }
    });
    $scope.getAccountDetails = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getAccountDetails',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            $scope.ad = response.data;
            $scope.uname = $scope.ad[0].name;
            $scope.uemail = $scope.ad[0].email;
            $scope.uphone = $scope.ad[0].phone;
            $scope.udob = $scope.ad[0].dob;
            $scope.uanniversary = $scope.ad[0].anniversary;
            $scope.isLoaded = true;
        },function errorCallback(response){
            console.log(response.data);
            $("#acfetcherr").modal('show');
            $timeout(function () {
                $("#acfetcherr").modal('hide');
            }, 2000);
        });
    }
    $rootScope.$on('loadAccountDetailsEvent', function () {
        $scope.getAccountDetails();
    });
}]);