var addressapp = angular.module("vivoAddress", ['vivoCommon']);

addressapp.controller('addressCtrl',['$scope','$rootScope','$http','$timeout','API_URL', function ($scope, $rootScope, $http, $timeout, API_URL) {
    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else 
        {
            return false;
        }
    }
    function validatePhone(txtPhone) {
        var a = txtPhone;
        var filter = /^[0-9-+]+$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }
    function validateName(txtName) {
        var a = txtName;
        var filter = /^[a-zA-Z\s]+$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }
    $scope.uname = '';
    $scope.uphone = '';
    $scope.uaddress = '';
    $scope.upincode = '';
    $scope.ucity = '';
    $scope.ustate = '';
    $scope.ucountry = '';
    $("#addaddress").click(function(){
        $timeout(function () {
            $scope.isVisible = false;
        }, 500);
    });
    $scope.$watch('upincode', function () {
        if ($scope.upincode && $scope.upincode.length > 5) {
			var city = '';
            var state = '';
            var country = '';
            waitingDialog.show();
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+$scope.upincode).success(function(response){
                if(response.status == 'OK')
                {      if(response.results[0].address_components.length)
                    {
                        var address_components = response.results[0].address_components;
                        $.each(address_components, function(index, component){
                          var types = component.types;  
                          $.each(types, function(index, type){
                            if(type == 'locality') {
                                city = component.long_name;
                                //$scope.address.city = component.long_name;
                                return true;
                            }
                            if(type == 'administrative_area_level_1') {
                                state = component.long_name;
                                //$scope.address.state = component.long_name;
                                return true;
                            }
                            if(type == 'country') {
                                country = component.long_name;
                                //$scope.address.country = component.long_name;   
                                return true;
                            }
                          });  
                        }); 
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ucity = city;
                                $scope.ustate = state;
                                $scope.ucountry = country;
                                waitingDialog.hide();
                            });
                        }, 2000);
                    }
                    else
                    {
                        waitingDialog.hide();
                    }
                }
                else
                {
                    $("#WrongPinCode").modal('show');

                    $timeout(function () {
                        $("#WrongPinCode").modal('hide');
                    }, 2000);
                    waitingDialog.hide();
                }
                
            });
        }
    }, true);
    $("#saveaddress").click(function(){
        $timeout(function () {
            if($scope.uname == '' || $scope.uphone == '' || $scope.uaddress == '' || $scope.upincode == '' || $scope.ucity == '' || $scope.ustate == '' || $scope.ucountry == '')
            {
                $("#verifydetails").modal('show');
                $timeout(function () {
                    $("#verifydetails").modal('hide');
                }, 2000);
            }
            else
            {
                if (!validateName($scope.uname)) 
                {
                    $rootScope.responseCUMsg = "Enter correct name";
                    $('#contactusMsg').modal('show');
                    $timeout(function () {
                        $('#contactusMsg').modal('hide');
                    }, 2000);
                }
                else if (!validatePhone($scope.uphone)) 
                {
                    $rootScope.responseCUMsg = "Enter correct phone number";
                    $('#contactusMsg').modal('show');
                    $timeout(function () {
                        $('#contactusMsg').modal('hide');
                    }, 2000);
                }
                else if (!validatePhone($scope.upincode) || $scope.upincode.length < 5) 
                {
                    $rootScope.responseCUMsg = "Enter correct pincode";
                    $('#contactusMsg').modal('show');
                    $timeout(function () {
                        $('#contactusMsg').modal('hide');
                    }, 2000);
                }
                else
                {
                    waitingDialog.show();
                    var url = API_URL + 'addAddressFromAccount';
                    $http({
                        method: 'POST',
                        url: url, 
                        params: {
                                uid:$rootScope.uid,
                                name: $scope.uname,
                                phone : $scope.uphone,
                                address : $scope.uaddress,
                                pincode : $scope.upincode,
                                city : $scope.ucity,
                                state : $scope.ustate,
                                country : $scope.ucountry
                            }
                    })
                    .then(function successCallback(response){
                        waitingDialog.hide();
                        if(response.data == 'success')
                        {
                            $rootScope.responseCUMsg = "Address is added successfully";
                            $('#contactusMsg').modal('show');
                            $timeout(function () {
                                $('#contactusMsg').modal('hide');
                            }, 2000);
                            
                            $rootScope.$on('loadAddressEvent', function () {
                                    $scope.getAddressDetails();
                            }); 
                        }
                        else
                        {
                            $rootScope.responseCUMsg = "Please try again";
                            $('#contactusMsg').modal('show');
                            $timeout(function () {
                                $('#contactusMsg').modal('hide');
                            }, 2000);
                        }
                    },function errorCallback(response){
                        console.log(response.data);
                        waitingDialog.hide();
                    });
                }
            }
        }, 500);
    });
    $scope.getAddressDetails = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getAddressDetails',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){  
            if(response.data != 'error')
            {
                $scope.isVisible = false;
                $scope.ad = response.data;
                $scope.uname = $scope.ad.name;
                $scope.uphone = $scope.ad.phone;
                $scope.uaddress = $scope.ad.address;
                $scope.upincode = $scope.ad.pincode;
                $scope.ucity = $scope.ad.city;
                $scope.ustate = $scope.ad.state;
                $scope.ucountry = $scope.ad.country;
                $scope.isLoaded = true;
            }
            else
            {
                $scope.isVisible = true;
                $scope.isLoaded = true;
            }
            
        },function errorCallback(response){
            console.log(response.data);
            $("#acfetcherr").modal('show');
            $timeout(function () {
                $("#acfetcherr").modal('hide');
            }, 2000);
        });
    }
    $rootScope.$on('loadAddressEvent', function () {
        $scope.getAddressDetails();
    });
}]);