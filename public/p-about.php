<!DOCTYPE html>
<html lang="en" data-ng-app="vivoAboutus">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/about-us" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="VivoCarat Advantage | About US | Affordable Jewellery" />
    <meta property="og:description" content="VivoCarat is an Online Jewellery store for gold & diamond rings, Earrings, Pendants with latest designs from trusted brands at lowest prices." />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>VivoCarat Advantage | About US | Affordable Jewellery</title>
    <meta name="keywords" content="jewellery, online jewellery, jewelry, engagement rings, wedding rings, diamond rings, rings, diamond earrings, gold earrings" />
    <meta name="description" content="VivoCarat is an Online Jewellery store for gold & diamond rings, Earrings, Pendants with latest designs from trusted brands at lowest prices." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/about-us">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/about-us">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/about-us" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body>
    <vivo-header></vivo-header>
    <div data-ng-controller='aboutCtrl' ng-cloak>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>ABOUT VIVOCARAT</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="exclude m-t-0 m-b-30">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>ABOUT VIVOCARAT</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            VivoCarat is a promising e-commerce platform that features authentic and remarkable jewellery products. We take pride in showcasing our vivid, spectacular and assorted diamond, gold and gemstone jewellery collection. Each piece is distinctive and flawless in itself since it has been passed under the eye of strict professionals resulting in unparalleled craftsmanship.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p>
                            Since its inception, Vivocarat has positively collaborated with pioneering jewellery brands in and around your city to avoid the tedious procedure of browsing through jewellery pieces from store to store. We ensure to bring out top notch products from your trusted neighbourhood jewellery brands that excel in quality and design.
                        </p>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>OUR STORY</span></h2>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <p>
                            Turning back the pages of our book, the story behind conceiving Vivocarat leads us to an interesting story. The mastermind this portal Ritesh Oza found out that his friend was struggling real hard to purchase an engagement ring. Sooner, they adapted the fact that the options for the unique design was real less plus the absence of a catalogue for prior “make to order” rings added to the inconvenience.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p>
                            So, learning from this experience, Vivocarat has in store selected products not based on categories, but based on collections. As Ritesh Oza says “There is no point in displaying a fancy ring to someone who’s looking for a daily-wear ring” and thus we believe in catering to the needs of each customer.
                        </p>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>VIVO OFFERINGS</span></h2>
                    </div>
                </div>
                <!--<div class="row">
                    <div class="col-md-12 no-pad">
                        <div class="row discovery-platform-background">
                            <div class="col-md-6 no-pad">
                                <h3 class="offerings-heading">Discovery Platform</h3>
                                <p class="snippet-description">
                                    Presenting, Vivocarat “Lookbook” - your personal fashion stylist and jewellery encyclopaedia. It is an assemblage of jewellery tips, fashion guide posts, brands introductions, latest arrivals, latest updates, celebrity inspirations and much more to be added to the gallery. Stay tuned to receive up to date infotainment regarding fashion and jewellery that will help you to experiment with your style and explore your creative side.
                                </p>
                            </div>
                            <div class="col-md-6">
                            <img src="/images/about-us/discovery-platform.png" class="absolute" alt="discovery platform">
                        </div>
                        </div>
                    </div>
                </div> Old code hidden by PR -->
                <div class="row image-out-of-the-box">
                    <div class="col-md-12 m-lr-15 pad-lr-50 pad-t-50 pad-b-15 bg-pink2 relative-parent">
                        <div class="row absolute-container">
                            <div class="col-md-6 m-t-40">
                                <h3 class="exclude m-b-15 m-t-0">Discovery Platform</h3>
                                <p>
                                    Presenting, Vivocarat “Lookbook” - your personal fashion stylist and jewellery encyclopaedia. It is an assemblage of jewellery tips, fashion guide posts, brands introductions, latest arrivals, latest updates, celebrity inspirations and much more to be added to the gallery. Stay tuned to receive up to date infotainment regarding fashion and jewellery that will help you to experiment with your style and explore your creative side.
                                </p>
                            </div>
                            <div class="col-md-6">
                                <img src="/images/about-us/discovery-platform.png" alt="discovery platform">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row image-out-of-the-box">
                    <div class="col-md-12 m-lr-15 pad-lr-50 pad-t-50 pad-b-15 bg-purple relative-parent">
                        <div class="row absolute-container">
                            <div class="col-md-6">
                                <img src="/images/about-us/trusted-brands.png" alt="trusted brands">
                            </div>
                            <div class="col-md-6 m-t-40">
                                <h3 class="exclude m-b-15 m-t-0">Trusted Brands</h3>
                                <p>
                                    Vivocarat takes pride in announcing that we have associated with prestigious jewellery brands all over Mumbai, Surat, Jaipur, Delhi and planning to expand more. From traditional vintage gold jewellery to contemporary diamond jewellery we have a vivid spread of versatile products in terms of design, style, category and occasion. We assure 100 percent authentic and genuine products with certification while delivering it to your doorstep.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--<div class="row">
                    <div class="col-md-12 no-pad">
                        <div class="row trusted-brands-background" style="margin-bottom:136px;">
                            <div class="col-md-6 pad-l-50">
                                <img src="/images/about-us/trusted-brands.png" class="absolute exclude" alt="trusted brands" style="margin-top:150px;">
                            </div>
                            <div class="col-md-6 trusted-brands-paragraph-position">
                            <h3 class="offerings-heading">Trusted Brands</h3>
                                <p class="snippet-description">
                                    Vivocarat takes pride in announcing that we have associated with prestigious jewellery brands all over Mumbai, Surat, Jaipur, Delhi and planning to expand more. From traditional vintage gold jewellery to contemporary diamond jewellery we have a vivid spread of versatile products in terms of design, style, category and occasion. We assure 100 percent authentic and genuine products with certification while delivering it to your doorstep.
                                </p>
                        </div>
                        </div>
                    </div>
                </div> Old code hidden by PR -->
                <!--<div class="row">
                    <div class="col-md-12 no-pad">
                        <div class="row discovery-platform-background">
                            <div class="col-md-6 no-pad">
                                <h3 class="offerings-heading">Curated Products</h3>
                                <p class="snippet-description">
                                    Unlike other upcoming jewellery portals, Vivocarat has a restricted collection of diamond and gold jewellery. We do not buy the idea of displaying around 50,000 design with taking design and quality into consideration. Instead, we aim to deal with limited jewellery collection that is crafted to perfection using superior quality metals and impeccable diamonds and coruscating gemstones. Because, at the end of the day, its quality that matters not quantity.
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6">
                            <img src="/images/about-us/curated products.png" class="absolute" alt="curated products">
                        </div>
                        </div>
                    </div>
                </div> Old code hidden by PR -->
                <div class="row image-out-of-the-box">
                    <div class="col-md-12 m-lr-15 pad-lr-50 pad-t-50 pad-b-15 bg-fair relative-parent">
                        <div class="row absolute-container">
                            <div class="col-md-6 m-t-40">
                                <h3 class="exclude m-b-15 m-t-0">Curated Products</h3>
                                <p class="exclude m-0">
                                    Unlike other upcoming jewellery portals, Vivocarat has a restricted collection of diamond and gold jewellery. We do not buy the idea of displaying around 50,000 design with taking design and quality into consideration. Instead, we aim to deal with limited jewellery collection that is crafted to perfection using superior quality metals and impeccable diamonds and coruscating gemstones. Because, at the end of the day, its quality that matters not quantity.
                                </p>
                            </div>
                            <div class="col-md-6">
                                <img src="/images/about-us/curated products.png" alt="curated products">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row image-out-of-the-box">
                    <div class="col-md-12 m-lr-15 pad-lr-50 pad-t-50 pad-b-15 bg-lblue relative-parent">
                        <div class="row absolute-container">
                            <div class="col-md-6">
                                <img src="/images/about-us/custom-jewellry-compressor.gif" alt="customization gif">
                            </div>
                            <div class="col-md-6 relative">
                                <div class="absolute top-35">
                                    <h3 class="exclude m-b-15 m-t-0">Customization</h3>
                                    <p class="exclude m-0 t13">
                                        At Vivocarat, we aspire to provide you with the opportunity to design your jewellery product according to your taste and preference. We give you the liberty to customise your valuables, awaken the artist in you and come up with precious jewellery so that you can cherish it in the years to come. From opting your precious metal to choosing the correct diamond size or settling for a subtle gemstone colour, we will do it all for you and make your beloved possession just like the way you had dreamt of. And, the best part is, our customisation offer is not limited to rings but also other products including necklaces, earrings, pendant, and much more.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="exclude m-0 pad-t-20">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>VIVO ON THE GO</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 padding-top-60px">
                        <img src="/images/about-us/vivo-on-the-go.png">
                    </div>
                </div>
            </section>
            <section class="pad-b-15">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>VIVO ADVANTAGE</span></h2>
                    </div>
                </div>
                <div class="row m-tb-15">
                    <div class="col-md-12 no-pad-r">
                        <div class="row">
                            <div class="col-md-6 pad-lr-0">
                                <div class="vivo-advtg vivo-certified bg-blue">
                                    <div class="vivo-advtg-content">
                                        <h3>Certified &amp; Hallmarked</h3>
                                        <p>
                                            We ensure our products are B.I.S hallmarked which considerably is the highest form of gold certification and for diamond certification we provide certificates from superior international institutes which include G.I.A (Gemological Institute of America), I.G.I (International Gemological Institute) I.G.L (International Gemological Laboratory) and S.G.L. (Solitaire Gemological Laboratories).
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6 pad-lr-0">
                                <div class="vivo-advtg vivo-return-policy bg-yellow">
                                    <div class="vivo-advtg-content pad-t-10pc">
                                        <h3>Free Shipping</h3>
                                        <p>
                                            At VivoCarat we commit the best of service, for our precious customers and thus we provide free shipping on every order and ensure timely and safe delivery of the product to your doorstep.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row m-t-40 m-b-15">
                    <div class="col-md-12 pad-r-0">
                        <div class="col-md-6 pad-lr-0">
                            <div class="vivo-advtg vivo-free-shipping bg-pink3">
                                <div class="vivo-advtg-content">
                                    <h3>Payment Gateways</h3>
                                    <p>
                                        For a hassle free and smooth shopping experience at Vivocarat, we are offering various secure payment gateways for your utmost convenience. We gladly accept Credit Card, Debit Card, Net Banking, Paytm Wallet and Cash on Delivery (COD). We hereby ensure that all your personal banking information is protected while the payment options are reliable and trustworthy.
                                    </p>
                                </div>
                            </div>
                        </div>
                            
                        <div class="col-md-6 pad-lr-0">
                            <div class="vivo-advtg vivo-payment-options bg-purple">
                                <div class="vivo-advtg-content pad-t-10pc">
                                    <h3>Easy Return Policy</h3>
                                    <p>
                                        If by any chance you are not satisfied with a product, you can easily return it to us, and the amount will be refunded to your account through the same payment mode in a span of 5-7 working days.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoAboutus.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>