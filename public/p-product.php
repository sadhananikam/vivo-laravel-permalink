<!DOCTYPE html>
<html lang="en" data-ng-app="vivoProduct">
<head>
    <title>$title</title>
    <meta name="keywords" content="$meta_keywords">
    <meta name="description" content="$meta_description" />
    <meta name="robots" content="$meta_robots">
    <meta property="og:url" content="$og_url" />
    <meta property="og:description" content="$og_description" />
    <meta property="og:title" content="$og_title" />
    <meta property="og:type" content="$og_type" />
    <meta property="og:image" content="$og_image" />
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <!-- SEO-->
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="$canurl">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="$moburl">
    <link rel="alternate" media="handheld" href="$moburl" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <!--start customize modal  -->
    <link rel="stylesheet" href="/css/jquery.fileupload.css">
    <!--end customize modal  -->
    <style>
        li.tab-product.vivo-main-only-product.pointer.active {
            border-bottom: 3px solid #e62739;
        }
        .carousel-indicators li {
            border: none;
            width: 7px !important;
            height: 7px !important;
            margin: 0px !important;
            border-radius: 7px;
            border: none;
            margin: 0px;
            background-color: #888888;
        }  
        .carousel-indicators .active {
            width: 12px;
            margin-bottom: -1px;
            background-color: #E62739 !important;
        }
        /*SELECT tag CSS*/ 
        .styled-select select {
            background: transparent;
            width: 76px;
            padding: 5px;
            font-size: 12px;
            font-family: 'leela';
            line-height: 1;
            border: 0;
            border-radius: 0;
            height: 34px;
            -webkit-appearance: none;
        }
        .styled-select {
            width: 76px;
            height: 34px;
            overflow: hidden;
            background: url('/images/product/icons/select.png') no-repeat 46px;
            background-color: #dddfe5;
            border-bottom: 2px solid #a6a6a6;
        } 
        .styled-select:hover {
            background-color: #EFEFEF;
        } 
        select>option:checked {
            color: white;
            background-color: #E62739;
        }
        /*END of SELECT tag CSS*/
        .nv-file-over {
            border: dotted 3px red;
        }
        /* Default class applied to drop zones on over */
        canvas {
            background-color: #f3f3f3;
            -webkit-box-shadow: 3px 3px 3px 0 #e3e3e3;
            -moz-box-shadow: 3px 3px 3px 0 #e3e3e3;
            box-shadow: 3px 3px 3px 0 #e3e3e3;
            border: 1px solid #c3c3c3;
            height: 100px;
            margin: 6px 0 0 6px;
        }
    </style>    
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body data-ng-controller='productCtrl' nv-file-drop="" uploader="uploader" ng-cloak>
    <vivo-header></vivo-header>
    <!-- Start MicroData   -->
    <div style="display:none;" itemscope itemtype="http://schema.org/Product">
        <span itemprop="brand">$brand</span>
        <span itemprop="name">$name</span>
        <img src="$img" itemprop="image" />
        <span itemprop="description">$desc</span>
        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <meta itemprop="priceCurrency" content="INR" />
            <span itemprop="price">$price</span>
            <link itemprop="availability" href="http://schema.org/InStock" />In stock! Order now!
        </div>
    </div>
    <!-- End MicroData   -->
    <div class="container m-b-15">
        <section class="exclude m-tb-15">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="breadcrumb bg-white pad-0 m-b-0">
                        <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                        $otherurls
                    </ul>
                </div>
            </div>
        </section>
        <section class="exclde m-t-0 m-b-30">
            <div class="row">
                <div class="grid col-sm-4 no-pad-r">
                    <div class="row">
                        <div class="col-sm-11 no-pad-lr">
                            <ul id="etalage">
                                <li>
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-1.jpg">
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-1.jpg" title="">
                                </li>
                                <li data-ng-if="img_count>1">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-2.jpg">
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-2.jpg" title="">
                                </li>
                                <li data-ng-if="img_count>2">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-3.jpg" class="img-responsive">
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-3.jpg">
                                </li>
                                <li data-ng-if="img_count>3">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-4.jpg">

                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-4.jpg">
                                </li>
                                <li data-ng-if="img_count>4">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-5.jpg">
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-5.jpg">
                                </li>

                                <li data-ng-if="img_count>5">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-6.jpg" />
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-6.jpg" />
                                </li>
                                <li data-ng-if="img_count>6">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-7.jpg" />
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-7.jpg" />
                                </li>
                                <li data-ng-if="img_count>7">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-8.jpg" />
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-8.jpg" />
                                </li>
                                <li data-ng-if="img_count>8">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-9.jpg" />
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-9.jpg" />
                                </li>
                                <li data-ng-if="img_count>9">
                                    <img class="etalage_thumb_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-10.jpg" />
                                    <img class="etalage_source_image" data-ng-src="/images/products-v2/{{p.VC_SKU}}-10.jpg" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-sm-5 no-pad-lr">
                    <h1 class="h2 exclude m-0">{{p.title}}</h1>
                    <h3 class="exclude m-0">Jewellers Name Title</h3>
                    <div class="m-tb-10" data-ng-hide="totalRating==0">
                        <span class="m-r-10 pad-tb-5 pad-lr-10 bg-lgray white br-5 bg-green-rate bold">{{totalRating}} &#9733;</span>
                        <!--<span class="theme-text">982 Rating & 189 Reviews</span>-->
                    </div>
                    <!--<p>
                        <img class="jeweller-logo exclude" data-ng-src="/images/header/brands logos dropdown/{{p.supplier_name}} hover.png" title={{p.supplier_name}} alt="{{p.supplier_name}}">
                    </p> Old code hidden by PR -->
                    <div data-ng-if="!isOutOfStock" class="row">
                        <div class="col-sm-4 no-pad-lr product-price">
                            RS. {{p.price_after_discount | INR}}
                        </div>
                        <div class="col-sm-4 cut-price no-pad-lr" data-ng-hide="p.discount==0">
                            RS. {{p.price_before_discount | INR}}
                        </div>
                        <div class="col-sm-4 no-pad-lr discount" data-ng-hide="p.discount==0">
                            {{p.discount}}% OFF
                        </div>
                    </div>
                    <div class="expected-text">
                        (Expected shipping date {{p.estimated_delivery_time}})
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-3 ring-size-structure no-pad-lr" data-ng-show="p.category=='Rings'">
                            <h class="ring-size-text">
                                Ring size&nbsp;:&nbsp;
                            </h>
                        </div>
                        <div class="col-lg-3 col-md-3 no-pad-l all-select-size">
                            <div data-ng-init="p.ring_size=null" data-ng-show="p.category=='Rings'" class="col-sm-12 styled-select no-pad-l">
                                <select data-ng-change="getProductVariant(p.ring_size,'ring_size')" data-ng-model="p.ring_size">
                                     <option value="SIZE" disabled selected>SIZE</option>
                                     <option data-ng-repeat="s in psize">{{s.ring_size}}</option>
                                </select>
                            </div>
                            <div class="row no-pad-l bracelet-select-position" data-ng-show="p.category=='Bracelets'">
                                <div class="col-sm-3 no-pad-lr">
                                    <h class="ring-size-text">
                                        Bracelet size &nbsp;:&nbsp;
                                    </h>
                                </div>
                                <div class="col-sm-4 no-pad-lr">
                                    <div data-ng-show="p.category=='Bracelets'" class="col-sm-12 styled-select no-pad-lr bracelet-select-structure">
                                        <select data-ng-model="p.bracelet_size" data-ng-change="getProductVariant(p.bracelet_size,'bracelet_size')">
                                           <option data-ng-repeat="s in psize">{{s.bracelet_size}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row bangle-size-select-structure" data-ng-show="p.category=='Bangles'">
                                <div class="col-sm-3 no-pad-lr m-t-5">
                                    <h class="ring-size-text">
                                        Bangle size &nbsp;:&nbsp;
                                    </h>
                                </div>
                                <div class="col-sm-3 no-pad-lr">
                                    <div data-ng-show="p.category=='Bangles'" class="col-sm-12 styled-select">
                                        <select data-ng-model="p.bangle_size" data-ng-change="getProductVariant(p.bangle_size,'bangle_size')">
                                            <option data-ng-repeat="s in psize">{{s.bangle_size}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 ring-guide-structure" data-ng-show="p.category=='Rings'">
                            <a href="/find-your-ring-size" target="_blank" class="ring-guide-text">
                                Ring size guide
                            </a>
                        </div>
                    </div>
                    <div class="row theme-text text-center bg-f1 b1-d5 pad-5 s12-t11" data-ng-hide="p.price_breakup=='0'">
                        <div class="inline-block">
                            <div class="uppercase">Gold</div>
                            <div>Rs. {{p.gold_price | INR}}</div>
                        </div>
                        <span class="pad-lr-5 bold plus-sign">+</span>
                        <div class="inline-block">
                            <div class="uppercase">Diamond</div>
                            <div>Rs. {{p.diamond_price | INR}}</div>
                        </div>
                        <span class="pad-lr-5 bold plus-sign">+</span>
                        <div class="inline-block">
                            <div class="uppercase">Gemstone</div>
                            <div>Rs. {{p.gem_price | INR}}</div>
                        </div>
                        <span class="pad-lr-5 bold plus-sign">+</span>
                        <div class="inline-block">
                            <div class="uppercase">Making</div>
                            <div>Rs. {{p.making_charges | INR}}</div>
                        </div>
                        <span class="pad-lr-5 bold plus-sign">+</span>
                        <div class="inline-block">
                            <div class="uppercase">Tax</div>
                            <div>Rs. {{p.tax | INR}}</div>
                        </div>
                    </div>
                    <div data-ng-init="isAlert=true" data-ng-if="isOutOfStock" class="well">
                        <h class="normal-text">
                            Out of stock.Please enter your details to get notified when it's restocked
                        </h>
                        <div class="row" data-ng-if="isAlert">
                            <form name="outalert">
                                <div class="row text-center">
                                    <div class="col-lg-12 placeholdlight padding-2px">
                                        <div class="col-lg-6 no-pad-l margin-bottom-15px">
                                            <input type="email" name="email" id="email" placeholder="Email" data-ng-model="customer_new.email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
                                        </div>
                                        <div class="col-lg-6 margin-bottom-15px">
                                            <input type="text" name="phone" id="phone" placeholder="Phone Number" data-ng-model="customer_new.phone" maxlength="13" pattern="(\+?\d[- .]*){7,13}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4" style="padding-left: 2px;">
                                        <a data-ng-show="p.category=='Rings'" data-ng-click="sendAlert(customer_new.email,customer_new.phone,p.ring_size)" class="btn btn-sm btn-vivo" ng-disabled="!outalert.$valid">
                                            Send Alert
                                        </a>
                                        <a data-ng-show="p.category=='Bracelets'" data-ng-click="sendAlert(customer_new.email,customer_new.phone,p.bracelet_size)" class="btn btn-sm btn-vivo" ng-disabled="!outalert.$valid">
                                            Send Alert
                                        </a>
                                        <a data-ng-show="p.category=='Bangles'" data-ng-click="sendAlert(customer_new.email,customer_new.phone,p.bangle_size)" class="btn btn-sm btn-vivo" ng-disabled="!outalert.$valid">
                                            Send Alert
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row grey-colour" style="font-size:15px;" data-ng-show="p.is_active==0">
                        Product is out of stock now !!
                    </div>
                    <div class="row" data-ng-hide="isOutOfStock">
                        <div class="col-md-5 no-pad-lr m-tb-15">
                            <vivo-buy-now></vivo-buy-now>
                        </div>
                        <div class="col-sm-6 no-pad-lr m-tb-15">
                            <vivo-add-to-cart></vivo-add-to-cart>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 lgray-79 underline pad-lr-0">
                            Product details
                        </div>
                    </div>
                    <div class="row">
                        <div data-ng-if="p.parent_category=='Gold' || p.parent_category=='Diamond'" class="col-sm-6 pad-t-15 t15 pad-lr-0">
                            <div class="row">
                                <div class="col-sm-7 no-pad-lr lgray-79">
                                    Stock Number
                                </div>
                                <div class="col-sm-5 no-pad-lr lgray-79">
                                    {{p.VC_SKU}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7 no-pad-lr lgray-79">
                                    Metal
                                </div>
                                <div class="col-sm-5 no-pad-lr lgray-79">
                                    {{p.metal}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7 no-pad-lr lgray-79">
                                    Gold Purity
                                </div>
                                <div class="col-sm-5 no-pad-lr lgray-79">
                                    {{p.purity}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7 no-pad-lr lgray-79">
                                    Gold weight
                                </div>
                                <div class="col-sm-5 no-pad-lr lgray-79">
                                    {{p.weight}}
                                </div>
                            </div>
                            <div class="row">
                                <a id="viewMore" ng-click="viewMore()" class="theme-text-special">+ view more</a>
                            </div>
                        </div>
                        <div data-ng-if="p.parent_category=='Silver'" class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-7 no-pad-lr lgray-79">
                                    Stock Number
                                </div>
                                <div class="col-sm-5 no-pad-lr lgray-79">
                                    {{p.VC_SKU}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7 no-pad-lr lgray-79">
                                    Metal
                                </div>
                                <div class="col-sm-5 no-pad-lr lgray-79">
                                    {{p.metal}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7 no-pad-lr lgray-79">
                                    Silver Purity
                                </div>
                                <div class="col-sm-5 no-pad-lr lgray-79">
                                    {{p.purity}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7 no-pad-lr lgray-79">
                                    Silver weight
                                </div>
                                <div class="col-sm-5 no-pad-lr lgray-79">
                                    {{p.weight}}
                                </div>
                            </div>
                            <div class="row pad-t-10">
                                <a id="viewMore" ng-click="viewMore()" class="theme-text-special">+ view more</a>
                            </div>
                        </div>
                        <div class="col-sm-6 customize-button-structure no-pad-lr">
                            <a class="btn btn-customize pull-right m-t-50" data-ng-click="customize(p)">
                                    Customize this
                                </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 no-pad-lr">
                    <div class="row">
                        <!--<div class="col-sm-6">
                            <img src="/images/product/certifications.jpg" alt="certifications">
                            <img data-ng-if="p.is_combo==1" src="/images/product/combo offer.png" alt="combo offer">
                        </div>-->
                        <div class="col-md-12">
                            <div class="vivo-usp bg-f1 pad-lr-10 pad-b-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="text-center hr-both"><span class="bg-f1 pad-lr-10">Certified Jewellery</span></h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pad-r-5 pad-l-0">
                                        <div class="bg-white pad-5">
                                            <img class="bg-f1 pad-5" src="/images/product/iji-logo.jpg" alt="IJI Logo" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 pad-r-0 pad-l-5">
                                        <div class="bg-white pad-5">
                                            <img class="bg-f1 pad-5" src="/images/product/bis-logo.png" alt="BIS Logo" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="text-center hr-both"><span class="bg-f1 pad-lr-10">Our Commitment</span></h3>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <div class="col-md-6 pad-r-5 pad-l-0">
                                        <div class="bg-white pad-5">
                                            <div class="bg-f1">
                                                <div class="trusted m-t-5"></div>
                                                <p class="exclude m-0 pad-0 t10 r12-t13">Trusted Jewellers</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pad-r-0 pad-l-5">
                                        <div class="bg-white pad-5">
                                            <div class="bg-f1">
                                                <div class="certified m-t-5"></div>
                                                <p class="exclude m-0 pad-0 t10 r12-t13">Certified Jewellery</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row text-center m-t-10">
                                    <div class="col-md-6 pad-r-5 pad-l-0">
                                        <div class="bg-white pad-5">
                                            <div class="bg-f1">
                                                <div class="shipping m-t-5"></div>
                                                <p class="exclude m-0 pad-0 t10 r12-t13">Free Shipping</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pad-r-0 pad-l-5">
                                        <div class="bg-white pad-5">
                                            <div class="bg-f1">
                                                <div class="return m-t-5"></div>
                                                <p class="exclude m-tb-0 pad-0 t10 r12-t13">Easy Return Policy</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  Old code - Img to Html 
                        <div class="col-sm-6 pad-l-0">
<img src="/images/product/our_commitment.png" alt="VivoCarat commitment">
                            <div class="vivo-commitment text-center">
                                <h3 class="exclude m-0 white pad-5 t-center uppercase bg-theme t12 r1200-t10">Our Commitment</h3>
                                <div class="trusted m-t-10"></div>
                                <p>Trusted Jewellers</p>
                                
                                <div class="certified m-t-10"></div>
                                <p>Certified Jewellery</p>
                                
                                <div class="shipping m-t-10"></div>
                                <p>Free Shipping</p>
                                
                                <div class="return m-t-10"></div>
                                <p>Easy Return Policy</p>
                                
                                <p class="bg-theme pad-10 white m-t-10 text-left r1200-pad-5">
                                    <strong class="t12 r1200-t10 l-space">Call or Whatsapp</strong>
                                    <br><span class="t11 r1200-t10">+91 9167645314</span>
                                    <br><strong class="t12 r1200-t10 l-space">Email</strong>
                                    <br><span class="t11 r1200-t10">hello@vivocarat.com</span>
                                </p>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </section>
        <section>
            <ul id="product-details" class="nav nav-tabs text-center m-lr-15" role="tablist">
                <li role="presentation" class="tab-product vivo-main-only-product active pointer">
                    <a class="product-tab-head pointer uppercase" data-target="#detail" aria-controls="home" role="tab" data-toggle="tab">
                    Product details
                </a>
                </li>
                <li role="presentation" class="tab-product vivo-main-only-product pointer">
                    <a class="product-tab-head pointer uppercase" data-target="#desc" aria-controls="profile" role="tab" data-toggle="tab">
                    Jeweller Description
                </a>
                </li>
                <li role="presentation" class="tab-product vivo-main-only-product pointer">
                    <a class="product-tab-head pointer uppercase" data-target="#review" aria-controls="messages" role="tab" data-toggle="tab">
                    Rating and Review
                </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content m-t-15">
                <div role="tabpanel" class="tab-pane active" id="detail">
                    <div data-ng-if="p.category=='Pendants' || p.category=='Earrings' || p.category=='NosePins' || p.category=='Bangles'" class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#basic" class="">
               BASIC INFORMATION
              </a>
                                                </h4>
                                            </div>
                                            <div id="basic" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Stock Number</div>
                                                        <div class="col-sm-5 colfont">{{p.VC_SKU}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Product Type</div>
                                                        <div class="col-sm-5 colfont">{{p.category}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Brand</div>
                                                        <div class="col-sm-5 colfont">{{p.supplier_name}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Item Package Quantity</div>
                                                        <div class="col-sm-5 colfont">{{p.unit_quantity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gender</div>
                                                        <div class="col-sm-5 colfont">{{p.gender}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont pad-b-10">Occasion</div>
                                                        <div class="col-sm-5 colfont">{{p.occasion}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#goldinfo" class="">
                                                        PRODUCT DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="goldinfo" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Metal</div>
                                                        <div class="col-sm-5 colfont">{{p.metal}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Purity</div>
                                                        <div class="col-sm-5 colfont">{{p.purity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Approx Metal Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Height</div>
                                                        <div class="col-sm-5 colfont">{{p.height}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Width</div>
                                                        <div class="col-sm-5 colfont">{{p.width}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default" data-ng-if="p.is_stone_present==1">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#dimen">
                                                        DIAMOND DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="dimen" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.min_total_diamond_weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_pieces}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Diamond Quality</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_quality}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Shape</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_shape}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ng-if="p.is_gem_present==1" class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#diamond">
                    GEMSTONE DETAILS
                </a>
                                                </h4>
                                            </div>
                                            <div id="diamond" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Name</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Color</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_color}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_count}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_weight}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-ng-if="p.category=='Goldcoins'" class="row mar-top-20">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#basic" class="">
                    BASIC INFORMATION
                </a>
                                                </h4>
                                            </div>
                                            <div id="basic" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Stock Number</div>
                                                        <div class="col-sm-5 colfont">{{p.VC_SKU}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Product Type</div>
                                                        <div class="col-sm-5 colfont">{{p.category}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Brand</div>
                                                        <div class="col-sm-5 colfont">{{p.supplier_name}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Item Package Quantity</div>
                                                        <div class="col-sm-5 colfont">{{p.unit_quantity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gender</div>
                                                        <div class="col-sm-5 colfont">{{p.gender}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Occasion</div>
                                                        <div class="col-sm-5 colfont">{{p.occasion}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#goldinfo">
                                                        PRODUCT DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="goldinfo" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Metal</div>
                                                        <div class="col-sm-5 colfont">{{p.metal}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Purity</div>
                                                        <div class="col-sm-5 colfont">{{p.purity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Approx Metal Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Sizes Available</div>
                                                        <div class="col-sm-5 colfont">{{p.ring_size}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default" data-ng-if="p.is_stone_present==1">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#dimen" class="">
                                                        DIAMOND DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="dimen" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.min_total_diamond_weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_pieces}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Diamond Quality</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_quality}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Shape</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_shape}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ng-if="p.is_gem_present==1" class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#diamond" class="">
                        GEMSTONE DETAILS
                    </a>
                                                </h4>
                                            </div>
                                            <div id="diamond" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Name</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Color</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_color}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_count}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_weight}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-ng-if="p.category=='Tanmaniya'" class="row mar-top-20">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#basic" class="">
                                                        BASIC INFORMATION
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="basic" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Stock Number</div>
                                                        <div class="col-sm-5 colfont">{{p.VC_SKU}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Product Type</div>
                                                        <div class="col-sm-5 colfont">{{p.category}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Brand</div>
                                                        <div class="col-sm-5 colfont">{{p.supplier_name}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Item Package Quantity</div>
                                                        <div class="col-sm-5 colfont">{{p.unit_quantity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gender</div>
                                                        <div class="col-sm-5 colfont">{{p.gender}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Occasion</div>
                                                        <div class="col-sm-5 colfont">{{p.occasion}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#goldinfo" class="">
                                                        PRODUCT DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="goldinfo" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Metal</div>
                                                        <div class="col-sm-5 colfont">{{p.metal}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Purity</div>
                                                        <div class="col-sm-5 colfont">{{p.purity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Approx Metal Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Sizes Available</div>
                                                        <div class="col-sm-5 colfont">{{p.ring_size}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default" data-ng-if="p.is_stone_present==1">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#dimen">
                                                        DIAMOND DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="dimen" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.min_total_diamond_weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_pieces}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Diamond Quality</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_quality}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Shape</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_shape}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ng-if="p.is_gem_present==1" class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#diamond">
                                                        GEMSTONE DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="diamond" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Name</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Color</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_color}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_count}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_weight}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-ng-if="p.category=='Rings'" class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#basic" class="">
                                                        BASIC INFORMATION
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="basic" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Stock Number</div>
                                                        <div class="col-sm-5 colfont">{{p.VC_SKU}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Product Type</div>
                                                        <div class="col-sm-5 colfont">{{p.category}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Brand</div>
                                                        <div class="col-sm-5 colfont">{{p.supplier_name}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Item Package Quantity</div>
                                                        <div class="col-sm-5 colfont">{{p.unit_quantity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gender</div>
                                                        <div class="col-sm-5 colfont">{{p.gender}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Occasion</div>
                                                        <div class="col-sm-5 colfont">{{p.occasion}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#goldinfo" class="">
                                                        PRODUCT DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="goldinfo" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Metal</div>
                                                        <div class="col-sm-5 colfont">{{p.metal}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Purity</div>
                                                        <div class="col-sm-5 colfont">{{p.purity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Approx Metal Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Sizes Available</div>
                                                        <div class="col-sm-5 colfont">{{p.ring_size}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default" data-ng-if="p.is_stone_present==1">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#dimen" class="">
                                                        DIAMOND DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="dimen" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.min_total_diamond_weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_pieces}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Diamond Quality</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_quality}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Shape</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_shape}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ng-if="p.is_gem_present==1" class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#diamond" class="">
                                                        GEMSTONE DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="diamond" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Name</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Color</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_color}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_count}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_weight}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-ng-if="p.category=='Necklaces' || p.category=='Chains' || p.category=='Bracelets'" class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#basic">
                                                        BASIC INFORMATION
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="basic" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Stock Number</div>
                                                        <div class="col-sm-5 colfont">{{p.VC_SKU}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Product Type</div>
                                                        <div class="col-sm-5 colfont">{{p.category}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Brand</div>
                                                        <div class="col-sm-5 colfont">{{p.supplier_name}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Item Package Quantity</div>
                                                        <div class="col-sm-5 colfont">{{p.unit_quantity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gender</div>
                                                        <div class="col-sm-5 colfont">{{p.gender}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Occasion</div>
                                                        <div class="col-sm-5 colfont">{{p.occasion}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#goldinfo" class="">
                                                        PRODUCT DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="goldinfo" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Metal</div>
                                                        <div class="col-sm-5 colfont">{{p.metal}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Purity</div>
                                                        <div class="col-sm-5 colfont">{{p.purity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Approx Metal Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.min_total_diamond_weight}}</div>
                                                    </div>
                                                    <div class="row ">
                                                        <div class="col-sm-7 lpad colfont">Length</div>
                                                        <div class="col-sm-5 colfont">{{p.height}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default" data-ng-if="p.is_stone_present==1">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#dimen" class="">
                                                        DIAMOND DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="dimen" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.min_total_diamond_weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_pieces}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Diamond Quality</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_quality}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Shape</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_shape}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ng-if="p.is_gem_present==1" class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#diamond" class="">
                                                        GEMSTONE DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="diamond" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Name</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Color</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_color}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_count}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_weight}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-ng-if="p.category=='Accessories'" class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#basic">
                                                        BASIC INFORMATION
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="basic" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Stock Number</div>
                                                        <div class="col-sm-5 colfont">{{p.VC_SKU}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Product Type</div>
                                                        <div class="col-sm-5 colfont">{{p.category}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Brand</div>
                                                        <div class="col-sm-5 colfont">{{p.supplier_name}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Item Package Quantity</div>
                                                        <div class="col-sm-5 colfont">{{p.unit_quantity}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gender</div>
                                                        <div class="col-sm-5 colfont">{{p.gender}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Occasion</div>
                                                        <div class="col-sm-5 colfont">{{p.occasion}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#goldinfo" class="">
                        PRODUCT DETAILS
                    </a>
                                                </h4>
                                            </div>
                                            <div id="goldinfo" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Metal</div>
                                                        <div class="col-sm-5 colfont">{{p.metal}}</div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-7 lpad colfont">Purity</div>
                                                        <div class="col-sm-5 colfont">{{p.purity}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default" data-ng-if="p.is_stone_present==1">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#dimen" class="">
                                                        DIAMOND DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="dimen" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.min_total_diamond_weight}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Diamond(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_pieces}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Diamond Quality</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_quality}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Shape</div>
                                                        <div class="col-sm-5 colfont">{{p.stone_shape}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ng-if="p.is_gem_present==1" class="panel panel-default">
                                            <div class="panhead">
                                                <h4 class="panel-title b1-b-black exclude pad-tb-10">
                                                    <a data-toggle="collapse" data-parent="#accordion" data-target="#diamond">
                                                        GEMSTONE DETAILS
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="diamond" class="panel-collapse collapse in">
                                                <div class="panel-body panelpad panhead">
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Name</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Gem Color</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_color}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem(s)</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_count}}</div>
                                                    </div>
                                                    <div class="row product">
                                                        <div class="col-sm-7 lpad colfont">Total Gem Weight</div>
                                                        <div class="col-sm-5 colfont">{{p.gem1_weight}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="desc">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="exclude m-b-15">
                                <img class="jeweller-logo-accordion exclude m-0" src="/images/header/brands logos dropdown/{{p.supplier_name}} hover.png" alt="{{p.supplier_name}}">
                            </p>
                            <h3 class="jeweller-name-accordion">
                                {{jeweller.name}}
                            </h3>
                            <div jeweller-star-rating rating-value="{{jratingValue}}" max="5"></div>
                            <p data-ng-bind-html='jeweller.description'>
                            </p>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="review">
                    <div class="row">
                        <div class="col-md-6 rating-position">
                            <div class="row bottom-gradient" data-ng-hide="reviewList.length<1">
                                <div class="col-md-12">
                                    <span class="overall-rating">{{totalRating}}</span><span class="total-rating">/5</span>
                                    <div jeweller-star-rating rating-value="{{totalRating}}" max="5"></div>
                                </div>
                            </div>
                            <div class="row" data-ng-if="reviewList.length<1">
                                <div class="col-xs-12 no-review-present-text">
                                    NO REVIEWS FOR THIS PRODUCT
                                </div>
                            </div>
                            <div class="row" data-ng-hide='reviewList.length<1'>
                                <div class="col-md-12 scroll-review-list">
                                    <ul>
                                        <li>
                                            <div class="row">
                                                <div class="col-md-1 user-rating-background">
                                                    {{reviewList[0].score}} &star;
                                                </div>
                                                <div class="col-md-10 review-top-position">
                                                    <div class="row">
                                                        <div class="col-md-12 no-pad">
                                                            <span class="review-title">{{reviewList[0].title}} </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 no-pad">
                                                            <p class="reviewer-details">
                                                                by {{reviewList[0].name}} on {{reviewList[0].created_at | dateToISO | date:'dd MM yyyy'}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 no-pad">
                                                            <span class="review-text">
                                                            {{reviewList[0].review}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div id="morereview" class="row" data-ng-hide='reviewList.length<2'>
                                        <div class="col-xs-12 text-right">
                                            <p class="read-more-review-text">Read more review +</p>
                                        </div>
                                    </div>
                                    <ul id="more_review_list">
                                        <li data-ng-repeat="rlist in reviewList track by $index" data-ng-hide="$first">
                                            <div class="row">
                                                <div class="col-xs-1 user-rating-background">
                                                    {{rlist.score}} &star;
                                                </div>
                                                <div class="col-xs-10 review-top-position">
                                                    <div class="row">
                                                        <div class="col-xs-12 no-pad">
                                                            <span class="review-title">{{rlist.title}} </span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 no-pad">
                                                            <p class="reviewer-details">
                                                                by {{rlist.name}} on {{rlist.created_at | dateToISO | date:'dd MM yyyy'}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 no-pad">
                                                            <span class="review-text">
                                                                {{rlist.review}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p class="write-review-heading">Write your review</p>
                            <div class="row compose-review-structure">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="score-text">Score</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div product-star-rating rating-value="{{reviewscore}}" max="5" reviewscore="reviewscore"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 pad-b-20">
                                            <input class="review-input-field" type="text" data-ng-model="reviewtitle" Placeholder="Title" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Title'">
                                        </div>
                                    </div>
                                    <div class="row pad-t-10">
                                        <div class="col-xs-12">
                                            <textarea class="review-textarea" data-ng-model="reviewmsg" Placeholder="Review" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Review'"></textarea>
                                        </div>
                                    </div>
                                    <div class="row pad-t-30 pad-b-50">
                                        <div class="col-xs-12 text-center">
                                            <a data-ng-click="addProductReview(reviewtitle,reviewmsg)" class="post-review-button">
                                                Post Review
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-xs-11">
                <h2 class="hr"><span>SIMILAR PRODUCTS</span></h2>
            </div>
            <div class="col-xs-1">
                <a data-ng-init="look1='/images/home/icons/home icons.png'" data-ng-mouseout="look1='/images/home/icons/home icons.png'" data-ng-mouseover="look1='/images/home/icons/home icons.png'" class="left carousel-control featured-products-arrow-background" data-target="#carousel-similar" data-slide="prev">
                    <div class="left-arrow" data-ng-src="{{look1}}"></div>
                </a>
                <a data-ng-init="look2='/images/home/icons/home icons.png'" data-ng-mouseout="look2='/images/home/icons/home icons.png'" data-ng-mouseover="look2='/images/home/icons/home icons.png'" class="right carousel-control featured-products-arrow-background" data-target="#carousel-similar" data-slide="next">
                    <div class="right-arrow featured-products-right-arrow-position" data-ng-src="{{look2}}"></div>
                </a>
            </div>
        </div>
        <vivo-product-carousel></vivo-product-carousel>
        <section>
            <div class="row m-b-15">
                <div class="col-xs-11">
                    <h2 class="hr"><span>PRODUCTS FROM SAME JEWELLER</span></h2>
                </div>
                <div class="col-xs-1">
                    <a data-ng-init="look3='/images/home/icons/home icons.png'" data-ng-mouseout="look3='/images/home/icons/home icons.png'" data-ng-mouseover="look3='/images/home/icons/home icons.png'" class="left carousel-control featured-products-arrow-background" data-target="#carousel-product-same-jeweller" data-slide="prev">
                        <div class="left-arrow" data-ng-src="{{look3}}"></div>
                    </a>
                    <a data-ng-init="look4='/images/home/icons/home icons.png'" data-ng-mouseout="look4='/images/home/icons/home icons.png'" data-ng-mouseover="look4='/images/home/icons/home icons.png'" class="right carousel-control featured-products-arrow-background" data-target="#carousel-product-same-jeweller" data-slide="next">
                        <div class="right-arrow featured-products-right-arrow-position" data-ng-src="{{look4}}"></div>
                    </a>
                </div>
            </div>
            <vivo-product-same-jeweller-carousel></vivo-product-same-jeweller-carousel>
        </section>
        <section>
            <vivo-recently-viewed-carousel></vivo-recently-viewed-carousel>
        </section>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon-org.js"></script>
    <script src="/app/modules/vivoProduct.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- start scripts for file upload -->
    <script src="/js/angular-file-upload.min.js"></script>
    <!-- end scripts for file upload -->
    <script id="sourcecode">
        $(function() {
            $('#products').slides({
                preload: true,
                preloadImage: '/images/list/icons/image filler.jpg',
                effect: 'slide, fade',
                crossfade: true,
                slideSpeed: 350,
                fadeSpeed: 500,
                generateNextPrev: true,
                generatePagination: false
            });
        });
    </script>
    <script>
        //show more review
        $("#more_review_list").hide();
        $("#morereview").click(function() {
            $("#more_review_list").show();
            $("#morereview").hide();
        });
        $("#more_review_listT").hide();
        $("#morereviewT").click(function() {
            $("#more_review_listT").show();
            $("#morereviewT").hide();
        });
    </script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->       
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>