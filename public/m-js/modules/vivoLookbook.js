var lbapp = angular.module("vivoLookbook", ['vivoCommon']);

lbapp.controller("lookBookCtrl", ["$scope", "$http", "$stateParams", "$rootScope", "$location", "API_URL", function ($scope, $http, $stateParams, $rootScope, $location, API_URL) {
    
    $scope.getTitle = function () {
                                
        $http({
            method: 'GET',
            url : API_URL + 'getlookbook'
        }).then(function successCallback(response){
            $scope.title = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });

    }

    //to format date in dd mmmm
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    
    $scope.getTitle();

    }]);

lbapp.filter('dateToISO', function() {
  return function(input) {
    if(input == undefined)
    {

    }
    else
    {
        input = new Date(input).toISOString();
        return input;
    }
 }});

lbapp.controller("lookBookArticleCtrl", ["$scope", "$http", "$stateParams", "$rootScope", "$location","API_URL", function ($scope, $http, $stateParams, $rootScope, $location, API_URL) {
    
 $scope.buyNow = function (p) {
  if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {

                //                alert("Please select a braclet size");
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                window.location.href = "/m/checkout";
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {

                //                alert("Please select a ring size");
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                window.location.href = "/m/checkout";
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {

                //                alert("Please select a bangle size");
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                window.location.href = "/m/checkout";
            }

        } else {
            $rootScope.addToCart(p);
            window.location.href = "/m/checkout";
        }

    }
 
     $scope.getAllLookbook = function(){
        $http({
            method: 'GET',
            url : API_URL + 'getLookbookList'
        }).then(function successCallback(response){
            $scope.title = response.data;
            $scope.lcount = response.data.length;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    $scope.getAllLookbook(); 
 
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[2];
    urlslug = urlslug.replace('.html','');

    $http({
        method: 'GET',
        url : API_URL + 'getLookbookDetailByUrlslug',
        params : {urlslug:urlslug}
    }).then(function successCallback(response){
         
        $scope.products = response.data;
        $scope.list = response.data[0];
        if($scope.list.id == 1)
        {
            var previd = $scope.lcount;
            var nextid = parseInt($scope.list.id) + 1;   
        }
        else if($scope.list.id == $scope.lcount)
        {
            var previd = parseInt($scope.list.id) - 1;
            var nextid = 1;   
        }
        else
        {
            var previd = parseInt($scope.list.id) - 1;
            var nextid = parseInt($scope.list.id) + 1;
        }
        $scope.getPrevUrl(previd);
        $scope.getNextUrl(nextid);
        $scope.getRelatedBlog();
        $scope.getSimilarProducts();
        if (response.data == "invalid") {
            $("#urlinvalid").modal('show');

            $timeout(function () {
                $("#urlinvalid").modal('hide');
            }, 2000);

        } else {
            var result = response.data;
        }
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $scope.getPrevUrl = function(id){
        $http({
            method: 'GET',
            url : API_URL + 'getLookbookDetail',
            params : {id:id}
        }).then(function successCallback(response){

            $scope.prevUrl = "/m/lookbook/" + response.data[0].urlslug + ".html";
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.getNextUrl = function(id){
        $http({
            method: 'GET',
            url : API_URL + 'getLookbookDetail',
            params : {id:id}
        }).then(function successCallback(response){

            $scope.nextUrl = "/m/lookbook/" + response.data[0].urlslug + ".html";
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.getRelatedBlog = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getRelatedBlog',
            params : {
                id1 : $scope.list.look_1,
                id2 : $scope.list.look_2,
                id3 : $scope.list.look_3
            }
        }).then(function successCallback(response){
            $scope.related = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.getSimilarProducts = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getSimilarProducts',
            params : {
                    s1 : $scope.list.similar_1,
                    s2 : $scope.list.similar_2,
                    s3 : $scope.list.similar_3,
                    s4 : $scope.list.similar_4
                }
        }).then(function successCallback(response){
            $scope.slist = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.readMore = false;
    $scope.toggleReadMore = function () {
        if ($scope.readMore) {
            $scope.readMore = false;
        } else {
            $scope.readMore = true;
        }
    }
    
    //to format date in dd mmmm
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };

}]);