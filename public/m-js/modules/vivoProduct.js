var productapp = angular.module("vivoProduct", ['vivoCommon','angularFileUpload']);

productapp.filter('dateToISO', function() {
  return function(input) {
    if(input == undefined)
    {

    }
    else
    {
        input = new Date(input).toISOString();
        return input;
    }
 }});

productapp.controller("productCtrl", ["$scope", "$http", "$stateParams", "$rootScope", "$location", '$timeout', 'FileUploader', '$window','API_URL', function ($scope, $http, $stateParams, $rootScope, $location, $timeout, FileUploader,$window,API_URL) {
        
    $scope.isDisabled = false;
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[2];
    urlslug = urlslug.replace('.html','');
    $scope.urlslug = urlslug;
    
    /* clipboard copy start */
    $scope.onSuccess = function(e) {

        $("#coupon-code").notify("Coupon code copied", {
            style: 'bootstrap',
            className: 'success',
            autoHide: true,
            autoHideDelay: 1500,
            clickToHide: true,
            elementPosition: 'top center'
        });
        
        $("#coupon-code-sm").notify("Coupon code copied", {
            style: 'bootstrap',
            className: 'success',
            autoHide: true,
            autoHideDelay: 1500,
            clickToHide: true,
            elementPosition: 'top center'
        });
    };

    $scope.onError = function(e) {

        $("#coupon-code").notify("Please try again", {
            style: 'bootstrap',
            className: 'error',
            autoHide: true,
            autoHideDelay: 1500,
            clickToHide: true,
            elementPosition: 'top right'
        });
        
        $("#coupon-code-sm").notify("Please try again", {
            style: 'bootstrap',
            className: 'error',
            autoHide: true,
            autoHideDelay: 1500,
            clickToHide: true,
            elementPosition: 'top right'
        });
    }
    /* clipboard copy end */
    
    $scope.viewMore = function () {
        $("html,body").animate({
            scrollTop: $('#product-details').position().top
        }, 800);
    }
    
    $scope.jratingValue = 0;
    $scope.whatsappurl = "https://www.vivocarat.com/product/" + $scope.urlslug+".html";
 
    var chatmsg1 = "Hi there! Can you help me? https://www.vivocarat.com/product/" + $scope.urlslug + ".html"; 
    $scope.chatmsg = "whatsapp://send?phone=+919503781870&abid=+919503781870&text=" + encodeURIComponent(chatmsg1);
    
    $scope.reviewscore = 0;
    $scope.totalRating = 0;
    
    $scope.imagename = [];
    $scope.resetcustomize = function () {
        $scope.customizef = {};
        //angular.copy({}, $scope.customizeForm);
      }
    $scope.resetcustomize();

  $scope.saveCustomizeform = function(){
    $scope.imagename = [];
      waitingDialog.show();
      if($scope.uploader.queue.length > 0)
      {
        $scope.uploader.uploadAll();
      }
      else
      {
        $scope.saveCustomizeformData();
      }
  };

  $scope.saveCustomizeformData = function(){
      waitingDialog.show();
      var url = API_URL + 'saveCustomizeformData';
      $http({
          method: 'POST',
          url: url,
          params : {
              name: $scope.customizef.cmname,
              email: $scope.customizef.cmemail,
              phone: $scope.customizef.cmphone,
              comment: $scope.customizef.cmcomment,
              imagename: JSON.stringify($scope.imagename)
          }
      }).then(function successCallback(response){
          waitingDialog.hide();
          if(response.data === 'success')
          {
              $scope.imagename = [];
              $scope.uploader.clearQueue();
              $scope.resetcustomize();
              $("#CustomizeRing").modal('hide');

              $("#customizeSuccess").modal('show');
              $timeout(function () {
                  $("#customizeSuccess").modal('hide');
              }, 2000);
          }
          else if(response.data === 'error')
          {
              $scope.imagename = [];
              $scope.uploader.clearQueue();
              $scope.resetcustomize();
              $("#CustomizeRing").modal('hide');

              $("#customizeError").modal('show');
              $timeout(function () {
                  $("#customizeError").modal('hide');
              }, 2000);
          }
      },function errorCallback(response){
          waitingDialog.hide();
          $scope.imagename = [];
          $scope.uploader.clearQueue();
          $scope.resetcustomize();
          $("#CustomizeRing").modal('hide');

          $("#customizeError").modal('show');
          $timeout(function () {
              $("#customizeError").modal('hide');
          }, 2000);
      });
  };

    // start of file upload code
    var uploader = $scope.uploader = new FileUploader({
        url: API_URL + 'uploadimages'
    });

    // FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
        $scope.imagename.push(response.filename);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
        $scope.saveCustomizeformData();
    };

    console.info('uploader', uploader);

    // end of file upload code
	
	$scope.buyNow = function (s) {
        
        if (s.category == 'Bracelets') {
            if (s.bracelet_size == null) {

                //                alert("Please select a braclet size");
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(s);
        
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                    $http({
                        method: 'POST',
                        url : API_URL + 'savetocart',
                        data: {
                            items: JSON.stringify($rootScope.cart),
                            uid: $rootScope.uid
                        }
                    }).then(function successCallback(response){
                        window.location.href = "/m/checkout";
                    },function errorCallback(response){
                        console.log(response.data);
                    });
                }
                else
                {              
                    window.location.href = "/m/checkout";
                } 
            }

        } else if (s.category == 'Rings') {
            if (s.ring_size == null) {

                //                alert("Please select a ring size");
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(s);
        
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                    $http({
                        method: 'POST',
                        url : API_URL + 'savetocart',
                        data: {
                            items: JSON.stringify($rootScope.cart),
                            uid: $rootScope.uid
                        }
                    }).then(function successCallback(response){
                        window.location.href = "/m/checkout";
                    },function errorCallback(response){
                        console.log(response.data);
                    });
                }
                else
                {              
                    window.location.href = "/m/checkout";
                } 
            }

        } else if (s.category == 'Bangles') {
            if (s.bangle_size == null) {

                //                alert("Please select a bangle size");
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(s);
        
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                    $http({
                        method: 'POST',
                        url : API_URL + 'savetocart',
                        data: {
                            items: JSON.stringify($rootScope.cart),
                            uid: $rootScope.uid
                        }
                    }).then(function successCallback(response){
                        window.location.href = "/m/checkout";
                    },function errorCallback(response){
                        console.log(response.data);
                    });
                }
                else
                {              
                    window.location.href = "/m/checkout";
                } 
            }

        } else {
            $rootScope.addToCart(s);
        
            if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    window.location.href = "/m/checkout";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else
            {              
                window.location.href = "/m/checkout";
            }
        }        
        
        
    }

    $scope.customize = function (p) {
		$("#CustomizeRing").modal('show');

    }
    
    $scope.zoomImage = function (i) {
        $scope.isZoom = true;
    }

    $scope.review = {
        pid: null,
        uid: null,
        name: null,
        title: null,
        review: null,
        score: null
    }
    
    $scope.addProductReview = function (title,review) {
        //alert($scope.reviewscore);
        if ($rootScope.authenticated) {
            $scope.review.pid = $scope.productId;
            $scope.review.uid = $rootScope.uid;
            $scope.review.name = $rootScope.name;
            $scope.review.score = $scope.reviewscore;
            $scope.review.title = title;
            $scope.review.review = review;

            if ($scope.review.score <= 0) {

                $("#reviewError").modal('show');

                    $timeout(function () {
                        $("#reviewError").modal('hide');
                    }, 2000);
            }
            else
            {
                if ($scope.review.title == null || $scope.review.title == ""){
                    $scope.review.title = null;
                }
                
                if($scope.review.review == null || $scope.review.review == ""){
                    $scope.review.review = null;
                }
                
                $http({
                    method: 'POST',
                    url : API_URL + 'addProductReview',
                    params : {
                    review:JSON.stringify($scope.review)
                        }
                }).then(function successCallback(response){
                    $scope.getProductReviewList();
                    $scope.getJewellerRating();
                    //Review added successfully
                   
                   $scope.reviewscore = 0;
                   $scope.reviewtitle = '';
                   $scope.reviewmsg = '';
                    $("#myCarousel").carousel("prev");
                    $("#myCarouselT").carousel("prev");
                    $("#reviewSuccess").modal('show');

                    $timeout(function () {
                        $("#reviewSuccess").modal('hide');
                    }, 2000);
                },function errorCallback(response){
                    console.log(response.data);
                    //Review error
                    $("#reviewError").modal('show');

                    $timeout(function () {
                        $("#reviewError").modal('hide');
                    }, 2000);
                });
                
            }

        } else {
            //Please login to add review
            $("#login").modal('show');

            $timeout(function () {
                $("#login").modal('hide');
            }, 2000);
        }
    }
    
    $scope.getProductCumulativeReview = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getProductCumulativeReview',
            params : {pid:$scope.productId}
        }).then(function successCallback(response){
            $scope.cummReview = response.data[0];
            $scope.cummReview.score == null ? $scope.score = 0 : $scope.score = parseFloat($scope.cummReview.score);
            $scope.totalRating = Math.ceil($scope.score);
        },function errorCallback(response){
            console.log(response.data);
            //Cumulative Review error
            $("#reviewCummErr").modal('show');

            $timeout(function () {
                $("#reviewCummErr").modal('hide');
            }, 2000);
        });
    }
        
    $scope.getProductReviewList = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getProductReviewList',
            params : {pid:$scope.productId}
        }).then(function successCallback(response){
            $scope.reviewList = response.data;
            $scope.getProductCumulativeReview();
        },function errorCallback(response){
            console.log(response.data);
            //Review List error
            $("#reviewListErr").modal('show');

            $timeout(function () {
                $("#reviewListErr").modal('hide');
            }, 2000);
        });
    }

    $scope.sendAlert = function (email, phone, size) {
        $http({
            method: 'GET',
            url : API_URL + 'setAlert',
            params : {
                    phone:phone,
                    id:$scope.p.id,
                    email:email,
                    size:size
                }
        }).then(function successCallback(response){
            $('#customizeSuccess').modal('show');
            $timeout(function () {
                $('#customizeSuccess').modal('hide');

            }, 2000);
            $scope.isAlert = false;
        },function errorCallback(response){
            console.log(response.data);
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });    
    }

    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else 
        {
            return false;
        }
    }
    function validatePhone(txtPhone) {
        var a = txtPhone;
        var filter = /^[0-9-+]+$/;
        if (filter.test(a)) {
            return true;
        }
        else {
            return false;
        }
    }
    
    $scope.sendPriceDropAlert = function (email, phone) {
        waitingDialog.show();
        if (email == null || email == "" || phone == null || phone == "") 
        {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } 
        else 
        { 
            if (!validateEmail(email)) 
            {
                $("#verifydetails").modal('show');

                $timeout(function () {
                    $("#verifydetails").modal('hide');
                }, 2000);
            }
            else if (!validatePhone(phone)) 
            {
                $("#verifydetails").modal('show');

                $timeout(function () {
                    $("#verifydetails").modal('hide');
                }, 2000);
            }
            else
            {
                $http({
                    method: 'POST',
                    url : API_URL + 'sendPriceDropAlert',
                    params : {
                            phone:phone,
                            id:$scope.p.id,
                            email:email
                        }
                }).then(function successCallback(response){

                    $scope.pricealertemail = '';
                    $scope.pricealertphone = '';

                   /*$rootScope.responseCUMsg = 'Thanks for contacting us. We will get in touch with you when product price will be dropped';
                    $('#contactusMsg').modal('show');
                    $timeout(function () {
                        $('#contactusMsg').modal('hide');

                    }, 2000);*/
                    $('#customizeSuccess').modal('show');
                    $timeout(function () {
                        $('#customizeSuccess').modal('hide');

                    }, 5000);
                },function errorCallback(response){
                    console.log(response.data);
                    $("#jewelerr").modal('show');

                    $timeout(function () {
                        $("#jewelerr").modal('hide');
                    }, 2000);
                });
            }  
        }
        waitingDialog.hide();
    }  
    
    $scope.isOutOfStock = false;

    $scope.getProductVariant = function (v, type) {
        
        $http({
            method: 'GET',
            url : API_URL + 'getProductVariant',
            params : {
                    sku:$scope.p.VC_SKU,
                    v:v,
                    type:type
                }
        }).then(function successCallback(response){
            if (response.data == "") {
                $scope.isOutOfStock = true;
            } else {
                $scope.p.id = response.data[0].id;
                $scope.isOutOfStock = false;
                $scope.p.VC_SKU = response.data[0].VC_SKU;
                $scope.p.VC_SKU_2 = response.data[0].VC_SKU_2;
                $scope.p.price_after_discount = response.data[0].price_after_discount;
                $scope.p.price_before_discount = response.data[0].price_before_discount;
                $scope.deliveryDate = $rootScope.date.AddDays($scope.p.estimated_delivery_time);
                $scope.img_count = parseInt(response.data[0].img_count);
            }
        },function errorCallback(response){
            console.log(response.data);
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }

    $('a[data-toggle="collapse"]').on('click', function () {

        var objectID = $(this).attr('href');

        if ($(objectID).hasClass('in')) {
            $(objectID).collapse('hide');
        } else {
            $(objectID).collapse('show');
        }
    });
    $scope.getProduct = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getProductDetailByUrlslug',
            params : {urlslug:$scope.urlslug}
        }).then(function successCallback(response){
            $scope.p = response.data[0];
            
            $scope.p.link = window.location.href;
            
            $rootScope.recentlyViewed();
            
            if($scope.p.is_active == 0)
            {
                $scope.isDisabled = true;
            }
            $scope.productId =$scope.p.id;
            $scope.p.ring_size = null;
            $scope.p.bangle_size = null;
            $scope.p.bracelet_size = null;
            $scope.getSimilarProducts();
            $scope.getSimilarJewellerProducts();
            $scope.getJewellerInformation();
            $scope.getJewellerRating();
            $scope.getProductSize();
            $scope.getProductReviewList();
            $scope.deliveryDate = $rootScope.date.AddDays($scope.p.estimated_delivery_time);
            $scope.img_count = parseInt(response.data[0].img_count);
            
            if($scope.p.price_after_discount <= 5000)
            {
                $scope.dispcoupon = 'VC300';
                $scope.dispcprice = '300';
            }
            else if($scope.p.price_after_discount >= 5001)
            {
                $scope.dispcoupon = 'VC500';
                $scope.dispcprice = '500';
            }
            
//            localStorage.removeItem('visited_links');
            var temp = localStorage.getItem('visited_links');
            var current_url = window.location.href;
            var isExistLink = false;
            if (temp != null && temp != "") 
            {
                var items = JSON.parse(temp);

                for (var i = 0; i < items.length; i++) 
                {
                    if (items[i].link == current_url) 
                    {
                        isExistLink = true;
                    }
                }
                if (!isExistLink) 
                {
                    items.push($scope.p);
                    localStorage.setItem('visited_links', JSON.stringify(items));
                }
            } 
            else 
            {
                localStorage.setItem('visited_links', JSON.stringify([$scope.p]));
            }
//            console.log(localStorage.getItem('visited_links'));
            
        },function errorCallback(response){
            console.log(response.data);
            
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }
    $scope.getProduct();
    
    $scope.getProductSize = function (){
        var category = $scope.p.category;
        var sizetype = '';
        if(category === 'Rings'){
            sizetype = 'ring_size';
        }else if(category === 'Bangles'){
            sizetype = 'bangle_size';
        }else if(category === 'Bracelets'){
            sizetype = 'bracelet_size';
        }
        
        if(sizetype != null && sizetype != ''){
            $http({
            method: 'GET',
            url : API_URL + 'getProductSize',
            params : {
                    sku:$scope.p.VC_SKU,
                    category:category,
                    sizetype:sizetype
                }
            }).then(function successCallback(response){
                $scope.psize = response.data;
            },function errorCallback(response){
                console.log(response.data);
            });   
        }
    }
    
    $scope.ringsizeupdate = function() {
        $scope.p.ring_size = $( "#ringsize option:selected" ).text();
        console.log($scope.pring_size);
    }
    
    $scope.braceletsizeupdate = function() {
       $scope.p.bracelet_size = $( "#braceletsize option:selected" ).text();
        console.log($scope.pbracelet_size);
    }
    
    $scope.banglesizeupdate = function() {
        $scope.p.bangle_size = $( "#banglesize option:selected" ).text();
        console.log($scope.pbangle_size);
    }
    
    $scope.getSimilarJewellerProducts = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getSimilarJewellerProducts',
            params : {s:$scope.p.supplier_name}
        }).then(function successCallback(response){
            $scope.list = response.data;
        },function errorCallback(response){
            console.log(response.data);
            
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }

    $scope.getJewellerInformation = function () {
      
        $http({
            method: 'GET',
            url : API_URL + 'getJewellerInformation',
            params : {
                    s:$scope.p.jeweller_id
                }
        }).then(function successCallback(response){
           
            $scope.jeweller = response.data[0];
        },function errorCallback(response){
            console.log(response.data);
            
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }
    
    $scope.getJewellerRating = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getJewellerRating',
            params : {
                    s:$scope.p.jeweller_id
                }
        }).then(function successCallback(response){
            $scope.jscore = response.data[0].score;
            $scope.jscore == null ? $scope.jratingValue = 0 : $scope.jratingValue = parseFloat($scope.jscore);
            //$scope.jratingValue = parseFloat(response.data[0].score);
        },function errorCallback(response){
            console.log(response.data);

            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }
    
    $scope.getSimilarProducts = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getSimilarProductsByCat',
            params : {c:$scope.p.category}
        }).then(function successCallback(response){
            $scope.same_list = response.data;
                
        },function errorCallback(response){
            console.log(response.data);
            
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);

        });
    }

    }]);