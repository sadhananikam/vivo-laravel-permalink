var ordersapp = angular.module("vivoOrders", ['vivoCommon']);

ordersapp.controller("ordersCtrl", ["$scope", "$http", "$rootScope", "$timeout", "API_URL", function ($scope, $http, $rootScope, $timeout, API_URL) {
    
    $scope.isLoaded = false;
    
     $scope.buyNow = function (o) {
        var products = o.products;
        var check = false;
        var k = 1;
        for (var i = 0; i < products.length; i++) 
        {
            var id= products[i].item.id;
            var p = null;
            
        $http({
            method: 'GET',
            url : API_URL + 'getProductDetail',
            params : {id:id}
        }).then(function successCallback(response){
            p = response.data[0];
            
            if(p.bracelet_size == '')
            {
                p.bracelet_size = null;
            }
            if(p.ring_size == '')
            {
                p.ring_size = null;
            }
            if(p.bangle_size == '')
            {
                p.bangle_size = null;
            }
            
            if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {

                //                alert("Please select a braclet size");
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                
                //                $location.path('i/checkout');
                if(k == products.length){
                    window.location.href = "/m/checkout";
                }
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {

                //                alert("Please select a ring size");
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                
                //                $location.path('i/checkout');
                if(k == products.length){
                    window.location.href = "/m/checkout";
                }
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {

                //                alert("Please select a bangle size");
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                
                //                $location.path('i/checkout');
                if(k == products.length){
                    window.location.href = "/m/checkout";
                }
            }

        } else {
            check=true;
            $rootScope.addToCart(p);
            
            //            $location.path('i/checkout');
            if(k == products.length){
                window.location.href = "/m/checkout";
            }
        }
            k = k+1;
            
        },function errorCallback(response){
            console.log(response.data);
        }); 
        }
//        if(check === true){
//            window.location.href = "p-checkout.html";   
//        }
    }
    
    $scope.removeProductFromOrder = function (oId, index, pIndex) {

        var products = JSON.parse($scope.orderHistoryList[pIndex].items);
        products.splice(index, 1);
        $scope.order = {
            oid: oId,
            items: products,
        }

       $http({
            method: 'POST',
            url : API_URL + 'removeProductFromOrder',
            params : {order: JSON.stringify($scope.order)}
        }).then(function successCallback(response){
            $("#orderupdate").modal('show');

            $timeout(function () {
                $("#orderupdate").modal('hide');
            }, 2000);

            $scope.getOrderHistory();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.returnOrder = function (id) {
        $scope.rid = id;
        $("#returnModal").modal('show');
    }
    
    $scope.returnOrderConfirm = function (id, reason) {
        
        $http({
            method: 'GET',
            url : API_URL + 'returnOrder',
            params : {oid:id,reason:reason}
        }).then(function successCallback(response){
            $("#returnsuccess").modal('show');

            $timeout(function () {
                $("#returnsuccess").modal('hide');
            }, 2000);

            $("#returnModal").modal('hide');
            $scope.getOrderHistory();

        },function errorCallback(response){
            console.log(response.data);
            $("#returnfail").modal('show');

            $timeout(function () {
                $("#returnfail").modal('hide');
            }, 2000);
        });
        
    }
    
    $scope.cancelOrder = function (id) {
        $http({
            method: 'GET',
            url : API_URL + 'cancelOrder',
            params : {oid:id}
        }).then(function successCallback(response){
            $("#ordercancel").modal('show');

            $timeout(function () {
                $("#ordercancel").modal('hide');
            }, 2000);

            $scope.getOrderHistory();
        },function errorCallback(response){
            console.log(response.data);
            $("#orderfetcherr").modal('show');

            $timeout(function () {
                $("#orderfetcherr").modal('hide');
            }, 2000);
        }); 
    }
    $scope.getOrderHistory = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getOrderHistory',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){

            if (response.data == "") {
                $scope.orderHistoryList = [];
            } else {
                $scope.orderHistoryList = response.data;
                for (var i = 0; i < response.data.length; i++) {
                    $scope.orderHistoryList[i].products = JSON.parse($scope.orderHistoryList[i].items);
                }
            }
            $scope.isLoaded = true;

        },function errorCallback(response){
            console.log(response.data);
            $("#orderfetchhisterr").modal('show');

            $timeout(function () {
                $("#orderfetchhisterr").modal('hide');
            }, 2000);

        });
    }
    $rootScope.$on('loadOrderHistoryEvent', function () {
        $scope.getOrderHistory();
    });
}]);