Date.prototype.AddDays = function (noOfDays) {
    this.setTime(this.getTime() + (noOfDays * (1000 * 60 * 60 * 24)));
    return this;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    // Creating modal dialog's DOM
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m" style="width: 300px;">' +
        '<div class="modal-content"style="background-color: rgba(255, 0, 0, 0);box-shadow: none;border: none;">' +

        '<div class="modal-body text-center">' +
        '<svg width="60px" height="60px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ring"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><circle cx="50" cy="50" r="47.5" stroke-dasharray="193.99334635916975 104.45795573186061" stroke="#e62739 " fill="none" stroke-width="5"><animateTransform attributeName="transform" type="rotate" values="0 50 50;180 50 50;360 50 50;" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite" begin="0s"></animateTransform></circle></svg>' +
        '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Please wait';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);

//app starts here
var app = angular.module('vivoCommon', ['ui.router', 'ngSanitize', 'ui.select', 'ngAnimate', 'toaster', 'ui-rangeSlider', 'googleplus', 'satellizer', 'tagged.directives.infiniteScroll', 'angularFileUpload','ngclipboard']).constant('API_URL','/api/v1/');

app.config(['$stateProvider', '$urlRouterProvider', 'GooglePlusProvider', '$authProvider', function ($stateProvider, $urlRouterProvider, GooglePlusProvider, $authProvider) {
    $authProvider.facebook({
        clientId: '235303226837165',
        responseType: 'token'
    });
    $authProvider.google({
        clientId: '491733683292-ic3mvgnhbq4ek4k5gcmgfvhnout2ejvb.apps.googleusercontent.com'
    });
    $authProvider.github({
        clientId: 'GitHub Client ID'
    });
    $authProvider.linkedin({
        clientId: 'LinkedIn Client ID'
    });
    $authProvider.instagram({
        clientId: 'Instagram Client ID'
    });
    $authProvider.yahoo({
        clientId: 'Yahoo Client ID / Consumer Key'
    });
    $authProvider.live({
        clientId: 'Microsoft Client ID'
    });

    $authProvider.twitch({
        clientId: 'Twitch Client ID'
    });
    $authProvider.bitbucket({
        clientId: 'Bitbucket Client ID'
    });
    // No additional setup required for Twitter
    $authProvider.oauth2({
        name: 'foursquare',
        url: '/auth/foursquare',
        clientId: 'Foursquare Client ID',
        redirectUri: window.location.origin,
        authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
    });
    // Facebook
    $authProvider.facebook({
        name: 'facebook',
        url: '/api/v1/oauth_login',
        authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
        redirectUri: window.location.origin,
        requiredUrlParams: ['display', 'scope'],
        scope: ['email'],
        scopeDelimiter: ',',
        display: 'popup',
        type: '2.0',
        popupOptions: {
            width: 580,
            height: 400
        }
    });
    // Google
    $authProvider.google({
        url: '/api/v1/oauth_login_google',
        authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
        redirectUri: window.location.origin,
        requiredUrlParams: ['scope'],
        optionalUrlParams: ['display'],
        scope: ['profile', 'email'],
        scopePrefix: 'openid',
        scopeDelimiter: ' ',
        display: 'popup',
        type: '2.0',
        popupOptions: {
            width: 452,
            height: 633
        }
    });
    // GitHub
    $authProvider.github({
        url: '/auth/github',
        authorizationEndpoint: 'https://github.com/login/oauth/authorize',
        redirectUri: window.location.origin,
        optionalUrlParams: ['scope'],
        scope: ['user:email'],
        scopeDelimiter: ' ',
        type: '2.0',
        popupOptions: {
            width: 1020,
            height: 618
        }
    });
    // Instagram
    $authProvider.instagram({
        name: 'instagram',
        url: '/auth/instagram',
        authorizationEndpoint: 'https://api.instagram.com/oauth/authorize',
        redirectUri: window.location.origin,
        requiredUrlParams: ['scope'],
        scope: ['basic'],
        scopeDelimiter: '+',
        type: '2.0'
    });
    // LinkedIn
    $authProvider.linkedin({
        url: '/auth/linkedin',
        authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
        redirectUri: window.location.origin,
        requiredUrlParams: ['state'],
        scope: ['r_emailaddress'],
        scopeDelimiter: ' ',
        state: 'STATE',
        type: '2.0',
        popupOptions: {
            width: 527,
            height: 582
        }
    });
    // Twitter
    $authProvider.twitter({
        url: '/auth/twitter',
        authorizationEndpoint: 'https://api.twitter.com/oauth/authenticate',
        redirectUri: window.location.origin,
        type: '1.0',
        popupOptions: {
            width: 495,
            height: 645
        }
    });
    // Twitch
    $authProvider.twitch({
        url: '/auth/twitch',
        authorizationEndpoint: 'https://api.twitch.tv/kraken/oauth2/authorize',
        redirectUri: window.location.origin,
        requiredUrlParams: ['scope'],
        scope: ['user_read'],
        scopeDelimiter: ' ',
        display: 'popup',
        type: '2.0',
        popupOptions: {
            width: 500,
            height: 560
        }
    });
    // Windows Live
    $authProvider.live({
        url: '/auth/live',
        authorizationEndpoint: 'https://login.live.com/oauth20_authorize.srf',
        redirectUri: window.location.origin,
        requiredUrlParams: ['display', 'scope'],
        scope: ['wl.emails'],
        scopeDelimiter: ' ',
        display: 'popup',
        type: '2.0',
        popupOptions: {
            width: 500,
            height: 560
        }
    });
    // Yahoo
    $authProvider.yahoo({
        url: '/auth/yahoo',
        authorizationEndpoint: 'https://api.login.yahoo.com/oauth2/request_auth',
        redirectUri: window.location.origin,
        scope: [],
        scopeDelimiter: ',',
        type: '2.0',
        popupOptions: {
            width: 559,
            height: 519
        }
    });
    // Bitbucket
    $authProvider.bitbucket({
        url: '/auth/bitbucket',
        authorizationEndpoint: 'https://bitbucket.org/site/oauth2/authorize',
        redirectUri: window.location.origin + '/',
        optionalUrlParams: ['scope'],
        scope: ['email'],
        scopeDelimiter: ' ',
        type: '2.0',
        popupOptions: {
            width: 1020,
            height: 618
        }
    });
    // Generic OAuth 2.0
    $authProvider.oauth2({
        name: null,
        url: null,
        clientId: null,
        redirectUri: null,
        authorizationEndpoint: null,
        defaultUrlParams: ['response_type', 'client_id', 'redirect_uri'],
        requiredUrlParams: null,
        optionalUrlParams: null,
        scope: null,
        scopePrefix: null,
        scopeDelimiter: null,
        state: null,
        type: null,
        popupOptions: null,
        responseType: 'code',
        responseParams: {
            code: 'code',
            clientId: 'clientId',
            redirectUri: 'redirectUri'
        }
    });
    // Generic OAuth 1.0
    $authProvider.oauth1({
        name: null,
        url: null,
        authorizationEndpoint: null,
        redirectUri: null,
        type: null,
        popupOptions: null
    });
}]);
app.run(function ($rootScope, $location, Data, $stateParams, $http, $window, $timeout, $timeout, API_URL) {
    var currenturl = window.location.href;
    var chatmsg1 = "Hi VivoCarat Team! Can you help me? " + currenturl; 
    $rootScope.chatmodalmsg = "whatsapp://send?phone=+919503781870&abid=+919503781870&text=" + encodeURIComponent(chatmsg1);
    Tawk_API.onLoad = function(){
        Tawk_API.hideWidget();
    };
    
    $rootScope.showTawk = function(){
        Tawk_API.maximize();
    };
    
    $rootScope.testalert = function(){
        alert("test");    
    };
    
    $rootScope.openMenuItem = function (l) {
        alert(l);
    };

    $rootScope.mouseOverCartIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-cart").attr("src", '/images/icons/cart-filled.png');
    }


    $rootScope.mouseOutCartIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-cart").attr("src", '/images/icons/cart-color.png');
    }
    $rootScope.mouseOverShareIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-share").attr("src", '/images/icons/share-filled.png');
    }
    $rootScope.mouseOutShareIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-share").attr("src", '/images/icons/share-color.png');
    }

    $rootScope.mouseOverWishIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-wish").attr("src", '/images/icons/wishlist-filled.png');
    }
    $rootScope.mouseOutWishIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-wish").attr("src", '/images/icons/wishlist.png');
    }

    $rootScope.cartIcon = '/images/mobile/header/icons/cart_bag.png';
    $rootScope.goToLogin = function () {
        $location.path("login/c");
        $('#registerModal').modal('hide');
    }
    $rootScope.couponSignUp = function (coupon) {
        Data.post('couponSignUp', {
            coupon: coupon
        }).then(function (results) {
            Data.toast(results);
        });
    };

    $rootScope.date = new Date();
    $rootScope.isGrid = true;
    $rootScope.setLayout = function (isGrid) {
        $rootScope.isGrid = isGrid;
    }
    $rootScope.addToWatchlist = function (p) {
            if (!$rootScope.authenticated) {
                //Please Login.
                $("#login").modal('show');
                $timeout(function () {
                    $("#login").modal('hide');
                }, 2000);
            } else {
                
                $http({
                    method: 'GET',
                    url : API_URL + 'addToWatchlist',
                    params : {
                            uid:$rootScope.uid,
                            pid:p.id
                        }
                }).then(function successCallback(response){                
                    //Product added to  wishlist.

                    $("#addwish").modal('show');
                    $timeout(function () {
                        $("#addwish").modal('hide');
                    }, 2000);

                },function errorCallback(response){
                    console.log(response.data);
                    //Oops! We are facing some error adding your product to watchlist. Just give us some time and we will be up and running.
                    $("#addwisherr").modal('show');
                    $timeout(function () {
                        $("#addwisherr").modal('hide');
                    }, 2000);
                });                
            }

        }
        /* recently viewed products */
        localStorage.removeItem('vivo-recently-viewed');

        $rootScope.recentlyViewed = function () 
        {
            var current_url = window.location.href;
            var isExistLink = false;
            var temp = localStorage.getItem('visited_links');
            if (temp != null && temp != "") 
            {
                var link_items = JSON.parse(temp);
                for (var i = 0; i < link_items.length; i++) 
                {
                    var isAdded = false;
                    var recently = localStorage.getItem('vivo-recently-viewed');
                    var isExist = false;
                    if (recently != null && recently != "") {
                        var items = JSON.parse(recently);

                        for (var j = 0; j < items.length; j++) 
                        {
                            if (items[j].id == link_items[i].id) 
                            {
                                isExist = true;
                            }
                        }

                        if (!isExist) 
                        {
                            items.push(link_items[i]);
                            localStorage.setItem('vivo-recently-viewed', JSON.stringify(items));
                        }

                    } else {
                        localStorage.setItem('vivo-recently-viewed', JSON.stringify([link_items[i]]));
                    }
                    
                }
            }
            $rootScope.recently = JSON.parse(localStorage.getItem('vivo-recently-viewed'));
//            console.log($rootScope.recently);
        }

    
    $rootScope.addToCart = function (p) {
        if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {

                //                    alert("Please select a braclet size");
                $("#BraceletSize").modal('show');
                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {
                $rootScope.addCartConfirmed(p);
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {

                //                    alert("Please select a ring size");
                $("#RingSize").modal('show');
                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addCartConfirmed(p);
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {

                //                alert("Please select a bangle size");
                $("#BangleSize").modal('show');
                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addCartConfirmed(p);
            }

        } else {

            $rootScope.addCartConfirmed(p);
        }
    }

    $rootScope.addCartConfirmed = function (p) {
        $rootScope.feed = null;
        var isAdded = false;
        var cart = localStorage.getItem('vivo-cart');
        if (cart != null && cart != "") {
            var items = JSON.parse(cart);
            for (var i = 0; i < items.length; i++) {
                if ((items[i].item.VC_SKU == p.VC_SKU) && (items[i].item.VC_SKU_2 == p.VC_SKU_2)) {

                    //                    alert("The product has already been added to the cart. If you would want to increase the quantity of please do so during checkout");
                    $("#addToCartAlready").modal('show');
                    $timeout(function () {
                        $("#addToCartAlready").modal('hide');
                        $('.modal-backdrop').remove();
                    }, 2000);
                    isAdded = true;
                }
            }
            if (!isAdded) {
                if(p.quantity !== undefined && p.quantity !== null){
                    items.push({
                        item: p,
                        quantity: p.quantity
                    });
                }else{
                    items.push({
                        item: p,
                        quantity: 1
                    });
                }
                $("#addToCartSuccess").modal('show');
                $timeout(function () {
                    $("#addToCartSuccess").modal('hide');
                    $('.modal-backdrop').remove();
                }, 2000);
            }
            localStorage.setItem('vivo-cart', JSON.stringify(items));
        } else {
            if(p.quantity !== undefined && p.quantity !== null){
                localStorage.setItem('vivo-cart', JSON.stringify([{
                    item: p,
                    quantity: p.quantity
                }]));
            }else{
                localStorage.setItem('vivo-cart', JSON.stringify([{
                    item: p,
                    quantity: 1
                }]));
            }
        }
        $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
        
        if($rootScope.uid !== undefined && $rootScope.uid !== null){
            $http({
                method: 'POST',
                url : API_URL + 'savetocart',
                data: {
                    items: JSON.stringify($rootScope.cart),
                    uid: $rootScope.uid
                }  
            }).then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }

    $rootScope.subQuantity = function (item, index) {
        if ($rootScope.cart[index].quantity == 1) {
            $rootScope.cart.splice(index, 1);
        } else {
            $rootScope.cart[index].quantity--;
        }
        localStorage.setItem('vivo-cart', JSON.stringify($rootScope.cart));
        
        if($rootScope.uid !== undefined && $rootScope.uid !== null){
            $http({
                method: 'POST',
                url : API_URL + 'savetocart',
                data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
            }).then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }
    
    $rootScope.addQuantity = function (item, index) {

        $rootScope.cart[index].quantity++;
        localStorage.setItem('vivo-cart', JSON.stringify($rootScope.cart));
        
        if($rootScope.uid !== undefined && $rootScope.uid !== null){
            $http({
                method: 'POST',
                url : API_URL + 'savetocart',
                data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
            }).then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }
    
    if (localStorage.getItem('vivo-cart') != "" && localStorage.getItem('vivo-cart') != null && localStorage.getItem('vivo-cart') != undefined) {
        $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
    } else {
        $rootScope.cart = null;
    }
    $rootScope.search = function () {
        $location.path('c/q/' + $rootScope.searchText);
    }
    $rootScope.login = {};
    $rootScope.signup = {};
    
    $rootScope.doLogin = function (customer) {
        waitingDialog.show();
        $http({
            method: 'POST',
            url : API_URL + 'login',
            params : {
                customer: JSON.stringify(customer),
                items: JSON.stringify($rootScope.cart),
                login_url: window.location.href
            }
        }).then(function successCallback(response){
            waitingDialog.hide();
            var results = response.data;
            $rootScope.response = results.message;
            $('#loggedIn').modal('show');
            $timeout(function () {
                $('#loggedIn').modal('hide');
            }, 2000);
            $rootScope.from = getUrlParameter('from');
            if (results.status == "success") {
                
                if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);
                        
                        var k = 1;
                        for (var i = 0; i < pr.length; i++) 
                        {

                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;              

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;

                                $rootScope.addToCart(p);
                                
                                if(k == pr.length){ 
                                    if ($rootScope.from == 'c') 
                                    {
                                        window.location.href = "/m/checkout";

                                    } else {
                                        window.location.href = "/m/home";
                                    }
                                }
                                k = k+1;
                            
                            },function errorCallback(response){
                                console.log(response.data);
                            });

                        }
                    }
                    else
                    {
                        if ($rootScope.from == 'c') {
                            window.location.href = "/m/checkout";

                        } else {
                            window.location.href = "/m/home";
                        }
                    }
            }
        },function errorCallback(response){
            console.log(response.data);
            waitingDialog.hide();
        });

    };
    $rootScope.doOAuthLogin = function (customer) {
        Data.post('oauth_login', {
            customer: customer
        }).then(function (results) {
            alert(results.message);

            if (results.status == "success") {
                if ($stateParams.from == 'c') {
                    window.location.href = "/m/checkout";

                } else {
                    window.location.href = "/m/home";
                }
            }
        });
    };
    $rootScope.signup = {
        email: '',
        password: '',
        name: '',
        phone: '',
        address: ''
    };
    $rootScope.signUp = function (customer) {
        
        waitingDialog.show();
            
        $http({
            method: 'POST',
            url : API_URL + 'signUp',
            params : {
                customer: JSON.stringify(customer),
                items: JSON.stringify($rootScope.cart),
                register_url: window.location.href
            }
        }).then(function successCallback(response){
            var results = response.data;
            if (results.status == "error") {
                // alert(results.message);
                $rootScope.response = results.message;
                $rootScope.response = results.message;
                $('#loggedIn').modal('show');
                $timeout(function () {
                    $('#loggedIn').modal('hide');
                }, 2000);
            } else {
                if(results.items != null)
                {
                    var pr = JSON.parse(results.items);

                    var k = 1;
                    for (var i = 0; i < pr.length; i++) 
                    {
                        var p_id = pr[i].product_id;
                        var ring_size = pr[i].ring_size;
                        var bangle_size = pr[i].bangle_size;
                        var bracelet_size = pr[i].bracelet_size;
                        var quantity = pr[i].quantity;               

                        $http({
                            method: 'GET',
                            url : API_URL + 'getProductDetail',
                            params : {id:p_id}
                        }).then(function successCallback(response){
                            var p = response.data[0];
                            p.ring_size = pr[k-1].ring_size;
                            p.bangle_size = pr[k-1].bangle_size;
                            p.bracelet_size = pr[k-1].bracelet_size;
                            p.quantity = pr[k-1].quantity;
                            $rootScope.addToCart(p);

                            if(k == pr.length){     
                                location.reload(true);
                            }
                            k = k+1;

                        },function errorCallback(response){
                            console.log(response.data);
                        });

                    }
                }
                else
                {
                    location.reload(true);
                }
            }
            waitingDialog.hide();

        },function errorCallback(response){
            console.log(response.data);
            waitingDialog.hide();
        });
    
    };
    
    $rootScope.couponSignUp = function (coupon) {
        Data.post('couponSignUp', {
            coupon: coupon
        }).then(function (results) {
            alert(results.message);
        });
    };
    $rootScope.logout = function () {
        
        var cart = localStorage.getItem('vivo-cart');
        if(cart != '')
        {
            var items = JSON.parse(cart);
            items=[];
        }
        else{
            var items=[];
        }

        localStorage.setItem('vivo-cart', JSON.stringify(items));

        $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
        
        Data.get('logout').then(function (results) {
            Data.toast(results);
            location.reload(true);
            
            FB.init({
                appId: '235303226837165',
                //  channelUrl: 'app/channel.html',
                status: true,

                cookie: true,

                xfbml: true
            });

            FB.getLoginStatus(function (response) {
                if (response && response.status === 'connected') {
                    FB.logout(function (response) {
                        location.reload(true);
                    });
                }
            });

        });
    }

    $rootScope.$on('$stateChangeSuccess',function (event, toState, toParams, fromState, fromParams) {
                $('body').removeClass('no-scroll');
                $timeout(function () {
                    waitingDialog.hide();
                }, 2000);
    });
    
    $rootScope.$on("$stateChangeStart", function (event, next, current) {
        waitingDialog.show();
        $window.ga('send', 'pageview', {
            page: $location.url()
        });

        $rootScope.authenticated = false;
        Data.get('session').then(function (results) {
            if (results.uid) {
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                $rootScope.name = results.name;
                $rootScope.phone = results.phone;
                $rootScope.email = results.email;

                $timeout(function () {
                    if (next.url == "/m/orders") {
                        $rootScope.$broadcast('loadOrderHistoryEvent');

                    }
                    if (next.url == "/m/account" || next.url == "/m/editaccount") {
                        $rootScope.$broadcast('loadAccountDetailsEvent');

                    }
                    if (next.url == "/m/wishlist") {
                        $rootScope.$broadcast('loadWatchlistEvent');
                    }
                    if (next.url == "/confirmOrder/:orderId") {
                        $rootScope.$broadcast('loadConfirmOrderEvent');
                    }
                    if (next.url == "/m/address") {
                        $rootScope.$broadcast('loadAddressEvent');
                    }
                }, 1500);

                var nextUrl = next.url;
                if (nextUrl == '/signup' || nextUrl == '/m/login/:from') {
                    window.location.href = "/m/home";
                }
            } else {
                $rootScope.$broadcast('loadHomeEvent');
                var nextUrl = next.url;
                if (nextUrl == '/m/wishlist' || nextUrl == '/m/account' || nextUrl == '/m/orders' || nextUrl == '/m/address') {
                    window.location.href = "/m/login";
                }
            }
        });
    });
    
    Data.get('session').then(function (results) {

        if (results.uid) {
            $rootScope.authenticated = true;
            $rootScope.uid = results.uid;
            $rootScope.name = results.name;

            $rootScope.email = results.email;
            $rootScope.phone = results.phone;
            $timeout(function () {
                if (window.location.pathname == '/m/orders') {
                    $rootScope.$broadcast('loadOrderHistoryEvent');

                }
                if (window.location.pathname == "/m/account" || window.location.pathname == "/m/edit-account") {
                    $rootScope.$broadcast('loadAccountDetailsEvent');

                }
                if (window.location.pathname == "/m/wishlist") {
                    $rootScope.$broadcast('loadWatchlistEvent');
                }
                if (window.location.pathname == "/m/confirm-order") {
                    $rootScope.$broadcast('loadConfirmOrderEvent');
                }
                if (window.location.pathname == "/m/address") {
                    $rootScope.$broadcast('loadAddressEvent');
                }
            }, 1500);

            if (window.location.pathname == '/m/signup' || window.location.pathname == '/m/login') {
                window.location.href = "/m/home";
            }
            
            var url = API_URL + 'addPageVisits';
            $http({
                method: 'POST',
                url: url, 
                params: {
                        uid:$rootScope.uid,
                        visited_url: window.location.href
                    }
            })
            .then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        } else {
            if (window.location.pathname == '/m/wishlist' || window.location.pathname == '/m/account' || window.location.pathname == '/m/orders' || window.location.pathname == '/m/address') {
                window.location.href = "/m/login";
            }
        }
    });
    
});
app.factory('sAuth', function ($q, $rootScope) {
    return {
        logout: function () {
            var _self = this;
            FB.logout(function (response) {

                $rootScope.$apply(function () {

                    $rootScope.user = _self.user = {};
                    $rootScope.logout();
                });
            });
        },
        getUserInfo: function () {
            var _self = this;
            FB.api('/me?fields=first_name,last_name,gender,email', function (res) {
                $rootScope.$apply(function () {
                    $rootScope.doOAuthLogin({
                        name: res.first_name + res.last_name,
                        email: res.email,
                        gender: res.gender
                    });
                });

            });

        },
        watchLoginChange: function () {
            var _self = this;

            FB.Event.subscribe('auth.login', function (res) {
                if (res.status === 'connected') {
                    _self.getUserInfo();
                } else {
                }

            });
        }
    }
});

//Below code is for showing grey images in list,splist page while product images gets loaded
app.directive('actualImage', ['$timeout', function ($timeout) {
    return {
        link: function ($scope, element, attrs) {
            function waitForImage(url) {
                var tempImg = new Image();
                tempImg.onload = function () {
                    $timeout(function () {
                        element.attr('src', url);
                    });
                }
                tempImg.src = url;
            }
            attrs.$observe('actualImage', function (newValue) {
                if (newValue) {
                    waitForImage(newValue);
                }
            });
        }
    }
}]);

app.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function (item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }
        return out;
    }
});

//Below code is for INR comma system
app.filter('INR', function () {
    return function (input) {
        if (!isNaN(input)) {
            var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            if (result.length > 1) {
                output += "." + result[1];
            }

            return currencySymbol + output;
        }
    }
});

app.filter('num', function () {
    return function (input) {
        return parseFloat(input, 10);
    };
});

app.directive('jewellerStarRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '@',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };
            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});

app.directive('productStarRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '@',
            max: '=',
            onRatingSelected: '&',
            reviewscore: '='
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };
            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };
            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                    scope.reviewscore=scope.ratingValue;
                }
            });
        }
    }
});

app.directive('generatesafeurlChat', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            if (element[0].tagName !== 'A') {
                return;  // simply do nothing (or raise an error)
            }
            element[0].href = scope.chatmsg;
        }
    };
 });

app.directive('generatesafeurlChatModal', function () {
    return {
        restrict: 'A',
        link: function(rootScope, element, attrs) {
            if (element[0].tagName !== 'A') {
                return;  // simply do nothing (or raise an error)
            }
            element[0].href = rootScope.chatmodalmsg;
        }
    };
 });

app.directive('vivoHeader', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {

            $rootScope.dshow = function(){
                alert('hello');
            }

            $rootScope.openMenuItem = function (l) {
                alert(l);
            };

            // default options (all of them)
            var options = {
                    maxWidth: 300,
                    speed: 0.2,
                    animation: 'ease-out',
                    topBarHeight: 56,
                    modifyViewContent: true,
                    useActionButton: true
                }
                
            $rootScope.openSearchSm = function () {

                $("#topbarsm").animate({
                    top: '60px'
                }, 1000);

            }
            $rootScope.closeSearchSm = function () {

                $("#topbarsm").animate({
                    top: '0px'
                }, 1000);
            }
            $rootScope.openSearchXs = function () {

                $("#topbarxs").animate({
                    top: '77px'
                }, 500);

            }
            $rootScope.closeSearchXs = function () {

                $("#topbarxs").animate({
                    top: '-500px'
                }, 500);
            }
            $rootScope.isSortSmOpen = false;
            $rootScope.isSortXsOpen = false;
            $rootScope.openSortSm = function () {
                if ($rootScope.isSortSmOpen) {
                    $rootScope.closeSortSm();
                } else {
                    $rootScope.isSortSmOpen = true;
                    $("#sortsm").animate({
                        bottom: '43px'
                    }, 1000);
                }

            }
            $rootScope.closeSortSm = function () {
                $rootScope.isSortSmOpen = false;
                $("#sortsm").animate({
                    bottom: '0px'
                }, 1000);
            }
            $rootScope.openSortXs = function () {

                if ($rootScope.isSortXsOpen) {
                    $rootScope.closeSortXs();
                } else {
                    $rootScope.isSortXsOpen = true;
                    $("#sortxs").animate({
                        bottom: '44px'
                    }, 500);
                }
            }
            $rootScope.closeSortXs = function () {
                $rootScope.isSortXsOpen = false;
                $("#sortxs").animate({
                    bottom: '0px'
                }, 500);
            }

        },
        link: function ($scope, $rootScope, Data, $location) {

            if (localStorage.getItem('vivo-cart') != "" && localStorage.getItem('vivo-cart') != null && localStorage.getItem('vivo-cart') != undefined) {
                $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
            } else {
                $rootScope.cart = null;
            }
        },
        templateUrl: '/m-partials/vivo-header.html'
    };
});

app.directive('vivoFooter', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {

        },
        link: function ($scope, $rootScope, Data, $location) {

        },
        templateUrl: '/m-partials/vivo-footer.html'
    };
});

app.directive('vivoProductList', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: '/m-partials/vivo-product-list.html'
    };
});

app.directive('recentlyViewedProducts', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: '/m-partials/recently-viewed-products.html'
    };
});

app.directive('featuredProducts', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: '/m-partials/featured-products.html'
    };
});

app.directive('vivoFilter', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: '/m-partials/vivo-filter.html'
    };
});

app.directive('errSrc', function() {
      return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.errSrc) {
              attrs.$set('src', attrs.errSrc);
            }
          });
          
          attrs.$observe('ngSrc', function(value) {
            if (!value && attrs.errSrc) {
              attrs.$set('src', attrs.errSrc);
            }
          });
        }
      }
    });

app.directive('ngThumb', ['$window', function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function(scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
}]);

app.directive('starRating',
    function () {
        return {
            restrict: 'A',
            template: '<ul class="rating"> <li ng-repeat="star in stars" ng-class="star" ng-click= "toggle($index)"><i class = "fa fa-star-o"></i></li></ul>',
            scope: {
                ratingValue: '=',
                max: '=',
                onRatingSelected: '&'
            },
            link: function (scope, elem, attrs) {
                var updateStars = function () {
                    scope.stars = [];
                    for (var i = 0; i < scope.max; i++) {
                        scope.stars.push({
                            filled: i < scope.ratingValue
                        });
                    }
                };

                scope.toggle = function (index) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelected({
                        rating: index + 1
                    });
                };

                scope.$watch('ratingValue',
                    function (oldVal, newVal) {
                        if (newVal) {
                            updateStars();
                        }
                    }
                );
            }
        };
    }
);

app.filter('dateToISO', function() {
  return function(input) {
    if(input == undefined)
    {

    }
    else
    {
        input = new Date(input).toISOString();
        return input;
    }
 }});

app.directive("datepicker", function () {
  return {
    restrict: "A",
    require: "ngModel",
    link: function (scope, elem, attrs, ngModelCtrl) {
      var updateModel = function (dateText) {
        scope.$apply(function () {
          ngModelCtrl.$setViewValue(dateText);
        });
      };
      var options = {
        dateFormat: "yy-mm-dd",
        onSelect: function (dateText) {
          updateModel(dateText);
        }
      };
      elem.datepicker(options);
    }
  }
});