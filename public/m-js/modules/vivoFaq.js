var faqapp = angular.module("vivoFaq", ['vivoCommon']);
faqapp.controller("faqCtrl", ["$scope", "$http", "$stateParams", function ($scope, $http, $stateParams) {
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[2];
        var id = urlslug;

        if(id != undefined)
        {
            var str = "#"+id;
            
            // $( str ).on( "click", function( event ) {
                var currentAttrValue = $(str).attr('href');
            
                    close_accordion_section();

                    // Add active class to section title
                    $(str).addClass('active');
                    // Open up the hidden content panel
                    $('.accordion ' + currentAttrValue).slideDown(50).addClass('open'); 
                    $(str).children(".plusminus").text('-');
        }

        function close_accordion_section() {
            $('.accordion .accordion-section-title').removeClass('active');
            $('.accordion .accordion-section-content').slideUp(50).removeClass('open');
            $('.accordion .accordion-section-title').children(".plusminus").text('+');
        }

        $('.accordion-section-title').click(function(e) {
            // Grab current anchor value
            var currentAttrValue = $(this).attr('href');

            if($(e.target).is('.active')) {
                close_accordion_section();
                $(this).children(".plusminus").text('+');
            }else {
                close_accordion_section();

                // Add active class to section title
                $(this).addClass('active');
                // Open up the hidden content panel
                $('.accordion ' + currentAttrValue).slideDown(50).addClass('open'); 
                $(this).children(".plusminus").text('-');
            }

            e.preventDefault();
        });
        
        function close_accordion_ans() {
            $('.accordion .accordion-section-ques').removeClass('active');
            $('.accordion .accordion-section-ans').slideUp(50).removeClass('open');
            $('.accordion .accordion-section-ques').children(".plusminus").text('+');
        }
        
        $('.accordion-section-ques').click(function(e) {
            // Grab current anchor value
            var currentAttrValue = $(this).attr('href');

            if($(e.target).is('.active')) {
                close_accordion_ans();
                $(this).children(".plusminus").text('+');
            }else {
                close_accordion_ans();

                // Add active class to section title
                $(this).addClass('active');
                // Open up the hidden content panel
                $('.accordion ' + currentAttrValue).slideDown(50).addClass('open'); 
                $(this).children(".plusminus").text('-');
            }

            e.preventDefault();
        });
    
        function openmaincat(id)
        {
            var str = "#"+id;
            var currentAttrValue = $(str).attr('href');

            close_accordion_section();

            // Add active class to section title
            $(str).addClass('active');
            // Open up the hidden content panel
            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
            $(str).children(".plusminus").text('-');

            setTimeout(function(){
                $("html,body").animate({
                    scrollTop: $(str).position().top-80
                }, 2000);
            }, 500);
        } 

        $('#Lifetime-ques1').click(function(e){
            openmaincat('Lifetime');
        });

}]);	