var app = angular.module("vivoHome", ['vivoCommon']);

app.controller('authCtrl', function ($scope, $rootScope, $stateParams, $location, $http, Data, $window, API_URL) {
    
    $scope.buyNow = function (s) {
        $rootScope.addToCart(s);
        window.location.href = "/m/checkout";
    }

    $("#scroller").simplyScroll();
    $scope.coupon = {
        email: null,
        password: null
    }

    $scope.goTo = function (path) {
        $('#categoryEarringsModal').modal('hide');
        $('#categoryBraceletsModal').modal('hide');
        $('#categoryPendantsModal').modal('hide');
        $('#categoryRingsModal').modal('hide');
        $('#categoryNecklacesModal').modal('hide');


        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        window.location.href = path;
    }
    $scope.signUpCoupon = function () {
        if ($scope.coupon.email == null || $scope.coupon.email == "" || $scope.coupon.password == null || $scope.coupon.password == "") {

            $('#verifydetails').modal('show');
            $timeout(function () {
                $('#verifydetails').modal('hide');

            }, 2000);


        } else {

            $rootScope.couponSignUp($scope.coupon);
            if ($stateParams.from == 'c') {
                window.location.href = "/m/checkout";
            }
            $window.location.reload();
        }

    }
    $scope.showFirstModal = function () {
        if (!$rootScope.authenticated) {
            if (!$rootScope.isCouponModalShown) {
                $rootScope.isCouponModalShown = false;
            }
            if (!$rootScope.isCouponModalShown) {
                $('#registerCouponModal').modal('show');


                $rootScope.isCouponModalShown = true;
            }
        }
    }
    
    $rootScope.$on('loadHomeEvent', function () {

    });
    
    //lookbook article
     $scope.getTitle = function (id) {
        $scope.id = getUrlParameter('id');
         
         $http({
            method: 'GET',
            url : API_URL + 'getLookbookList'
        }).then(function successCallback(response){
            $scope.title = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $http({
        method: 'GET',
        url : API_URL + 'getVivoHeader',
        params : {p:'home',m:1}
    }).then(function successCallback(response){
        $scope.banners = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $http({
        method: 'GET',
        url : API_URL + 'getfeaturedproducts'
    }).then(function successCallback(response){
        $scope.list = response.data;
        if (response.data == "invalid") {
        
            $('#invalidUrl').modal('show');
            $timeout(function () {
                $('#invalidUrl').modal('hide');

            }, 2000);

        } else {
            var result = response.data;

        }
    },function errorCallback(response){
        console.log(response.data);
    });
    
    //to format date in dd mmmm
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    
    $scope.getTitle();

});