var paymentSuccessapp = angular.module("vivoPaymentSuccess", ['vivoCommon']);

paymentSuccessapp.controller("paymentsuccessCtrl", ["$scope", "$http", "$stateParams", '$timeout', 'API_URL', function ($scope, $http, $stateParams, $timeout, API_URL) {
    
    var params = window.location.pathname.split('/').slice(1); 
    var urlslug = params[1];
    urlslug = urlslug.replace('.html','');
    
    $scope.id = urlslug.split('-').slice(1);

    $scope.getOrderDetails = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getOrderDetails',
            params : {id:$scope.id}
        }).then(function successCallback(response){
            $scope.order = response.data[0];
            $scope.ship = response.data[1];
            $scope.products = JSON.parse(response.data[0].items);
        },function errorCallback(response){
            console.log(response.data);
            $("#facingerror").modal('show');

            $timeout(function () {
                $("#facingerror").modal('hide');
            }, 2000);
        });
    }
    $scope.getOrderDetails();
    
}]);