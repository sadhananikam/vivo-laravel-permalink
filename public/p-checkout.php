<!DOCTYPE html>
<html lang="en" data-ng-app="vivoCheckout">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta name="keywords" content="" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/checkout">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/checkout">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/checkout" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <style>
        /*CSS for materializing input fields*/
        * {
            box-sizing: border-box;
        }
        .group {
            position: relative;
        }
        input {
            font-size: 13px;
            padding: 10px 10px 10px 5px;
            display: block;
            border: none;
            border-bottom: 1px solid #888888;
            box-shadow: none;
        }       
        input:focus {
            outline: none;
        }
        label {
            color: #797979 !important;
            font-size: 15px;
            font-weight: normal;
            font-family: 'leela';
            pointer-events: none;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }    
        input:focus~label,
        input:valid~label {
            top: -20px;
            font-size: 14px;
            color: #E62739;
        }
        .bar {
            position: relative;
            display: block;
            width: 100%;
        }
        .bar:before,
        .bar:after {
            content: '';
            height: 2px;
            width: 0;
            bottom: 1px;
            position: absolute;
            background: #E62739;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }
        .bar:before {
            left: 50%;
        }
        .bar:after {
            right: 50%;
        }
        input:focus~.bar:before,
        input:focus~.bar:after {
            width: 50%;
        }
        .highlight {
            position: absolute;
            height: 60%;
            width: 100px;
            top: 25%;
            left: 0;
            pointer-events: none;
            opacity: 0.5;
        }
        input:focus~.highlight {
            -webkit-animation: inputHighlighter 0.3s ease;
            -moz-animation: inputHighlighter 0.3s ease;
            animation: inputHighlighter 0.3s ease;
        }
        /* ANIMATIONS ================ */
        @-webkit-keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }  
        @-moz-keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        } 
        @keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }
        /*END of Material CSS design*/    
        .form-radio,
        .form-group {
            position: relative;
        }
        .form-inline>.form-group,
        .form-inline>.btn {
            display: inline-block;
            margin-bottom: 0;
        }       
        .form-group input {
            height: 1.9rem;
        } 
        .form-group textarea {
            resize: none;
        }        
        .form-group .control-label {
            position: absolute;
            -webkit-transition: all 0.28s ease;
            transition: all 0.28s ease;
        }       
        .form-group .bar {
            position: relative;
            border-bottom: 0.0625rem solid #999;
            display: block;
        }       
        .form-group .bar::before {
            content: '';
            height: 0.125rem;
            width: 0;
            left: 50%;
            bottom: -0.0625rem;
            position: absolute;
            background: #E62739;
            -webkit-transition: left 0.28s ease, width 0.28s ease;
            transition: left 0.28s ease, width 0.28s ease;
            z-index: 2;
        }        
        .form-group input,
        .form-group textarea {
            display: block;
            background: none;
            margin-top: 28px;
            font-size: 18px;
            border: none;
            line-height: 1.9;
            width: 100%;
            color: transparent;
            -webkit-transition: all 0.28s ease;
            transition: all 0.28s ease;
            box-shadow: none;
        }        
        .form-group textarea:focus,
        .form-group textarea:valid,
        .form-group textarea.has-value {
            color: #333;
        }       
        .form-group textarea:focus~.control-label,
        .form-group textarea:valid~.control-label,
        .form-group textarea.form-file~.control-label,
        .form-group textarea.has-value~.control-label {
            font-size: 0.8rem;
            color: gray;
            top: -1rem;
            left: 0;
        }      
        .form-group textarea:focus {
            outline: none;
        }      
        .form-group textarea:focus~.control-label {
            color: #E62739;
        }      
        .form-group textarea:focus~.bar::before {
            width: 100%;
            left: 0;
        }
        /*END of textarea Material design CSS*/
        /*Breadcrumb CSS*/
        ul,
        li {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }
        #crumbs {
            height: 77px;
            border: 1px solid #e5e5e5;
            font-family: leela;
            font-size: 15px;
            font-weight: bold;
        }
        #crumbs li {
            float: left;
            line-height: 77px;
            padding-left: 85px;
        }
        #crumbs li a {
            background: url(/images/checkout/icons/crumbs.png) no-repeat right center;
            display: block;
            padding-right: 100px;
            color: #797979 !important;
        }
        /*END of Breadcrumb CSS*/
        /*Cart progress bar CSS*/
        .progress {
            height: 3px !important;
            border-radius: 10px !important;
            box-shadow: none;
            -webkit-box-shadow: none;
            background-color: transparent;
        }
        .progress-bar {
            box-shadow: none;
            border-radius: 10px;
            -webkit-box-shadow: none;
            background: #e62739;
        }
        .big-ht {
            height: 550px;
        }   
        .small-ht {
            height: 255px;
        }    
        input[type="text"]::-webkit-input-placeholder {
            color: #797979 !important;
        }       
        input[type="text"]:-moz-placeholder {
            color: #797979 !important;
        }  
        input[type="text"]::-moz-placeholder {
            color: #797979 !important;
        }    
        input[type="text"]:-ms-input-placeholder {
            color: #797979 !important;
        }
    </style>
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='checkoutCtrl'>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>CHECKOUT</li>
                        </ul>
                    </div>
                </div>
            </section>
            <div class="row well text-center no-products-in-cart" data-ng-hide="cart.length>0">
                No products in cart...
                <a class="shop-more" href="/" target="_self">Shop more</a>
            </div>
            <div data-ng-show="cart.length>0">
                <div class="tabs-left">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul id="crumbs" class="cartGrad" role="tablist">
                                <li id="a-tab" role="presentation">
                                    <a role="tab" data-toggle="tab" class="active">Cart</a>
                                </li>
                                <li id="b-tab" role="presentation">
                                    <a role="tab" data-toggle="tab" class="grey">Account details</a>
                                </li>
                                <li id="c-tab" role="presentation">
                                    <a role="tab" data-toggle="tab" class="">Shipping details</a>
                                </li>
                                <li id="d-tab" role="presentation">
                                    <a role="tab" data-toggle="tab" style="background: none;">Product summary</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="pad-l-15">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 9%;">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 tab-content no-padding">
                            <div role="tabpanel" class="row tab-pane active pad-t-0" id="a">
                                <div class="panel-body no-pad-lr w-95pc block-center">
                                    <div style="box-shadow: none;" class="col-lg-12 m-tb-20 b1-b-e8 vivo-panel-checkout-product no-pad" data-ng-class="c.item.is_combo=='1'?'big-ht':'small-ht'" data-ng-repeat="c in cart track by $index">
                                        <div class="row">
                                            <div class="col-md-3 no-pad-lr">
                                                <img data-ng-src="/images/products-v2/{{c.item.VC_SKU}}-1.jpg" style="border: 1px solid #e8e8e8;width:232px;height:210px;" alt="{{c.item.title}}">
                                            </div>
                                            <div class="col-md-5 no-pad-lr pad-t-20">
                                                <div class="row">
                                                    <div class="col-md-12 text-left pad-t-0 pad-b-20">
                                                        <span style="font-size: 20px;font-weight: bold;font-family:'leela';">
        <a class="lgray-79" data-ng-href="/product/{{c.item.urlslug}}.html" target="_self">{{c.item.title}}</a>
       </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <img data-ng-src="/images/header/brands logos dropdown/{{c.item.supplier_name}} hover.png" style="width:50px;" title={{c.item.supplier_name}} alt="{{c.item.supplier_name}}">
                                                    </div>
                                                </div>
                                                <div class="row pad-t-15">
                                                    <div class="col-md-12">
                                                        <a class="btn btn-xs btn-vivo exclude subtract-quantity" data-ng-click="subQuantity(c,$index)">-</a> {{c.quantity}}
                                                        <a data-ng-click="addQuantity(c,$index)" class="btn btn-xs btn-vivo exclude add-quantity">+</a>
                                                    </div>
                                                </div>
                                                <div data-ng-show="c.item.ring_size != null && c.item.ring_size != '' && c.item.ring_size != 0" class="row pad-t-15">
                                                    <div class="col-md-12 lgray-79">
                                                        Size : {{c.item.ring_size}}
                                                    </div>
                                                </div>
                                                <div data-ng-show="c.item.bangle_size != null && c.item.bangle_size != '' && c.item.bangle_size != 0" class="row pad-t-15">
                                                    <div class="col-md-12 lgray-79">
                                                        Size : {{c.item.bangle_size}}
                                                    </div>
                                                </div>
                                                <div data-ng-show="c.item.bracelet_size != null && c.item.bracelet_size != '' && c.item.bracelet_size != 0" class="row pad-t-15">
                                                    <div class="col-md-12 lgray-79">
                                                        Size : {{c.item.bracelet_size}}
                                                    </div>
                                                </div>
                                                <div class="row pad-t-15">
                                                    <div class="col-md-12">
                                                        <a data-ng-click="removeFromCart($index)">
                                                            <img class="exclude middle" data-ng-src="/images/checkout/icons/remove.png" style="padding-right: 10px;" alt="remove">
                                                            <span class="remove-link middle">Remove</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 no-pad-lr pad-t-20">
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr" style="padding-right: 24px !important;">
                                                        Price
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        RS.&nbsp;{{c.item.price_before_discount | INR}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr">
                                                        Discount
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        {{c.item.discount}} %
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr">
                                                        Quantity
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        {{c.quantity}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr">
                                                        Subtotal
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        RS.&nbsp;{{c.item.price_after_discount*c.quantity | INR}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-ng-if="c.item.is_combo=='1'" class="pad-t-20">
                                            <span class="free-gift-text">
                                                Free Gift
                                            </span>
                                        </div>
                                        <div class="row pad-t-20" data-ng-if="c.item.is_combo=='1'" data-ng-init="getCombo(c.item.combo_id)">
                                            <div class="col-lg-3 no-pad-l">
                                                <img data-ng-src="/images/products-v2/{{combo.VC_SKU}}-1.jpg" style="border: 1px solid #e8e8e8;width:232px; height:210px;" alt="{{combo.title}}">
                                            </div>
                                            <div class="col-lg-5">
                                                <span class="bold theme-text t13" style="font-size: 20px;">
                                                  <a class="lgray-79">
                                                    {{combo.title}}
                                                  </a>
                                                </span>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-lg-7 rightfont no-pad-lr pad-r-25">
                                                        Price
                                                    </div>
                                                    <div class="col-lg-5 rightfont no-pad-lr" align="right">
                                                        RS.&nbsp;{{combo.price_before_discount | INR}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-7 rightfont no-pad-lr">
                                                        Quantity
                                                    </div>
                                                    <div class="col-lg-5 rightfont no-pad-lr" align="right">
                                                        1
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-7 rightfont no-pad-lr">
                                                        Subtotal
                                                    </div>
                                                    <div class="col-lg-5 rightfont no-pad-lr" align="right">
                                                        FREE
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- start free coin-->
                                        <div data-ng-if="pcoin != ''" class="pad-t-20">
                                            <span class="free-gift-text">
                                                Free Gift
                                            </span>
                                        </div>
                                        <div class="row pad-t-20" data-ng-if="pcoin != ''" data-ng-init="getCombo(pcoin.id)">
                                            <div class="col-lg-3 no-pad-l">
                                                <img data-ng-src="/images/products-v2/{{combo.VC_SKU}}-1.jpg" style="border: 1px solid #e8e8e8;width:232px; height:210px;" alt="{{combo.title}}">
                                            </div>
                                            <div class="col-lg-5">
                                                <span class="bold theme-text t13" style="font-size: 20px;">
                                                  <a class="lgray-79">
                                                    {{combo.title}}
                                                  </a>
                                                </span>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-lg-7 rightfont no-pad-lr pad-r-25">
                                                        Price
                                                    </div>
                                                    <div class="col-lg-5 rightfont no-pad-lr" align="right">
                                                        RS.&nbsp;{{combo.price_before_discount | INR}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-7 rightfont no-pad-lr">
                                                        Quantity
                                                    </div>
                                                    <div class="col-lg-5 rightfont no-pad-lr" align="right">
                                                        1
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-7 rightfont no-pad-lr">
                                                        Subtotal
                                                    </div>
                                                    <div class="col-lg-5 rightfont no-pad-lr" align="right">
                                                        FREE
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!--end free coin-->
                                </div>
                                <div class="row">
                                    <div class="col-lg-7"></div>
                                    <div class="col-lg-5 no-pad-lr">
                                        <div class="row">
                                            <div class="col-lg-9 col-md-10 theme-text t15 no-pad-lr">
                                                <input type="text" Placeholder="Apply promo code" class="promocode exclude pull-right" size="100" data-ng-model="ccode" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Apply promo code'">
                                            </div>
                                            <div class="col-lg-3 col-md-2 no-pad-lr">
                                                <a data-ng-click="applyCode()" class="btn btn-sm btn-block btn-vivo theme-text t15">Apply</a>
                                            </div>
                                        </div>
                                        <div class="row" data-ng-if="isCouponApplied" style="padding-top:12px;">
                                            <div class="col-lg-12 text-right">
                                                <span class="lgray-79 t15">
    <img class="exclude pad-r-10" src="/images/checkout/icons/tick.png">
         {{disp_code}} promo code applied successfully &nbsp;&nbsp;&nbsp;
    <a data-ng-click="removeCode()">Remove</a>
    </span>
                                            </div>
                                        </div>
                                        <div class="row price_box_checkout_page theme-text t13 m-t-25 m-l-0">
                                            <div class="col-md-6 pad-tb-10 text-center ">
                                                <h5 class="total-price-text">
                                                    TOTAL
                                                </h5>
                                            </div>
                                            <div class="col-md-6 text-center pad-tb-10">
                                                <h5 class="total-price-text ">
                                                    RS. {{total | INR}}
                                                </h5>
                                            </div>
                                        </div>
                                        <a data-ng-click="confirmOrder(total)" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" aria-expanded="true" class="proceedbutton btn btn-block btn-vivo-checkout m-tb-15">
                                            PROCEED TO ACCOUNT DETAILS
                                        </a>
                                        <h6 class="m-t-15 text-right">
                                            <a href="/" target="_self" class="continue-shopping-text">
                                               Continue shopping
                                            </a>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane row no-pad" id="b">
                                <div class="panel-body no-pad">
                                    <div class="col-sm-6 m-t-0 pad-t-0 b1-r-eee">
                                        <div class="vivo-login-panel">
                                            <h4 class="text-center gothbody t15">New Customer</h4>
                        <div class="register_account">
                            <div class="wrap">
                                <form name="form">
                                    <div class="row text-center">
                                        <div class="col-lg-12 placeholdlight pad-2">
                                            <div class="group m-b-15 pad-b-10">
                                                <input class="exclude" type="text" data-ng-model="customer_new.name" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Name
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="group m-b-15 pad-b-10">
                                                <input class="exclude" type="text" data-ng-model="customer_new.phone" maxlength="13" pattern="(\+?\d[- .]*){7,13}" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Phone Number
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="group m-b-15 pad-b-10">
                                                <input class="exclude" type="email" data-ng-model="customer_new.email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Email
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="group m-b-15">
                                                <input class="exclude" type="password" data-ng-class="isPasswordError?'error-text':'correct-text'" name="password" data-ng-model="customer_new.password" required>{{message}}
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Password
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="row theme-text lgray-79 t15" align="left">
                                                <p>
                                                    By clicking 'Create Account' you agree to the
                                                    <a class="underline lgray-79" href="/terms" target="_blank">Terms &amp; Conditions</a>.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div>
                                    <input class="always-bold pad-10 t18 w-75pc block-center btn btn-vivo-bold bold-400 uppercase" type="submit" name="Submit" data-ng-click="signup()" value="Create an Account">
                                </div>
                            </div>
                        </div>             
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="vivo-login-panel">
                                            <h4 class="text-center gothbody t15">Registered Customers</h4>
                        <div class="register_account">
                            <div class="wrap">
                                <form>
                                    <div class="row text-center">
                                        <div class="col-lg-12 placeholdlight pad-2">
                                            <div class="group m-b-15 pad-b-10">
                                                <input class="exclude" id="modlgn_username" data-ng-model="customer.email" type="text" name="email" size="18" autocomplete="off" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Email
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="group m-b-15">
                                                <input class="exclude" type="password" id="modlgn_passwd" name="password" data-ng-model="customer.password" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label>Password
             <sup class="required-star">*</sup>
            </label>
                                            </div>
                                            <div class="underline" align="left">
                                                <a class="theme-text lgray-79 t13 pointer" data-toggle="modal" data-target="#myModal">Forgot Your Password ? </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="m-tb-20">
                                    <input class="always-bold pad-10 t18 w-75pc block-center btn btn-vivo-bold bold-400 uppercase" type="submit" name="Submit" data-ng-click="loginUser(customer)" value="Login">
                                </div>
                                <div class="row m-t-20 text-center bold t15">OR</div>
                                <div class="row">
                                    <div ng-click="authenticate('facebook')" class="col-lg-6 col-md-12 m-tb-20">
                                        <div class="btn facebook-login-button row">
                                            <div class="col-md-8 login-with-text">
                                                Login with
                                            </div>
                                            <div class="col-md-4 no-pad-lr">
                                                <img class="exclude" src="/images/login/icons/facebook login.png" alt="Facebook login">
                                            </div>
                                        </div>
                                    </div>
                                    <div ng-click="authenticate('google')" class="col-lg-6 col-md-12 m-tb-20">
                                        <div class="btn google-plus-login-button row">
                                            <div class="col-md-8 login-with-text">
                                                Login with
                                            </div>
                                            <div class="col-md-4 no-pad-lr">
                                                <img class="exclude" src="/images/login/icons/google login.png" alt="Google login">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <fb:login-button scope="public_profile,email" show-faces="true" max-rows="1" size="large"></fb:login-button>
                            </div>
                        </div>   
                                        </div>
                                    </div>
                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel" style="font-family: 'leela';">Enter your email</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input class="exclude" type="email" data-ng-model="femail" required>
                                                </div>
                                                <div class="modal-footer">
                                                    <a data-ng-click="forgotPassword()" class="btn btn-vivo btn-sm" style="font-family: 'leela';"> Forgot Password</a>
                                                    <a class="btn btn-default btn-sx" data-dismiss="modal" style="font-family: 'leela';">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" data-ng-init="isNewAddress=true;" class="tab-pane row pad-t-0" id="c">
                                <div class="panel-body no-pad">
                                    <div class="row">
                                        <div class="col-sm-6 no-pad">
                                            <div class="vivo-login-panel no-pad-lr pad-t-0 m-t-0">
                                                <h4 class="text-center bold theme-text t13 lgray-79 m-t-0 m-b-30 t15">
                                                    Shipping Address
                                                </h4>
                                                <div class="register_account">
                                                    <div class="wrap">
                                                        <div class="row vivo-row">
                                                            <div class="col-lg-12 placeholdlight group m-b-20">
                                                                <input type="text" class="theme-text t13 exclude" data-ng-model="ship_address.name" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>Name
       <sup class="required-star">*</sup>
      </label>
                                                            </div>
                                                        </div>
                                                        <div class="row vivo-row m-b-20">
                                                            <div class="col-lg-12 placeholdlight form-group m-b-20">
                                                                <textarea type="text" class="theme-text t13 b1-b-eee" data-ng-model="ship_address.first_line" required></textarea>
                                                                <span class="highlight"></span>
                                                                <i class="bar"></i>
                                                                <label class="block b1-t-8" for="textarea">Address
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                        </div>
                                                        <div class="row vivo-row m-b-20">
                                                            <div class="col-md-6 placeholdlight group m-b-20">
                                                                <input type="text" class="theme-text exclude t13" data-ng-model="ship_address.phone" style="width: 150px;" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>Phone No
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                            <div class="col-md-6 placeholdlight group m-b-20">
                                                                <input type="text" class="theme-text t13 exclude" data-ng-model="ship_address.pin" style="width: 150px;" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>Pincode
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                        </div>
                                                        <div class="row vivo-row">
                                                            <div class="col-md-6 placeholdlight group m-b-20">
                                                                <input type="text" class="theme-text t13 exclude" data-ng-model="ship_address.city" style="width:150px;" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>City
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                            <div class="col-md-6 placeholdlight group m-b-20">
                                                                <input type="text" class="theme-text t13 exclude" data-ng-model="ship_address.state" style="width:150px;" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>State
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                        </div>
                                                        <div class="row vivo-row">
                                                            <div class="col-lg-2">
                                                                <input class="exclude" type="checkbox" name="address" data-ng-model="isNewAddress" style="width: 50%;">
                                                            </div>
                                                            <div class="col-lg-10 no-padding">
                                                                <h class="theme-text t13 text-left lgray-79 t15">
                                                                    My billing address is the same as shipping address
                                                                </h>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 no-pad m-t-0">
                                            <div data-ng-show="!isNewAddress" class="vivo-login-panel pad-t-0 no-pad-l" style="border-left: 1px solid #E5E5E5;">
                                                <h4 class="text-center m-t-0 theme-text t13 bold uppercase lgray-79 no-pad-l" style="margin-bottom: 30px;font-size:15px;">
                                                    Billing Address
                                                </h4>
                                                <div class="register_account">
                                                    <div class="wrap">
                                                        <div class="row vivo-row m-b-20">
                                                            <div class="col-lg-12 placeholdlight group" style="margin-bottom:0px">
                                                                <input type="text" class="theme-text t13 exclude" data-ng-model="address.name" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>Name
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                        </div>
                                                        <div class="row vivo-row m-b-20">
                                                            <div class="col-lg-12 placeholdlight form-group m-b-20">
                                                                <textarea type="text" class="theme-text t13" data-ng-model="address.first_line" required></textarea>
                                                                <label for="textarea" class="theme-text t13">Address
        <sup class="required-star">*</sup>
       </label>
                                                                <i class="bar"></i>
                                                            </div>
                                                        </div>
                                                        <div class="row vivo-row m-b-20">
                                                            <div class="col-md-6 placeholdlight group m-b-20">
                                                                <input type="text" class="theme-text t13 exclude" data-ng-model="address.phone" style="width:150px;" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>Phone No
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                            <div class="col-md-6 placeholdlight group m-b-20">
                                                                <input type="text" class="vivo-input exclude" data-ng-model="address.pin" style="width:150px;" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>Pincode
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                        </div>
                                                        <div class="row vivo-row">
                                                            <div class="col-md-6 placeholdlight group m-b-20">
                                                                <input type="text" class="vivo-input exclude" data-ng-model="address.city" style="width:150px;" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>City
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                            <div class="col-md-6 placeholdlight m-b-20">
                                                                <input type="text" class="vivo-input exclude" data-ng-model="address.state" style="width:150px;" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>State
        <sup class="required-star">*</sup>
       </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="w-50pc block-center">
                                            <input class="exclude btn btn-vivo-checkout proceedbutton m-b-50" type="submit" name="Submit" data-ng-click="addAddress()" value="PROCEED TO PRODUCT SUMMARY">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane row pad-t-0" id="d">
                                <div class="panel-body theme-text t15">
                                    <div class="row pad-b-10">
                                        <div class="col-md-2 lgray-79 bold">
                                            Name
                                        </div>
                                        <div class="col-md-1 no-pad-lr">
                                            :
                                        </div>
                                        <div class="col-md-8 lgray-79 no-pad-lr">
                                            {{order.name}}
                                        </div>
                                    </div>
                                    <div class="row pad-b-10">
                                        <div class="col-md-2 lgray-79 bold">
                                            Email
                                        </div>
                                        <div class="col-md-1 no-pad-lr">
                                            :
                                        </div>
                                        <div class="col-md-8 lgray-79 no-pad-lr">
                                            {{order.email}}
                                        </div>
                                    </div>
                                    <div class="row pad-b-10">
                                        <div class="col-md-2 lgray-79 bold">
                                            Phone
                                        </div>
                                        <div class="col-md-1 no-pad-lr">
                                            :
                                        </div>
                                        <div class="col-md-8 lgray-79 no-pad-lr">
                                            {{order.phone}}
                                        </div>
                                    </div>
                                    <div class="row pad-b-10">
                                        <div class="col-md-2 lgray-79 bold">
                                            Billing Address
                                        </div>
                                        <div class="col-md-1 no-pad-lr">
                                            :
                                        </div>
                                        <div class="col-md-8 lgray-79 no-pad-lr">
                                            {{order.first_line}} {{order.city}} {{order.state}} - {{order.pin}} {{order.country}}
                                        </div>
                                    </div>
                                    <div class="row pad-b-10 m-b-20">
                                        <div class="col-md-2 lgray-79 bold">
                                            Shipping Address
                                        </div>
                                        <div class="col-md-1 no-pad-lr">
                                            :
                                        </div>
                                        <div class="col-md-8 lgray-79 no-pad-lr">
                                            {{ship.first_line}} {{ship.city}} {{ship.state}} - {{ship.pin}} {{ship.country}}
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center no-pad-lr product-summary-structure" data-ng-repeat="c in products">
                                        <div class="row">
                                            <div class="col-md-3 no-pad-lr">
                                                <img class="product-summary-image exclude" data-ng-src="/images/products-v2/{{c.item.VC_SKU}}-1.jpg" alt="{{c.item.title}}">
                                            </div>
                                            <div class="col-md-5 pad-t-20">
                                                <div class="row">
                                                    <div class="col-md-12 text-left pad-t-0 pad-b-20">
                                                        <span class="bold theme-text t20">
           <a class="lgray-79" data-ng-href="/product/{c.item.urlslug}}.html" target="_self">
                   {{c.item.title}}
           </a>
           </span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <img data-ng-src="/images/header/brands logos dropdown/{{c.item.supplier_name}} hover.png" style="float: left;width: 50px;" alt="{{c.item.supplier_name}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 no-pad-lr pad-t-20">
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-l" style="padding-right: 23px !important;">
                                                        Price
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        RS.&nbsp;{{c.item.price_before_discount | INR}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr">
                                                        Discount
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        {{c.item.discount}} %
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr" style="padding-bottom: 0px !important;">
                                                        Quantity
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr pad-b-0" align="right">
                                                        {{c.quantity}}
                                                    </div>
                                                </div>
                                                <hr style="color:#e8e8e8;margin-top:0px;margin-bottom: 10px;">
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr">
                                                        Subtotal
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        RS.&nbsp;{{c.item.price_after_discount*c.quantity | INR}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-center no-pad-lr product-summary-structure" data-ng-if="pcoin != ''" data-ng-init="getCombo(pcoin.id)">
                                        <div class="row pad-t-20 text-left">
                                            <span class="free-gift-text">
                                                Free Gift
                                            </span>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 no-pad-lr">
                                                <img class="product-summary-image exclude" data-ng-src="/images/products-v2/{{combo.VC_SKU}}-1.jpg" alt="{{combo.title}}">
                                            </div>
                                            <div class="col-md-5 pad-t-20">
                                                <div class="row">
                                                    <div class="col-md-12 text-left pad-t-0 pad-b-20">
                                                        <span class="bold theme-text t20">
                                                           <a class="lgray-79" data-ng-href="/product/{combo.urlslug}}.html" target="_self">
                                                                   {{combo.title}}
                                                           </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 no-pad-lr pad-t-20">
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-l" style="padding-right: 23px !important;">
                                                        Price
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        RS.&nbsp;{{combo.price_before_discount | INR}}
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr" style="padding-bottom: 0px !important;">
                                                        Quantity
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr pad-b-0" align="right">
                                                        1
                                                    </div>
                                                </div>
                                                <hr style="color:#e8e8e8;margin-top:0px;margin-bottom: 10px;">
                                                <div class="row">
                                                    <div class="col-md-7 rightfont no-pad-lr">
                                                        Subtotal
                                                    </div>
                                                    <div class="col-md-5 rightfont no-pad-lr" align="right">
                                                        FREE
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row pad-t-10 pad-b-15">
                                    <div class="col-md-12 text-center">
                                        <span class="pad-r-50">
                                            <img src="/images/checkout/icons/gift wrap.png" class="gift-wrap-position exclude" alt="gitft wrap">
                                            <span class="t15">I want to gift wrap</span>
                                        </span>
                                        <input class="inline exclude pointer" name="radioGroup1" id="radio3" data-ng-model="isGift" value="1" type="radio">
                                        <label class="inline theme-text t15 lgray-79 m-r-50">Yes</label>
                                        <span>
                                            <input class="inline exclude pointer" name="radioGroup1" id="radio4" data-ng-model="isGift" value="0" checked="" type="radio">
                                            <label class="inline theme-text t15 pad-l-20 lgray-79 ">No</label>
                                        </span>
                                    </div>
                                </div>
                                <div class="row pad-tb-25" style="background-color:#e8e8e8; border-bottom: #a6a6a6 2px solid;">
                                    <div class="col-md-3 no-pad-r pad-t-10">
                                        <h class="theme-text t20 lgray-79 pad-l-20">PAYMENT MODE : </h>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-12 no-pad-r text-left">
                                                <div class="row">
                                                    <input class="inline pointer exclude" type="radio" data-ng-model="isCod" name="cod2" value="1">
                                                    <h class="theme-text lgray-79 t20">
                                                        COD
                                                    </h><br>
                                                    <input class="inline pointer exclude" type="radio" data-ng-model="isCod" name="cod2" value="0">
                                                    <h class="theme-text lgray-79 t20">
                                                        Online Payment
                                                    </h>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 text-right pad-t-10">
                                        <span class="grand-total-text">
                                           Grand Total : {{total | INR}}/-
                                       </span>
                                    </div>
                                </div>
                                <div class="row pad-t-30">
                                    <div class="col-md-12 m-tb-30">
                                        <a class="btn btn-vivo exclude pad-15 w-35pc block-center t18" data-ng-click="finalConfirmation(total)">
                                            Confirm Order
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoCheckout.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>