<!DOCTYPE html>
<html lang="en" data-ng-app="vivoWishlist">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta name="keywords" content="" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/wishlist" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="My Wishlist | VivoCarat.com" />
    <meta property="og:description" content="VivoCarat is an Online Jewellery store for gold & diamond rings, Earrings, Pendants with latest designs from trusted brands at lowest prices." />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>My Wishlist | VivoCarat.com</title>
    <meta name="keywords" content="account,jewellery, online jewellery, jewelry, engagement rings, wedding rings, diamond rings, rings, diamond earrings, gold earrings" />
    <meta name="description" content="VivoCarat is an Online Jewellery store for gold & diamond rings, Earrings, Pendants with latest designs from trusted brands at lowest prices." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/wishlist">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/wishlist">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/wishlist" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <style>
        /*Arrow nav tabs CSS*/
        .arrow {
            position: relative;
            color: white !important;
            margin-bottom: 5px;
        }
        .red-gradient {
            height: 20px;
            padding: 10px;
            width: 200px;
            color: #000000;
            background: #e62739;
        }
        .red {
            color: white !important;
            background: #e62739;
            padding: 8px 97px 8px 25px;
            font-weight: bold;
        } 
        .red:hover {
            color: white !important;
            background: #e62739;
            padding: 8px 97px 8px 25px;
        }
        /*Arrow tip css*/
        #arrow4:after {
            content: '';
            height: 0;
            display: block;
            border-color: transparent transparent transparent #e62739;
            border-width: 17px;
            border-style: solid;
            position: absolute;
            top: -1px;
            left: 173px;
        }
        /*END of Arrow nav tabs CSS*/
    </style>    
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='wishlistCtrl'>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>WISHLIST</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="exclude m-t-0 m-b-45">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="account-nav">
                            <li class="current">
                                <a class="inactive-tab-text-colour" href="/wishlist" target="_self">Wishlist</a>
                            </li>
                            <li>
                                <a class="inactive-tab-text-colour" href="/orders" target="_self">Order history</a>
                            </li>
                            <li>
                                <a href="/account" target="_self">Account details</a>
                            </li>
                            <li>
                                <a class="inactive-tab-text-colour" href="/address" target="_self">Address</a>
                            </li>
                        </ul>
                    </div>
                    <div data-ng-hide="!authenticated || isLoaded">
                        <div class="col-md-9 text-center home-panel-row">
                            <div class="well">Please wait...</div>
                        </div>
                    </div>
                    <div data-ng-show="!isauthenticated || isLoaded" class="register_account col-md-9 pad-0">
                        <div class="well not-loggedin-pad" data-ng-show="!authenticated">
                            <h4 class="normal-text">
                                Not Logged In
                            </h4>
                            <p class="normal-text m-b-20">
                                Kindly <a class="link-hover" href="/login" target="_self">login</a> to see your wishlist.
                            </p>
                        </div>
                        <div class="m-b-20" data-ng-show="authenticated && watchlist.length<1" style="padding: 36px;">
                            <p class="normal-text m-b-20">Your wishlist is empty.</p>
                        </div>
                        <div data-ng-hide="!authenticated" class="col-md-9 pad-r-0">
                            <div class="row m-t-0" data-ng-show="watchlist!=null && watchlist.length>0">
                                <div class="col-md-12 no-pad-lr pad-b-20" data-ng-repeat="c in watchlist">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img data-ng-src="/images/products-v2/{{c.VC_SKU}}-1.jpg" style="border: 1px solid #eee;" alt="{{c.title}}">
                                        </div>
                                        <div class="col-md-5 pad-lr-0">
                                            <div class="row m-b-10">
                                                <h3 class="normal-text bold lgray-79 exclude m-0">
                                                    <a class="lgray-79" data-ng-href="/product/{{c.urlslug}}.html" target="_self">{{c.title}}</a>
                                                </h3>
                                            </div>
                                            <div class="row m-b-10">
                                                <img class="exclude" style="width:50px;" data-ng-src="/images/header/brands logos dropdown/{{c.supplier_name}} hover.png" alt="{{c.supplier_name}}">
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-4 no-pad bold normal-text theme-text-special pad-l-0">
                                                    RS.&nbsp;{{c.price_after_discount | INR}}
                                                </div>
                                                <div class="col-xs-4 no-pad pad-l-0 normla-text lgray-79" style="text-decoration:line-through;">
                                                    RS.&nbsp;{{c.price_before_discount | INR}}
                                                </div>
                                                <div class="col-xs-4 no-pad pad-l-0 normal-text lgray-79">
                                                    {{c.discount}}% OFF
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 no-pad-lr m-t-10">
                                                    <div class="row" data-ng-show="c.category=='Rings'">
                                                        <div class="col-md-6 m-t-5 no-pad-lr">
                                                            <h class="ring-size-text">
                                                                Ring size&nbsp;:&nbsp;
                                                            </h>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div data-ng-init="c.ring_size=null" data-ng-show="c.category=='Rings'" class="col-sm-12 bg-e2 styled-select no-pad-lr">
                                                                <select data-ng-model="c.ring_size" class="bg-none">
                                                             <option value="SIZE" disabled selected>SIZE</option>
                                                             <option data-ng-repeat="s in c.psize">{{s.ring_size}}</option>
                                                        </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 pad-lr-0 underline m-t-15" data-ng-show="c.category=='Rings'">
                                                            <a href="/find-your-ring-size" target="_blank" class="ring-guide-text">
                                                        Ring size guide
                                                    </a>
                                                        </div>
                                                    </div>
                                                    <div class="row" data-ng-show="c.category=='Bracelets'">
                                                        <div class="col-md-6 no-pad-lr m-t-5">
                                                            <h class="ring-size-text">
                                                                Bracelet size &nbsp;:&nbsp;
                                                            </h>
                                                        </div>
                                                        <div class="col-md-6 styled-select" data-ng-show="c.category=='Bracelets'" >
                                                            <div class="bg-e2">
                                                                <select class="bg-none" data-ng-model="c.bracelet_size">
                                                                   <option data-ng-repeat="s in c.psize">{{s.bracelet_size}}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row m-t-10" data-ng-show="c.category=='Bangles'">
                                                        <div class="col-md-6 no-pad-lr m-t-5">
                                                            <h class="ring-size-text">
                                                                Bangle size &nbsp;:&nbsp;
                                                            </h>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div data-ng-show="c.category=='Bangles'" class="col-sm-12 pad-lr-0 styled-select">
                                                                <div class="bg-e2">
                                                                    <select class="bg-none" data-ng-model="c.bangle_size">
                                                                    <option data-ng-repeat="s in c.psize">{{s.bangle_size}}</option>
                                                                </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12 no-pad-lr text-right rightrem">
                                                    <a class="remove-link h-full" data-ng-click="removeFromWatchlist(c.id)">
                                                        <img class="exclude pad-r-5 middle" src="/images/checkout/icons/remove.png" alt="remove">
                                                        <span class="middle">Remove</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row pull-right">
                                                <div class="col-md-12 no-pad-lr" style="padding-top: 85px;">
                                                    <a class="pad-t-5 btn small-button btn-vivo" data-ng-click="addToCartFromWishlist(c)"> 
                                                        ADD TO CART 
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoWishlist.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>