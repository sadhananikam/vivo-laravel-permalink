<!DOCTYPE html>
<html lang="en" data-ng-app="vivoContactus">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/contact-us" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Contact Us | VivoCarat" />
    <meta property="og:description" content="We love to hear from you. Contact VivoCarat for any questions, queries or just to say Hi." />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>Contact Us | VivoCarat</title>
    <meta name="keywords" content="contact vivocarat,reach us,customer support" />
    <meta name="description" content="We love to hear from you. Contact VivoCarat for any questions, queries or just to say Hi." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/contact-us">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/contact-us">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/contact-us" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='contactusCtrl'>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>CONTACT US</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="exclude m-t-0 m-b-30">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hr"><span>CONTACT US</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pad-t-50">
                        <div class="row">
                            <div class="col-md-6 no-pad-lr">
                                <h3 class="heading bold">General</h3>

                                <p class="contact-information">
                                    +91 9167 645 314
                                    <br>hello@vivocarat.com
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h3 class="heading bold">Seller Connect</h3>

                                <p class="contact-information">
                                    +91 9503 781 870 partners@vivocarat.com
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 pad-t-50 no-pad-lr">
                                <h3 class="heading bold">Address</h3>

                                <address class="contact-information">
                                302,Swaroop Centre <br>
                                JB Nagar Circle, Chakala <br>
                                Andheri East <br>
                                Mumbai - 400 059. <br>
                            </address>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 pad-t-5">
                        <div class="row">
                            <div class="col-md-12 have-a-query">
                                HAVE A QUERY?
                            </div>
                        </div>
                        <div class="row">
                            <form name="contactForm" ng-submit="saveContactform()" autocomplete="off" novalidate>
                                <div class="row contact-fields w-full exclude-background">
                                    <div class="col-md-12">
                                        <div class="row pad-t-20">
                                            <div class="col-md-2 t13 lgray-79 no-pad-lr bold">
                                                Name
                                            </div>
                                            <div class="col-md-1 t13 lgray-79 bold">
                                                :
                                            </div>
                                            <div class="col-md-9 no-pad-lr">
                                                <input class="exclude contact-fields exclude" type="text" Placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" data-ng-model="contact.cname" name="cname" id="cname" data-ng-required="true" data-ng-pattern="/^[a-zA-Z\s]+$/">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-9 col-md-offset-3 t13 lgray-79" data-ng-show="contactForm.cname.$dirty && contactForm.cname.$error.required">Name is required</div>
                                                <div class="col-md-9 col-md-offset-3 t13 lgray-79" data-ng-show="contactForm.cname.$dirty && contactForm.cname.$error.pattern">Name can contain only alphabets</div>
                                            </div>
                                        </div>
                                        <div class="row pad-t-20">
                                            <div class="col-md-2 t13 lgray-79 no-pad-lr bold">
                                                Email
                                            </div>
                                            <div class="col-md-1 t13 lgray-79 bold">
                                                :
                                            </div>
                                            <div class="col-md-9 no-pad-lr">
                                                <input class="contact-fields exclude" type="email" Placeholder="Enter your email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email'" data-ng-model="contact.cemail" name="cemail" id="cemail" data-ng-required="true">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-9 col-md-offset-3 t13 lgray-79" data-ng-show="contactForm.cemail.$dirty && contactForm.cemail.$error.required">Email is required</div>
                                                <div class="col-md-9 col-md-offset-3 t13 lgray-79" data-ng-show="contactForm.cemail.$dirty && contactForm.cemail.$error.email">Invalid Email</div>
                                            </div>
                                        </div>
                                        <div class="row pad-t-20">
                                            <div class="col-md-2 t13 lgray-79 no-pad-lr bold">
                                                Phone
                                            </div>
                                            <div class="col-md-1 t13 lgray-79 bold">
                                                :
                                            </div>
                                            <div class="col-md-9 no-pad-lr">
                                                <input class="contact-fields exclude" type="text" Placeholder="Enter your phone number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your phone number'" data-ng-model="contact.cphone" name="cphone" id="cphone" data-ng-required="true" data-ng-pattern="/^[0-9]{10,10}$/">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-9 col-md-offset-3 t13 lgray-79" data-ng-show="contactForm.cphone.$dirty && contactForm.cphone.$error.required">Phone is required</div>
                                                <div class="col-md-9 col-md-offset-3 t13 lgray-79" data-ng-show="contactForm.cphone.$dirty && contactForm.cphone.$error.pattern">Enter correct phone number</div>
                                            </div>
                                        </div>
                                        <div class="row pad-t-20 pad-b-20">
                                            <div class="col-md-2 t13 lgray-79 no-pad-lr bold">
                                                Message
                                            </div>
                                            <div class="col-md-1  lgray-79 bold">
                                                :
                                            </div>
                                            <div class="col-md-9 no-pad-lr">
                                                <input class="contact-fields exclude" type="text" Placeholder="Enter your message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your message'" data-ng-model="contact.cmessage" name="cmessage" id="cmessage">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button class="btn contact-button" type="submit" ng-disabled="!contactForm.$valid">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row m-tb-50 google_map">
                    <div class="col-md-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.0342944589356!2d72.8632563144684!3d19.106151387070582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c8478e27b5d5%3A0xf3d09043b2998e9e!2sSwaroop+Centre!5e0!3m2!1sen!2sin!4v1482234650022" width="1111" height="370" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoContactus.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>