<!DOCTYPE html>
<html lang="en" data-ng-controller='searchCtrl' data-ng-app="vivoSearch">
<head>
    <title>$title</title>
    <meta name="keywords" content="$meta_keywords">
    <meta name="description" content="$meta_description" />
    <meta name="robots" content="$meta_robots">
    <meta property="og:url" content="$og_url" />
    <meta property="og:description" content="$og_description" />
    <meta property="og:title" content="$og_title" />
    <meta property="og:type" content="$og_type" />
    <meta property="og:image" content="$og_image" />
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <!-- SEO-->
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="$canurl">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="$moburl">
    <link rel="alternate" media="handheld" href="$moburl" />
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <style>
        .no-padding {
            padding: 0px;
        } 
        .no-pad {
            padding: 0px;
        }     
        .ngrs-range-slider .ngrs-join {
            position: absolute;
            z-index: 1;
            top: 50%;
            left: 0;
            right: 100%;
            height: 8px;
            margin: -4px 0 0 0;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            background-color: #e62739;
            background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
            background-size: 100%;
            background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #E22B46), color-stop(100%, #E22B46));
            background-image: -moz-linear-gradient(#5bc0de, #2f96b4);
            background-image: -webkit-linear-gradient(#5bc0de, #2f96b4);
            background-image: linear-gradient(#e62739, #e62739);
        }
        .vivo-grid {
            padding: 3px;
            width: 100%;
            height: 334px;
        }   
        .vivo-box-padding {
            padding: 2px;
        }
        /*Done to adjust dropdown of jewellers within the filter box area*/ 
        .k-animation-container {
            height: 117px !important;
        }  
        .k-list-container {
            height: 117px !important;
        } 
        .k-list-scroller {
            height: 117px !important;
        }  
        .k-popup.k-list-container {
            padding-top: 0px !important;
        }
    </style>    
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div>
        <div class="container no-pad-lr">
            <div class="main">
                <div class="row">
                    <div class="col-sm-12 cont no-pad-lr">
                        <div class="row">
                            <div class="col-xs-12 pad-t-30 pad-lr-30">
                                <img class="img-responsive w-full" ng-src="{{banImg}}" err-src="{{ defaultImage }}">
                            </div>
                        </div>
                        <div class="container pad-lr-30">
                            <section class="exclude m-tb-15">
                                <div class="row">
                                    <div class="col-xs-12 pad-lr-0">
                                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                                            <li>$tagtodisplay</li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                            <div class="row vivo-main-header m-t-0 pad-b-10">
                                <div class="col-sm-12 no-pad-lr">
                                    <div class="row">
                                        <div class="col-sm-6 pad-t-10">
                                            <h1 class="result-for m-0 exclude bold">
                                                <h class="head">Results for: {{resultfor}}</h>
                                            </h1>
                                        </div>
                                        <div class="col-sm-6 filter-header-text pad-t-10 pad-b-10">
                                            <h class="pull-right">
                                                Total {{count}} items
                                            </h>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-4 text-center">
                                            <span class="text-center b1-theme btn-filter pad-t-2 pad-b-5 theme-text-special unbold" data-ng-mouseover="openFilter()">
                                                    <span class="theme-text t15 pad-l-5">Filter by</span>
                                            <i class="fa fa-fw fa-caret-down t18 vivo-text-hover" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <div class="filter-header-text">
                                                Sort By
                                                <select class="exclude" name="singleSelect" id="singleSelect" data-ng-model="selectedFilter">
                                                <option value="">Popularity</option>
                                                <option value="Price L to H">Price L to H </option>
                                                <option value="Price H to L">Price H to L</option>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="display:none" id="filterArea">
                                            <div class="row m-t-20">
                                                <div class="col-md-3 filter-header-text no-pad-l pad-r-80">
                                                    <span>Price(in Rs.)</span>
                                                    <br/>
                                                    <div range-slider min="0" max="500000" model-min="demo1.min" model-max="demo1.max">
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-5 no-pad-l">
                                                            <input class="pad-lr-5 w-full exclude" placeholder="Min" type="number" data-ng-model="demo1.min">
                                                        </div>
                                                        <div class="col-md-2 no-pad-lr">TO</div>
                                                        <div class="col-md-5 no-pad-lr">
                                                            <input class="pad-lr-5 w-full exclude" type="number" placeholder="Max" data-ng-model="demo1.max">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 theme-text no-pad-l">
                                                    <div class="m-b-10">Metal</div>
                                                    <div class="row">
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">
                                <input class="checkbox-position" type="checkbox" data-ng-model="isGold"> 
                                Yellow Gold
                                </label>
                                                        </div>
                                                        <div class="col-md-12 no-pad-lr">
                                                            <label class="filter-text">
                                <input class="checkbox-position" type="checkbox" data-ng-model="isWhiteGold"> 
                                White gold
                                </label>
                                                        </div>
                                                        <div class="col-md-12 no-pad-lr">
                                                            <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="isRoseGold"> 
                                Rose Gold
                                </label>
                                                        </div>
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="isSilver">   
                                Silver
                                </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 theme-text">
                                                    <div class="m-b-10">Metal purity</div>
                                                    <div class="row">
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">                               <input class="checkbox-position" type="checkbox" data-ng-model="is22"> 
                                    22 KT
                                </label>
                                                        </div>
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="is18"> 
                                  18 KT
                                </label>
                                                        </div>
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="is14"> 
                                    14 KT
                                </label>
                                                        </div>
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">
                                    <input class="checkbox-position" type="checkbox" data-ng-model="isOther"> 
                                    Other
                                </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 theme-text">
                                                    <div class="m-b-10">Occasion</div>
                                                    <div class="row">
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="isCasual"> 
                                     Casual
                                 </label>
                                                        </div>
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">                               <input class="checkbox-position" type="checkbox" data-ng-model="isFashion"> 
                                    Fashion
                             </label>
                                                        </div>
                                                        <div class="col-md-12 no-pad">
                                                            <label class="filter-text">
                                    <input class="checkbox-position" type="checkbox" data-ng-model="isBridal"> 
                                    Bridal
                                </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 theme-text no-pad-r">
                                                    <div class="demo-section k-content">
                                                        <select kendo-multi-select k-options="selectOptions" k-ng-model="jewellers"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 text-right no-pad-r">
                                                    <a data-ng-click="resetFilter()" class="btn btn-vivo-bold uppercase">
                          Clear
                        </a>
                                                    <a data-ng-click="filter()" class="btn btn-vivo-bold uppercase">
                          Search
                        </a>
                                                    <a class="inline-block pointer" data-ng-click="closeFilter()">
                                                        <i class="fa fa-times-circle t35 theme-text-special t-middle vivo-text-hover" aria-hidden="true"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <vivo-product-list></vivo-product-list>
                        <div class="row" id='loadmore' tagged-infinite-scroll='loadMore()'></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- start tag description  -->
        <div class="middle-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="grey-colour">
                            <?php echo $description; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end tag description  -->
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoSearch.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>