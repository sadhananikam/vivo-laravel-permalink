<!DOCTYPE html>
<html lang="en" data-ng-app="vivoEditAccount">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="noindex,nofollow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />

    <link rel="canonical" href="https://www.vivocarat.com/edit-account">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/edit-account">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/edit-account" />

    <link href="/css/style.css" rel="stylesheet" media="all">

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">

    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        var w = window.innerWidth;

        if (isMobile.any() && w <= 1024) {
            document.location = "/m" + document.location.pathname;
        }
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        /*fbq('init', '293278664418362', {
            em: 'insert_email_variable,'
        });*/
        fbq('init', '293278664418362');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->
</head>

<body ng-cloak>

    <style>
        /*CSS for materializing input fields*/
        
        * {
            box-sizing: border-box;
        }
        /* form starting stylings ------------------------------- */
        
        .group {
            position: relative;
            margin-bottom: 45px;
        }
        
        input {
            font-size: 13px;
            padding: 10px 10px 10px 5px;
            display: block;
            width: 300px;
            border: none;
            border-bottom: 1px solid #888888;
            /*For Mozilla Gecko browser rendering compatibility,make box shadow nonw*/
            box-shadow: none;
        }
        
        input:focus {
            outline: none;
        }
        /* LABEL ======================================= */
        
        label {
            color: #797979 !important;
            font-size: 13px;
            font-weight: normal;
            font-family: 'leela';
            position: absolute;
            pointer-events: none;
            left: 18px;
            top: 10px;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }
        /* active state */
        
        input:focus~label,
        input:valid~label {
            top: -20px;
            font-size: 14px;
            color: #E62739;
        }
        /* BOTTOM BARS ================================= */
        
        .bar {
            position: relative;
            display: block;
            width: 67%;
        }
        
        .bar:before,
        .bar:after {
            content: '';
            height: 2px;
            width: 0;
            bottom: 1px;
            position: absolute;
            background: #E62739;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }
        
        .bar:before {
            left: 50%;
        }
        
        .bar:after {
            right: 50%;
        }
        /* active state */
        
        input:focus~.bar:before,
        input:focus~.bar:after {
            width: 50%;
        }
        /* HIGHLIGHTER ================================== */
        
        .highlight {
            position: absolute;
            height: 60%;
            width: 100px;
            top: 25%;
            left: 0;
            pointer-events: none;
            opacity: 0.5;
        }
        /* active state */
        
        input:focus~.highlight {
            -webkit-animation: inputHighlighter 0.3s ease;
            -moz-animation: inputHighlighter 0.3s ease;
            animation: inputHighlighter 0.3s ease;
        }
        /* ANIMATIONS ================ */
        
        @-webkit-keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }
        
        @-moz-keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }
        
        @keyframes inputHighlighter {
            from {
                background: #E62739;
            }
            to {
                width: 0;
                background: transparent;
            }
        }
        /*END of Material CSS design*/
        /*CSS for textarea Material CSS design*/
        /*------------------------------input type text and textarea and other starts-----*/
        
        .form-radio,
        .form-group {
            position: relative;
        }
        
        .form-inline>.form-group,
        .form-inline>.btn {
            display: inline-block;
            margin-bottom: 0;
        }
        
        .form-group input {
            height: 1.9rem;
        }
        
        .form-group textarea {
            resize: none;
        }
        
        .form-group .control-label {
            position: absolute;
            -webkit-transition: all 0.28s ease;
            transition: all 0.28s ease;
        }
        
        .form-group .bar {
            position: relative;
            border-bottom: 0.0625rem solid #999;
            display: block;
        }
        
        .form-group .bar::before {
            content: '';
            height: 0.125rem;
            width: 0;
            left: 50%;
            bottom: -0.0625rem;
            position: absolute;
            background: #E62739;
            -webkit-transition: left 0.28s ease, width 0.28s ease;
            transition: left 0.28s ease, width 0.28s ease;
            z-index: 2;
        }
        
        .form-group input,
        .form-group textarea {
            display: block;
            background: none;
            margin-top: 28px;
            font-size: 13px;
            border-width: 0;
            border-color: transparent;
            line-height: 1.9;
            width: 66%;
            color: transparent;
            -webkit-transition: all 0.28s ease;
            transition: all 0.28s ease;
            box-shadow: none;
        }
        
        .form-group textarea:focus,
        .form-group textarea:valid,
        .form-group textarea.has-value {
            color: #333;
        }
        
        .form-group textarea:focus~.control-label,
        .form-group textarea:valid~.control-label,
        .form-group textarea.form-file~.control-label,
        .form-group textarea.has-value~.control-label {
            font-size: 0.8rem;
            color: gray;
            top: -1rem;
            left: 0;
        }
        
        .form-group textarea:focus {
            outline: none;
        }
        
        .form-group textarea:focus~.control-label {
            color: #E62739;
        }
        
        .form-group textarea:focus~.bar::before {
            width: 100%;
            left: 0;
        }
        /*------------input type text and textarea and other ends-----*/
        /*END of textarea Material design CSS*/
        /*Arrow nav tabs CSS*/
        
        .red {
            color: white !important;
            background: #e62739;
            padding: 8px 46px 8px 25px;
            font-weight: bold;
        }
        
        .red:hover {
            color: white !important;
            background: #e62739;
            padding: 8px 46px 8px 25px;
        }
        /*Arrow tip css*/
        
        #arrow4:after {
            content: '';
            height: 0;
            display: block;
            border-color: transparent transparent transparent #e62739;
            border-width: 17px;
            border-style: solid;
            position: absolute;
            top: -7px;
            left: 172px;
        }
        /*END of Arrow nav tabs CSS*/
    </style>
    <vivo-header></vivo-header>

    <div data-ng-controller='editAccountCtrl'>

        <div class="container padding-bottom-60px">

            <div class="row">
                <div class="tabs-left">
                    <ul class="col-lg-2 no-padding-left-right tabs-style">
                        <li class="row text-style tab-head first-tab-size">
                            <a class="inactive-tab-text-colour" href="/wishlist" target="_self">Wishlist</a>
                        </li>

                        <li class="row text-style tab-head second-tab-size" style="padding: 10px 0px 17px 25px;">
                            <a class="inactive-tab-text-colour" href="/orders" target="_self">Order history</a>
                        </li>

                        <li class="row text-style tab-head arrow" id="arrow4">
                            <a class="red" href="/account" target="_self">Account details</a>
                        </li>
                    </ul>

                    <div data-ng-if="!authenticated && isLoaded" class="register_account col-lg-10 padding-top-17px">

                        <div class="well not-loggedin-pad">
                            <h4 class="normal-text">
                                Not Logged In
                            </h4>
                            <p class="normal-text margin-bottom-20px">
                                Kindly <a class="link-hover" href="/login" target="_self">login</a> to edit your account.
                            </p>
                        </div>
                    </div>

                    <div data-ng-show="isauthenticated || isLoaded" class="col-lg-10" style="padding-top: 33px;padding-left: 30px;">
                        <form name="form">
                            <div class="row">
                                <div class="col-lg-6 placeholdlight padding-2px">

                                    <div class="group bottom-margin-15px-padding-25px">
                                        <input class="normal-text" type="text" data-ng-model="name" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label class="normal-text">Name</label>
                                    </div>

                                    <div class="group bottom-margin-15px-padding-25px">
                                        <input class="normal-text" type="text" data-ng-model="email" disabled>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label class="normal-text bold-font" style="top: -18px;">Email</label>
                                    </div>

                                    <div class="group bottom-margin-15px-padding-10px">
                                        <input class="normal-text" type="text" data-ng-model="phone" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label class="normal-text bold-font">Phone</label>
                                    </div>

                                    <div class="group bottom-margin-15px-padding-10px">
                                        <input class="normal-text" type="text" data-ng-model="password" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label class="normal-text bold-font">Password</label>
                                    </div>

                                    <div class="row vivo-row">
                                        <div class="col-lg-12 placeholdlight form-group" style="margin-bottom:20px;padding: 0px;padding-bottom: 25px;">
                                            <textarea class="normal-text" type="text" class="vivo-input" data-ng-model="first_line" required></textarea>
                                            <label for="textarea" class="normal-text bold-font">Address</label>
                                            <i class="bar"></i>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-6 placeholdlight">
                                    <div class="group bottom-margin-15px-padding-25px">
                                        <input class="normal-text" type="text" name="dateofbirth" data-ng-model="dob" required>
                                        <span class="highlight"></span>
                                        <span class="bar" style="width: 71%;"></span>
                                        <label class="normal-text bold-font" style="top:-11px;">DOB</label>
                                    </div>

                                    <div class="row pull-right" style="padding-top: 280px;">
                                        <a class="btn btn-vivo small-button" role="button" align="right" data-ng-click="updateAccountDetails()">Apply</a>
                                        <a class="btn btn-vivo small-button" role="button" align="right" href="/account" target="_self">OK</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <vivo-footer></vivo-footer>

    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>

    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoEditAccount.js"></script>
    <!-- end angularjs modules -->

    <script src="/app/data.js"></script>
    <!--<script src="/app/directives.js"></script>-->

    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->

    <script src="/device-router.js"></script>

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67690535-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>

</html>