<!DOCTYPE html>
<html lang="en" data-ng-app="vivoSitemap">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/sitemap" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Sitemap | VivoCarat" />
    <meta property="og:description" content="Sitemap" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>Sitemap | VivoCarat</title>
    <meta name="keywords" content="Sitemap" />
    <meta name="description" content="VivoCarat Sitemap" />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/sitemap">
    <link href="/css/style.css" rel="stylesheet" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='sitemapCtrl'>
        <div class="container">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>SITEMAP</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="exclude m-t-0 m-b-30">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hr"><span>SITEMAP</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h3 class="sitemap-heading" style="width: 155px;">GOLD JEWELLERY</h3>
                        <ul class="normal-text li-none">
                            <li class="bottom-list-space"><a href="/jewellery/gold-rings.html" target="_self">Gold Rings</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/gold-earrings.html" target="_self">Gold Earrings</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/gold-pendants.html" target="_self">Gold Pendants</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/gold-nosepins.html" target="_self">Gold Nose Pins</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/gold-bangles.html" target="_self">Gold Bangles</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/gold-bracelets.html" target="_self">Gold Bracelets</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/gold-necklaces.html" target="_self">Gold Necklaces</a></li>
                            <li><a href="/jewellery/Gold-All.html" target="_self">All Gold Jewelleries</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3 class="sitemap-heading" style="width: 188px;">
                            DIAMOND JEWELLERY
                        </h3>
                        <ul class="normal-text li-none">
                            <li class="bottom-list-space"><a href="/jewellery/diamond-rings.html" target="_self">Diamond Rings</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/diamond-earrings.html" target="_self">Diamond Earrings</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/diamond-pendants.html" target="_self">Diamond Pendants</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/diamond-nosepins.html" target="_self">Diamond Nose Pins</a></li>
                            <li><a href="/jewellery/Diamond-All.html" target="_self">All Diamond Jewelleries</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3 class="sitemap-heading" style="width: 163px;">SILVER JEWELLERY</h3>
                        <ul class="normal-text li-none">
                            <li class="bottom-list-space"><a href="/jewellery/silver-rings.html" target="_self">Silver Rings</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/silver-earrings.html" target="_self">Silver Earrings</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/silver-pendants.html" target="_self">Silver Pendants</a></li>
                            <li class="bottom-list-space"><a href="/jewellery/silver-accessories.html" target="_self">Silver Accessories</a></li>
                            <li><a href="/jewellery/Silver-All.html" target="_self">All Silver Jewelleries</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h3 class="sitemap-heading" style="width: 71px;">BRANDS</h3>
                        <ul class="normal-text li-none">
                            <li class="bottom-list-space"><a href="brands" target="_self">Brands</a></li>
                            <li class="bottom-list-space"><a href="/brands/Kundan Jewellers" target="_self">Kundan Jewellers</a></li>
                            <li class="bottom-list-space"><a href="/brands/Lagu Bandhu" target="_self">Lagu Bandhu</a></li>
                            <li class="bottom-list-space"><a href="/brands/Megha Jewellers" target="_self">Megha Jewellers</a></li>
                            <li class="bottom-list-space"><a href="/brands/Mayura Jewellers" target="_self">Mayura Jewellers</a></li>
                            <li class="bottom-list-space"><a href="/brands/Mani Jewellers" target="_self">Mani Jewellers</a></li>
                            <li class="bottom-list-space"><a href="/brands/Shankaram Jewellers" target="_self">Shankaram Jewellers</a></li>
                            <li><a href="/brands/Incocu Jewellers" target="_self">Incocu Jewellers</a></li>
                            <li><a href="/brands/Glitter Jewels" target="_self">Glitter Jewels</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3 class="sitemap-heading" style="width: 143px;">MISCELLANEOUS</h3>
                        <ul class="normal-text li-none">
                            <li class="bottom-list-space"><a href="about-us" target="_self">About Us</a></li>
                            <li class="bottom-list-space"><a href="faq" target="_self">FAQ</a></li>
                            <li class="bottom-list-space"><a href="terms" target="_self">Terms Of Use</a></li>
                            <li class="bottom-list-space"><a href="privacy" target="_self">Privacy Policy</a></li>
                            <li class="bottom-list-space"><a href="return" target="_self">Return Policy</a></li>
                            <li class="bottom-list-space"><a href="lookbook" target="_self">Look Book</a></li>
                            <li class="bottom-list-space"><a href="jewellery-education" target="_self">Jewellery Education</a></li>
                            <li class="bottom-list-space"><a href="moissanite" target="_self">Moissanite</a></li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoSitemap.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>