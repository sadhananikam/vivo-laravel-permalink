<div id="carousel-product-same-jeweller" class="carousel slide pad-t-50 relative" data-ride="carousel" style="z-index:1;">
    <!-- Wrapper for slides -->
    <div class="height carousel-inner">
        <div class="item active row">
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[0].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[0].VC_SKU}}-t.jpg" alt="{{list[0].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[0].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[0].category != 'Rings' && list[0].category != 'Bangles' && list[0].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[0])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[0].category == 'Rings' || list[0].category == 'Bangles' || list[0].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[0].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[1].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[1].VC_SKU}}-t.jpg" alt="{{list[1].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[1].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[1].category != 'Rings' && list[1].category != 'Bangles' && list[1].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[1])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[1].category == 'Rings' || list[1].category == 'Bangles' || list[1].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[1].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[2].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[2].VC_SKU}}-t.jpg" alt="{{list[2].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[2].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[2].category != 'Rings' && list[2].category != 'Bangles' && list[2].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[2])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[2].category == 'Rings' || list[2].category == 'Bangles' || list[2].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[2].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[3].urlslug}}.html" target="_self">
                    <img data-ng-src="/images/products-v2/{{list[3].VC_SKU}}-t.jpg" alt="{{list[3].title}}">
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[3].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[3].category != 'Rings' && list[3].category != 'Bangles' && list[3].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[3])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[3].category == 'Rings' || list[3].category == 'Bangles' || list[3].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[3].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="item row">
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[4].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[4].VC_SKU}}-t.jpg" alt="{{list[4].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[4].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[4].category != 'Rings' && list[4].category != 'Bangles' && list[4].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[4])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[4].category == 'Rings' || list[4].category == 'Bangles' || list[4].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[4].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[5].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[5].VC_SKU}}-t.jpg" alt="{{list[5].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[5].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[5].category != 'Rings' && list[5].category != 'Bangles' && list[5].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[5])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[5].category == 'Rings' || list[5].category == 'Bangles' || list[5].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[5].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[6].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[6].VC_SKU}}-t.jpg" alt="{{list[6].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[6].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[6].category != 'Rings' && list[6].category != 'Bangles' && list[6].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[6])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[6].category == 'Rings' || list[6].category == 'Bangles' || list[6].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[6].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[7].urlslug}}.html" target="_self">
                    <img data-ng-src="/images/products-v2/{{list[7].VC_SKU}}-t.jpg" alt="{{list[7].title}}">
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[7].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[7].category != 'Rings' && list[7].category != 'Bangles' && list[7].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[7])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[7].category == 'Rings' || list[7].category == 'Bangles' || list[7].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[7].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="item row">
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[8].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[8].VC_SKU}}-t.jpg" alt="{{list[8].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[8].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[8].category != 'Rings' && list[8].category != 'Bangles' && list[8].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[8])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[8].category == 'Rings' || list[8].category == 'Bangles' || list[8].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[8].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[9].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[9].VC_SKU}}-t.jpg" alt="{{list[9].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[9].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[9].category != 'Rings' && list[9].category != 'Bangles' && list[9].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[9])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[9].category == 'Rings' || list[9].category == 'Bangles' || list[9].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[9].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[10].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{list[10].VC_SKU}}-t.jpg" alt="{{list[10].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[10].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[10].category != 'Rings' && list[10].category != 'Bangles' && list[10].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[10])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[10].category == 'Rings' || list[10].category == 'Bangles' || list[10].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[10].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{list[11].urlslug}}.html" target="_self">
                    <img data-ng-src="/images/products-v2/{{list[11].VC_SKU}}-t.jpg" alt="{{list[11].title}}">
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{list[11].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="list[11].category != 'Rings' && list[11].category != 'Bangles' && list[11].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(list[11])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="list[11].category == 'Rings' || list[11].category == 'Bangles' || list[11].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{list[11].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>