<div id="carousel-similar" class="carousel slide pad-t-50" data-ride="carousel" style="position:relative;z-index:1;">
    <!-- Wrapper for slides -->
    <div class="height carousel-inner">
        <div class="item active row">
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[0].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[0].VC_SKU}}-t.jpg" alt="{{same_list[0].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[0].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[0].category != 'Rings' && same_list[0].category != 'Bangles' && same_list[0].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[0])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[0].category == 'Rings' || same_list[0].category == 'Bangles' || same_list[0].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[0].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[1].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[1].VC_SKU}}-t.jpg" alt="{{same_list[1].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[1].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[1].category != 'Rings' && same_list[1].category != 'Bangles' && same_list[1].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[1])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[1].category == 'Rings' || same_list[1].category == 'Bangles' || same_list[1].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[1].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[2].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[2].VC_SKU}}-t.jpg" alt="{{same_list[2].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[2].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[2].category != 'Rings' && same_list[2].category != 'Bangles' && same_list[2].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[2])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[2].category == 'Rings' || same_list[2].category == 'Bangles' || same_list[2].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[2].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[3].urlslug}}.html" target="_self">
                    <img data-ng-src="/images/products-v2/{{same_list[3].VC_SKU}}-t.jpg" alt="{{same_list[3].title}}">
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[3].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[3].category != 'Rings' && same_list[3].category != 'Bangles' && same_list[3].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[3])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[3].category == 'Rings' || same_list[3].category == 'Bangles' || same_list[3].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[3].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="item row">
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[4].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[4].VC_SKU}}-t.jpg" alt="{{same_list[4].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[4].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[4].category != 'Rings' && same_list[4].category != 'Bangles' && same_list[4].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[4])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[4].category == 'Rings' || same_list[4].category == 'Bangles' || same_list[4].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[4].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[5].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[5].VC_SKU}}-t.jpg" alt="{{same_list[5].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[5].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[5].category != 'Rings' && same_list[5].category != 'Bangles' && same_list[5].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[5])"> 
                                    Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[5].category == 'Rings' || same_list[5].category == 'Bangles' || same_list[5].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[5].urlslug}}.html" target="_self"> 
                                    Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[6].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[6].VC_SKU}}-t.jpg" alt="{{same_list[6].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[6].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[6].category != 'Rings' && same_list[6].category != 'Bangles' && same_list[6].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[6])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[6].category == 'Rings' || same_list[6].category == 'Bangles' || same_list[6].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[6].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[7].urlslug}}.html" target="_self">
                    <img data-ng-src="/images/products-v2/{{same_list[7].VC_SKU}}-t.jpg" alt="{{same_list[7].title}}">
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[7].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[7].category != 'Rings' && same_list[7].category != 'Bangles' && same_list[7].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[7])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[7].category == 'Rings' || same_list[7].category == 'Bangles' || same_list[7].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[7].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="item row">
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[8].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[8].VC_SKU}}-t.jpg" alt="{{same_list[8].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[8].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[8].category != 'Rings' && same_list[8].category != 'Bangles' && same_list[8].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[8])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[8].category == 'Rings' || same_list[8].category == 'Bangles' || same_list[8].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[8].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[9].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[9].VC_SKU}}-t.jpg" alt="{{same_list[9].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[9].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[9].category != 'Rings' && same_list[9].category != 'Bangles' && same_list[9].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[9])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[9].category == 'Rings' || same_list[9].category == 'Bangles' || same_list[9].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[9].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[10].urlslug}}.html" target="_self">
                    <div class="product_image">
                        <img data-ng-src="/images/products-v2/{{same_list[10].VC_SKU}}-t.jpg" alt="{{same_list[10].title}}">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[10].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[10].category != 'Rings' && same_list[10].category != 'Bangles' && same_list[10].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[10])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[10].category == 'Rings' || same_list[10].category == 'Bangles' || same_list[10].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[10].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3 no-pad-lr inner_content1 clearfix">
                <a data-ng-href="/product/{{same_list[11].urlslug}}.html" target="_self">
                    <img data-ng-src="/images/products-v2/{{same_list[11].VC_SKU}}-t.jpg" alt="{{same_list[11].title}}">
                    <div class="row">
                        <div class="col-sm-12 grid-price-after1">
                            RS. {{same_list[11].price_after_discount | INR}}
                        </div>
                    </div>
                    <div class="row m-b-10 pad-t-10">
                        <div class="col-sm-12 text-center" data-ng-if="same_list[11].category != 'Rings' && same_list[11].category != 'Bangles' && same_list[11].category != 'Bracelets'">
                            <a class="btn btn-book" data-ng-click="buyNow(same_list[11])"> 
                                Buy Now
                            </a>
                        </div>
                        <div class="col-sm-12 text-center" data-ng-if="same_list[11].category == 'Rings' || same_list[11].category == 'Bangles' || same_list[11].category == 'Bracelets'">
                            <a class="btn btn-book" data-ng-href="/product/{{same_list[11].urlslug}}.html" target="_self"> 
                                Buy Now
                            </a>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>