<div class="row vivo-main-header pad-b-10">
    <div class="col-md-12 no-pad-lr">
        <div class="row subheader">
            <div class="col-md-6 pad-t-10">
                <h class="head theme-text-special bold subheader bold uppercase pad-b-20">
                    {{resultfor === "1" ? "All" : resultfor}} ({{subtype}})
                </h>
            </div>
            <div class="col-md-6 filter-header-text pad-t-10 pad-b-10">
                <h class="pull-right normal-text">
                    Total {{count}} items
                </h>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <span class="text-center b1-theme btn-filter pad-tb-5 theme-text-special unbold" data-ng-mouseover="openFilter()">
                    <span class="theme-text t15 pad-l-5">Filter by</span>
                    <i class="fa fa-fw fa-caret-down t18 vivo-text-hover" aria-hidden="true"></i>
                </span>
            </div>
            <div class="col-md-4 text-right">
                <div class="filter-header-text">
                    Sort By
                    <select class="exclude" name="singleSelect" id="singleSelect" data-ng-model="selectedFilter">
                        <option value="">Popularity</option>
                        <option value="Price L to H">Price L to H </option>
                        <option value="Price H to L">Price H to L</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="display:none" id="filterArea">
                <div class="row m-t-20">
                    <div class="col-md-3 filter-header-text no-pad-l pad-r-80">
                        <span>Price(in Rs.)</span>
                        <br/>
                        <div range-slider min="0" max="500000" model-min="demo1.min" model-max="demo1.max">
                        </div>
                        <div class="row">
                            <div class="col-md-5 no-pad-l">
                                <input class="pad-lr-5 w-full exclude" placeholder="Min" type="number" data-ng-model="demo1.min">
                            </div>
                            <div class="col-md-2 no-pad-lr">TO</div>
                            <div class="col-md-5 no-pad-lr">
                                <input class="pad-lr-5 w-full exclude" type="number" placeholder="Max" data-ng-model="demo1.max">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 normal-text no-pad-l">
                        <div class="m-b-10">Metal</div>
                        <div class="row">
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">
                                <input class="checkbox-position" type="checkbox" data-ng-model="isGold"> 
                                Yellow Gold
                                </label>
                            </div>
                            <div class="col-md-12 no-pad-lr">
                                <label class="filter-text">
                                <input class="checkbox-position" type="checkbox" data-ng-model="isWhiteGold"> 
                                White gold
                                </label>
                            </div>
                            <div class="col-md-12 no-pad-lr">
                                <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="isRoseGold"> 
                                Rose Gold
                                </label>
                            </div>
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="isSilver">   
                                Silver
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 normal-text">
                        <div class="m-b-10">Metal purity</div>
                        <div class="row">
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">                               <input class="checkbox-position" type="checkbox" data-ng-model="is22"> 
                                    22 KT
                                </label>
                            </div>
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="is18"> 
                                  18 KT
                                </label>
                            </div>
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="is14"> 
                                    14 KT
                                </label>
                            </div>
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">
                                    <input class="checkbox-position" type="checkbox" data-ng-model="isOther"> 
                                    Other
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 theme-text">
                        <div class="m-b-10">Occasion</div>
                        <div class="row">
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">                           <input class="checkbox-position" type="checkbox" data-ng-model="isCasual"> 
                                     Casual
                                 </label>
                            </div>
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">                               <input class="checkbox-position" type="checkbox" data-ng-model="isFashion"> 
                                    Fashion
                             </label>
                            </div>
                            <div class="col-md-12 no-pad">
                                <label class="filter-text">
                                    <input class="checkbox-position" type="checkbox" data-ng-model="isBridal"> 
                                    Bridal
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 normal-text no-pad-r">
                        <div class="demo-section k-content">
                            <select kendo-multi-select k-options="selectOptions" k-ng-model="jewellers"></select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-right no-pad-r">
                        <a data-ng-click="resetFilter()" class="btn btn-vivo-bold uppercase">
                          Clear
                        </a>
                        <a data-ng-click="filter()" class="btn btn-vivo-bold uppercase">
                          Search
                        </a>
                        <a class="inline-block pointer" data-ng-click="closeFilter()">
                            <i class="fa fa-times-circle t35 theme-text-special t-middle vivo-text-hover" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>