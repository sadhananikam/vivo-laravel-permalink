<nav id="nav-1" class="menushadow block">
    <div class="row theme_bg_header_gradient menushadow fixed-header-structure">
        <div class="container" data-ng-init="brandMenuImg='/images/header/menu/gold menu header.jpg'">
            <div class="row" id="main-menu" style="margin-bottom:0px;">
                <div class="col-md-2">
                    <a href="/" target="_self" title="VivoCarat Retail Pvt. Ltd.">
                        <img src="/images/header/logos/VivoCarat logo.png" class="exclude" data-ng-show="logo" style="padding-top: 4px;" alt="VivoCarat Retail Pvt. Ltd.">
                    </a>
                </div>
                <div class="col-md-8 text-center">
                    <ul class="megamenu">
                        <li>
                            <a href="#" class="bold black">
                                COLLECTIONS
                            </a>
                            <div class="megapanel collection-menu" id="arrow">
                                <div class="row text-left">
                                    <div data-ng-init="brandMenuImg='/images/header/menu/gold menu header.jpg'" data-ng-mouseover="brandMenuImg='/images/header/menu/gold menu header.jpg'" data-ng-mouseout="brandMenuImg='/images/header/menu/gold menu header.jpg'" class="col-xs-2">
                                        <div class="h_nav" style="padding-left: 8px;">
                                            <h4 class="base-metal-heading exclude">
                                                <a href="/jewellery/Gold-All.html" target="_self">
                                                    Gold
                                                </a>
                                            </h4>
                                            <ul>
                                                <li><a href="/jewellery/gold-rings.html" target="_self">Rings</a></li>
                                                <li><a href="/jewellery/gold-earrings.html" target="_self">Earrings</a></li>
                                                <li><a href="/jewellery/gold-pendants.html" target="_self"> Pendants</a></li>
                                                <li><a href="/jewellery/gold-nosepins.html" target="_self">Nose Pins</a></li>
                                                <li><a href="/jewellery/gold-bangles.html" target="_self">Bangles</a></li>
                                                <li><a href="/jewellery/gold-bracelets.html" target="_self">Bracelets</a></li>
                                                <li><a href="/jewellery/gold-necklaces.html" target="_self">Necklaces</a></li>
                                                <li><a href="/jewellery/gold-goldcoins.html" target="_self">Coins</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div data-ng-init="brandMenuImg='/images/header/menu/diamond menu header.jpg'" data-ng-mouseover="brandMenuImg='/images/header/menu/diamond menu header.jpg'" data-ng-mouseout="brandMenuImg='/images/header/menu/gold menu header.jpg'" class="col-xs-2">
                                        <div class="h_nav padding-left-10px">
                                            <h4 class="base-metal-heading exclude">
                                                <a href="/jewellery/Diamond-All.html" target="_self">
                                                    Diamonds
                                                </a>
                                            </h4>
                                            <ul>
                                                <li><a href="/jewellery/diamond-rings.html" target="_self">Rings</a></li>
                                                <li><a href="/jewellery/diamond-earrings.html" target="_self">Earrings </a></li>
                                                <li><a href="/jewellery/diamond-pendants.html" target="_self"> Pendants</a></li>
                                                <li><a href="/jewellery/diamond-nosepins.html" target="_self">Nose Pins</a></li>
                                                <li><a href="/jewellery/diamond-Tanmaniya.html" target="_self">Tanmaniya</a></li>
                                                <li><a href="/jewellery/diamond-bracelets.html" target="_self">Bracelets</a></li>
                                                <li><a href="/jewellery/diamond-necklaces.html" target="_self">Necklaces</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div data-ng-init="brandMenuImg='/images/header/menu/silver menu header.jpg'" data-ng-mouseover="brandMenuImg='/images/header/menu/silver menu header.jpg'" data-ng-mouseout="brandMenuImg='/images/header/menu/gold menu header.jpg'" class="col-xs-2">
                                        <div class="h_nav">
                                            <h4 class="base-metal-heading exclude">
                                                <a href="/jewellery/Silver-All.html" target="_self">
                                                Silver
                                                </a>
                                            </h4>
                                            <ul>
                                                <li><a href="/jewellery/silver-rings.html" target="_self">Rings</a></li>
                                                <li><a href="/jewellery/silver-earrings.html" target="_self">Earrings</a></li>
                                                <li><a href="/jewellery/silver-pendants.html" target="_self"> Pendants</a></li>
                                                <li><a href="/jewellery/silver-accessories.html" target="_self">Accessories</a></li>
                                                <li><a href="/jewellery/silver-bangles.html" target="_self">Bangles</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 col-xs-offset-1" style="padding: 15px 15px 15px 0px;">
                                        <div class="h_nav">
                                            <ul>
                                                <li class="no-pad-lr">
                                                    <img data-ng-src="{{brandMenuImg}}" alt="">
                                                </li>
                                            </ul>
                                        </div>
                                    </div></div></div>
                        </li>
                        <li>
                            <a class="lookbook-menu" href="/lookbook" target="_self">
                                LOOK BOOK
                            </a>
                        </li>
                        <li>
                            <a class="text-center bold black" href="/brands" target="_self">
                                BRANDS
                            </a>
                            <div class="megapanel brands-menu" id="caret1">
                                <div class="row">
                                    <div class="col-xs-12 no-pad-lr">
                                        <div class="h_nav pull-left">
                                            <ul class="text-left-img-ul">
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner1='/images/header/brands logos dropdown/incocu.png';" data-ng-mouseover="brandBanner1='/images/header/brands logos dropdown/Incocu Jewellers hover.png';" data-ng-mouseout="brandBanner1='/images/header/brands logos dropdown/incocu.png';">
                                                        <a href="/brands/Incocu Jewellers" target="_self">
                                                            <img data-ng-src="{{brandBanner1}}" alt="Incocu Jewellers">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner2='/images/header/brands logos dropdown/charu.png';" data-ng-mouseover="brandBanner2='/images/header/brands logos dropdown/Charu-Jewels hover.png';" data-ng-mouseout="brandBanner2='/images/header/brands logos dropdown/charu.png';">
                                                        <a href="/brands/Charu-Jewels" target="_self">
                                                            <img data-ng-src="{{brandBanner2}}" alt="Charu-Jewels">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner3='/images/header/brands logos dropdown/shankaram.png';" data-ng-mouseover="brandBanner3='/images/header/brands logos dropdown/Shankaram Jewellers hover.png';" data-ng-mouseout="brandBanner3='/images/header/brands logos dropdown/shankaram.png';">
                                                        <a href="/brands/Shankaram Jewellers" target="_self">
                                                            <img data-ng-src="{{brandBanner3}}" alt="Shankaram Jewellers">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner4='/images/header/brands logos dropdown/lagu.png';" data-ng-mouseover="brandBanner4='/images/header/brands logos dropdown/Lagu Bandhu hover.png';" data-ng-mouseout="brandBanner4='/images/header/brands logos dropdown/lagu.png';">
                                                        <a href="/brands/Lagu Bandhu" target="_self">
                                                            <img data-ng-src="{{brandBanner4}}" alt="Lagu Bandhu">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner5='/images/header/brands logos dropdown/arkina.png';" data-ng-mouseover="brandBanner5='/images/header/brands logos dropdown/Arkina-Diamonds hover.png';" data-ng-mouseout="brandBanner5='/images/header/brands logos dropdown/arkina.png';">
                                                        <a href="/brands/Arkina-Diamonds" target="_self">
                                                            <img data-ng-src="{{brandBanner5}}" alt="Arkina-Diamonds">
                                                        </a>
                                                    </div>
                                                </li>
                                                <!-- Another 5 Brands -->
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner6='/images/header/brands logos dropdown/ornomart.png';" data-ng-mouseover="brandBanner6='/images/header/brands logos dropdown/OrnoMart hover.png';" data-ng-mouseout="brandBanner6='/images/header/brands logos dropdown/ornomart.png';">
                                                        <a href="/brands/OrnoMart" target="_self">
                                                            <img data-ng-src="{{brandBanner6}}" alt="OrnoMart">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner7='/images/header/brands logos dropdown/kundan.png';" data-ng-mouseover="brandBanner7='/images/header/brands logos dropdown/Kundan Jewellers hover.png';" data-ng-mouseout="brandBanner7='/images/header/brands logos dropdown/kundan.png';">
                                                        <a href="/brands/Kundan Jewellers" target="_self">
                                                            <img data-ng-src="{{brandBanner7}}" alt="Kundan Jewellers">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner8='/images/header/brands logos dropdown/mayura.png';" data-ng-mouseover="brandBanner8='/images/header/brands logos dropdown/Mayura Jewellers hover.png';" data-ng-mouseout="brandBanner8='/images/header/brands logos dropdown/mayura.png';">
                                                        <a href="/brands/Mayura Jewellers" target="_self">
                                                            <img data-ng-src="{{brandBanner8}}" alt="Mayura Jewellers">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner9='/images/header/brands logos dropdown/megha.png';" data-ng-mouseover="brandBanner9='/images/header/brands logos dropdown/Megha Jewellers hover.png';" data-ng-mouseout="brandBanner9='/images/header/brands logos dropdown/megha.png';">
                                                        <a href="/brands/Megha Jewellers" target="_self">
                                                            <img data-ng-src="{{brandBanner9}}" alt="Megha Jewellers">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner10='/images/header/brands logos dropdown/regaalia.png';" data-ng-mouseover="brandBanner10='/images/header/brands logos dropdown/Regaalia Jewels hover.png';" data-ng-mouseout="brandBanner10='/images/header/brands logos dropdown/regaalia.png';">
                                                        <a href="/brands/Regaalia Jewels" target="_self">
                                                            <img data-ng-src="{{brandBanner10}}" alt="Regaalia Jewels">
                                                        </a>
                                                    </div>
                                                </li>
                                                <!-- 3rd 5 Brands -->
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner11='/images/header/brands logos dropdown/glitter.png';" data-ng-mouseover="brandBanner11='/images/header/brands logos dropdown/Glitter Jewels hover.png';" data-ng-mouseout="brandBanner11='/images/header/brands logos dropdown/glitter.png';">
                                                        <a href="/brands/Glitter Jewels" target="_self">
                                                            <img data-ng-src="{{brandBanner11}}" alt="Glitter Jewels">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner13='/images/header/brands logos dropdown/pp.png';" data-ng-mouseover="brandBanner13='/images/header/brands logos dropdown/PP-Gold hover.png';" data-ng-mouseout="brandBanner13='/images/header/brands logos dropdown/pp.png';">
                                                        <a href="/brands/PP-Gold" target="_self">
                                                            <img data-ng-src="{{brandBanner13}}" alt="PP-Gold">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner14='/images/header/brands logos dropdown/karatcraft.png';" data-ng-mouseover="brandBanner14='/images/header/brands logos dropdown/KaratCraft hover.png';" data-ng-mouseout="brandBanner14='/images/header/brands logos dropdown/karatcraft.png';">
                                                        <a href="/brands/KaratCraft" target="_self">
                                                            <img data-ng-src="{{brandBanner14}}" alt="KaratCraft">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner15='/images/header/brands logos dropdown/ZAVERI KAPOORCHAND DALICHAND & SONS.png';" data-ng-mouseover="brandBanner15='/images/header/brands logos dropdown/ZKD-Jewels hover.png';" data-ng-mouseout="brandBanner15='/images/header/brands logos dropdown/ZAVERI KAPOORCHAND DALICHAND & SONS.png';">
                                                        <a href="/brands/ZKD-Jewels" target="_self">
                                                            <img data-ng-src="{{brandBanner15}}" alt="ZAVERI KAPOORCHAND DALICHAND & SONS Jewels">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner16='/images/header/brands logos dropdown/iski uski.png';" data-ng-mouseover="brandBanner16='/images/header/brands logos dropdown/IskiUski hover.png';" data-ng-mouseout="brandBanner16='/images/header/brands logos dropdown/iski uski.png';">
                                                        <a href="/brands/IskiUski" target="_self">
                                                            <img data-ng-src="{{brandBanner16}}" alt="IskiUski">
                                                        </a>
                                                    </div>
                                                </li>
                                                <!-- Last 3 Brands -->
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner17='/images/header/brands logos dropdown/myrah.png';" data-ng-mouseover="brandBanner17='/images/header/brands logos dropdown/Myrah-Silver-Works hover.png';" data-ng-mouseout="brandBanner17='/images/header/brands logos dropdown/myrah.png';">
                                                        <a href="/brands/Myrah-Silver-Works" target="_self">
                                                            <img data-ng-src="{{brandBanner17}}" alt="Myrah-Silver-Works">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner18='/images/header/brands logos dropdown/tsara.png';" data-ng-mouseover="brandBanner18='/images/header/brands logos dropdown/tsara hover.png';" data-ng-mouseout="brandBanner18='/images/header/brands logos dropdown/tsara.png';">
                                                        <a href="/brands/Tsara-Jewellery" target="_self">
                                                            <img data-ng-src="{{brandBanner18}}">
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="text-left-img">
                                                    <div data-ng-init="brandBanner19='/images/header/brands logos dropdown/sarvada.png';" data-ng-mouseover="brandBanner19='/images/header/brands logos dropdown/Sarvada-Jewels hover.png';" data-ng-mouseout="brandBanner19='/images/header/brands logos dropdown/sarvada.png';">
                                                        <a href="/brands/Sarvada-Jewels" target="_self">
                                                            <img data-ng-src="{{brandBanner19}}">
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div></li></ul>
                            </div>
                            <div class="col-md-2 text-right" data-ng-show="logo">
                                <a href="/checkout" target="_self">
                                    <img class="exclude" src="/images/header/icons/stick cart bag.png" style="padding-top: 8px;" alt="checkout cart">

                                    <div data-ng-if="cart.length>0" class="fixed-cart-length-structure">
                                        <p class="exclude" style="padding-right: 7px;">({{cart.length}})</p>
                                    </div>
                                    <div data-ng-if="cart.length<1 || !cart || !cart.length" class="fixed-cart-length-nil">
                                        <p class="exclude" style="padding-right: 4px;">(0)</p>
                                    </div>
                                </a>
                            </div>
                            </div>
                </div>
            </div>
</nav>