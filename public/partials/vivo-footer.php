<div class="home-panel-row footer-background-colour">
    <div class="container">
        <div class="row footer-structure">
            <div class="col-xs-3">
                <h4 class="footer-headings">KNOW VIVOCARAT</h4>
                <ul class="li-none hover-black li-m-b-10">
                    <li>
                        <a href="/about-us" target="_self">VivoCarat Advantage</a>
                    </li>
                    <li>
                        <a href="/brands" target="_self">Brands</a>
                    </li>
                    <li>
                        <a href="/contact-us" target="_self">Contact Us</a>
                    </li>
                    <li>
                        <a href="/privacy" target="_self">Privacy Policy</a>
                    </li>
                    <li>
                        <a href="/terms" target="_self">Terms Of Use</a>
                    </li>
                    <li>
                        <a href="/sitemap" target="_self">Sitemap</a>
                    </li>
                    <li>
                        <a href="http://blog.vivocarat.com" target="_blank">Our Blog</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-3">
                <h4 class="footer-headings">CUSTOMER CONNECT</h4>
                <ul class="li-none hover-black li-m-b-10">
                    <li>
                        <a href="/lookbook" target="_self">Look Book</a>
                    </li>
                    <li>
                        <a href="/jewellery-education" target="_self">Jewellery Education</a>
                    </li>
                    <li>
                        <a href="/faq" target="_self">FAQs</a>
                    </li>
                    <li>
                        <a href="/faq/Lifetime" target="_self">Lifetime Return Policy</a>
                    </li>
                    <li>
                        <a href="/faq/Shipping" target="_self">Shipping Policy</a>
                    </li>
                    <li>
                        <a href="/faq/Payment" target="_self">Payment Options</a>
                    </li>
                    <li>
                        <a href="/faq/Return" target="_self">Return Policy</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-3 no-pad-lr">
                <h4 class="footer-headings">PARTNER WITH US</h4>
                <ul class="li-none hover-black li-m-b-10">
                    <li>
                        <a href="/partner-with-us" target="_self">Seller Connect</a>
                    </li>
                    <li>
                        <a href="/affiliate" target="_self">Affiliates</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-3 text-right no-pad-lr">
                <h4 class="footer-headings text-left">SUBSCRIBE WITH US</h4>
                <div class="pad-b-20" style="margin-left:-84px;">
                    <div data-ng-controller='footerCtrl'>
                        <form name="newsletterForm" data-ng-submit="sendNewsletter()">
                            <div class="row">
                                <div class="col-xs-8 pad-tb-0 pad-r-0 pad-l-20">
                                    <input type="email" class="newsletter-input-field" Placeholder="{{ emailPlaceholder }}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email'" id="newsletteremail" name="newsletteremail" data-ng-model='newsletter.newsletteremail' data-ng-required="true">
                                    <div data-ng-show="newsletterForm.newsletteremail.$dirty && newsletterForm.newsletteremail.$invalid">
                                        <div data-ng-show="newsletterForm.newsletteremail.$error.required">Email is required</div>
                                        <div data-ng-show="newsletterForm.newsletteremail.$error.email">Invalid Email</div>
                                    </div>
                                </div>
                                <div class="col-xs-4 newsletter-submit-button-structure">
                                    <button type="submit" class="btn newsletter-submit-button" data-ng-disabled="newsletterForm.$dirty && newsletterForm.$invalid">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <a href="https://play.google.com/store/apps/details?id=com.vivocarat.catalogapp&hl=en" target="_blank" rel="noopener noreferrer" title="Click to see VivoCarat Android app">
                    <img src="/images/footer/app download.png" alt="VivoCarat Android app">
                </a>
            </div>
        </div>
    </div>
    <div class="middle-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-11 pad-lr-0">
                    <div class="row">
                        <div class="col-md-12">
                            <b class="middle-footer-heading">Diamond</b> :
                            <span>
     <a class="middle-footer-links" href="/jewellery/diamond-rings.html" target="_self">Rings</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/diamond-earrings.html" target="_self"> Earrings</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/diamond-pendants.html" target="_self"> Pendants</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/diamond-nosepins.html" target="_self">Nose Pins</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/diamond-Tanmaniya.html" target="_self">Tanmaniya</a>
    </span>
                        </div>
                    </div>
                    <div class="row pad-t-10">
                        <div class="col-md-12">
                            <b class="middle-footer-heading">Gold</b> :
                            <span>
     <a class="middle-footer-links" href="/jewellery/gold-rings.html" target="_self">Rings</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/gold-earrings.html" target="_self"> Earrings</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/gold-pendants.html" target="_self"> Pendants</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/gold-nosepins.html" target="_self">Nose Pins</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/gold-bangles.html" target="_self">Bangles</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/gold-bracelets.html" target="_self">Bracelets</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/gold-necklaces.html" target="_self">Necklaces</a> <b class="text-grey-colour">|</b>
     <a class="middle-footer-links" href="/jewellery/gold-goldcoins.html" target="_self">Coins</a>
    </span>
                        </div>
                    </div>
                    <div class="row pad-t-10">
                        <div class="col-md-12">
                            <b class="middle-footer-heading">Silver</b> :
                            <span>
                                 <a class="middle-footer-links" href="/jewellery/silver-rings.html" target="_self">Rings</a> <b class="text-grey-colour">|</b>
                                 <a class="middle-footer-links" href="/jewellery/silver-earrings.html" target="_self"> Earrings</a> <b class="text-grey-colour">|</b>
                                 <a class="middle-footer-links" href="/jewellery/silver-pendants.html" target="_self"> Pendants</a> <b class="text-grey-colour">|</b>
                                 <a class="middle-footer-links" href="/jewellery/silver-accessories.html" target="_self">Accessories</a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 pad-lr-0">
                    <a href="https://msg91.com/startups/?utm_source=startup-banner" class="block m-t-5"><img src="https://msg91.com/images/startups/msg91Badge.png" width="120" height="90" title="MSG91 - SMS for Startups" alt="Bulk SMS - MSG91"></a>
                    <div class="text-center theme-text t14">SMS Partners</div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-end">
        <div class="wrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 no-pad-l">
                        <img src="/images/footer/payments.png" alt="payment partners" class="exclude">
                    </div>
                    <div class="col-lg-6">
                        <p class="copyright-text">© 2015 VivoCarat Retail Pvt Ltd</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<!-- Modal -->
<div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
    <div class="modal-dialog custom-dialog-size">
        <div class="at-modal-content">
            <div class="row">
                <div class="col-md-8 no-pad-lr">
                    <img src="/images/footer/modal/at popup.png" alt="modal popup image">
                </div>
                <div class="col-md-4 no-pad-lr at-modal-background">
                    <div class="row">
                        <div class="col-md-12 no-pad-lr">
                            <a data-dismiss="modal" aria-label="Close" data-dismiss="modal">
                                <img src="/images/product/customize/close.png" class="pull-right exclude" alt="close signup modal">
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 modal-heading-top-space">
                            <span class="modal-form-heading">SIGN UP and WIN</span>
                        </div>
                    </div>
                    <div class="row">
                        <form name="memberForm" data-ng-submit="savememberform()" autocomplete="off" novalidate>
                            <div class="row">
                                <div class="col-md-12 at-fields-sidespace">
                                    <div class="row pad-t-10">
                                        <input class="at-fields" type="text" Placeholder="Name*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name*'" data-ng-model="member_new.name" name="mname" id="mname" data-ng-required="true" data-ng-pattern="/^[a-zA-Z]+$/">
                                        <div class="at-validation-message" data-ng-show="memberForm.mname.$dirty && memberForm.mname.$error.required">Name is required</div>
                                        <div class="at-validation-message" data-ng-show="memberForm.mname.$dirty && memberForm.mname.$error.pattern">Name can contain only alphabets</div>
                                    </div>
                                    <div class="row pad-t-10">
                                        <input class="at-fields" type="text" Placeholder="Phone*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone*'" data-ng-model="member_new.phone" name="mphone" id="mphone" data-ng-required="true" data-ng-pattern="/^[0-9]{10,10}$/">
                                        <div class="at-validation-message" data-ng-show="memberForm.mphone.$dirty && memberForm.mphone.$error.required">Phone is required</div>
                                        <div class="at-validation-message" data-ng-show="memberForm.mphone.$dirty && memberForm.mphone.$error.pattern">Enter correct phone number</div>
                                    </div>
                                    <div class="row pad-t-10">
                                        <input class="at-fields" type="email" Placeholder="Email*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email*'" data-ng-model="member_new.email" name="memail" id="memail" data-ng-required="true">
                                        <div class="at-validation-message" data-ng-show="memberForm.memail.$dirty && memberForm.memail.$error.required">Email is required</div>
                                        <div class="at-validation-message" data-ng-show="memberForm.memail.$dirty && memberForm.memail.$error.email">Invalid Email</div>
                                    </div>
                                    <div class="row pad-t-10">
                                        <input class="at-fields" type="password" Placeholder="Password*" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password*'" data-ng-model="member_new.password" name="mpassword" id="mpassword" ng-minlength="8" data-ng-required="true">
                                        <div class="at-validation-message" data-ng-show="memberForm.mpassword.$dirty && memberForm.mpassword.$error.required">Password is required</div>
                                        <div class="at-validation-message" data-ng-show="memberForm.mpassword.$dirty && memberForm.mpassword.$error.minlength">Password must be atleast 8 characters</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 no-pad-r text-right">
                                            <sup class="subscript-style">*</sup><a href="/terms" target="_self" class="terms-text">T&amp;C Apply</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pad-t-10 text-center pad-b-10">
                                <button class="modal-submit-button" type="submit" ng-disabled="!memberForm.$valid">Sign up</button>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center pad-b-30">
                                    <span class="have-text">Have an account? </span><span><a href="/login" target="_self" class="modal-login-link">Login</a></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Modal for Alex-->
<div class="modal fade" id="CustomizeRing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog w-80pc" role="document">
        <div class="customize-modal-outer">
            <div class="row customize-background-image">
                <div class="col-md-12 no-pad-lr">
                    <div class="row">
                        <div class="col-md-1 col-md-offset-11 no-pad-lr">
                            <a data-dismiss="modal" aria-label="Close" data-dismiss="modal">
                                <img src="/images/product/customize/close.png" class="pull-right exclude" alt="close customization modal">
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="customize-heading">Customize your Jewellery</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <img src="/images/product/customize/customization steps.png" alt="customization step numbers">
                        </div>
                    </div>
                    <div class="row pad-t-10 pad-b-20">
                        <div class="col-md-6 col-md-offset-3 no-pad-lr">
                            <div class="row">
                                <div class="col-md-3 no-pad-lr">
                                    <p class="customize-steps">Upload / Describe <br/>What you like</p>
                                </div>
                                <div class="col-md-4 col-md-offset-1 no-pad-lr">
                                    <p class="customize-steps">Free consultation with our <br/>Jewellery experts</p>
                                </div>
                                <div class="col-md-2 col-md-offset-2 no-pad-lr">
                                    <p class="customize-steps">Get it made <br/>with love</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form name="customizeForm" data-ng-submit="saveCustomizeform()" autocomplete="off" novalidate>
                <div class="customize-modal-body">
                    <div class="row">
                        <div class="col-md-6 no-pad-lr">
                            <div class="row m-b-10">
                                <div class="col-md-2 no-pad-lr">
                                    <p class="customize-credential">Name</p>
                                </div>
                                <div class="col-md-1 no-pad-lr">
                                    <p class="grey-colour">:</p>
                                </div>
                                <div class="col-md-9 no-pad-lr">
                                    <input type="text" class="customize-fields exclude" name="cmname" id="cmname" data-ng-model="customizef.cmname" Placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" data-ng-required="true" data-ng-pattern="/^[a-zA-Z\s]+$/">
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-4 theme-text" data-ng-show="customizeForm.cmname.$dirty && customizeForm.cmname.$error.required">Name is required</div>
                                    <div class="col-md-9 col-md-offset-4 theme-text" data-ng-show="customizeForm.cmname.$dirty && customizeForm.cmname.$error.pattern">Name can contain only alphabets</div>
                                </div>
                            </div>
                            <div class="row m-b-10">
                                <div class="col-md-2 no-pad-lr">
                                    <p class="customize-credential">Phone</p>
                                </div>
                                <div class="col-md-1 no-pad-lr">
                                    <p class="grey-colour">:</p>
                                </div>
                                <div class="col-md-9 no-pad-lr">
                                    <input type="text" class="customize-fields exclude" name="cmphone" id="cmphone" data-ng-model="customizef.cmphone" Placeholder="Enter your phone number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your phone number'" data-ng-required="true" data-ng-pattern="/^[0-9]{10,10}$/">
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-4 theme-text" data-ng-show="customizeForm.cmphone.$dirty && customizeForm.cmphone.$error.required">Phone is required</div>
                                    <div class="col-md-9 col-md-offset-4 theme-text" data-ng-show="customizeForm.cmphone.$dirty && customizeForm.cmphone.$error.pattern">Enter correct phone number</div>
                                </div>
                            </div>
                            <div class="row m-b-10">
                                <div class="col-md-2 no-pad-lr">
                                    <p class="customize-credential">Email</p>
                                </div>
                                <div class="col-md-1 no-pad-lr">
                                    <p class="grey-colour">:</p>
                                </div>
                                <div class="col-md-9 no-pad-lr">
                                    <input type="email" class="customize-fields exclude" Placeholder="Enter your email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email'" data-ng-model="customizef.cmemail" name="cmemail" id="cmemail" data-ng-required="true">
                                </div>
                                <div class="row">
                                    <div class="col-md-9 col-md-offset-4 theme-text" data-ng-show="customizeForm.cmemail.$dirty && customizeForm.cmemail.$error.required">Email is required</div>
                                    <div class="col-md-9 col-md-offset-4 theme-text" data-ng-show="customizeForm.cmemail.$dirty && customizeForm.cmemail.$error.email">Invalid Email</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 no-pad-r">
                            <div class="row m-b-20">
                                <div class="col-md-2 no-pad-lr pad-t-10">
                                    <p class="customize-credential">Comments</p>
                                </div>
                                <div class="col-md-1 no-pad-lr pad-t-10">
                                    <p class="grey-colour">:</p>
                                </div>
                                <div class="col-md-9 no-pad-lr">
                                    <textarea name="cmcomment" id="cmcomment" class="customize-comments exclude" Placeholder="Tell us what you want" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tell us what you want'" data-ng-model="customizef.cmcomment"></textarea>
                                </div>
                            </div>
                            <div class="row m-b-10">
                                <div class="col-md-3 no-pad-l pad-t-30">
                                    <fieldset data-ng-disabled="uploader.queue.length > 2">
                                        <span id="upload" class="btn fileinput-button upload-image" data-ng-class="{disabled: disabled}">
                                        <span>Upload Image</span>
                                        <input type="file" nv-file-select="" uploader="uploader" data-ng-disabled="disabled" /><br>
                                        </span>
                                    </fieldset>
                                </div>
                                <div class="col-md-9 no-pad-lr">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td data-ng-repeat="item in uploader.queue" style="border:none;">
                                                    <div data-ng-show="uploader.isHTML5" ng-thumb="{ file: item._file,width:100, height: 100 }"></div>
                                                    <span class="remove-custom-image" title="remove" data-ng-click="item.remove()"></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pad-t-20">
                        <div class="col-md-12 text-center">
                            <button class="customization-submit-button" type="submit" data-ng-disabled="!customizeForm.$valid">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addToCartSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none; padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Product has been added to cart successfully</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="verify" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content w-60pc">
            <div class="modal-header" style="border: none; padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Please verify the details entered</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="urlinvalid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px; margin-left: 455px;">
        <div class="modal-content w-60pc">
            <div class="modal-header" style="border: none; padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">The url is invalid</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="noemail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Oops we do not have this email id registered with us</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="validdetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content w-60pc">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Please use valid credentials and try again.</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="facingerror" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content w-full">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Oops! We are facing some error fetching your order for you. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="pwdreset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content w-60pc">
            <div class="modal-header" style="border: none;padding: 8px;">

            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">An email has been sent to you to reset the password</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="fillShipDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Kindly fill all fields of Shipping details to enable us ship your jewellery</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="pwdchanged" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Password changed successfully. Please Login with your new password</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="orderupdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Updated order successfully</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="WrongPromoCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Wrong promocode</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="GoldcoinsPromoErr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="padding-top: 230px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">
                    Coupon code is not applicable for Goldcoins
                </h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="DiwaliPromoErr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="padding-top: 230px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">
                    Coupon code is not applicable for cart whose grand total is greater than 15000
                </h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="WrongPinCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content w-60pc">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Wrong PINCODE</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="returnsuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Return has been placed successfully</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="returnfail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Oops! We are facing some error placing a return for you. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ordercancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Order has been cancelled successfully</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Please login</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addpartner" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Thanks for contacting us. We will get in touch with you shortly</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="enterValidDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="padding-top: 230px;">
        <div class="modal-content w-full">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Please enter valid details and then click submit</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addToCartAlready" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">The product has already been added to the cart. If you would want to increase the quantity of please do so during checkout</h>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="verifydetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Please verify the details entered</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mailsent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">An email has been sent to you to reset the password</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="removewatch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content w-60pc">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Product removed from watchlist</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="removewatcherr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content w-full">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Oops! We are facing some error removing products from your watchlist. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="loadwatcherr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Oops! We are facing some error loading products from your watchlist. Just give us some time and we will be up and running.</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="orderplaced" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Your order has been placed successfully</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reviewError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center pad-t-0">
                <h class="modal-text">Review error</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reviewSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Review added successfully</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reviewCummErr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Cumulative Review error</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reviewListErr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Review List error</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="orderplacederr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Oops! We are facing some error placing the order for you. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="jewelerr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Oops! We are facing some error loading the jewellry for you. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VC500" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Oops!! Promo code is applicable for minimum purchase of Rs. 10000</h>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VC500-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Oops!! Promo code is applicable for minimum purchase of Rs. 5000</h>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addwish" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Product added to wishlist</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addwisherr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Oops! We are facing some error adding your product to watchlist. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="acfetcherr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Oops! We are facing some error fetching your account details for you. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="orderfetcherr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Oops! We are facing some error fetching your order for you. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="orderfetchhisterr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Oops! We are facing some error fetching your order history for you. Just give us some time and we will be up and running</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ordersuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Your order has been placed successfully</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addToCompareSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Product added for comparison</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="diffCompare" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Kindly compare products of same category</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="alreadyAddedToCompare" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text"> Product already added to compare</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addToCompareFail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Maximum 4 products can be added for comparison</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="alreadyInCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">The product has already been added to the cart. Would you like to add another one</h>
            </div>
            <div class="modal-footer">
                <button type="button" data-ng-click="feed='yes'" class="btn btn-default" data-dismiss="modal">Yes</button>
                <button type="button" data-ng-click="feed='no'" class="btn btn-default" data-dismiss="modal">No. Thanks</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="loggedIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">{{response}}</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="BraceletSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Please select Bracelet size before proceeding</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="RingSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Please select Ring size before proceeding</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="BangleSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Please select Bangle size before proceeding</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="newsletterMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">{{responseNLMsg}}</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="contactusMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">{{responseCUMsg}}</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="partnerMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">{{responsePartnerMsg}}</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="customizeSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">Thank you.We will get back to you</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="customizeError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 230px;margin-left: 455px;">
        <div class="modal-content" style="width: 61%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">There is some error, please try again</h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="promocodesuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="padding-top: 230px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">
                    Promo code is applied successfully
                </h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="promocodeerr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="padding-top: 230px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">
                    Oops!! Promo code is already applied
                </h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="dateerr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="padding-top: 230px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">
                    Oops!! You have selected wrong date
                </h>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="passworderr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="padding-top: 230px;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header" style="border: none;padding: 8px;">
            </div>
            <div class="modal-body text-center" style="padding-top: 0px;">
                <h class="modal-text">
                    New password and Re-enter new password are not matching
                </h>
            </div>
        </div>
    </div>
</div>
<script src="/js/jquery.easydropdown.js"></script>
<!-- BackToTop Button -->
<a href="javascript:void(0);" id="scroll" title="Scroll to Top" style="display: none;z-index: 1;">Top<span></span></a>
<!-- End   -->
<script>
    $(document).ready(function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('#scroll').fadeIn();
            } else {
                $('#scroll').fadeOut();
            }
        });
        $('#scroll').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 700);
            return false;
        });
    });
</script>
<toast></toast>
<!--Script for showing modal after website finishes loading-->
<script>
    jQuery(document).ready(function() {
        function showmodal() {
            $('#memberModal').modal('show');
        }
        if (document.cookie.indexOf('visited=true') == -1) {
            var fifteenDays = 1000 * 60 * 60 * 24 * 15;
            var expires = new Date((new Date()).valueOf() + fifteenDays);
            setTimeout(function() {
                showmodal();
                document.cookie = "visited=true";
            }, 10000)
        }
    });
</script>
<!--END of script for loading modal popup-->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/59198f0664f23d19a89b22bb/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
    function maximizetawk(){
        Tawk_API.maximize();
    }
</script>
<!--End of Tawk.to Script-->