<div class="well" data-ng-hide="isLoaded"> Loading</div>
<div class="pad-b-10" data-ng-mouseover="closeFilter()" data-ng-show="isLoaded">
    <div data-ng-show="resultList.length<1" class="row well">
        No products found
    </div>
    <div class="container pad-lr-25">
        <div class="row">
            <div class="col-sm-3" data-ng-show="isGrid" data-ng-repeat="p in resultList track by $index | orderBy:predicate:reverse" style="padding: 0 5px;">
                <div class="row">
                    <div class="col-sm-3 col-md-12 pad-lr-0 m-t-10">
                        <div class="inner_content1 clearfix pad-lr-10 pad-t-10 bg-f8 product-card">
                            <div class="tag">
                                <img data-ng-if="p.ready_to_ship==1" src="/images/list/icons/ready-to-ship.png" alt="ready to ship">
                            </div>
                            <div class="tag">
                                <img data-ng-if="p.bestseller==1" src="/images/list/icons/best-seller-icon.png" alt="ready to ship">
                            </div>
                            <div class="relative">
                                <div class="row product-hover pad-t-5">
                                    <div class="col-md-3 text-center pad-lr-0">
                                        <span class="customize pointer" data-toggle="tooltip" data-placement="top" title="Customize"></span>
                                    </div>
                                    <div class="col-md-6 pad-lr-0 text-center">
                                        <a class="bg-theme br-5 white t14 unbold pad-lr-5 pad-tb-5 inline-block" data-ng-href="/product/{{p.urlslug}}.html" target="_blank">View Details</a>
                                    </div>
                                    <div class="col-md-3 pad-r-5 pad-l-10 text-center">
                                        <span class="wishlist not-me pointer" data-ng-click="addToWatchlist(p)" data-toggle="tooltip" data-placement="top" title="Wishlist!"></span>
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $('[data-toggle="tooltip"]').tooltip();   
                                        });
                                    </script>
                                </div>
                                <a data-ng-href="/product/{{p.urlslug}}.html" target="_blank">
                                    <img src="/images/list/icons/image filler.jpg" actual-image="/images/products-v2/{{p.VC_SKU}}-t.jpg" alt="{{p.title}}">
                                </a>
                            </div>
                            <div class="row">
                                <div class="col-md-12 pad-lr-0">
                                    <h3 class="text-limit t15 unbold exclude m-b-5 m-t-15">{{p.title}}</h3>
                                </div>
                            </div>
                            <div class="row m-b-5">
                                <div class="col-md-12 pad-lr-0">
                                    <span class="t15 theme-text-special bold pad-r-15  r12-t14">
                                        RS. {{p.price_after_discount | INR}}
                                    </span>
                                    <span class="line-through pad-r-15 r12-t13"> RS. {{p.price_before_discount | INR}} </span>
                                    <span class="pull-right bg-blue2 inline-block white pad-lr-5 r12-t13">{{p.discount}}% Off</span>
                                </div>
                                <!--<div class="col-sm-8 pad-l-0 pad-r-5">
                                    <div class="pad-t-5 t12">
                                        
                                        
                                    </div>
                                    <div>
                                        
                                    </div>
                                </div> Hidden by PR -->
                                <!--<div class="col-md-4 pad-r-0 pad-l-5">
                                    <div class="row" style="padding-top:11px;">
                                        <div class="col-md-6 pad-l-0 pad-r-5">
                                            <img class="pointer" style="width:30px;" src="/images/list/icons/compare.png" title="Compare" alt="compare products">
                                        </div>
                                        <div class="col-md-6 pad-l-5 pad-r-0" data-ng-click="addToWatchlist(p)">
                                            <img class="pointer" src="/images/list/icons/wishlist.png" title="Wishlist" alt="add product to wishlist">
                                        </div>
                                    </div>
                                </div> Hidden by PR -->
                            </div>
                            <div class="row">
                                <div product-star-rating rating-value="{{totalRating}}" max="5" reviewscore="totalRating"></div>
                                
                                <script>
                                    
                                </script>
                                
                                <!--<div class="col-md-12 pad-lr-0">
                                    <ul class="product-card-rating">
                                        <li class="star-rated"></li>
                                        <li class="star-rated"></li>
                                        <li class="star-rated"></li>
                                        <li class="star-unrated"></li>
                                        <li class="star-unrated"></li>
                                    </ul>
                                </div> Hidden by PR Tempo basis -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    categoryapp.directive('productStarRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '@',
            max: '=',
            onRatingSelected: '&',
            reviewscore: '='
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                    scope.reviewscore=scope.ratingValue;
                }
            });
        }
    }
});
</script>