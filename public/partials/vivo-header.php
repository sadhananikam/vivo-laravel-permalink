<div class="hidden-background-colour">
    <div class="container">
<!--         class removed of container hidden-header-structure-->
        <div class="row">
            <div class="col-md-8">
                <i class="fa fa-envelope lgray-c" aria-hidden="true"></i>
                <span class="white m-lr-10">
                    hello@vivocarat.com
                </span>
                <span class="white">|</span>
                <i class="fa fa-phone lgray-c pad-l-10 lgray-c" aria-hidden="true"></i>
                <span class="white m-lr-10">
                    +91 &nbsp;9167645314
                </span>
                <span class="white">|</span>
                <div class="p_loc_text m-l-10 inline-block black">
                    <i class="fa fa-map-marker t15" aria-hidden="true"></i>
                    <span class="pad-lr-5">Partner Locator</span>
                    <span class="p_loc_caret">
                        <i class="fa fa-caret-down"></i>
                    </span>
                    <i class="fa fa-question-circle t15" aria-hidden="true"></i>
                    <!-- Partner Locator Dialogue -->
                    <div class="p_loc_dialogue">
                        <div class="bg-skin-fair">
                            <div class="pad-10">
                                <h3 class="uppercase blue-custom t21 bold text-center">Vivo Partner Stores</h3>
                            </div>
                            <div class="row">
                                <div class="w-full">
                                    <div class="icon-home sprite-plocate float-left"></div>
                                    <div class="pad-t-15">
                                        <p class="exclude">
                                            <span class="uppercase blue-custom bold-600">Trusted Jewellers</span> offering 10000+designs on the VivoCarat Platform.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="w-full">
                                    <div class="icon-ring-customize sprite-plocate float-left"></div>
                                    <div class="pad-t-25">
                                        <p class="exclude">
                                            <span class="uppercase blue-custom bold-600">REPAIR/POLISH</span> jewellery bought on VivoCarat.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="w-full">
                                    <div class="icon-mesurement sprite-plocate float-left"></div>
                                    <div class="pad-t-15">
                                        <p class="exclude"><span class="uppercase blue-custom bold-600">RING/BANGLE SIZING</span> assistance before ordering online.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <a href="/seller-locate" target="_self" class="btn btn-vivo-bold m-tb-15 block-center w-50pc t18 uppercase">Locate Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-ng-if="!authenticated" class="col-md-4 text-right unauthenticated-structure" style="margin-top:3px;">
                <a class="white" href="/login" target="_self">
                    My Account
                </a>
            </div>
            <div data-ng-if="authenticated" class="col-md-4 authenticated-structure">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-inline m-b-0 white pull-right">
                            <li>
                                <span class="hi-username">Hi, {{name}}</span>
                                <h class="white">|</h>
                            </li>
                            <li class="relative my_account">
                                <a class="pointer white" href="/account" target="_self">
                                    My Account
                                </a>
                                <span class="my_account_caret">
                                    <i class="fa fa-caret-down black-38 t20"></i>
                                </span>
                                <ul class="my_account_menus">
                                    <li>
                                        <a class="pointer" href="/orders" target="_self">
                                            My Orders
                                        </a>
                                    </li>
                                    <li>
                                        <a class="pointer" href="/wishlist" target="_self">
                                            My Wishlist
                                        </a>
                                    </li>
                                    <li>
                                        <a class="pointer" href="/address" target="_self">
                                            My Address
                                        </a>
                                    </li>
                                    <li>
                                        <a class="userProfile pointer" data-ng-click="logoutJquery()">
                                            Sign Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container" style="height: 88px;">
    <div class="row" style="margin-top: 8px;">
        <div class="col-md-2">
            <a href="/" target="_self" title="VivoCarat Retail Pvt. Ltd.">
                <img src="/images/header/logos/VivoCarat logo.png" class="pad-tb-20" alt="VivoCarat Retail Pvt. Ltd.">
            </a>
        </div>
        <div class="col-md-8 text-center usp-header">
            <a href="/faq" target="_self">
                Trusted Jewellers &nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;Certified &amp; Hallmarked &nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Free Shipping &nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; Easy Return Policy
            </a>
        </div>
        <div class="col-md-2 text-right">
            <a href="/checkout" target="_self">
                <img class="cart-image-position exclude" src="/images/header/icons/cart bag.png" alt="checkout cart">
                <div class="cart-length" data-ng-if="cart.length>0">
                    ({{cart.length}})
                </div>
                <div class="cart-length-nil" data-ng-if="cart.length<1 || !cart || !cart.length">
                    (0)
                </div>
            </a>
        </div>
    </div>
</div>
<vivo-header-menu></vivo-header-menu>
<div class="row">
    <div class="row compare-background" data-ng-if="compare.length>0">
        <div data-ng-repeat="p in compare" class="col-md-2 pad-l-0 pad-r-10">
            <div class="row">
                <div class="col-md-12 compare-product-background">
                    <span class="remove" title="remove" data-ng-click="removeFromCompare($index)"></span>
                    <a href="/product/{{p.urlslug}}.html" target="_self">
                        <img class="img-responsive" src="/images/products-v2/{{p.VC_SKU}}-1.jpg" alt="{{p.title}}">
                        <div class="row bg-white pad-b-10">
                            <div class="col-md-12">
                                <div class="row" data-ng-hide="p.discount==0">
                                    <div class="col-md-6 grid-price-before pad-l-0">
                                        RS. {{p.price_before_discount | INR}}
                                    </div>
                                    <div class="col-md-6 grid-off">
                                        {{p.discount}}% OFF
                                    </div>
                                </div>
                                <div class="row" data-ng-if="!(p.discount==0)">
                                    <div class="col-md-12 grid-price-after pad-lr-0">
                                        RS. {{p.price_after_discount | INR}}
                                    </div>
                                </div>
                                <div class="row" data-ng-if="p.discount==0">
                                    <div class="col-md-12 grid-price-after pad-lr-0 pad-t-20">
                                        RS. {{p.price_after_discount | INR}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-2 compare-background" data-ng-if="compare.length>0 && compare.length<4">
            <a href="/jewellery/All-{{compare[0].category}}.html" target="_self">
                <img src="/images/product/add product.jpg" alt="add product to compare">
            </a>
        </div>
        <div class="col-md-2 pull-right">
            <div class="row">
                <div class="col-md-12 text-center" style="padding-top: 65px;">
                    <div class="text-center pad-t-20">
                        <a class="btn btn-vivo br-0" data-ng-click="checkCompare()" role="button" align="right">COMPARE</a>
                    </div>
                    <div class="text-center pad-t-20">
                        <a class="btn btn-vivo" data-ng-click="removeAllCompare()" role="button" style="border-radius: 0;" align="right">CLEAR ALL</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>