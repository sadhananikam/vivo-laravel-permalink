<div class="new-filter bg-pink-light">
    <div class="row m-b-15 pad-t-15">
        <div class="col-md-6">
            <h1 class="exclude m-0 t21 inline-block">Gold Rings: </h1>
            <span class="t16 theme-text">Total 5000 items</span>
        </div>
        <div class="col-md-6">
            <div id="easy-sort" class="btn-group float-right b1-lgray-d5 pad-0 bg-white br-5">
                <button type="button" class="btn-easy-sort w-full form-control btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="pad-r-5">Sort By: </span>
                    <span class="blue-light">Popularity</span>
                    <span class="pad-lr-10">|</span>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="dropdown-menu b-none w-full" role="menu">
                    <li><a href="#"><span class="blue-light">Popularity</span></a></li>
                    <li><a href="#"><span class="blue-light">Price L to H</span></a></li>
                    <li><a href="#"><span class="blue-light">Price H to L</span></a></li>
                </ul>
            </div>
            <script>
                $(document).ready(function(){
                    //Kill Me Function
                    $('.kill-me').click(function(){
                        $(this).fadeOut(100);
                    });
                    
                    //Easy Sort Select Function
                    $('.dropdown-menu a').on('click', function(){    
                        $('.dropdown-toggle').html($(this).html() +
                        '<span class="pad-lr-10">|</span>' +
                        '<i class="fa fa-angle-down" aria-hidden="true"></i>').prepend('<span class="pad-r-5">Sort By: </span>');    
                    })
                });
            </script>
        </div>
    </div>
    <div class="row m-tb-15">
        <div class="col-md-12">
            <div class="btn-filter relative inline-block" >
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Price <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Below 5000
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 5000 - 10000
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 10000 - 15000
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 15000 - 25000
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 25000 - 50000
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Above 50000
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Metal <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Yellow Gold
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> White Gold
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Rose Gold
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Platinum
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Silver
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Metal Purity <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu w-240">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 24 KT(999)--only goldcoins
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 24 KT(995)--only goldoins
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 22 KT
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 18 KT
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 14 KT
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Sterling Silver(995)
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Gold Weight <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu w-200">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 0 - 2 grams
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 2 - 5 grams
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 5 - 10 grams
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> More than 10 grams
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Occasion <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Dailywear
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Partywear
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> OfficeWear
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Religious
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Casual
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Traditional
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Gifting
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Bridal
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Gender <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Men
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Women
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Kids
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Baby Girls
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Baby Boys
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Stone <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li><span class="uppercase blue-light bold">Stone Type</span>
                        <ul class="li-none m-t-15 tiny-scroll">
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> Diamond
                                </label>
                            </li>
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> Ruby
                                </label>
                            </li>
                        </ul>
                    </li>
                    <li class="divider"></li>
                    <li><span class="uppercase blue-light bold">Stone Colour</span>
                        <ul class="btn-filter-nested-menus li-none m-t-15 tiny-scroll">
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> White
                                </label>
                            </li>
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> Red
                                </label>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block hidden-md">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Diamond Count <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Solitaire
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Single Stone
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Two Stone
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Multi Stone
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block hidden-md">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Discount <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 30 - 40% off
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 20 - 30% off
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 10 - 20% off
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> 0 - 10% off
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Brands <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Data
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Data
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Data
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Data
                        </label>
                    </li>
                </ul>
            </div>
            
            <div class="btn-filter relative inline-block hidden-md im-last">
                <button class="btn btn-default" type="button" data-toggle="dropdown">
                    Delivery Time <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Data
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Data
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Data
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Data
                        </label>
                    </li>
                </ul>
            </div>
            <div id="sp-case1" class="btn-filter relative inline-block hidden-lg im-last">
                <button class="btn btn-default pad-extra" type="button" data-toggle="dropdown">
                    More <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <ul class="btn-filter-dropdown inline-block li-none m-b-0 dropdown-menu">
                    <li><span class="uppercase blue-light bold">Diamond Count</span>
                        <ul class="btn-filter-nested-menus li-none m-t-15 tiny-scroll">
                            <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Solitaire
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Single Stone
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Two Stone
                        </label>
                    </li>
                    <li>
                        <label for="price1">
                            <input type="checkbox" name="price1"> Multi Stone
                        </label>
                    </li>
                        </ul>
                    </li>
                    
                    <li class="divider"></li>
                    
                    <li><span class="uppercase blue-light bold">Discount</span>
                        <ul class="btn-filter-nested-menus li-none m-t-15 tiny-scroll">
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> 30 - 40% off
                                </label>
                            </li>
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> 20 - 30% off
                                </label>
                            </li>
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> 10 - 20% off
                                </label>
                            </li>
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> 0 - 10% off
                                </label>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="divider"></li>
                    
                    <li><span class="uppercase blue-light bold">Delivery Time</span>
                        <ul class="li-none m-t-15 tiny-scroll">
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> Data
                                </label>
                            </li>
                            <li>
                                <label for="price1">
                                    <input type="checkbox" name="price1"> Data
                                </label>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row m-t-15 pad-b-15 hmin-37">
        <div class="col-md-12">
            <div class="w-10pc float-left">
                <span class="uppercase m-r-5">Filtered By: </span>
            </div>
            <div class="w-90pc float-left">
                <span class="kill-me filter-tags bg-theme white pad-3-7 br-10 m-r-10">
                    18 k <span class="t16">&times;</span>
                </span>
            </div>
        </div>
    </div>
</div>