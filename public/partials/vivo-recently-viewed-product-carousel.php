<div data-ng-hide="recently.length<1 || recently == null">
    <div class="row">
        <div class="col-xs-11">
            <h2 class="hr"><span>RECENTLY VIEWED</span></h2>
        </div>
        <div class="col-xs-1">
            <a data-ng-init="look5='/images/home/icons/home icons.png'" data-ng-mouseout="look5='/images/home/icons/home icons.png'" data-ng-mouseover="look5='/images/home/icons/home icons.png'" class="left carousel-control featured-products-arrow-background" data-target="#carousel-product-recently-viewed" data-slide="prev">
                <div class="left-arrow" data-ng-src="{{look5}}"></div>
            </a>

            <a data-ng-init="look6='/images/home/icons/home icons.png'" data-ng-mouseout="look6='/images/home/icons/home icons.png'" data-ng-mouseover="look6='/images/home/icons/home icons.png'" class="right carousel-control featured-products-arrow-background" data-target="#carousel-product-recently-viewed" data-slide="next">
                <div class="right-arrow featured-products-right-arrow-position" data-ng-src="{{look6}}"></div>
            </a>
        </div>
    </div>
    <div id="carousel-product-recently-viewed" class="carousel slide pad-t-50" data-ride="carousel" style="position:relative;z-index:1;">
        <!-- Wrapper for slides -->
        <div class="height carousel-inner m-b-15">
            <div class="item active row">
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>0">
                    <a data-ng-href="/product/{{recently[0].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[0].VC_SKU}}-t.jpg" alt="{{recently[0].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[0].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[0].category != 'Rings' && recently[0].category != 'Bangles' && recently[0].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[0])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[0].category == 'Rings' || recently[0].category == 'Bangles' || recently[0].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[0].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>1">
                    <a data-ng-href="/product/{{recently[1].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[1].VC_SKU}}-t.jpg" alt="{{recently[1].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[1].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[1].category != 'Rings' && recently[1].category != 'Bangles' && recently[1].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[1])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[1].category == 'Rings' || recently[1].category == 'Bangles' || recently[1].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[1].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>2">
                    <a data-ng-href="/product/{{recently[2].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[2].VC_SKU}}-t.jpg" alt="{{recently[2].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[2].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[2].category != 'Rings' && recently[2].category != 'Bangles' && recently[2].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[2])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[2].category == 'Rings' || recently[2].category == 'Bangles' || recently[2].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[2].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>3">
                    <a data-ng-href="/product/{{recently[3].urlslug}}.html" target="_self">
                        <img data-ng-src="/images/products-v2/{{recently[3].VC_SKU}}-t.jpg" alt="{{recently[3].title}}">
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[3].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[3].category != 'Rings' && recently[3].category != 'Bangles' && recently[3].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[3])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[3].category == 'Rings' || recently[3].category == 'Bangles' || recently[3].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[3].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item row" data-ng-if="recently.length>4">
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>4">
                    <a data-ng-href="/product/{{recently[4].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[4].VC_SKU}}-t.jpg" alt="{{recently[4].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[4].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[4].category != 'Rings' && recently[4].category != 'Bangles' && recently[4].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[4])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[4].category == 'Rings' || recently[4].category == 'Bangles' || recently[4].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[4].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>5">
                    <a data-ng-href="/product/{{recently[5].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[5].VC_SKU}}-t.jpg" alt="{{recently[5].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[5].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[5].category != 'Rings' && recently[5].category != 'Bangles' && recently[5].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[5])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[5].category == 'Rings' || recently[5].category == 'Bangles' || recently[5].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[5].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>6">
                    <a data-ng-href="/product/{{recently[6].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[6].VC_SKU}}-t.jpg" alt="{{recently[6].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[6].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[6].category != 'Rings' && recently[6].category != 'Bangles' && recently[6].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[6])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[6].category == 'Rings' || recently[6].category == 'Bangles' || recently[6].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[6].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>7">
                    <a data-ng-href="/product/{{recently[7].urlslug}}.html" target="_self">
                        <img data-ng-src="/images/products-v2/{{recently[7].VC_SKU}}-t.jpg" alt="{{recently[7].title}}">
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[7].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[7].category != 'Rings' && recently[7].category != 'Bangles' && recently[7].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[7])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[7].category == 'Rings' || recently[7].category == 'Bangles' || recently[7].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[7].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item row" data-ng-if="recently.length>8">
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>8">
                    <a data-ng-href="/product/{{recently[8].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[8].VC_SKU}}-t.jpg" alt="{{recently[8].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[8].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[8].category != 'Rings' && recently[8].category != 'Bangles' && recently[8].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[8])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[8].category == 'Rings' || recently[8].category == 'Bangles' || recently[8].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[8].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>9">
                    <a data-ng-href="/product/{{recently[9].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[9].VC_SKU}}-t.jpg" alt="{{recently[9].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[9].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[9].category != 'Rings' && recently[9].category != 'Bangles' && recently[9].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[9])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[9].category == 'Rings' || recently[9].category == 'Bangles' || recently[9].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[9].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>10">
                    <a data-ng-href="/product/{{recently[10].urlslug}}.html" target="_self">
                        <div class="product_image">
                            <img data-ng-src="/images/products-v2/{{recently[10].VC_SKU}}-t.jpg" alt="{{recently[10].title}}">
                        </div>
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[10].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[10].category != 'Rings' && recently[10].category != 'Bangles' && recently[10].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[10])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[10].category == 'Rings' || recently[10].category == 'Bangles' || recently[10].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[10].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3 no-pad-lr inner_content1 clearfix" data-ng-if="recently.length>11">
                    <a data-ng-href="/product/{{recently[11].urlslug}}.html" target="_self">
                        <img data-ng-src="/images/products-v2/{{recently[11].VC_SKU}}-t.jpg" alt="{{recently[11].title}}">
                        <div class="row">
                            <div class="col-sm-12 grid-price-after1">
                                RS. {{recently[11].price_after_discount | INR}}
                            </div>
                        </div>
                        <div class="row m-b-10 pad-t-10">
                            <div class="col-sm-12 text-center" data-ng-if="recently[11].category != 'Rings' && recently[11].category != 'Bangles' && recently[11].category != 'Bracelets'">
                                <a class="btn btn-book" data-ng-click="buyNow(recently[11])"> 
                                    Buy Now
                                </a>
                            </div>
                            <div class="col-sm-12 text-center" data-ng-if="recently[11].category == 'Rings' || recently[11].category == 'Bangles' || recently[11].category == 'Bracelets'">
                                <a class="btn btn-book" data-ng-href="/product/{{recently[11].urlslug}}.html" target="_self"> 
                                    Buy Now
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>