<!DOCTYPE html>
<html lang="en" data-ng-app="vivoBrands">
<head>
    <meta http-equiv="Content-Language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="VivoCarat showcases the best jewellery brands from all across India. They are the real mastermind behind our impeccable designs and unmatched quality products.">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="https://www.vivocarat.com/brands" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Our Brands | VivoCarat" />
    <meta property="og:description" content="VivoCarat showcases the best jewellery brands from all across India. They are the real mastermind behind our impeccable designs and unmatched quality products." />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>Our Brands | VivoCarat</title>
    <meta name="keywords" content="jewellery brands,vivocarat brands" />
    <meta name="description" content="VivoCarat showcases the best jewellery brands from all across India. They are the real mastermind behind our impeccable designs and unmatched quality products." />
    <!-- SEO-->
    <meta name="robots" content="index,follow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />
    <link rel="canonical" href="https://www.vivocarat.com/brands">
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands">
    <link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" media="all">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
    <script>
        var getUrlParameter=function getUrlParameter(sParam){var sPageURL=decodeURIComponent(window.location.search.substring(1)),sURLVariables=sPageURL.split('&'),sParameterName,i;for(i=0;i<sURLVariables.length;i++){sParameterName=sURLVariables[i].split('=');if(sParameterName[0]===sParam){return sParameterName[1]===undefined?!0:sParameterName[1]}}};var isMobile={Android:function(){return navigator.userAgent.match(/Android/i)},BlackBerry:function(){return navigator.userAgent.match(/BlackBerry/i)},iOS:function(){return navigator.userAgent.match(/iPhone|iPad|iPod/i)},Opera:function(){return navigator.userAgent.match(/Opera Mini/i)},Windows:function(){return navigator.userAgent.match(/IEMobile/i)},any:function(){return(isMobile.Android()||isMobile.BlackBerry()||isMobile.iOS()||isMobile.Opera()||isMobile.Windows())}};var w=window.innerWidth;if(isMobile.any()&&w<=1024){if(window.location.search.indexOf('utm_campaign')>-1)
        {var utm_campaign=getUrlParameter('utm_campaign');if(utm_campaign.length)
        {var utm_source=getUrlParameter('utm_source');var utm_medium=getUrlParameter('utm_medium');document.location=document.location="/m"+document.location.pathname+"?utm_campaign="+utm_campaign+"&utm_source="+utm_source+"&utm_medium="+utm_medium}
        else{document.location="/m"+document.location.pathname}}
        else{document.location="/m"+document.location.pathname}}
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-67690535-1','auto');ga('send','pageview')
    </script>
</head>
<body ng-cloak>
    <vivo-header></vivo-header>
    <div data-ng-controller='brandsCtrl'>
        <div class="container pad-b-15">
            <section class="exclude m-tb-15">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="breadcrumb bg-white pad-0 m-b-0">
                            <li><a href="/" target="_self" class="theme-text-special">HOME</a></li>
                            <li>BRANDS</li>
                        </ul>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hr"><span>OUR BRANDS</span></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12 no-pad-lr">
                                <a href="/brands/KaratCraft" target="_self">
                                    <img src="/images/brands/our-brands/karatcraft.png" alt="Karatcraft Jewellery">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/IskiUski" target="_self">
                                    <img src="/images/brands/our-brands/iski-uski.png" alt="Iski Uski Jewellery">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <!--    <p class="brands-heading">What</p>-->
                                <p class="exclude">
                                    Introducing the chain of retails stores that has collaborated with Vivocarat.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Glitter Jewels" target="_self">
                                    <img src="/images/brands/our-brands/glitter.png" alt="Glitter Jewellery">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Charu-Jewels" target="_self">
                                    <img src="/images/brands/our-brands/charu.png" alt="Charu Jewels">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Kundan Jewellers" target="_self">
                                    <img src="/images/brands/our-brands/kundan.png" alt="Kundan Jewellery">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/ZKD-Jewels" target="_self">
                                    <img src="/images/brands/our-brands/zkd.png" alt="ZKD Jewels">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-20">
                                <p class="exclude">
                                    They are the real mastermind behind our impeccable designs and unmatched quality products.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Incocu Jewellers" target="_self">
                                    <img src="/images/brands/our-brands/incocu.png" alt="INCOCU Jewellery">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/OrnoMart" target="_self">
                                    <img src="/images/brands/our-brands/ornamart.png" alt="Ornomart">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Myrah-Silver-Works" target="_self">
                                    <img src="/images/brands/our-brands/myrah.png" alt="Myrah Silver Works">
                                </a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Lagu Bandhu" target="_self">
                                    <img src="/images/brands/our-brands/lagu.png" alt="Lagu Bandhu Jewellers">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Mayura Jewellers" target="_self">
                                    <img src="/images/brands/our-brands/mayura.png" alt="Mayura Jewellers">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Tsara-Jewellery" target="_self">
                                    <img src="/images/brands/our-brands/tsara.png" alt="Tsara Jewellery">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12 no-pad-lr">
                                <a href="/brands/Megha Jewellers" target="_self">
                                    <img src="/images/brands/our-brands/megha.png" alt="Megha Jewellery">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Regaalia Jewels" target="_self">
                                    <img src="/images/brands/our-brands/regaalia.png" alt="Regalia Jewellery House">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-20">
                                <p class="exclude">
                                    Take a look at our celebrated stores in and across India that deals in the best of gold and diamond jewellery.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Arkina-Diamonds" target="_self">
                                    <img src="/images/brands/our-brands/arkina.png" alt="Arkina Diamonds">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/PP-Gold" target="_self">
                                    <img src="/images/brands/our-brands/parshwa-padmavati.png" alt="Parshwa Padmavati Gold">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Sarvada-Jewels" target="_self">
                                    <img src="/images/brands/our-brands/sarvada.png" alt="Sarvada Jewels">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-pad-lr pad-t-30">
                                <a href="/brands/Shankaram Jewellers" target="_self">
                                    <img src="/images/brands/our-brands/shankaram.png" alt="Shankaram Fine Diamond Jewellery">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <vivo-footer></vivo-footer>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>
    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoBrands.js"></script>
    <!-- end angularjs modules -->
    <script src="/app/data.js"></script>
    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->
    <script src="/device-router.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(e,t,n,c,o,a,f){e.fbq||(o=e.fbq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},e._fbq||(e._fbq=o),o.push=o,o.loaded=!0,o.version="2.0",o.queue=[],(a=t.createElement(n)).async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js",(f=t.getElementsByTagName(n)[0]).parentNode.insertBefore(a,f))}(window,document,"script"),fbq("init","293278664418362"),fbq("track","PageView");
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->    
    <!-- onesignal start   -->
    <link rel="manifest" href="/manifest.json">
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        var OneSignal = window.OneSignal || [];
        OneSignal.push(["init", {
            appId: "07f1f127-398a-4956-abf1-3d026ccd94d2",
            autoRegister: true,
            notifyButton: {
                enable: false /* Set to false to hide */
            }
        }]);
    </script>
    <!-- onesignal end   -->    
</body>
</html>