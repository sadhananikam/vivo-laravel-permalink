<!DOCTYPE html>
<html lang="en" data-ng-app="vivoMoissanite">

<head>

    <meta http-equiv="Content-Language" content="en" />

    <meta name="keywords" content="" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Vivocarat">
    <meta name="author" content="Vivo">
    <link rel="icon" href="/images/icons/favico.png" type="image/x-icon" />
    <meta property="og:url" content="http://www.vivocarat.com" />
    <meta property="og:type" content="Online Jewellery" />
    <meta property="og:title" content="Vivocarat" />
    <meta property="og:description" content="India's finest online jewellery" />
    <meta property="og:image" content="https://www.vivocarat.com/images/about-us/vivo-on-the-go.png" />
    <title>VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store</title>
    <meta name="keywords" content="online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery" />
    <meta name="description" content="VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more." />

    <!-- SEO-->
    <meta name="robots" content="noindex,nofollow" />
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />

    <link rel="canonical" href="https://www.vivocarat.com/moissanite">

    <link href="/css/style.css" rel="stylesheet" media="all">

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/megamenu.css" rel="stylesheet" media="all">
    <link href="/css/etalage.css" rel="stylesheet" media="all">
    <link href="/css/angular.rangeSlider.css" rel="stylesheet" media="all">
    <link href="/css/kendo.common-material.min.css" rel="stylesheet">
    <link href="/css/kendo.material.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">

    <script>
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        var w = window.innerWidth;

        if (isMobile.any() && w <= 1024) {
            document.location = "/m"+document.location.pathname;
        }
    </script>
    
    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        /*fbq('init', '293278664418362', {
            em: 'insert_email_variable,'
        });*/
        fbq('init', '293278664418362');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>

<body>
    <vivo-header></vivo-header>

    <div data-ng-controller='moissaniteCtrl'>

        <div class="container">

            <!-- Old code hidden by PR
            <div class="row padding-bottom-20px">
                <div class="col-lg-4 vivocarat-heading">
                    <h2>WHAT IS MOISSANITE</h2>
                </div>

                <div class="col-lg-8 categoryGrad no-padding-left-right margin-top-68px">

                </div>
            </div>
            Old code hidden by PR -->

            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>WHAT IS MOISSANITE</span></h2>
                    </div>
                </div>
            </section>
            <section>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" width="606" height="341" src="https://www.youtube-nocookie.com/embed/5mC4reX_Xj8?rel=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 paragraph-structure">
                        <p class="short-snippet">
                            A jewel born from the stars...and perfected on earth
                        </p>

                        <p class="snippet-description">
                            Moissanite is a new category of gemstones with fire, brilliance and lustre more than any other gemstones in the world including diamonds!
                            <br><br> Discovered from meteorites each Moissanite gemstone is hand cut to attain perfection. Essentially composed of carbon and silica it has an incredible hardness of 9.25 just next to diamonds. This would never let it scratch or become cloudy with wear or dull with time. Moissanites are guaranteed to never lose their optical properties.
                            <br><br> In fact, they are such close match to diamonds that only a laboratory can tell the difference from diamonds – NO ONE ELSE!
                        </p>

                        <p class="snippet-description">
                            What more could you ask for? – How about the fact that Fiona Moissanite Solitaires cost about 1/10th the cost of a diamond!
                            <br><br> Moissanite gemstones truly are Forever Brilliant.
                        </p>

                    </div>

                </div>
            </section>

            <!-- Old code hidden by PR
            <div class="row padding-bottom-30px">
                <div class="col-lg-6 vivocarat-heading">
                    <h2>GEMSTONE COMPARISON CHART</h2>
                </div>

                <div class="col-lg-6 categoryGrad no-padding-left-right margin-top-68px">

                </div>
            </div>
            Old code hidden by PR -->

            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>GEMSTONE COMPARISON CHART</span></h2>
                    </div>
                </div>
            </section>

            <section>
                <div class="row">
                    <div class="col-md-12">

                        <table class="w-full tbl_moissanite">
                            <tbody>
                                <tr class="second-heading-background text-center">
                                    <th></th>
                                    <th colspan="3">BEAUTY</th>
                                    <th>DURABILITY</th>
                                </tr>

                                <tr class="second-heading-background text-center">
                                    <th>GEMSTONE</th>
                                    <th>BRILLIANCE REFRACTIVE INDEX(RI)</th>
                                    <th>FIRE DISPERSION</th>
                                    <th>HARDNESS MOHS SCALE</th>
                                    <th>TOUGHNESS</th>
                                </tr>

                                <tr class="bg_pink">
                                    <td>MOISSANITE</td>
                                    <td>2.65-2.69</td>
                                    <td>0.104</td>
                                    <td>9.25</td>
                                    <td>EXCELLENT</td>
                                </tr>

                                <tr>
                                    <td>DIAMOND</td>
                                    <td>2.42</td>
                                    <td>0.044</td>
                                    <td>10</td>
                                    <td>EXCELLENT <sup>*</sup></td>
                                </tr>

                                <tr>
                                    <td>RUBY</td>
                                    <td>1.77</td>
                                    <td>0.018</td>
                                    <td>9</td>
                                    <td>EXCELLENT <sup>*</sup><sup>*</sup></td>
                                </tr>

                                <tr>
                                    <td>SAPHHIRE</td>
                                    <td>1.77</td>
                                    <td>0.018</td>
                                    <td>9</td>
                                    <td>EXCELLENT <sup>*</sup><sup>*</sup><sup>*</sup></td>
                                </tr>

                                <tr>
                                    <td>EMERALD</td>
                                    <td>1.58</td>
                                    <td>0.014</td>
                                    <td>7.50</td>
                                    <td>GOOD TO POOR</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

            <!-- Old code hidden by PR
            <div class="row padding-bottom-30px">
                <div class="col-lg-1 vivocarat-heading">
                    <h2>FAQs</h2>
                </div>

                <div class="col-lg-11 categoryGrad no-padding-left-right margin-top-68px">

                </div>
            </div>
            Old code hidden by PR -->

            <section>
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="hr"><span>FAQs</span></h2>
                    </div>
                </div>
            </section>

            <section>
                <div class="row">
                    <div class="col-md-12">

                        <h3>Q. Will the brilliance of moissanite ever fade?</h3>

                        <p>
                            No. Charles &amp; Colvard® provides a Lifetime Warranty which guarantees that every Charles &amp; Colvard Created Moissanite® gemstone will maintain its brilliance and fire for its lifetime.
                        </p>

                        <h3>Q. Is moissanite natural or man-made?</h3>

                        <p>
                            Moissanite does occur in nature; however, the crystals are extremely small and scarce. Today, moissanite is grown through a proprietary and patented thermal growth process, then fashioned into beautiful gems and distributed exclusively by Charles &amp; Colvard®.
                        </p>

                        <h3>Q. Will moissanite fade or change color over time?
                        </h3>

                        <p>
                            No. There are no likely situations in which the color of moissanite will be permanently changed.
                        </p>

                        <h3>Q. Is moissanite resistant to scratching?</h3>

                        <p>
                            Yes. Moissanite is durable, tough, and extremely resistant to scratching and abrasion. With a hardness of 9.25, moissanite is harder than all other gemstones except diamond.
                        </p>

                        <h3>Q. Does moissanite break easily?</h3>

                        <p>
                            No. Moissanite is one of the toughest known gemstones. Studies in high pressure research have shown that moissanite is highly resistant to breaking and chipping.
                        </p>

                        <h3>Q. How should I care for my moissanite jewelry?</h3>

                        <p>
                            You may clean moissanite the same way you would diamond or any other fine gemstone. You can also clean your moissanite jewelry using a commercial (non-acid based) jewelry cleaner, or with mild soap and water using a soft toothbrush.
                        </p>

                        <h3>Q. Can moissanite gemstones be re-set into new jewelry?
                        </h3>

                        <p>
                            Yes. Re-setting your moissanite gemstone into a new piece of jewelry can be done by an experienced jeweller.
                        </p>

                    </div>
                </div>
            </section>

        </div>

    </div>

    <vivo-footer></vivo-footer>

    <script src="/js/jquery.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/css3-mediaqueries.js"></script>
    <script src="/js/megamenu.js"></script>
    <script src="/js/slides.min.jquery.js"></script>
    <script src="/js/jquery.jscrollpane.min.js"></script>
    <script src="/js/jquery.easydropdown.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/angular-ui-router.min.js"></script>
    <script src="/js/angular-animate.min.js"></script>
    <script src="/js/angular-sanitize.js"></script>
    <script src="/js/satellizer.min.js"></script>
    <script src="/js/angular.rangeSlider.js"></script>
    <script src="/js/select.js"></script>
    <script src="/js/toaster.js"></script>
    <script src="/js/kendo.all.min.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="/js/taggedInfiniteScroll.js"></script>
    <script src="/js/jquery.easing.min.js"></script>
    <script src="/js/angular-google-plus.min.js"></script>
    <script src="/js/jquery.etalage.min.js"></script>
    <script src="/js/jquery.simplyscroll.js"></script>

    <!--  start angularjs modules  -->
    <script src="/app/modules/vivoCommon.js"></script>
    <script src="/app/modules/vivoMoissanite.js"></script>
    <!-- end angularjs modules -->

    <script src="/app/data.js"></script>
    <!--<script src="/app/directives.js"></script>-->

    <!-- Start include Controller for angular -->
    <script src="/app/ctrls/footerCtrl.js"></script>
    <!--  Start include Controller for angular -->

    <script src="/device-router.js"></script>

    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-67690535-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>

</html>