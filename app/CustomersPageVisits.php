<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomersPageVisits extends Model
{
    protected $table = 'customers_page_visits';
    protected $fillable = array('id','uid','visited_at','visited_url','visited_through');
}
