<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customize extends Model
{
  protected $table = 'customize';
  protected $fillable = array('id','name','email','phone','comment','image_1','image_2','image_3');
}
