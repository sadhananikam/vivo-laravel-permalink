<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = array('cart_id','cart_ref_id','user_id','product_id','quantity','is_active');
}