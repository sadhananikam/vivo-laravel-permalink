<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Allroutes extends Controller
{
    public function listpage($urlslug){
        $canurl = "https://www.vivocarat.com/jewellery/".$urlslug.".html";
        $moburl = "https://www.vivocarat.com/m/jewellery/".$urlslug.".html";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
            
        $arr = explode("-",$urlslug);
        $description = DB::table('categories')
                    ->where('parent_category',$arr[0])
                    ->where('category',$arr[1])
                    ->first();
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $title = $description->page_title;
            $meta_keywords = $description->meta_keywords;
            $meta_description = $description->meta_description;
            $meta_robots = $description->meta_robots;
            $og_description = $description->og_description;
            $og_title = $description->og_title;
            $og_type = $description->og_type;
        }
        
        $data = \File::get(public_path() . '/p-list.php');
        $data = str_replace('$canurl', $canurl, $data);
        $data = str_replace('$moburl', $moburl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        
        if($arr[1] == 'All')
        {
            $otherurls = '<li>'.strtoupper($arr[0]).'</li>';
        }
        else
        {
            $otherurls = '<li><a href="/jewellery/'.$arr[0].'-All.html" target="_self" class="theme-text-special">'.strtoupper($arr[0]).'</a></li><li>'.strtoupper($arr[1]).'</li>';
        }
        
        $data = str_replace('$otherurls', $otherurls, $data);
        
        return $data;
    }
    
    public function lookbookarticle($urlslug){
        $canurl = "https://www.vivocarat.com/lookbook/".$urlslug.".html";
        $moburl = "https://www.vivocarat.com/m/lookbook/".$urlslug.".html";
        $SITE_ROOT = "https://www.vivocarat.com/";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
            
        $arr = explode("-",$urlslug);
        $description = DB::table('lookbook')
                    ->where('urlslug',$urlslug)
                    ->first();
        if($description == "" || $description == null)
        {   
            $articletodisplay = $urlslug;
        }
        else
        {
            $title = "VivoCarat | ".$description->title;
            $meta_keywords = $description->category;
            $meta_description = $description->lb_short_description;
            $meta_robots = "index,follow";
            $og_description = $description->lb_short_description;
            $og_title = "VivoCarat | ".$description->title;
            $og_type = "website";
            $img = "/images/lookbook/blogs/".$description->banner_img.".jpg";
            $og_image = $SITE_ROOT.$img;
            
            $articletodisplay = $description->title;
        }
        
        $data = \File::get(public_path() . '/p-lookbookArticle.php');
        $data = str_replace('$canurl', $canurl, $data);
        $data = str_replace('$moburl', $moburl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $data = str_replace('$articletodisplay', strtoupper($articletodisplay), $data);
        
        return $data;
    }
    
    public function storepage($store){
        $canurl = "https://www.vivocarat.com/brands/".$store;
        $moburl = "https://www.vivocarat.com/m/brands/".$store;
        $SITE_ROOT = "https://www.vivocarat.com/";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
            
        $description = DB::table('jeweller_information')
                    ->where('name',$store)
                    ->first();
        
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $title = "VivoCarat | ".$description->name;
            $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery,".$description->name;
            $meta_description = strip_tags($description->description);
            $meta_robots = "index,follow";
            $og_description = strip_tags($description->description);
            $og_title = "VivoCarat | ".$description->name;
            $og_type = "website";
            $img = "/images/store/banners/".$store.".jpg";
            $og_image = $SITE_ROOT.$img;
        }
        
        $data = \File::get(public_path() . '/p-store.php');
        $data = str_replace('$canurl', $canurl, $data);
        $data = str_replace('$moburl', $moburl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $storetodisplay = $store;
        $data = str_replace('$storetodisplay', strtoupper($storetodisplay), $data);
        
        return $data;
    }
    
    public function storecategorypage($store,$category){
        $canurl = "https://www.vivocarat.com/brands/".$store."/".$category.".html";
        $moburl = "https://www.vivocarat.com/m/brands/".$store."/".$category.".html";
        $SITE_ROOT = "https://www.vivocarat.com/";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
            
        $description = DB::table('jeweller_information')
                    ->where('name',$store)
                    ->first();
        
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $title = "VivoCarat | ".$description->name."-".$category;
            $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery,".$description->name.",".$category;
            $meta_description = strip_tags($description->description);
            $meta_robots = "index,follow";
            $og_description = strip_tags($description->description);
            $og_title = "VivoCarat | ".$description->name."-".$category;
            $og_type = "website";
            $img = "/images/store/banners/".$store.".jpg";
            $og_image = $SITE_ROOT.$img;
        }
        
        $data = \File::get(public_path() . '/p-splist.php');
        $data = str_replace('$canurl', $canurl, $data);
        $data = str_replace('$moburl', $moburl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $storetodisplay = $store;
        $storeurl = $store;
        $data = str_replace('$storetodisplay', strtoupper($storetodisplay), $data);
        $data = str_replace('$storeurl', $storeurl, $data);
        $data = str_replace('$storecategory', strtoupper($category), $data);
        
        return $data;
    }
    
    public function searchpage($tag){
        $canurl = "https://www.vivocarat.com/search/".$tag.".html";
        $moburl = "https://www.vivocarat.com/m/search/".$tag.".html";
        $SITE_ROOT = "https://www.vivocarat.com/";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
            
        $description = DB::table('search')
                    ->where('tag','like','%'.$tag.'%')
                    ->first();
        
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $title = $description->page_title;
            $meta_keywords = $description->meta_keywords;
            $meta_description = $description->meta_description;
            $meta_robots = $description->meta_robots;
            $og_description = $description->og_description;
            $og_title = $description->og_title;
            $og_type = $description->og_type;
            $og_image = $description->og_image;
        }
        
        $data = \File::get(public_path() . '/p-search.php');
        $data = str_replace('$canurl', $canurl, $data);
        $data = str_replace('$moburl', $moburl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $tagtodisplay = $tag;
        $tagtodisplay = str_replace("+", ",", $tagtodisplay);
        $data = str_replace('$tagtodisplay', strtoupper($tagtodisplay), $data);
        
        return $data;
    }
    
    public function productpage($urlslug){
        $canurl = "https://www.vivocarat.com/product/".$urlslug.".html";
        $moburl = "https://www.vivocarat.com/m/product/".$urlslug.".html";
        $SITE_ROOT = "https://www.vivocarat.com";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
        
        $brand = '';
        $name = '';
        $img = '';
        $desc = '';
        $price = '';
        
        $parent_category = '';
        $category = '';
        $ptitle = '';
        
        $description = DB::table('products')
                    ->where('urlslug',$urlslug)
                    ->first();
        
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $j_info = DB::table('jeweller_information')
                    ->where('id',$description->jeweller_id)
                    ->first();
            
            $title = "VivoCarat | ".$description->title."-".$description->supplier_name;
            $meta_keywords = $description->secondary_tags;
            $meta_description = $description->title.",".$description->short_description;
            $meta_robots = "index,follow";
            $og_description = $description->title.",".$description->short_description;
            $og_title = "VivoCarat | ".$description->title;
            $og_type = "website";
            $img = "/images/products-v2/".$description->VC_SKU."-1.jpg";
            $og_image = $SITE_ROOT.$img;
            
            $brand = $j_info->name;
            $name = $description->title;
            $desc = $description->title;
            $price = $description->price_after_discount;
            
            $ptitle = $description->title;
            $category = $description->category;
            $parent_category = $description->parent_category;
        }
        
        $data = \File::get(public_path() . '/p-product.php');
        $data = str_replace('$canurl', $canurl, $data);
        $data = str_replace('$moburl', $moburl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $data = str_replace('$brand', $brand, $data);
        $data = str_replace('$name', $name, $data);
        $data = str_replace('$desc', $desc, $data);
        $data = str_replace('$price', $price, $data);
        $data = str_replace('$img', $img, $data);
        
        $otherurls = '<li><a href="/jewellery/'.$parent_category.'-All.html" target="_self" class="theme-text-special">'.strtoupper($parent_category).'</a></li><li><a href="/jewellery/'.$parent_category.'-'.$category.'.html" target="_self" class="theme-text-special">'.strtoupper($category).'</a></li><li>'.strtoupper($ptitle).'</li>';
        
        $data = str_replace('$otherurls', $otherurls, $data);
        
        return $data;
    }
    
    public function mobilelistpage($urlslug){
        $canurl = "https://www.vivocarat.com/jewellery/".$urlslug.".html";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
            
        $arr = explode("-",$urlslug);
        $description = DB::table('categories')
                    ->where('parent_category',$arr[0])
                    ->where('category',$arr[1])
                    ->first();
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $title = $description->page_title;
            $meta_keywords = $description->meta_keywords;
            $meta_description = $description->meta_description;
            $meta_robots = $description->meta_robots;
            $og_description = $description->og_description;
            $og_title = $description->og_title;
            $og_type = $description->og_type;
        }
        
        $data = \File::get(public_path() . '/m-partials/list.html');
        $data = str_replace('$canurl', $canurl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        
        if($arr[1] == 'All')
        {
            $otherurls = '<li>'.strtoupper($arr[0]).'</li>';
        }
        else
        {
            $otherurls = '<li><a href="/jewellery/'.$arr[0].'-All.html" target="_self" class="theme-text-special">'.strtoupper($arr[0]).'</a></li><li>'.strtoupper($arr[1]).'</li>';
        }
        
        $data = str_replace('$otherurls', $otherurls, $data);
        
        return $data;
    }
    
    public function mobilelookbookarticle($urlslug){
        $canurl = "https://www.vivocarat.com/lookbook/".$urlslug.".html";
        $SITE_ROOT = "https://www.vivocarat.com/";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
            
        $arr = explode("-",$urlslug);
        $description = DB::table('lookbook')
                    ->where('urlslug',$urlslug)
                    ->first();
        if($description == "" || $description == null)
        {   
            $articletodisplay = $urlslug;
        }
        else
        {
            $title = "VivoCarat | ".$description->title;
            $meta_keywords = $description->category;
            $meta_description = $description->lb_short_description;
            $meta_robots = "index,follow";
            $og_description = $description->lb_short_description;
            $og_title = "VivoCarat | ".$description->title;
            $og_type = "website";
            $img = "/images/lookbook/blogs/".$description->banner_img.".jpg";
            $og_image = $SITE_ROOT.$img;
            
            $articletodisplay = $description->title;
        }
        
        $data = \File::get(public_path() . '/m-partials/lookbookArticle.html');
        $data = str_replace('$canurl', $canurl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $data = str_replace('$articletodisplay', strtoupper($articletodisplay), $data);
        
        return $data;
    }
    
    public function mobileproductpage($urlslug){
        $canurl = "https://www.vivocarat.com/product/".$urlslug.".html";
        $SITE_ROOT = "https://www.vivocarat.com";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
        
        $brand = '';
        $name = '';
        $img = '';
        $desc = '';
        $price = '';
        
        $parent_category = '';
        $category = '';
        $ptitle = '';
        
        $description = DB::table('products')
                    ->where('urlslug',$urlslug)
                    ->first();
        
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $j_info = DB::table('jeweller_information')
                    ->where('id',$description->jeweller_id)
                    ->first();
            
            $title = "VivoCarat | ".$description->title."-".$description->supplier_name;
            $meta_keywords = $description->secondary_tags;
            $meta_description = $description->title.",".$description->short_description;
            $meta_robots = "index,follow";
            $og_description = $description->title.",".$description->short_description;
            $og_title = "VivoCarat | ".$description->title;
            $og_type = "website";
            $img = "/images/products-v2/".$description->VC_SKU."-1.jpg";
            $og_image = $SITE_ROOT.$img;
            
            $brand = $j_info->name;
            $name = $description->title;
            $desc = $description->title;
            $price = $description->price_after_discount;
            
            $ptitle = $description->title;
            $category = $description->category;
            $parent_category = $description->parent_category;
        }
        
        $data = \File::get(public_path() . '/m-partials/product.html');
        $data = str_replace('$canurl', $canurl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $data = str_replace('$brand', $brand, $data);
        $data = str_replace('$name', $name, $data);
        $data = str_replace('$desc', $desc, $data);
        $data = str_replace('$price', $price, $data);
        $data = str_replace('$img', $img, $data);
        
        $otherurls = '<li><a href="/jewellery/'.$parent_category.'-All.html" target="_self" class="theme-text-special">'.strtoupper($parent_category).'</a></li><li><a href="/jewellery/'.$parent_category.'-'.$category.'.html" target="_self" class="theme-text-special">'.strtoupper($category).'</a></li><li>'.strtoupper($ptitle).'</li>';
        
        $data = str_replace('$otherurls', $otherurls, $data);
        
        return $data;
    }
    
    public function mobilesearchpage($tag){
        $canurl = "https://www.vivocarat.com/search/".$tag.".html";
        $SITE_ROOT = "https://www.vivocarat.com/";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
            
        $description = DB::table('search')
                    ->where('tag','like','%'.$tag.'%')
                    ->first();
        
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $title = $description->page_title;
            $meta_keywords = $description->meta_keywords;
            $meta_description = $description->meta_description;
            $meta_robots = $description->meta_robots;
            $og_description = $description->og_description;
            $og_title = $description->og_title;
            $og_type = $description->og_type;
            $og_image = $description->og_image;
        }
        
        $data = \File::get(public_path() . '/m-partials/search.html');
        $data = str_replace('$canurl', $canurl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $tagtodisplay = $tag;
        $tagtodisplay = str_replace("+", ",", $tagtodisplay);
        $data = str_replace('$tagtodisplay', strtoupper($tagtodisplay), $data);
        
        return $data;
    }
    
    public function mobilestorecategorypage($store,$category){
        $canurl = "https://www.vivocarat.com/brands/".$store."/".$category.".html";
        $SITE_ROOT = "https://www.vivocarat.com/";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
            
        $description = DB::table('jeweller_information')
                    ->where('name',$store)
                    ->first();
        
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $title = "VivoCarat | ".$description->name."-".$category;
            $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery,".$description->name.",".$category;
            $meta_description = strip_tags($description->description);
            $meta_robots = "index,follow";
            $og_description = strip_tags($description->description);
            $og_title = "VivoCarat | ".$description->name."-".$category;
            $og_type = "website";
            $img = "/images/store/banners/".$store.".jpg";
            $og_image = $SITE_ROOT.$img;
        }
        
        $data = \File::get(public_path() . '/m-partials/splist.html');
        $data = str_replace('$canurl', $canurl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $storetodisplay = $store;
        $storeurl = $store;
        $data = str_replace('$storetodisplay', strtoupper($storetodisplay), $data);
        $data = str_replace('$storeurl', $storeurl, $data);
        $data = str_replace('$storecategory', strtoupper($category), $data);
        
        return $data;
    }
    
    public function mobilestorepage($store){
        $canurl = "https://www.vivocarat.com/brands/".$store;
        $SITE_ROOT = "https://www.vivocarat.com/";
        
        $title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery";
        $meta_description = "VivoCarat.com - Buy the best Gold and Diamond Jewellery Online in India with the latest jewellery designs from trusted brands at low and affordable prices. We promise CERTIFIED & HALLMARKED jewellery, FREE SHIPPING, Cash on Delivery (COD), EASY RETURN POLICY, Lifetime exchange policy, best discounts, coupons and offers. VivoCarat offers Gold Coins, Solitaire Jewellery, Gold, Gemstone, Platinum and Diamond Jewellery Online for Men and Women at Best Prices in India. Buy Rings, Pendants, Ear Rings, Bangles, Necklaces, Bangles, Bracelets, Chains and more.";
        $meta_robots = "index,follow";
        $og_url = $canurl;
        $og_description = "India's finest online jewellery";
        $og_title = "VivoCarat - Online Jewellery Shopping Destination in India | Best Gold and Diamond Jewelry Designs at low prices | Trusted Online Jewellery store";
        $og_type = "website";
        $og_image = '';
            
        $description = DB::table('jeweller_information')
                    ->where('name',$store)
                    ->first();
        
        if($description == "" || $description == null)
        {   
        }
        else
        {
            $title = "VivoCarat | ".$description->name;
            $meta_keywords = "online jewellery shopping store, diamond jewellery, gold jewellery, online jewellery india, jewellery website, vivocarat jewellery, vivocarat designs, jewellery designs, fashion jewellery, indian jewellery, designer jewellery, diamond Jewellery, online jewellery shopping india, jewellery websites, diamond jewellery india, gold jewellery online, Indian diamond jewellery,".$description->name;
            $meta_description = strip_tags($description->description);
            $meta_robots = "index,follow";
            $og_description = strip_tags($description->description);
            $og_title = "VivoCarat | ".$description->name;
            $og_type = "website";
            $img = "/images/store/banners/".$store.".jpg";
            $og_image = $SITE_ROOT.$img;
        }
        
        $data = \File::get(public_path() . '/m-partials/store.html');
        $data = str_replace('$canurl', $canurl, $data);
        
        $data = str_replace('$title', $title, $data);
        $data = str_replace('$meta_keywords', $meta_keywords, $data);
        $data = str_replace('$meta_description', $meta_description, $data);
        $data = str_replace('$meta_robots', $meta_robots, $data);
        $data = str_replace('$og_url', $og_url, $data);
        $data = str_replace('$og_description', $og_description, $data);
        $data = str_replace('$og_title', $og_title, $data);
        $data = str_replace('$og_type', $og_type, $data);
        $data = str_replace('$og_image', $og_image, $data);
        
        $storetodisplay = $store;
        $data = str_replace('$storetodisplay', strtoupper($storetodisplay), $data);
        
        return $data;
    }
}
