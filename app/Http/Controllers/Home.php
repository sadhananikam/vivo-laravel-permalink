<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use App\Mail\SendContactusMail;
use App\Mail\SendPartnerMail;
use App\Mail\SendSellerLocationEnquiryAdminMail;
use App\Newsletter;
use App\Banners;
use App\Lookbook;
use App\Products;
use App\Contact;
use App\Partners;
use App\SellerLocationEnquiry;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Mail\SendOrderConfirmationMail;

class Home extends Controller
{
    //newsletter - send email
    public function newsletter(Request $request){
        $errors = array();
        $data = array();
    
    if(empty($request->input('newsletteremail')))
        $errors['newsletteremail'] = 'Email is required';
    
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $email = $request->input('newsletteremail');
            
            $newsletter = new Newsletter;
            $newsletter->email = $email;
            $newsletter->is_active = 1;            
            
            try {
                \Mail::to($email)->send(new SendMail($email));
                
                $newsletter->save();
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
            
        }  
        
        return json_encode($data);
    }
    
    public function getbanners(){
        $p = $_REQUEST['p'];
        $m = $_REQUEST['m'];
        $data = array();
        
        $banners = DB::table('banners')
                    ->select('id','img_url','alt','href','target','is_link')
                    ->where('page',$p)
                    ->where('is_active',1)
                    ->where('is_Mobile',$m)
                    ->orderBy('sequence','asc')
                    ->get();
                
        return json_encode($banners);
    }
    
    public function getlookbook($id = null){
        if($id == null){
            //return Lookbook::orderBy('id','desc')->get();
            if (Cache::has('lookbook'))
            {
                $lookbook = Cache::get('lookbook');
            }
            else
            {
                $lookbook = DB::table('lookbook')
                    ->select('id','title','urlslug','lb_short_description','lb_long_description','category','banner_img','similar_1','similar_2','similar_3','similar_4','look_1','look_2','look_3','created_at')
                    ->orderBy('id','desc')
                    ->get();
                Cache::put('lookbook', $lookbook);
            }
            return json_encode($lookbook);
        }else{
            return $this->show($id);
        }
    }
    
    public function getfeaturedproducts(){
        
        $featuredproducts = DB::table('products')
                    ->where('isFeatured',1)
                    ->groupBy('VC_SKU')
                    ->limit(12)
                    ->get();
                
        if($featuredproducts != null){
            return json_encode($featuredproducts);
        }
        else{
            return "invalid";
        }     
    }
    
    public function getsellerlocation()
    {
        $seller = DB::table('seller_location')
                    ->orderBy('name','asc')
                    ->get();
        return json_encode($seller);
    }
    
    public function getSellerDetailBySearch(){
        $searchtext = $_REQUEST['searchtext'];
        
        $seller = DB::table('seller_location')
                    ->where('name','like','%'.$searchtext.'%')
                    ->orWhere('address','like','%'.$searchtext.'%')
                    ->orWhere('pincode','like','%'.$searchtext.'%')
                    ->orderBy('name','asc')
                    ->get();
                
        return json_encode($seller);
    }
    
    public function saveSellerLocationEnquiry(Request $request)
    {
        $errors = array();
        $data = array();
    
        if(empty($request->input('pname')))
            $errors['name'] = 'Name is required';

        if(empty($request->input('pemail')))
            $errors['email'] = 'Email is required';

        if(empty($request->input('pphone')))
            $errors['phone'] = 'Phone is required';
        
        if(empty($request->input('plocation')))
            $errors['plocation'] = "City/Location is required";
        
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $partner = new SellerLocationEnquiry;
        
            $partner->name = $request->input('pname');
            $partner->email = $request->input('pemail');
            $partner->phone = $request->input('pphone');
            $partner->location = $request->input('plocation');
            
            $data = array(
                'name' => $request->input('pname'),
                'email' => $request->input('pemail'),
                'phone' => $request->input('pphone'),
                'location' => $request->input('plocation'),
            );
            
            try {
                $partner->save();
                
                /*$emailto1 = 'sadhana@vivocarat.com';
                \Mail::to($emailto1)->send(new SendSellerLocationEnquiryAdminMail($data));*/
                $emailto1 = 'a@vivocarat.com';
                $emailto2 = 'r@vivocarat.com';
                \Mail::to($emailto1)->send(new SendSellerLocationEnquiryAdminMail($data));
                \Mail::to($emailto2)->send(new SendSellerLocationEnquiryAdminMail($data));
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
        }  
        
        return json_encode($data);
    }
    
    
    public function saveContactusform(Request $request)
    {
        $errors = array();
        $data = array();
    
    if(empty($request->input('cname')))
        $errors['name'] = 'Name is required';

    if(empty($request->input('cemail')))
        $errors['email'] = 'Email is required';

    if(empty($request->input('cphone')))
        $errors['phone'] = 'Phone is required';
        
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $contact = new Contact;
        
            $contact->name = $request->input('cname');
            $contact->email = $request->input('cemail');
            $contact->mobile = $request->input('cphone');
            $contact->message = $request->input('cmessage');
            
            $data = array(
                'name' => $request->input('cname'),
                'email' => $request->input('cemail'),
                'phone' => $request->input('cphone'),
                'messagedata' => $request->input('cmessage')
            );
            
            try {
                $contact->save();
                
                $emailto1 = 'a@vivocarat.com';
                $emailto2 = 'r@vivocarat.com';
                \Mail::to($emailto1)->send(new SendContactusMail($data));
                \Mail::to($emailto2)->send(new SendContactusMail($data));
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
        }  
        
        return json_encode($data);
    }
    
    public function savePartnerform(Request $request)
    {
        $errors = array();
        $data = array();
    
    if(empty($request->input('pname')))
        $errors['name'] = 'Name is required';

    if(empty($request->input('pemail')))
        $errors['email'] = 'Email is required';

    if(empty($request->input('pphone')))
        $errors['phone'] = 'Phone is required';
        
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $partner = new Partners;
        
            $partner->name = $request->input('pname');
            $partner->email = $request->input('pemail');
            $partner->phone = $request->input('pphone');
            $partner->brand = $request->input('pbrand');
            $partner->address = $request->input('paddress');
            
            $data = array(
                'name' => $request->input('pname'),
                'email' => $request->input('pemail'),
                'phone' => $request->input('pphone'),
                'brand' => $request->input('pbrand'),
                'address' => $request->input('paddress')
            );
            
            try {
                $partner->save();
                
                $emailto1 = 'a@vivocarat.com';
                $emailto2 = 'r@vivocarat.com';
                \Mail::to($emailto1)->send(new SendPartnerMail($data));
                \Mail::to($emailto2)->send(new SendPartnerMail($data));
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
        }  
        
        return json_encode($data);
    }
    
    public function getLookbookDetail(){
        
        $id = $_REQUEST['id'];
        //$sql2="SELECT * from lookbook where id=".$_GET['id'];
        $detail = DB::table('lookbook')
                    ->where('id',$id)
                    ->get();
        
        return json_encode($detail);
    }
    
    public function getLookbookDetailByUrlslug(){
        
        $urlslug = $_REQUEST['urlslug'];
        $detail = DB::table('lookbook')
                    ->where('urlslug',$urlslug)
                    ->get();
        
        return json_encode($detail);
    }
    
    public function getLookbookList(){
        //$sql2="SELECT * from lookbook order by id DESC";
        if (Cache::has('lookbooklist'))
        {
            $lookbooklist = Cache::get('lookbooklist');
        }
        else
        {
            $lookbooklist = DB::table('lookbook')
                        ->orderBy('id','desc')
                        ->get();
            Cache::put('lookbooklist', $lookbooklist);
        }
        return json_encode($lookbooklist);
    }
    
    public function getRelatedBlog(){
        $id1= $_REQUEST['id1'];
        $id2 = $_REQUEST['id2'];
        $id3 = $_REQUEST['id3'];

        //$sql2="SELECT * from lookbook  where id=".$id1." or id=".$id2." or id=".$id3 ;
        
//        $relatedblog = DB::table('lookbook')
//                    ->where('id',$id1)
//                    ->orWhere('id',$id2)
//                    ->orWhere('id',$id3)
//                    ->get();
        
//        $relatedblog = DB::table('lookbook')
//                    ->whereIn('id', [$id1, $id2, $id3])
//                    ->get();
        
        $relatedblog = DB::table('lookbook')
                    ->where('id',$id1)
                    ->orWhere('id',$id2)
                    ->orWhere('id',$id3)
                    ->get();
        
        return json_encode($relatedblog);
    }
    
    public function getSimilarProducts(){
        
        $s1= $_REQUEST['s1'];
        $s2 = $_REQUEST['s2'];
        $s3 = $_REQUEST['s3'];
        $s4 = $_REQUEST['s4'];
        
        $similarproducts = DB::table('products')
                    ->whereIn('id', [$s1, $s2, $s3, $s4])
                    ->get();
        
        return json_encode($similarproducts);
    }
    
    public function sendSMS(Request $request)
    {
        $data = array();
        //Your authentication key
        $authKey = "173057AunX4NLcYuf59ad5e75";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $_REQUEST['mobile'];

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "VIVOCT";

        //Your message to send, Add URL encoding here.
        $message = urlencode($_REQUEST['message']);

        //Define route 
        $route = 4;

        //Prepare you post parameters
        $postData = array(
            'sender' => $senderId,
            'message' => $message,
            'mobiles' => $mobileNumber,
            'authkey' => $authKey,
            'campaign' => 'viaSOCKET',
            'unicode' => 0,
            'flash' => 0,
            'country' => 91,
            'route' => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            $data['success'] = false;
            $data['errors'] = curl_error($ch);
            return json_encode($data); 
        }

        curl_close($ch);

        $data['success'] = true;
        $data['output'] = $output;
        return json_encode($data); 
    }
    
    public function testemail()
    {
        $id = 2024;
        $name = 'Sadhana Vishwas Nikam';
        $cust_email = 'nikam.sadhana@gmail.com';  
        
        $order = DB::table('orders_temp')
                        ->where('id',$id)
                        ->first();
        
        $bill_address_id = $order->bill_address_id;
        $bill_address = '';
        if($bill_address_id != '')
        {
            $bill_address = DB::table('addresses')
                        ->where('address_id',$bill_address_id)
                        ->first();
        }
        
        $ship_address_id = $order->ship_address_id;
        $ship_addres = '';
        if($ship_address_id != '')
        {
            $ship_addres = DB::table('addresses')
                        ->where('address_id',$ship_address_id)
                        ->first();
        }

        $products=$order->items;
        $grand_total=$order->total;
        $payment_mode = '';
        if($order->isCod === '1')
        {
            $payment_mode='Cash on Delivery';
        }
        else if($order->isCod === '0')
        {
            $payment_mode='Prepaid';
        }
        $placed_on = date("j F Y", strtotime($order->created_at));
        $shipping_time = '7-9 Days';
        
        $parr = json_decode($order->items, true);
        $sub_total = 0;
        foreach ($parr as $p) {        
            $sub_total = $sub_total + $p['item']['price_after_discount'];
        }
        $promo_disc = $sub_total-$grand_total;
        
        $coinflag = 'false';
        $coin_sku = "";
        $coin_name = "";
        $coin_jeweller = "";

        if($grand_total >= 15000)
        {
            $coinflag = 'true';
            $coin_sku = "VIVOGCA245";
            $coin_name = "Plain Silver Coin 10 Grams in 999 Purity Fineness";
            $coin_jeweller = "VivoCarat";
        }

        $data = array(
                'id' => "VCORD".$id,
                'name' => $ship_addres->name,
                'shipping' => $ship_addres,
                'billing' => $bill_address,
                'products' => $products,
                'grand_total' => $grand_total,
                'sub_total' => $sub_total,
                'payment_mode' => $payment_mode,
                'placed_on' => $placed_on,
                'shipping_time' => $shipping_time,
                'promo_disc' => $promo_disc,
                'coinflag' => $coinflag,
                'coin_sku' => $coin_sku,
                'coin_name' => $coin_name,
                'coin_jeweller' => $coin_jeweller
            );

        \Mail::to($cust_email)->send(new SendOrderConfirmationMail($data));

        echo "email has been sent";
    }
    
    public function sendSMSToCustomerNotPurchasedCron()
    {
        date_default_timezone_set('Asia/Kolkata');
        $date = date_create(date('Y-m-d H:i:s'));    
        $userArr = DB::table('customers_auth')
                    ->whereNotIn('uid', function($query)
                    {
                        $query->select('uid')
                              ->from('orders_temp');
                    })
                    ->get();

        if ($userArr != NULL) 
		{
            foreach($userArr as $user)
            {
                $id = $user->uid;
                $created_at = date_create($user->created_at);
                $phone = $user->phone;
                $diff = date_diff($date,$created_at);
                $days = $diff->format("%a");
                if($days==='5' || $days==='10' || $days==='15' || $days==='30' || $days==='45' || $days==='60' || $days==='90')
                {
                    echo $days;
                    echo "</br>";
                    echo $phone;
                }
                /*if($days >= 5)
                {
                    echo $days;
                    echo "</br>";
                    echo $phone;
                }*/
//                echo $days."<br>";
            }
        }
    }
}