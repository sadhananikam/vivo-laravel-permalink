<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\SendRegisterMail;
use App\Mail\SendRegisterAdminMail;
use App\CustomersAuth;
use App\Addresses;
use App\Cart;
use App\Mail\SendResetpasswordMail;
use GuzzleHttp\Client;

class Customer extends Controller
{
    // blowfish
    private static $algo = '$2a';
    // cost parameter
    private static $cost = '$10';

    // mainly for internal use
    public static function unique_salt() {
        return substr(sha1(mt_rand()), 0, 22);
    }

    // this will be used to generate a hash
    public static function hash($password_new) {

        return crypt($password_new, self::$algo .
                self::$cost .
                '$' . self::unique_salt());
    }

    // this will be used to compare a password against a hash
    public static function check_password($hash2, $password2) {
        $full_salt = substr($hash2, 0, 29);
        $new_hash = crypt($password2, $full_salt);
        return ($hash2 == $new_hash);
    }
    
    /**
     * Verifying required params posted or not
     */
    public function verifyRequiredParams($required_fields,$request_params) 
    {
        $error = false;
        $error_fields = "";
        foreach ($required_fields as $field) {
            if (!isset($request_params->$field) || strlen(trim($request_params->$field)) <= 0) {
                $error = true;
                $error_fields .= $field . ', ';
            }
        }

        if ($error) {
            // Required field(s) are missing or empty
            // echo error json and stop the app
            $response = array();
            //$app = \Slim\Slim::getInstance();
            $response["status"] = "error";
            $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
            return json_encode($response);
            //echoResponse(200, $response);
            //$app->stop();
        }
    }
    
    public function sendSMS($mobile,$message)
    {
        $data = array();
        //Your authentication key
        $authKey = "173057AunX4NLcYuf59ad5e75";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $mobile;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "VIVOCT";

        //Your message to send, Add URL encoding here.
        $message = urlencode($message);

        //Define route 
        $route = 4;

        //Prepare you post parameters
        $postData = array(
            'sender' => $senderId,
            'message' => $message,
            'mobiles' => $mobileNumber,
            'authkey' => $authKey,
            'campaign' => 'viaSOCKET',
            'unicode' => 0,
            'flash' => 0,
            'country' => 91,
            'route' => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            $data['success'] = false;
            $data['errors'] = curl_error($ch);
            return json_encode($data); 
        }

        curl_close($ch);

        $data['success'] = true;
        $data['output'] = $output;
        return json_encode($data); 
    }    

    public function getSession(){
        if (!isset($_SESSION)) {
            session_start();
        }
        $sess = array();
        if(isset($_SESSION['uid']))
        {
            $sess["uid"] = $_SESSION['uid'];
            $sess["name"] = $_SESSION['name'];
            $sess["email"] = $_SESSION['email'];
            $sess["phone"] = $_SESSION['phone'];
        }
        else
        {
            $sess["uid"] = '';
            $sess["name"] = 'Guest';
            $sess["email"] = '';
                 $sess["phone"] = '';
        }
        return $sess;
    }
    
    public function destroySession(){
        if (!isset($_SESSION)) {
        session_start();
        }
        if(isSet($_SESSION['uid']))
        {
            unset($_SESSION['uid']);
            unset($_SESSION['name']);
            unset($_SESSION['email']);
                unset($_SESSION['phone']);
            $info='info';
            if(isSet($_COOKIE[$info]))
            {
                setcookie ($info, '', time() - $cookie_time);
            }
            $msg="Logged Out Successfully...";
        }
        else
        {
            $msg = "Not logged in...";
        }
        return $msg;
    }

    public function session(){
        $session = $this->getSession();
        $response["uid"] = $session['uid'];
        $response["email"] = $session['email'];
        $response["name"] = $session['name'];
        $response["phone"] = $session['phone'];
        return json_encode($session);
    }
    
    public function login()
    {
        
        $r = json_decode($_REQUEST['customer']);
        
        $products = json_decode($_REQUEST['items']);
        
        $login_url = $_REQUEST['login_url'];
        $login_through = 0;
        
        //$this->verifyRequiredParams(array('email', 'password'),$r);
        
        $response = array();
        $password1 = $r->password;
        $email = $r->email;

        //$user = $db->getOneRecord("select uid,name,password,email,phone,created from customers_auth where phone='$email' or email='$email'");
        $userArr = DB::table('customers_auth')
                    ->select(DB::raw('uid,name,password,email,phone,created_at'))
                    ->where('phone',$email)
                    ->orWhere('email',$email)
                    ->first();
        
        if ($userArr != NULL) {
            $user = json_decode(json_encode($userArr), True);
            
            if($this->check_password($user['password'],$password1))
            {
                DB::table('customers_auth')
                        ->where('uid',$user['uid'])
                        ->update(['login_through' => $login_through,'login_url' => $login_url,'updated_at' => date("Y-m-d H:i:s")]);
                
                $response['status'] = "success";
                $response['message'] = 'Logged in successfully.';
                $response['name'] = $user['name'];
                $response['uid'] = $user['uid'];
                $response['email'] = $user['email'];   $response['phone'] = $user['phone'];
                $response['createdAt'] = $user['created_at'];
                if (!isset($_SESSION)) {
                    session_start();
                }
                $_SESSION['uid'] = $user['uid'];
                $_SESSION['email'] = $email;
                $_SESSION['name'] = $user['name'];
                $_SESSION['phone'] = $user['phone'];
                
                $uid = $user['uid'];
                
                $this->savetocart($products,$uid);
                
                $items = $this->getCartItems($uid);
                $response['items'] = $items;
                
            } else {
                $response['status'] = "error";
                $response['message'] = 'Login failed. Incorrect credentials';
            }
        }
        else 
        {
            $response['status'] = "error";
            $response['message'] = 'No such user is registered';
        }
        
        return json_encode($response);
        
    }
    
    public function getCartItems($uid){
        $cart = DB::table('cart')
                    ->where('user_id',$uid)
                    ->where('is_active','1')
                    ->get();
        
        if(count($cart)){
            return json_encode($cart);
        }
        else
        {
            return null;
        }
        
        //return $cart;
    }
    
    public function savetocart($products,$uid){   
        $checkcart = DB::table('cart')
                    ->select('cart_ref_id')
                    ->where('user_id',$uid)
                    ->where('is_active','1')
                    ->first();
        
        if($checkcart != null)
        {
            $cart_ref_id = $checkcart->cart_ref_id;
            
            if(!empty($products) && !empty($uid))
            {
                $items = $products;
                for ($i = 0; $i < count($items); $i++) 
                {
                    $p = $items[$i]->item;

                    $product_id = $p->id;
                    
                $checkp = DB::table('cart')
                    ->where('user_id',$uid)
                    ->where('cart_ref_id',$cart_ref_id)
                    ->where('product_id',$product_id)
                    ->where('is_active','1')
                    ->first();
                    
                    if($checkp != null)
                    {
                        
                        DB::table('cart')
                        ->where('user_id',$uid)
                        ->where('cart_ref_id',$cart_ref_id)
                        ->where('product_id',$product_id)
                        ->where('is_active','1')
                        ->update(['quantity' => $items[$i]->quantity,'ring_size' => $p->ring_size,'bangle_size' => $p->bangle_size,'bracelet_size' => $p->bracelet_size,'updated_at' => date("Y-m-d H:i:s")]);
                
                    }
                    else
                    {
                        $cart = new Cart;

                        $cart->cart_ref_id = $cart_ref_id;
                        $cart->user_id = $uid;
                        $cart->product_id = $p->id;
                        $cart->ring_size = $p->ring_size;
                        $cart->bangle_size = $p->bangle_size;
                        $cart->bracelet_size = $p->bracelet_size;
                        $cart->quantity = $items[$i]->quantity;
                        $cart->is_active = '1';
                        $rs = $cart->save();
                    }                           
                }
            }
        }
        else
        {
            $query = "SHOW TABLE STATUS WHERE name='cart'";
            $result = DB::select(DB::raw($query));
            if($result != null){
                $r = json_decode(json_encode($result), True);
                $nextid = $r[0]['Auto_increment'];
            }
            
            if(!empty($products) && !empty($uid))
            {
                $items = $products;
                for ($i = 0; $i < count($items); $i++) {
                    $p = $items[$i]->item;

                    $cart = new Cart;

                    $cart->cart_ref_id = $uid.'-'.$nextid;
                    $cart->user_id = $uid;
                    $cart->product_id = $p->id;
                    $cart->ring_size = $p->ring_size;
                    $cart->bangle_size = $p->bangle_size;
                    $cart->bracelet_size = $p->bracelet_size;
                    $cart->quantity = $items[$i]->quantity;
                    $cart->is_active = '1';
                    $rs = $cart->save();    
                }
            }
        }   
    }
    
    public function signUp(){
        
        $response = array();
        
        $r = json_decode($_REQUEST['customer']);
        $products = json_decode($_REQUEST['items']);
        
        $register_url = $_REQUEST['register_url'];
        $register_through = 0;
        
        $login_url = $register_url;
        $login_through = $register_through;
        
        //verifyRequiredParams(array('email', 'name', 'password'),$r->customer);
        
        $phone = $r->phone;
        $name = $r->name;
        $email = $r->email;
        $password = $r->password;
        
        //$isUserExists = $db->getOneRecord("select 1 from customers_auth where phone='$phone' or email='$email'");
        
        $isUserExists = DB::table('customers_auth')
                    ->where('phone',$phone)
                    ->orWhere('email',$email)
                    ->first();
        
        if(!$isUserExists){
            
            $hashpassword = $this->hash($password);
            
            $customer = new CustomersAuth;
            
            $customer->name = $name;
            $customer->email = $email;
            $customer->phone = $phone;
            $customer->password = $hashpassword;
            $customer->register_through = $register_through;
            $customer->register_url = $register_url;
            $customer->login_url = $login_url;
            $customer->login_through = $login_through;
            $result = $customer->save();
            
            if ($result) {
                
                $user = DB::table('customers_auth')
                    ->select(DB::raw('uid'))
                    ->where('phone',$phone)
                    ->where('email',$email)
                    ->first();
                
                $response["status"] = "success";
                $response["message"] = "User account created successfully";
                $response["uid"] = $user->uid;
                if (!isset($_SESSION)) {
                    session_start();
                }
                $_SESSION['uid'] = $user->uid;
                $_SESSION['phone'] = $phone;
                $_SESSION['name'] = $name;
                $_SESSION['email'] = $email;
                
                $uid = $user->uid;
                
                $this->savetocart($products,$uid);
                
                $items = $this->getCartItems($uid);
                $response['items'] = $items;

                $data = array(
                    'name' => $name,
                    'phone' => $phone,
                    'email' => $email
                );
                
                \Mail::to($email)->send(new SendRegisterMail($data));
                
                $adminemail1 = "a@vivocarat.com";
                $adminemail2 = "r@vivocarat.com";
                
                \Mail::to($adminemail1)->send(new SendRegisterAdminMail($data));
                \Mail::to($adminemail2)->send(new SendRegisterAdminMail($data));
                
                $message = "Dear ".$name.". Thank you for signing up on VivoCarat.\r\nUse code VC500 and get Rs.500 off on your first order. Thank you.";
                $output = $this->sendSMS($phone,$message);
                
                /*
                $email_message = new email_message();
                $email_message->send($email,'Welcome to Vivocarat',getRegisterEmail($name));
                      $email_message2 = new email_message();
                $email_message2->send("r@vivocarat.com",'Yeah!!! New Registration',getRegisterData($name));
                 $email_message2->send("a@vivocarat.com",'Yeah!!! New Registration',getRegisterData($name));
                 */
                return json_encode($response);
                
            } else {
                $response["status"] = "error";
                $response["message"] = "Failed to create customer. Please try again";
                
                return json_encode($response);
            }            
        }else{
            $response["status"] = "error";
            $response["message"] = "An user with the provided phone or email exists!";
            
            return json_encode($response);
        }
    }
    
    public function logout(){
        
        $session = $this->destroySession();
        $response["status"] = "info";
        $response["message"] = "Logged out successfully";
        return json_encode($response);
    }
    
    public function forgotpassword(Request $request){
        $email = $_REQUEST['email'];
        
        //$sql = "SELECT * from customers_auth where email = '{$email}'";
        
        $user = DB::table('customers_auth')
                    ->select(DB::raw('uid,name,email'))
                    ->orWhere('email',$email)
                    ->first();
        
        if($user != NULL){
            
            $token=strtr(base64_encode($user->uid), '+/=', '-_,');
//            $resetUrl='http://localhost:8000/#/resetpassword/'.$token;
            //$resetUrl='http://localhost:8000/p-resetpassword.php?token='.$token;
            $resetUrl="https://www.vivocarat.com/reset-password-".$token.".html";
            
            $data = array(
                        'name' => $user->name,
                        'resetUrl' => $resetUrl
                    );
            
            DB::table('customers_auth')
                    ->where('uid', $user->uid)
                    ->update(['token' => $token,'updated_at' => date("Y-m-d H:i:s")]);

            \Mail::to($email)->send(new SendResetpasswordMail($data));
            return "success";
        }
        else{
            return "ND";
        }
    }
    
    public function getAccountDetails(){
        
        $uid = $_REQUEST['uid'];
        
        $details = DB::table('customers_auth')
            ->leftJoin('addresses', 'customers_auth.uid', '=', 'addresses.uid')
            ->select('customers_auth.uid','addresses.address_id', 'customers_auth.name', 'customers_auth.email','customers_auth.phone','customers_auth.dob','customers_auth.anniversary', 'addresses.first_line', 'addresses.city', 'addresses.state', 'addresses.pin','addresses.country')
            ->where('customers_auth.uid',$uid)
            ->orderBy('addresses.address_id','desc')
            ->offset(0)
            ->limit(2)
            ->get();
        
        
        return json_encode($details);

    }
    
    public function updateAccountName(){ 
        $uid = $_REQUEST['uid'];
        $name = $_REQUEST['name'];
        
        DB::table('customers_auth')
                    ->where('uid', $uid)
                    ->update(['name' => $name,'updated_at' => date("Y-m-d H:i:s")]);
        
        return 'success';
    }
    
    public function updateAccountEmail(){ 
        $uid = $_REQUEST['uid'];
        $email = $_REQUEST['email'];
        
        DB::table('customers_auth')
                    ->where('uid', $uid)
                    ->update(['email' => $email,'updated_at' => date("Y-m-d H:i:s")]);
        
        return 'success';
    }
    
    public function updateAccountPhone(){ 
        $uid = $_REQUEST['uid'];
        $phone = $_REQUEST['phone'];
        
        DB::table('customers_auth')
                    ->where('uid', $uid)
                    ->update(['phone' => $phone,'updated_at' => date("Y-m-d H:i:s")]);
        
        return 'success';
    }
    
    public function updateAccountDOB(){ 
        $uid = $_REQUEST['uid'];
        $dob = $_REQUEST['dob'];
        
        DB::table('customers_auth')
                    ->where('uid', $uid)
                    ->update(['dob' => $dob,'updated_at' => date("Y-m-d H:i:s")]);
        
        return 'success';
    }
    
    public function updateAccountAnniversary(){ 
        $uid = $_REQUEST['uid'];
        $anniversary = $_REQUEST['anniversary'];
        
        DB::table('customers_auth')
                    ->where('uid', $uid)
                    ->update(['anniversary' => $anniversary,'updated_at' => date("Y-m-d H:i:s")]);
        
        return 'success';
    }
    
    public function updatePassword()
    {
        $uid = $_REQUEST['uid'];
        $oldpassword = $_REQUEST['oldpassword'];
        $newpassword = $_REQUEST['newpassword'];
        
        $hashedOldPassword=$this->hash($oldpassword);
        
        $result = DB::table('customers_auth')
                    ->select('password')
                    ->where('uid',$uid)
                    ->first();

        $password= $result->password;
        if($this->check_password($password,$oldpassword))
        {
            $hashedPassword=$this->hash($newpassword);
            DB::table('customers_auth')
                    ->where('uid', $uid)
                    ->update(['password' => $hashedPassword,'updated_at' => date("Y-m-d H:i:s")]);
        
            return 'success';
        }
        else
        {
            return 'error';
        }
    }
    
    public function addAddressFromAccount()
    {
        $uid = $_REQUEST['uid'];
        $name = $_REQUEST['name'];
        $phone = $_REQUEST['phone'];
        $address = $_REQUEST['address'];
        $pincode = $_REQUEST['pincode'];
        $city = $_REQUEST['city'];
        $state = $_REQUEST['state'];
        $country = $_REQUEST['country'];
            
        $result = DB::table('customers_address')
                    ->where('uid',$uid)
                    ->first();

        if($result != null)
        {

            DB::table('customers_address')
                    ->where('uid', $uid)
                    ->update(['name' => $name,'phone' => $phone,'address' => $address,'pincode' => $pincode,'city' => $city,'state' => $state,'country' => $country]);
        
            return 'success';
        }
        else
        {

            DB::table('customers_address')->insert(
                ['uid' => $uid,'name' => $name,'phone' => $phone,'address' => $address,'pincode' => $pincode,'city' => $city,'state' => $state,'country' => $country,'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")]
            );
        
            return 'success';
        }
    }
    
    public function getAddressDetails(){
        
        $uid = $_REQUEST['uid'];
            
        $details = DB::table('customers_address')
                ->where('uid',$uid)
                ->first();
        
        if($details != null)
        {
            return json_encode($details);
        }
        else
        {
            return 'error';
        }

    }
    
    public function updateAccountDetails(){
        $response = array();
        
        $r = json_decode($_REQUEST['address']);
        
    }
    
    public function checkUrl(){
        
        if (isset($_REQUEST['t'])) {
            
            $token= $_REQUEST['t'];
             
            $uid=base64_decode(strtr($token, '-_,', '+/='));

            //$sql = "SELECT token from customers_auth where uid='$uid'";
            
            $result1 = DB::table('customers_auth')
                    ->select('token')
                    ->where('uid',$uid)
                    ->first();
            
            if($result1 != null){
                $dbtoken= $result1->token;

                if($dbtoken==$token){

                    //$sql = "SELECT name from customers_auth where uid='$uid'";
                    $result2 = DB::table('customers_auth')
                        ->select('name')
                        ->where('uid',$uid)
                        ->first();

                    $name= $result2->name;

                    $data = $uid.'-'.$name;
                    return $data;    
                    //send uid to angular

                }else{
                    return "invalid";
                }
            }
            else{
                return "invalid";
            }

        }else{
            return "invalid";
        }
    }
    
    public function resetPassword(){
        
        $uid = $_REQUEST['uid'];
        $newpassword= $_REQUEST['password2'];

        $hashedPassword=$this->hash($newpassword);

           //$sql = "UPDATE customers_auth set password='$hashedPassword' where uid='$uid'";
        
        DB::table('customers_auth')
                    ->where('uid', $uid)
                    ->update(['password' => $hashedPassword,'updated_at' => date("Y-m-d H:i:s")]);
        
        return 'success';
    }
    
    public function oauth_login()
    {
        $r = json_decode($_REQUEST['customer']);
        
        $products = json_decode($_REQUEST['items']);
        
        $login_url = $_REQUEST['login_url'];
        $login_through = 0;
        
        if (strpos($login_url, "meghajewellers.vivocarat.com") !== FALSE)
        {
            $jid = DB::table('jeweller_information')
                    ->select(DB::raw('id'))
                    ->where('name','like','%megha%')
                    ->first();
            $login_through = $jid->id;
        }
        
        $register_url = $login_url;
        $register_through = $login_through;
        
        $response = array();
        $email = $r->email;
		$name = $r->name;

        //$user = $db->getOneRecord("select uid,name,password,email,phone,created from customers_auth where phone='$email' or email='$email'");
        $userArr = DB::table('customers_auth')
                    ->select(DB::raw('uid,name,password,email,phone,created_at'))
                    ->where('phone',$email)
                    ->orWhere('email',$email)
                    ->first();
        
        if ($userArr != NULL) 
		{
            $user = json_decode(json_encode($userArr), True);
            DB::table('customers_auth')
                        ->where('uid',$user['uid'])
                        ->update(['login_through' => $login_through,'login_url' => $login_url,'updated_at' => date("Y-m-d H:i:s")]);
            
			$response['status'] = "success";
			$response['message'] = 'Logged in successfully.';
			$response['name'] = $user['name'];
			$response['uid'] = $user['uid'];
			$response['email'] = $user['email'];   
			$response['phone'] = $user['phone'];
			$response['createdAt'] = $user['created_at'];
			if (!isset($_SESSION)) {
				session_start();
			}
			$_SESSION['uid'] = $user['uid'];
			$_SESSION['email'] = $email;
			$_SESSION['name'] = $user['name'];
			$_SESSION['phone'] = $user['phone'];
			
			$uid = $user['uid'];
			
			$this->savetocart($products,$uid);
			
			$items = $this->getCartItems($uid);
			$response['items'] = $items;
			return json_encode($response);	
        }
        else 
        {
            $customer = new CustomersAuth;
            
            $customer->name = $name;
            $customer->email = $email;
            $customer->register_through = $register_through;
            $customer->register_url = $register_url;
            $customer->login_url = $login_url;
            $customer->login_through = $login_through;
            $result = $customer->save();
			
			if ($result) {
                
                $user = DB::table('customers_auth')
                    ->select(DB::raw('uid'))
                    ->where('email',$email)
                    ->first();
                
                $response["status"] = "success";
                $response["message"] = "User account created successfully";
                $response["uid"] = $user->uid;
                if (!isset($_SESSION)) {
                    session_start();
                }
                $_SESSION['uid'] = $user->uid;
                $_SESSION['phone'] = '';
                $_SESSION['name'] = $name;
                $_SESSION['email'] = $email;
                
                $uid = $user->uid;
                
                $this->savetocart($products,$uid);
                
                $items = $this->getCartItems($uid);
                $response['items'] = $items;

                $data = array(
                    'name' => $name,
                    'phone' => '',
                    'email' => $email
                );
                
                \Mail::to($email)->send(new SendRegisterMail($data));
                
                $adminemail1 = "a@vivocarat.com";
                $adminemail2 = "r@vivocarat.com";
                
                \Mail::to($adminemail1)->send(new SendRegisterAdminMail($data));
                \Mail::to($adminemail2)->send(new SendRegisterAdminMail($data));

                return json_encode($response);
                
            } 
			else 
			{
                $response["status"] = "error";
                $response["message"] = "Failed to create customer. Please try again";
                
                return json_encode($response);
            }
        }		
    }
	
	public function oauth_login_google()
	{
		return "success";
	}
	
	public function oauth_login_googleplus()
	{
		$client = new \GuzzleHttp\Client();
		$r = json_decode($_REQUEST['customer']);
        
        $login_url = $_REQUEST['login_url'];
        $login_through = 0;
        
        if (strpos($login_url, "meghajewellers.vivocarat.com") !== FALSE)
        {
            $jid = DB::table('jeweller_information')
                    ->select(DB::raw('id'))
                    ->where('name','like','%megha%')
                    ->first();
            $login_through = $jid->id;
        }
        
        $register_url = $login_url;
        $register_through = $login_through;
        
        $products = json_decode($_REQUEST['items']);
		$params = [
            'code' => $r->code,
            'client_id' => $r->clientId,
            'client_secret' => '_l9cv8gfL90moCJzxqfwSQJe',
            'redirect_uri' => $r->redirectUri,
            'grant_type' => 'authorization_code',
        ];
		$accessTokenResponse = $client->createRequest('POST', 'https://accounts.google.com/o/oauth2/token', [
            'body' => $params
        ]);
		$accessTokenResponse =  $client->send($accessTokenResponse);
        $accessToken = json_decode($accessTokenResponse->getBody(), true);
		
		$profileResponse = $client->createRequest('GET', 'https://www.googleapis.com/plus/v1/people/me/openIdConnect', [
            'headers' => array('Authorization' => 'Bearer ' . $accessToken['access_token'])
        ]);
		$profileResponse =  $client->send($profileResponse);
        $profile = json_decode($profileResponse->getBody(), true);
		        
        $response = array();
        $email = $profile["email"];
		$name = $profile["name"];

        //$user = $db->getOneRecord("select uid,name,password,email,phone,created from customers_auth where phone='$email' or email='$email'");
        $userArr = DB::table('customers_auth')
                    ->select(DB::raw('uid,name,password,email,phone,created_at'))
                    ->where('phone',$email)
                    ->orWhere('email',$email)
                    ->first();
        
        if ($userArr != NULL) 
		{
            $user = json_decode(json_encode($userArr), True);
            DB::table('customers_auth')
                        ->where('uid',$user['uid'])
                        ->update(['login_through' => $login_through,'login_url' => $login_url,'updated_at' => date("Y-m-d H:i:s")]);
            
			$response['status'] = "success";
			$response['message'] = 'Logged in successfully.';
			$response['name'] = $user['name'];
			$response['uid'] = $user['uid'];
			$response['email'] = $user['email'];   
			$response['phone'] = $user['phone'];
			$response['createdAt'] = $user['created_at'];
			if (!isset($_SESSION)) {
				session_start();
			}
			$_SESSION['uid'] = $user['uid'];
			$_SESSION['email'] = $email;
			$_SESSION['name'] = $user['name'];
			$_SESSION['phone'] = $user['phone'];
			
			$uid = $user['uid'];
			
			$this->savetocart($products,$uid);
			
			$items = $this->getCartItems($uid);
			$response['items'] = $items;
			return json_encode($response);	
        }
        else 
        {
            $customer = new CustomersAuth;
            
            $customer->name = $name;
            $customer->email = $email;
            $customer->register_through = $register_through;
            $customer->register_url = $register_url;
            $customer->login_url = $login_url;
            $customer->login_through = $login_through;
            $result = $customer->save();
			
			if ($result) {
                
                $user = DB::table('customers_auth')
                    ->select(DB::raw('uid'))
                    ->where('email',$email)
                    ->first();
                
                $response["status"] = "success";
                $response["message"] = "User account created successfully";
                $response["uid"] = $user->uid;
                if (!isset($_SESSION)) {
                    session_start();
                }
                $_SESSION['uid'] = $user->uid;
                $_SESSION['phone'] = '';
                $_SESSION['name'] = $name;
                $_SESSION['email'] = $email;
                
                $uid = $user->uid;
                
                $this->savetocart($products,$uid);
                
                $items = $this->getCartItems($uid);
                $response['items'] = $items;

                $data = array(
                    'name' => $name,
                    'phone' => '',
                    'email' => $email
                );
                
                \Mail::to($email)->send(new SendRegisterMail($data));
                
                $adminemail1 = "a@vivocarat.com";
                $adminemail2 = "r@vivocarat.com";
                
                \Mail::to($adminemail1)->send(new SendRegisterAdminMail($data));
                \Mail::to($adminemail2)->send(new SendRegisterAdminMail($data));

                return json_encode($response);
                
            } 
			else 
			{
                $response["status"] = "error";
                $response["message"] = "Failed to create customer. Please try again";
                
                return json_encode($response);
            }
        }
	}
    
    public function addPageVisits()
    {
        $uid = $_REQUEST['uid'];
        $visited_url = $_REQUEST['visited_url'];
        $visited_at = date("Y-m-d H:i:s");
        $visited_through = 0;
        
        if (strpos($visited_url, "login") !== FALSE)
        {
            $custArr['visit_modal'] = false;
            return json_encode($custArr);
        }
        
        if (strpos($visited_url, "https://www.vivocarat.com") !== FALSE)
        {
            $visited_through = 0;
        }
        else if (strpos($visited_url, "meghajewellers.vivocarat.com") !== FALSE)
        {
            $jid = DB::table('jeweller_information')
                    ->select(DB::raw('id'))
                    ->where('name','like','%megha%')
                    ->first();
            $visited_through = $jid->id;
        }
        
        $cust = DB::table('customers_page_visits')
                    ->where('uid',$uid)
                    ->orderBy('visited_at','desc')
                    ->first();
        
        $custArr = json_decode(json_encode($cust), True);
        if($custArr['visited_through'] != $visited_through)
        {
            $custArr['visit_modal'] = true;
        }
        else
        {
            $custArr['visit_modal'] = false;
        }
        
        DB::table('customers_page_visits')->insert(
                ['uid' => $uid,'visited_url' => $visited_url,'visited_at' => $visited_at,'visited_through' => $visited_through]
            );
                
        return json_encode($custArr);
        
    }
}