<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;

class Search extends Controller
{
    public function gettagdescription(){
        $tag = $_REQUEST['tag'];

        $description = DB::table('search')
                    ->where('tag','like','%'.$tag.'%')
                    ->first();
        
        if($description != null)
        {
        }
        else
        {
            $description = DB::table('search')
                    ->where('tag','like','default')
                    ->first();
        }
        return json_encode($description);
    }
    
    public function getProductsTag(){
        
        $c = isset($_REQUEST['c']) ? $_REQUEST['c'] : "";
        $parent = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
        $tag = isset($_REQUEST['tag']) ? $_REQUEST['tag'] : "";
        
        if (strpos($tag, '+') !== FALSE)
        {
            $tagarr = explode('+', $tag);
            $param = "OR";
            //print_r($tagarr);

            $sql2="SELECT * from products where ";
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
            
            $sql2=$sql2."GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            
            $tags = DB::select(DB::raw($sql2));
        }
        elseif (strpos($tag, ',') !== FALSE)
        {
            $tagarr = explode(',', $tag);
            $param = "AND";
            //print_r($tagarr);
            
            $sql2="SELECT * from products where ";
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
            
            $sql2=$sql2."GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            
            $tags = DB::select(DB::raw($sql2));
        }
        else
        {
//            $tags = DB::table('products')
//                    ->where('secondary_tags','like','%'.$tag.'%')
//                    ->groupBy('VC_SKU')
//                    ->orderBy('id','desc')
//                    ->offset(intval($page)*8)
//                    ->limit(8)
//                    ->get();
            $sql2="SELECT * from products where secondary_tags like '%".$tag."%' GROUP BY VC_SKU ORDER BY -sortorder DESC, id desc limit 8 offset ".(intval($page)*8);
            $tags = DB::select(DB::raw($sql2));
        }
        

        return json_encode($tags);
    }
    
    public function getProductsTagCount(){
        
        $c = isset($_REQUEST['c']) ? $_REQUEST['c'] : "";
        $parent = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
        $tag = isset($_REQUEST['tag']) ? $_REQUEST['tag'] : "";
        
        
        if (strpos($tag, '+') !== FALSE)
        {
            $tagarr = explode('+', $tag);
            $param = "OR";
            //print_r($tagarr);

            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where ";
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
            
            $tag_count = DB::select(DB::raw($sql2));
        }
        elseif (strpos($tag, ',') !== FALSE)
        {
            $tagarr = explode(',', $tag);
            $param = "AND";
            //print_r($tagarr);
            
            $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where ";
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
                        
            $tag_count = DB::select(DB::raw($sql2));
        }
        else
        {
            $tag_count = DB::table('products')
                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
                    ->where('secondary_tags','like','%'.$tag.'%')
                    ->get();
        }

        //$sql2="SELECT count(DISTINCT (VC_SKU)) as count  from products where secondary_tags like '%,".$tag.",%'";
        
//        $tag_count = DB::table('products')
//                    ->select(DB::raw('count(DISTINCT(VC_SKU)) as count'))
//                    ->where('secondary_tags','like','%'.$tag.'%')
//                    ->get();
        
        return json_encode($tag_count);
    }
    
    public function filterPriceTag(){
        
        $tag = isset($_REQUEST['tag']) ? $_REQUEST['tag'] : "";
        $c = isset($_REQUEST['c']) ? $_REQUEST['c'] : "";
        $parent = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";

        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];
        
        $isRoseGold = isset($_REQUEST['isRoseGold']) ? $_REQUEST['isRoseGold'] : false;
        
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];

        $sql2="SELECT * from products where ";
//        if($tag!="" && $tag!=null ){
//            $sql2=$sql2."where secondary_tags like'%,".$tag.",%'"; 
//        }
              
        if (strpos($tag, '+') !== FALSE)
        {
            $tagarr = explode('+', $tag);
            $param = "OR";
            //print_r($tagarr);
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
        }
        elseif (strpos($tag, ',') !== FALSE)
        {
            $tagarr = explode(',', $tag);
            $param = "AND";
            //print_r($tagarr);
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
        }
        else
        {
            $sql2=$sql2."secondary_tags like'%,".$tag.",%'"; 
        }
        
        //***********************************
        if($isGold=="true" || $isWhiteGold=="true"|| $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
            $sql2=$sql2." OR ";
        }

            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){
             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){
              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isRoseGold=="true"){

              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true" ||  $isSilver=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Rose Gold%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        
        if($is18=="true"){
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }
        
        if($is14=="true"){
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
              if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }

        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
              if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
              if($isCasual=="true" || $isBridal=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }
        
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        $jewel=json_decode($jewellers);
        if(count($jewel)>0)
        {
          $sql2=$sql2."AND (";

            for($i=0;$i<count($jewel);$i++){
                if($jewel[$i]!=null){
                if($i>0){
                 $sql2=$sql2."OR ";
                }
                $sql2=$sql2." supplier_name = '".$jewel[$i]."'";
            }
            }

          $sql2=$sql2.")";
        }


        if($s==null){
            $s=0;
        }
        if($e==null){
            $e=500000;
        }
        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e." GROUP BY VC_SKU limit 8 offset ".(intval($page)*8);

        
        $filterpricetag = DB::select(DB::raw($sql2));
        return json_encode($filterpricetag);
    }
    
    public function filterPriceTagCount(){
        
        $tag = isset($_REQUEST['tag']) ? $_REQUEST['tag'] : "";
        $c = isset($_REQUEST['c']) ? $_REQUEST['c'] : "";
        $parent = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";

        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];
        $isRoseGold = isset($_REQUEST['isRoseGold']) ? $_REQUEST['isRoseGold'] : false;
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];

        $sql2="SELECT count(DISTINCT (VC_SKU)) as count  from products where ";

        
        if (strpos($tag, '+') !== FALSE)
        {
            $tagarr = explode('+', $tag);
            $param = "OR";
            //print_r($tagarr);
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
        }
        elseif (strpos($tag, ',') !== FALSE)
        {
            $tagarr = explode(',', $tag);
            $param = "AND";
            //print_r($tagarr);
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
        }
        else
        {
            $sql2=$sql2."secondary_tags like'%,".$tag.",%'"; 
        }
        
        //***********************************
        if($isGold=="true" || $isWhiteGold=="true"|| $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){

             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){

            if($isGold=="true" || $isWhiteGold=="true" ||   $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isRoseGold=="true"){

              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true" ||  $isSilver=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Rose Gold%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isRoseGold=="true" || $isSilver=="true" || $isPlatinum=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        
        if($is18=="true"){
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }
        
        if($is14=="true"){
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
              if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }

        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
              if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }

            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
              if($isCasual=="true" || $isBridal=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }
        
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }

        $jewel=json_decode($jewellers);
        if(count($jewel)>0)
        {
          $sql2=$sql2."AND (";

            for($i=0;$i<count($jewel);$i++){
                if($jewel[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2." supplier_name = '".$jewel[$i]."'";
                }
            }

          $sql2=$sql2.")";
        }


        if($s==null){
            $s=0;
        }
        if($e==null){
            $e=500000;
        }
        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e;
        
        $filterpricetagcount = DB::select(DB::raw($sql2));
        return json_encode($filterpricetagcount);
    }
    
    public function sortListTag(){
        
        $tag = isset($_REQUEST['tag']) ? $_REQUEST['tag'] : "";
        $c = isset($_REQUEST['c']) ? $_REQUEST['c'] : "";
        $parent = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $param=$_REQUEST['p'];
        if($param == "" || $param == null)
        {
            $param = "-sortorder DESC,id desc";
        }
        if($param == "id desc")
        {
            $param = "-sortorder DESC,id desc";
        }
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];

        //$sql2="SELECT * from products ";

        $sql2="SELECT * from products where ";

        
        if (strpos($tag, '+') !== FALSE)
        {
            $tagarr = explode('+', $tag);
            $param1 = "OR";
            //print_r($tagarr);
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
        }
        elseif (strpos($tag, ',') !== FALSE)
        {
            $tagarr = explode(',', $tag);
            $param1 = "AND";
            //print_r($tagarr);
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
        }
        else
        {
            $sql2=$sql2."secondary_tags like'%,".$tag.",%'"; 
        }
        
        //***********************************
        if($isGold=="true" || $isWhiteGold=="true" || $isSilver=="true" || $isSilver=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){

             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){

              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isSilver=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        
        if($is18=="true"){
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }
        
        if($is14=="true"){
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
              if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }

        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
              if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
              if($isCasual=="true" || $isBridal=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }
        
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }
        
        $jewel=json_decode($jewellers);
        if(count($jewel)>0)
        {
          $sql2=$sql2."AND (";

            for($i=0;$i<count($jewel);$i++){
                if($jewel[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2." supplier_name = '".$jewel[$i]."'";
                }
            }

          $sql2=$sql2.")";
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e." GROUP BY VC_SKU order by ".$param." limit 8 offset ".(intval($page)*8);

        
        $sorttag = DB::select(DB::raw($sql2));
        return json_encode($sorttag);
    }
    
    public function sortListTagCount(){
        
        $tag = isset($_REQUEST['tag']) ? $_REQUEST['tag'] : "";
        $c = isset($_REQUEST['c']) ? $_REQUEST['c'] : "";
        $parent = isset($_REQUEST['type']) ? $_REQUEST['type'] : "";
        $s = $_REQUEST['s'];
        $e = $_REQUEST['e'];
        $param=$_REQUEST['p'];
        if($param == "" || $param == null)
        {
            $param = "-sortorder DESC,id desc";
        }
        if($param == "id desc")
        {
            $param = "-sortorder DESC,id desc";
        }
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
        $isGold = $_REQUEST['isGold'];
        $isWhiteGold = $_REQUEST['isWhiteGold'];
        $isPlatinum = $_REQUEST['isPlatinum'];
        $isSilver = $_REQUEST['isSilver'];
        $is22= $_REQUEST['is22'];
        $is18 = $_REQUEST['is18'];
        $is14 = $_REQUEST['is14'];
        $isOther = $_REQUEST['isOther'];
        $isCasual = $_REQUEST['isCasual'];
        $isFashion = $_REQUEST['isFashion'];
        $isBridal = $_REQUEST['isBridal'];
        $jewellers = $_REQUEST['jewellers'];
        //$isVS = $_REQUEST['isVS']; //$isVVS = $_REQUEST['isVVS']; //$isDEdu = $_REQUEST['isDEdu']; //$isMale = $_REQUEST['isMale']; //$isFemale= $_REQUEST['isFemale']; //$isUniSex= $_REQUEST['isUniSex'];

        //$sql2="SELECT * from products ";

        $sql2="SELECT count(DISTINCT (VC_SKU)) as count from products where ";
//        if($tag!="" && $tag!=null ){
//            $sql2=$sql2."where secondary_tags like'%,".$tag.",%'"; 
//
//        }
        
        if (strpos($tag, '+') !== FALSE)
        {
            $tagarr = explode('+', $tag);
            $param1 = "OR";
            //print_r($tagarr);
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
        }
        elseif (strpos($tag, ',') !== FALSE)
        {
            $tagarr = explode(',', $tag);
            $param1 = "AND";
            //print_r($tagarr);
            
            for($i=0;$i<count($tagarr);$i++){
                if($tagarr[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."AND ";
                    }
                    $sql2=$sql2."secondary_tags like'%,".$tagarr[$i].",%'";
                }
            }
        }
        else
        {
            $sql2=$sql2."secondary_tags like'%,".$tag.",%'"; 
        }
        
        //***********************************
        if($isGold=="true" || $isWhiteGold=="true" || $isSilver=="true" || $isSilver=="true"){
            $sql2=$sql2."AND (";
        }

        if($isGold=="true"){
            $sql2=$sql2."metal like '%Gold%'";
        }

        if($isWhiteGold=="true"){
            if($isGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%White Gold%'";
        }

        if($isPlatinum=="true" ){

             if($isGold=="true" || $isWhiteGold=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Platinum%'";
        }

        if($isSilver=="true"){

              if($isGold=="true" || $isWhiteGold=="true" || $isPlatinum=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."metal like '%Silver%'";
        }

        if($isGold=="true" || $isWhiteGold=="true" || $isSilver=="true"){
            $sql2=$sql2.")";
        }

        //***********************************

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2."AND (";
        }

        if($is22=="true"){
            $sql2=$sql2." purity= '22 KT'";
        }
        
        if($is18=="true"){
             if($is22=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '18 KT'";
        }
        
        if($is14=="true"){
             if($is22=="true" || $is18=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity= '14 KT'";
        }

        if($isOther=="true"){
              if($is22=="true" || $is18=="true" || $is14=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." purity != '14 KT'  OR purity != '18 KT'  OR purity != '22 KT'";
        }

        if($is22=="true" || $is18=="true" || $is14=="true" || $isOther=="true"){
            $sql2=$sql2.")";
        }

        //***********************************
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2."AND (";
        }

        if($isCasual=="true"){
            $sql2=$sql2."  occasion like '%Casual%'";
        }

        if($isBridal=="true"){
              if($isCasual=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2." occasion like '%Bridal%'";
        }

        if($isFashion=="true"){
              if($isCasual=="true" || $isBridal=="true"){
                $sql2=$sql2." OR ";
            }
            $sql2=$sql2."  occasion like '%Fashion%'";
        }
        
        if($isCasual=="true" || $isBridal=="true" || $isFashion=="true"){
            $sql2=$sql2.")";
        }
        
        $jewel=json_decode($jewellers);
        if(count($jewel)>0)
        {
          $sql2=$sql2."AND (";

            for($i=0;$i<count($jewel);$i++){
                if($jewel[$i]!=null)
                {
                    if($i>0){
                     $sql2=$sql2."OR ";
                    }
                    $sql2=$sql2." supplier_name = '".$jewel[$i]."'";
                }
            }

          $sql2=$sql2.")";
        }

        $sql2=$sql2. " and price_after_discount BETWEEN ".$s." AND ".$e;

        
        $sorttagcount = DB::select(DB::raw($sql2));
        return json_encode($sorttagcount);
    }
}