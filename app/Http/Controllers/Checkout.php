<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\CustomersAuth;
use App\Addresses;
use App\OrdersTemp;
use App\Coupon;
use App\Cart;
use App\Mail\SendOrderConfirmationMail;

class Checkout extends Controller
{
    public function sendSMS($mobile,$message)
    {
        $data = array();
        //Your authentication key
        $authKey = "173057AunX4NLcYuf59ad5e75";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $mobile;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "VIVOCT";

        //Your message to send, Add URL encoding here.
        $message = urlencode($message);

        //Define route 
        $route = 4;

        //Prepare you post parameters
        $postData = array(
            'sender' => $senderId,
            'message' => $message,
            'mobiles' => $mobileNumber,
            'authkey' => $authKey,
            'campaign' => 'viaSOCKET',
            'unicode' => 0,
            'flash' => 0,
            'country' => 91,
            'route' => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            $data['success'] = false;
            $data['errors'] = curl_error($ch);
            return json_encode($data); 
        }

        curl_close($ch);

        $data['success'] = true;
        $data['output'] = $output;
        return json_encode($data); 
    }
    
    public function object_to_array($data)
    {
        if (is_array($data) || is_object($data))
        {
            $result = array();
            foreach ($data as $key => $value)
            {
                $result[$key] = $this->object_to_array($value);
            }
            return $result;
        }
        return $data;
    }
    
    public function addOrder(Request $request){
        
//        $o = json_decode($_REQUEST['order']);
        $o = json_decode($request->input('order'));
        
        $uid= $o->uid;
        $total=$o->total;
        $promo=$o->promo;
        $items = $o->items;
        $comment = $o->comment;
        $isGift = $o->isGift;
        
        $orders = DB::table('orders_temp')
                    ->select('id','items')
                    ->where('uid',$uid)
                    ->where('status','NOT_CONFIRMED')
                    ->orderBy('id','desc')
                    ->get();
        
        $checkcart = DB::table('cart')
                    ->select('cart_ref_id')
                    ->where('user_id',$uid)
                    ->where('is_active','1')
                    ->first();
        $cart_ref_id = $checkcart->cart_ref_id;
        
        if($orders != null){
            
            foreach($orders as $order)
            {
                $o_id = $order->id;
                    
                $diff = strcmp($order->items, json_encode($items));
            
                if($diff === 0)
                {
                    DB::table('orders_temp')
                    ->where('id',$o_id)
                    ->update(['cart_ref_id' => $cart_ref_id,'updated_at' => date("Y-m-d H:i:s")]);
                    
                    $arr[0]=$order;
                    return json_encode($arr);
                }
            }
            
        }
        
        try 
        {
            //$sql3="INSERT INTO orders_temp (id,uid,items,total,comment,isGift,promo,created_at) VALUES (0,".$uid.",'".json_encode($items)."',".$total.",'".$comment."',".$isGift.",'".$promo."',NOW());";

            DB::table('orders_temp')->insert(
                ['uid' => $uid, 'items' => json_encode($items), 'total' => $total, 'comment' => $comment, 'isGift' => $isGift, 'promo' => $promo,'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")]
                );

            try{
                //$sql4="SELECT id from orders_temp where uid=".$uid." ORDER BY id desc LIMIT 1";

                $orderid = DB::table('orders_temp')
                    ->select('id')
                    ->where('uid',$uid)
                    ->orderBy('id','desc')
                    ->limit(1)
                    ->get();
                
                $o_id = $orderid[0]->id;
                DB::table('orders_temp')
                    ->where('id',$o_id)
                    ->update(['cart_ref_id' => $cart_ref_id,'updated_at' => date("Y-m-d H:i:s")]);

                return json_encode($orderid);    
            }
            catch(\Exception $e){
                return $e;
            }
        }
        catch (\Exception $e) {
            return $e;
        }
                
    }
    
    public function getAddress(){
        
        $uid = $_REQUEST['uid'];
        //$sql2="SELECT * from addresses where uid=".$uid." order by address_id desc";
        
        $address = DB::table('addresses')
                    ->where('uid',$uid)
                    ->orderBy('address_id','desc')
                    ->get();
        return json_encode($address);
    }
    
    public function getAddressFromAccount(){
        
        $uid = $_REQUEST['uid'];
    
        $address = DB::table('customers_address')
                    ->where('uid',$uid)
                    ->first();
        return json_encode($address);
    }
    
    public function addNewAddress(){
        
        $add=json_decode($_REQUEST['address']);
        $s_add=json_decode($_REQUEST['ship_address']);
        
        $orderId= $add->orderId;

        $name = $add->name;
        $ph= $add->phone;
        $fl = $add->first_line;
        $email = $add->email;
        $uid= $add->uid;

        $ct = $add->city;
        $sta= $add->state;
        $pin = $add->pin;
        $cty = $add->country;


        $s_name = $s_add->name;
        $s_ph= $s_add->phone;
        $s_fl = $s_add->first_line;
        $s_email = $s_add->email;
        $s_uid= $s_add->uid;

        $s_ct = $s_add->city;
        $s_sta= $s_add->state;
        $s_pin = $s_add->pin;
        $s_cty = $s_add->country;
            
        try{
            DB::table('addresses')->insert(
                ['name' => $name, 'phone' => $ph, 'first_line' => $fl, 'uid' => $uid, 'order_id' => $orderId, 'city' => $ct, 'state' => $sta, 'pin' => $pin, 'country' => $cty, 'updated_at' => date("Y-m-d H:i:s")]
                );

        }
        catch(\Exception $e){
            return $e;
        }
        
        try{
            DB::table('addresses')->insert(
                ['name' => $s_name, 'phone' => $s_ph, 'first_line' => $s_fl, 'uid' => $uid, 'order_id' => $orderId, 'city' => $s_ct, 'state' => $s_sta, 'pin' => $s_pin, 'country' => $s_cty, 'updated_at' => date("Y-m-d H:i:s")]
                );

        }
        catch(\Exception $e){
            return $e;
        }

        try{
            DB::table('orders_temp')
                    ->where('id', $orderId)
                    ->update(['name' => $name,'email' => $s_email,'phone' => $s_ph,'updated_at' => date("Y-m-d H:i:s")]);

        }
        catch(\Exception $e){
            return $e;
        }
        
        $addressArr = DB::table('addresses')
                    ->select('address_id')
                    ->where('order_id',$orderId)
                    ->orderBy('address_id','desc')
                    ->limit(2)
                    ->get();
        
        if ($addressArr != NULL) {
            $adrs = json_decode(json_encode($addressArr), True);
                        
            try{
                DB::table('orders_temp')
                    ->where('id', $orderId)
                    ->update(['bill_address_id' => $adrs[1]['address_id'],'ship_address_id' => $adrs[0]['address_id'],'updated_at' => date("Y-m-d H:i:s")]);
                return 'success';

            }
            catch(\Exception $e){
                return $e;
            }
            
        }
        else
        {
            return 'error';   
        }
    }
    
    public function getCoupons(){
        
        //$sql2="SELECT * from coupon where status='true'";
        
        try{

            $coupon = DB::table('coupon')
                ->where('status','true')
                ->get();

            return json_encode($coupon);  
        }
        catch(\Exception $e){
            return $e;
        }
    }
    
    public function getOrderDetails(){
        
        $id = $_REQUEST['id'];
        $orderdata = array();
        
        $result1 = DB::table('orders_temp')
            ->leftJoin('addresses', 'orders_temp.bill_address_id', '=', 'addresses.address_id')
            ->where('orders_temp.id',$id)
            ->get();

        array_push($orderdata,$result1[0]);
        
        $result2 = DB::table('orders_temp')
            ->leftJoin('addresses', 'orders_temp.ship_address_id', '=', 'addresses.address_id')
            ->where('orders_temp.id',$id)
            ->get();
        
        array_push($orderdata,$result2[0]);
        
        return json_encode($orderdata);
    }
    
    public function confirmOrder(){
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];
        $cust_email = $_REQUEST['email'];
        $isCod = $_REQUEST['isCod'];
		$isGift = $_REQUEST['isGift'];
        $txid = $_REQUEST['txid'];
        
        DB::table('orders_temp')
            ->where('id', $id)
            ->update(['status' => 'CONFIRMED','isCod' => $isCod,'isGift' => $isGift,'order_gen_id' => $txid,'updated_at' => date("Y-m-d H:i:s")]);
        
        $order = DB::table('orders_temp')
                        ->where('id',$id)
                        ->first();
        
        $bill_address_id = $order->bill_address_id;
        $bill_address = '';
        if($bill_address_id != '')
        {
            $bill_address = DB::table('addresses')
                        ->where('address_id',$bill_address_id)
                        ->first();
        }
        
        $ship_address_id = $order->ship_address_id;
        $ship_addres = '';
        if($ship_address_id != '')
        {
            $ship_addres = DB::table('addresses')
                        ->where('address_id',$ship_address_id)
                        ->first();
        }

        $products=$order->items;
        $grand_total=$order->total;
        $payment_mode = '';
        if($order->isCod === '1')
        {
            $payment_mode='Cash on Delivery';
        }
        else if($order->isCod === '0')
        {
            $payment_mode='Prepaid';
        }
        $placed_on = date("j F Y", strtotime($order->created_at));
        $shipping_time = '7-9 Days';
        
        $parr = json_decode($order->items, true);
        $sub_total = 0;
        $index = 0;
        $pname = '';
        foreach ($parr as $p) {        
            $sub_total = $sub_total + $p['item']['price_after_discount'];
            $pname = $p['item']['title'];
            $index++;
        }
        $promo_disc = $sub_total-$grand_total;
        
        $coinflag = 'false';
        $coin_sku = "";
        $coin_name = "";
        $coin_jeweller = "";

        /*if($grand_total >= 15000)
        {
            $coinflag = 'true';
            $coin_sku = "VIVOGCA245";
            $coin_name = "Plain Silver Coin 10 Grams in 999 Purity Fineness";
            $coin_jeweller = "VivoCarat";
        }*/

        $data = array(
                'id' => "VIVOORD".$id,
                'name' => $bill_address->name,
                'shipping' => $ship_addres,
                'billing' => $bill_address,
                'products' => $products,
                'grand_total' => $grand_total,
                'sub_total' => $sub_total,
                'payment_mode' => $payment_mode,
                'placed_on' => $placed_on,
                'shipping_time' => $shipping_time,
                'promo_disc' => $promo_disc,
                'coinflag' => $coinflag,
                'coin_sku' => $coin_sku,
                'coin_name' => $coin_name,
                'coin_jeweller' => $coin_jeweller
            );

        \Mail::to($cust_email)->send(new SendOrderConfirmationMail($data));
        
        $cartrefid = DB::table('orders_temp')
                    ->select('uid','cart_ref_id')
                    ->where('id',$id)
                    ->first();
        
        $cart_ref_id = $cartrefid->cart_ref_id;
        $uid = $cartrefid->uid;
        
        DB::table('cart')
            ->where('user_id',$uid)
            ->where('cart_ref_id',$cart_ref_id)
            ->update(['is_active' => '0','updated_at' => date("Y-m-d H:i:s")]);
        
        if($index > 1)
        {
            $index = $index-1;
            $message = 'Order Confirmation: Hi '.$bill_address->name.', VivoCarat has received your order for '.substr($pname,0,20).'... and '.$index.' more item(s). We will update you soon about the delivery date. Thank you.';
        }
        else
        {
            $message = 'Order Confirmation: Hi '.$bill_address->name.', VivoCarat has received your order for '.substr($pname,0,20).'.... We will update you soon about the delivery date. Thank you.';
        }
        $mobile = $bill_address->phone;
        $output = $this->sendSMS($mobile,$message);
        
        return 'success';
    }
    
    public function removeFromCart(){
        $uid = $_REQUEST['uid'];
        $item = json_decode($_REQUEST['item']);
        $product_id = $item->id;
        
        $checkp = DB::table('cart')
                    ->where('user_id',$uid)
                    ->where('product_id',$product_id)
                    ->where('is_active','1')
                    ->first();
        
        if($checkp != null){
            DB::table('cart')
                ->where('user_id',$uid)
                ->where('product_id',$product_id)
                ->where('is_active','1')
                ->delete();
        }   
    }
}