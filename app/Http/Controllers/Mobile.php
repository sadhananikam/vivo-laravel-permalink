<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Banners;

class Mobile extends Controller
{
    public function getVivoHeader(){
        $p = $_REQUEST['p'];
        $m = $_REQUEST['m'];

        //$sql2="SELECT * from banners where page='".$p."' and is_active=1 and is_Mobile=".$m." order by sequence";
        
        $banner = DB::table('banners')
                    ->select('id','img_url','alt','href','target','is_link')
                    ->where('page',$p)
                    ->where('is_active',1)
                    ->where('is_Mobile',$m)
                    ->orderBy('sequence','asc')
                    ->get();
        
        return json_encode($banner);
    }
}