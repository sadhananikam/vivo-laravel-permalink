<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;

class Sitemap extends Controller
{
    public function getSitemap()
    {
        $text='<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
        <url>
            <loc>https://www.vivocarat.com</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/home" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/home" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>1.00</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/Gold-All.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/Gold-All.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/Gold-All.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.99</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/Diamond-All.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/Diamond-All.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/Diamond-All.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.99</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/Silver-All.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/Silver-All.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/Silver-All.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.99</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/lookbook</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/lookbook" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/lookbook" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.99</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/gold-rings.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/gold-rings.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/gold-rings.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/gold-earrings.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/gold-earrings.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/gold-earrings.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/gold-pendants.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/gold-pendants.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/gold-pendants.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/gold-nosepins.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/gold-nosepins.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/gold-nosepins.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/gold-bangles.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/gold-bangles.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/gold-bangles.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/gold-bracelets.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/gold-bracelets.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/gold-bracelets.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/gold-necklaces.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/gold-necklaces.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/gold-necklaces.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/gold-goldcoins.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/gold-goldcoins.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/gold-goldcoins.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/diamond-rings.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/diamond-rings.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/diamond-rings.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/diamond-earrings.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/diamond-earrings.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/diamond-earrings.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/diamond-pendants.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/diamond-pendants.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/diamond-pendants.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/diamond-noepins.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/diamond-noepins.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/diamond-noepins.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/diamond-Tanmaniya.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/diamond-Tanmaniya.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/diamond-Tanmaniya.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/silver-rings.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/silver-rings.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/silver-rings.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/silver-earrings.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/silver-earrings.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/silver-earrings.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/silver-pendants.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/silver-pendants.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/silver-pendants.html" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery/silver-accessories.html</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery/silver-accessories.html" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery/silver-accessories.html" />            
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.90</priority>
        </url>

        <url>
            <loc>https://www.vivocarat.com/brands/Incocu%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Incocu%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Incocu%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Shankaram%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Shankaram%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Shankaram%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Lagu%20Bandhu</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Lagu%20Bandhu" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Lagu%20Bandhu" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Arkina-Diamonds</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Arkina-Diamonds" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Arkina-Diamonds" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/OrnoMart</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/OrnoMart" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/OrnoMart" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Kundan%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Kundan%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Kundan%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Mayura%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Mayura%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Mayura%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Megha%20Jewellers</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Megha%20Jewellers" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Megha%20Jewellers" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Regaalia%20Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Regaalia%20Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Regaalia%20Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Glitter%20Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Glitter%20Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Glitter%20Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/PP-Gold</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/PP-Gold" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/PP-Gold" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/KaratCraft</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/KaratCraft" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/KaratCraft" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/ZKD-Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/ZKD-Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/ZKD-Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/IskiUski</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/IskiUski" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/IskiUski" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Myrah-Silver-Works</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Myrah-Silver-Works" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Myrah-Silver-Works" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Charu-Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Charu-Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Charu-Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Sarvada-Jewels</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Sarvada-Jewels" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Sarvada-Jewels" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands/Tsara-Jewellery</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/Tsara-Jewellery" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/Tsara-Jewellery" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.85</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/find-your-ring-size</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/find-your-ring-size" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/find-your-ring-size" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/jewellery-education</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/jewellery-education" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/jewellery-education" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/about-us</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/about-us" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/about-us" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/brands</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/contact-us</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/contact-us" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/contact-us" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/privacy</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/privacy" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/privacy" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/terms</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/terms" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/terms" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/faq</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/faq" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/faq" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/partner-with-us</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/partner-with-us" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/partner-with-us" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/seller-locate</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/seller-locate" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/seller-locate" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>
        <url>
            <loc>https://www.vivocarat.com/affiliate</loc>
            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/affiliate" />
            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/affiliate" />
            <lastmod>'.date("Y-m-d").'</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.80</priority>
        </url>';
        
        $allstore = DB::table('jeweller_information')
                    ->select('name')
                    ->get();
        
        if($allstore != null)
        {       
            foreach($allstore as $s)
            {
                $categories = DB::table('products')
                            ->select('category')
                            ->distinct()
                            ->where('supplier_name',$s->name)
                            ->get();
                
                if($categories != null)
                {       
                    foreach($categories as $c)
                    {
                
                        $text=$text.'
                        <url>
                            <loc>https://www.vivocarat.com/brands/'.$s->name.'/'.$c->category.'.html</loc>
                            <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/brands/'.$s->name.'/'.$c->category.'.html" />
                            <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/brands/'.$s->name.'/'.$c->category.'.html" />
                            <lastmod>'.date("Y-m-d").'</lastmod>
                            <changefreq>daily</changefreq>
                            <priority>0.90</priority>
                        </url>';
                    }
                }
            }
        }
        
        $lookbookArticle = DB::table('lookbook')
                    ->select('urlslug')
                    ->get();
        
        if($lookbookArticle != null)
        {       
            foreach($lookbookArticle as $l)
            {
                $text=$text.'
                <url>
                    <loc>https://www.vivocarat.com/lookbook/'.$l->urlslug.'.html</loc>
                    <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/lookbook/'.$l->urlslug.'.html" />
                    <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/lookbook/'.$l->urlslug.'.html" />
                    <lastmod>'.date("Y-m-d").'</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>0.80</priority>
                </url>';
            }
        }
        
        $products = DB::table('products')
                    ->select('urlslug')
                    ->get();
        
        if($products != null)
        {       
            foreach($products as $p)
            {
                //$p_id = $p->id;
                $text=$text.'
                <url>
                    <loc>https://www.vivocarat.com/product/'.$p->urlslug.'.html</loc>
                    <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/product/'.$p->urlslug.'.html" />
                    <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/product/'.$p->urlslug.'.html" />
                    <lastmod>'.date("Y-m-d").'</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>0.98</priority>
                </url>';
            }
        }
        
        $searchtag = DB::table('search')
                    ->select('tag')
                    ->get();
        
        if($searchtag != null)
        {       
            foreach($searchtag as $s)
            {
                if($s->tag != 'default')
                {
                    $text=$text.'
                    <url>
                        <loc>https://www.vivocarat.com/search/'.$s->tag.'.html</loc>
                        <xhtml:link rel="alternate" media="only screen and (max-width: 640px)" href="https://www.vivocarat.com/m/search/'.$s->tag.'.html" />
                        <xhtml:link rel="alternate" media="handheld" href="https://www.vivocarat.com/m/search/'.$s->tag.'.html" />
                        <lastmod>'.date("Y-m-d").'</lastmod>
                        <changefreq>daily</changefreq>
                        <priority>0.98</priority>
                    </url>';
                }
            }
        }
        
        $text=$text. '</urlset>';
        
        file_put_contents('sitemap.xml', $text);
        
        echo "Sitemap generated successfully";
    }
}
