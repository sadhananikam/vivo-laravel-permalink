<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SendOrderConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->data = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'hello@vivocarat.com';
        $name = 'Vivocarat Support';
        $subject = 'Order Confirmation: Your VivoCarat.com Order - '.$this->data['id'].' has been sucessfully placed';
        
        return $this->view('email.orderconfirmation')
                    ->with([
                        'id1'=>$this->data['id']
                           ])
                    ->with([
                        'name1'=>$this->data['name']
                           ])
                    ->with([
                        'shipping1'=>$this->data['shipping']
                           ])
                    ->with([
                        'billing1'=>$this->data['billing']
                           ])
                    ->with([
                        'products1'=>$this->data['products']
                           ])
                    ->with([
                        'grand_total1'=>$this->data['grand_total']
                           ])
                    ->with([
                        'sub_total1'=>$this->data['sub_total']
                           ])
                    ->with([
                        'payment_mode1'=>$this->data['payment_mode']
                           ])
                    ->with([
                        'placed_on1'=>$this->data['placed_on']
                           ])
                    ->with([
                        'shipping_time1'=>$this->data['shipping_time']
                           ])
                    ->with([
                        'promo_disc1'=>$this->data['promo_disc']
                           ])
                    ->with([
                        'coinflag1'=>$this->data['coinflag']
                           ])
                    ->with([
                        'coin_sku1'=>$this->data['coin_sku']
                           ])
                    ->with([
                        'coin_name1'=>$this->data['coin_name']
                           ])
                    ->with([
                        'coin_jeweller1'=>$this->data['coin_jeweller']
                           ])
                    ->from($address,$name)
                    ->bcc($address,$name)
                    ->replyTo($address,$name)
                    ->subject($subject);
    }
}