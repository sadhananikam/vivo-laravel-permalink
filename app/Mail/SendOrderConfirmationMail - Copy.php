<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SendOrderConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->data = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'hello@vivocarat.com';
        $name = 'Vivocarat Support';
        $subject = 'Vivocarat - Order Confirmation';
        
        return $this->view('email.orderconfirmation')
                    ->with([
                        'id1'=>$this->data['id']
                           ])
                    ->with([
                        'name1'=>$this->data['name']
                           ])
                    ->with([
                        'order1'=>$this->data['order']
                           ])
                    ->with([
                        'total1'=>$this->data['total']
                           ])
                    ->with([
                        'promo1'=>$this->data['promo']
                           ])
                    ->with([
                        'coinflag1'=>$this->data['coinflag']
                           ])
                    ->with([
                        'coin_sku1'=>$this->data['coin_sku']
                           ])
                    ->with([
                        'coin_name1'=>$this->data['coin_name']
                           ])
                    ->with([
                        'coin_jeweller1'=>$this->data['coin_jeweller']
                           ])
                    ->from($address,$name)
                    ->bcc($address,$name)
                    ->replyTo($address,$name)
                    ->subject($subject);
    }
}