<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
    
    protected $data;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->data = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'hello@vivocarat.com';
        $name = 'Vivocarat Support';
        $subject = 'Vivocarat -  Newsletter Subscription';
        
        return $this->view('email.newsletter')
                    ->with(['email'=>$this->data])
                    ->from($address,$name)
                    ->replyTo($address,$name)
                    ->subject($subject);
    }
}
