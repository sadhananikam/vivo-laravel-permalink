<html>
 <head>
  <style>
  
   @font-face {
            font-family: 'Leelawadee';
            src: url('https://www.vivocarat.com/fonts/Leelawadee.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        
        body {
            font-family: 'Leelawadee',sans-serif;
            font-size: 15px;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
        }
 </style>
        
     <link href='https://www.vivocarat.com/fonts/Leelawadee.ttf' rel='stylesheet' type='text/css'>
     
</head>
    
<body align='center' style="font-family: 'Leelawadee';font-size: 15px;max-width:600px;margin:0 auto;">

<table cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>
 
 <tr>
  <td>
   <table cellpadding='0' cellspacing='0' align='center' style='padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;'> 
    <tr>
     <td align='center'>
      <img src='https://www.vivocarat.com/images/emailers/rounded-logo.png' alt='Vivocarat logo' title='Vivocarat logo'>  
     </td>
    </tr>
   </table>
  </td>
 </tr>
 
 <tr>
  <td align='center'>
   <a href='https://play.google.com/store/apps/details?id=com.vivocarat.catalogapp&hl=en' style='text-decoration:none;'>
    <img src='https://www.vivocarat.com/images/emailers/vivocarat-app.jpg' alt='Vivocarat Android app' title='Vivocarat Android app' style="width:100%;display:block;">   
   </a>
  </td>  
 </tr>

<tr style='border: 1px solid #e3e3e3;border-bottom: none;'>
 <table cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>
  <tr>
   <td>
    <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
     Hello Dear Customer {{ $email }},
    </p>

    <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
     Thank you for subscribing our newsletter!
    </p>

    <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
     Take a look and scroll through our alluring collection of gold and diamond jewellery at www.vivocarat.com where each product is uniquely crafted under the guidance of strict professionals.
    </p>
       
    <p style='margin-left:20px;margin-right:20px; margin-bottom:0px;font-family: Leelawadee;'>
        We have a registration offer for you, apply code &quot;VC500&quot;
    </p>
       
    <p style='margin-left:20px;margin-right:20px;margin-top:0px;font-family: Leelawadee;'> 
        and get Rs. 500 off on your first purchase at Vivocarat.
    </p> 
       
    <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Happy Shopping!
    </p>

    <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        In case of any query or assistance reach us at +91 9167645314 or mail us to
    </p>

    <p style='margin-left:20px;margin-right:20px;margin-bottom: 40px;font-family: Leelawadee;'>
        hello@vivocarat.com
    </p> 
                
    <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Warm Regards,
    </p>
       
    <p style='margin-bottom:0px;margin-left:20px;margin-right:20px;padding-bottom: 20px;font-family: Leelawadee;'>
     <b>Team VivoCarat.</b>
    </p> 
   </td>
  </tr>  
 </table>
</tr>

<tr>
 <td>
  <a href='https://www.vivocarat.com'>
   <img src='https://www.vivocarat.com/images/emailers/coupon.jpg' alt='VC500 coupon code' style="width: 100%;display:block;">
  </a>   
 </td>
</tr>


<table align='center' style='height: 50px;width:100%;background-color:#f4f4f4;border-right: solid 1px #d4d4d4;border-left: solid 1px #d4d4d4;'>
 <tr>
  <td align='center'>
   <a href='https://www.facebook.com/VivoCarat/' title='Facebook' target='_blank'>
    <img src='https://www.vivocarat.com/images/emailers/facebook-icon.png' style='padding-top: 5px;padding-bottom: 5px;' alt='Facebook'>
   </a>
  </td>
     
  <td align='center'>
   <a href='https://twitter.com/vivocarat' title='Twitter' target='_blank'>
    <img src='https://www.vivocarat.com/images/emailers/twitter-icon.png' alt='Twitter'>
   </a>
  </td>
     
  <td align='center'>
   <a href='https://www.pinterest.com/vivocarat/' title='Pinterest' target='_blank'>  
    <img src='https://www.vivocarat.com/images/emailers/pinterest-icon.png' alt='Pinterest'>
   </a>
  </td>
     
  <td align='center'>
   <a href='https://www.instagram.com/vivocarat/' title='Instagram' target='_blank'>
    <img src='https://www.vivocarat.com/images/emailers/instagram-icon.png' alt='Instagram'>
   </a>
  </td>
 </tr>
</table>
    
<table align='center' style='background-color:#E62739;width: 100%;'>
 <tr>
  <td style='font-size:13px;color:white;text-align: left;padding:7px;color:white;font-family: Leelawadee;'>+91 9167 645 314</td>
     
  <td style='font-size:13px;color:white;text-align: right;padding:7px;color:white;font-family: Leelawadee;'> 
   <a href='mailto:hello@vivocarat.com' style='text-decoration:none;color:white;'>
      hello@vivocarat.com
   </a>
  </td>
 </tr>
</table>
    
<table style='height: 7%;width: 100%;padding-top: 20px;padding-bottom: 20px;text-align: center;border: 1px solid #d4d4d4;font-weight: 500;'>
 <tr>
  <td>
   <span style='font-size:12px;font-family: Leelawadee;'>
         Privacy Policy | Terms &amp; Conditions
   </span>  
  </td>  
 </tr>
     
 <tr>
  <td>
   <span style='font-size:12px;font-family: Leelawadee;'>
         &copy;&nbsp;2016 VivoCarat Retail Pvt. Ltd.All Rights Reserved.
   </span> 
  </td>   
 </tr>
</table>
    
 </table>
    
 </body>
</html>