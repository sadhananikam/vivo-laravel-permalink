<html>
 <head>
  <style>
  
   @font-face {
            font-family: 'leela';
            src: url('https://www.vivocarat.com/fonts/LEELAWADEE.TTF') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        
        /* For Outlook email client compatibility*/
        
        @media screen {
            .webfont {
                font-family: 'Leelawadee', Roboto, Arial, sans-serif !important;
            }
        }
        /*End of compatibility css*/
        
        body {
            font-family: 'Leelawadee', Roboto, sans-serif;
            font-size: 15px;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
        }
 </style>
        
     <link href='https://www.vivocarat.com/fonts/LEELAWADEE.TTF' rel='stylesheet' type='text/css'>
     
</head>
    
<body align='center' style="font-family: 'LEELAWADEE';font-size: 15px;width:600px;margin-left: auto;margin-right: auto;">
            
 <table align='center' style='padding-top: 10px;padding-bottom: 10px;'> 
  <tr>
   <td align='center'>
    <img src='https://www.vivocarat.com/images/emailers/rounded-logo.png' alt='Vivocarat logo'>  
   </td>
  </tr>
 </table>
 
<a href='https://play.google.com/store/apps/details?id=com.vivocarat.catalogapp&hl=en' style='text-decoration:none;'>

<div align='center' style='text-align:left;height: 40px;width: 597px;margin-left: auto;margin-right: auto;border: 1px solid #e3e3e3;border-bottom:none;'>

        <div style="font-family: 'leela';text-align:left;font-size:16px;font-weight: 100;">
         <img src='https://www.vivocarat.com/images/emailers/vivocarat-app.jpg' alt='Vivocarat Android app'>
        </div>
</div>
</a>

<div style='text-align:left;width: 596px;margin-left: auto;margin-right: auto;border: 1px solid #e3e3e3;border-bottom: none;'>
    
<p style='margin-left:20px;margin-right:20px;'>Hello <b> {{ $name1 }},</b></p>

<p style='margin-left:20px;margin-right:20px;'>Greetings from VivoCarat.com!</p>

<p style='margin-bottom:20px;margin-left:20px;margin-right:20px;'>Thank you for choosing Vivocarat.com and purchasing from us.</p>

<p style='margin-left:20px;margin-right:20px;margin-bottom:15px;'>Your order details are as below</p>
 
@php    
$temp="";
$items=json_decode($order1);
$temp="

<table style='font-size: 13px;border-collapse: collapse;'>
<tr style='text-align: center;height: 30px;background-color: #c7c7c7;'>
    <th style='border-right: 1px solid #e3e3e3;'>SKU</th>
    <th style='border-right: 1px solid #e3e3e3;'>Name</th>
    <th style='border-right: 1px solid #e3e3e3;'>Jeweller</th>
    <th style='border-right: 1px solid #e3e3e3;'>Size</th>
    <th style='border-right: 1px solid #e3e3e3;'>Quantity</th>
    <th style='border-right: 1px solid #e3e3e3;'>Price before discount</th>
    <th style='border-right: 1px solid #e3e3e3;'>Discount</th>
    <th>Price after discount</th>
</tr>


";

foreach($items as $i){


$temp=$temp.'<tr>
<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;padding: 10px 1px;">'.$i->item->VC_SKU."</td>";
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;">'.$i->item->title."</td>";
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;">'.$i->item->supplier_name."</td>";

if($i->item->ring_size != null && $i->item->ring_size != '')
{
    $temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;'>".$i->item->ring_size."</td>"; 
}
elseif($i->item->bangle_size != null && $i->item->bangle_size != '')
{
    $temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;'>".$i->item->bangle_size."</td>"; 
}
elseif($i->item->bracelet_size != null && $i->item->bracelet_size != '')
{
    $temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;'>".$i->item->bracelet_size."</td>"; 
}
else{
    $temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;'>NA</td>"; 
}

$temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;'>".$i->quantity."</td>";
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;">'.$i->item->price_before_discount * $i->quantity."/-</td>";
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;">'.$i->item->discount."%</td>";
    
$temp=$temp.'<td style="border-bottom: 1px solid #e3e3e3;text-align: right;">'.$i->item->price_after_discount * $i->quantity."/-</td>";

}
$temp=$temp.'</tr>';


$temp=$temp.'<tr><td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;height: 25px;padding: 10px 1px;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>'; 
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;color: #E62739;width:103px;">Promotional Offer</td>';
    
$temp=$temp.'<td style="border-bottom: 1px solid #e3e3e3;color: #E62739;text-align: right;padding-right: 5px;">'.$promo1.'</td>';


$temp=$temp.'</tr>';


$temp=$temp.'<tr><td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;height:25px;padding: 10px 1px;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>'; 
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;color:#E62739;text-align: right;padding-right: 5px;">Total</td>';
    
$temp=$temp.'<td style="border-bottom: 1px solid #e3e3e3;color:#E62739;text-align: right;padding-right: 5px;">'.$total1.'/-</td>';
    
$temp=$temp.'</tr>';    
    
if($coinflag1 === 'true')
{
$temp=$temp.'<tr><td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;height: 25px;padding: 10px 1px;">'.$coin_sku1.'</td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;">'.$coin_name1.'</td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;">'.$coin_jeweller1.'</td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>'; 
    
$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;color: #E62739;width:103px;"></td>';
    
$temp=$temp.'<td style="border-bottom: 1px solid #e3e3e3;color: #E62739;text-align: right;padding-right: 5px;">Free</td>';


$temp=$temp.'</tr>';
}


$temp=$temp.'</table>';
    
$temp=$temp.'<h3 style="color: #E62739;font-weight:bold;margin-left:20px;margin-right:20px;">Total : '.$total1.'/-</h3>';

echo $temp;
@endphp

<p style='margin-bottom:20px;margin-left:20px;margin-right:20px;'>Your Order No. {{ $id1 }} has been confirmed. We will send your delivery details shortly.</p>

<p style='margin-bottom:20px;margin-left:20px;margin-right:20px;'>We hope you had a wonderful experience and look forward for a positive feedback from your end. Do visit us again!</p>

<p style='margin-bottom:20px;text-decoration:none;color:#000000 !important;margin-left:20px;margin-right:20px;'>In case of any query or assistance reach us at +91 9503 781 870 or mail us to <a href='mailto:hello@vivocarat.com' style='text-decoration:none;color: #000000!important;'>hello@vivocarat.com.</a>
</p>

<p style='margin-left:20px;margin-right:20px;'>Warm Regards,</p>
<p style='margin-bottom:0px;margin-left:20px;margin-right:20px;padding-bottom: 20px;'><b>Team VivoCarat.</b></p>

</div>

<table align='center' style='height: 50px;width: 597px;background-color:#f4f4f4;border-right: solid 1px #d4d4d4;border-left: solid 1px #d4d4d4;border-top: solid 1px #d4d4d4;' align='center'>
        <tr>

            <td align='center' style='width:25%'></td>
            <td align='center'>
                <a href='https://www.facebook.com/VivoCarat/' title='Facebook' target='_blank'><img src='https://www.vivocarat.com/images/emailers/facebook-icon.png' style='padding-top: 5px;padding-bottom: 5px;' alt='Facebook'></a>
            </td>
            <td align='center'>
                <a href='https://twitter.com/vivocarat' title='Twitter' target='_blank'><img src='https://www.vivocarat.com/images/emailers/twitter-icon.png' alt='Twitter'></a>
            </td>
            <td align='center'>
                <a href='https://www.pinterest.com/vivocarat/' title='Pinterest' target='_blank'><img src='https://www.vivocarat.com/images/emailers/pinterest-icon.png' alt='Pinterest'></a>
            </td>
            <td align='center'>
                <a href='https://www.instagram.com/vivocarat/' title='Instagram' target='_blank'><img src='https://www.vivocarat.com/images/emailers/instagram-icon.png' alt='Instagram'></a>
            </td>
            <td align='center' style='width:25%'></td>

        </tr>
    </table>
    
    <table align='center' style='background-color:#E62739;width: 597px;'>
        <tr>
            <td align='center' style='font-size:15px;color:white;text-align: center;padding:7px;color:white;'>CALL US: +91 9167 645 314</td>
            <td align='center' style='width:15%'></td>
            <td align='center' style='font-size:15px;color:white;text-align: center;padding:7px;color:white;'>EMAIL US: <a href='mailto:hello@vivocarat.com' style='text-decoration:none;color:white;'>hello@vivocarat.com</a></td>
        </tr>
    </table>
    
    <div style='height: 7%;padding-top: 20px;padding-bottom: 20px;text-align: center;border: 1px solid #d4d4d4;font-weight: 500;width: 595px;margin-left: auto;margin-right: auto;'>
        <p style='font-size:12px; margin:0px;'>Privacy Policy | Terms & Conditions</p>
        <p style='font-size:12px; margin:0px;'>&copy;&nbsp;2016 VivoCarat Retail Pvt. Ltd.All Rights Reserved.</p>
    </div>
    
    </body>
</html>