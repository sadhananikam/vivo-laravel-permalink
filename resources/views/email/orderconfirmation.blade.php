<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Jewellery Order with VivoCarat.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <style>
        @media screen {
            .webfont {
                font-family: 'Leelawadee', Roboto, Arial, sans-serif !important;
            }
        }
        
        .heading {
            font-size: 20px;
        }
        
        .price-green {
            font-size: 14px;
        }
        
        .contact {
            width: 50%; float: left; box-sizing: border-box; margin: 10px 0;padding-left: 10px;padding-right: 10px;
        }
         
         .contact.right{
             text-align: right;
         }
        
        .wrapper {
            padding: 15px;
        }
        
        @media all and (max-width: 460px) {
            * {
                font-size: 10px;
            }
            .heading {
                font-size: 14px !important;
            }
            .price-green {
                font-size: 12px !important;
            }
            .text-center {
                text-align: center;
            }
            .contact {
                width: 100%; text-align: center !important;
            }
            .wrapper {
                padding: 0;
            }
        }
    </style>
</head>
    
<body style="margin:0; padding:0;">
    <table class="wrapper" style="background-color: #f3f3f3; max-width:600px; margin:0 auto; font-family: 'Leelawadee', Roboto, sans-serif; font-size: 15px;" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <table cellpadding='0' cellspacing='0' style="width:100%;">
                        <tbody>
                            <tr>
                                <td style="text-align:center; padding-bottom:15px;">
                                    <img src='https://www.vivocarat.com/images/emailers/rounded-logo.png' alt='Vivocarat logo' title='Vivocarat logo'>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; background: #ccc; padding:3px;">
                                    <img src='https://www.vivocarat.com/images/emailers/google-play-icon.png' alt='Vivocarat Android app' title='Vivocarat Android app'>
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#f1f1f1; padding:10px;">
                                    <p style="line-height:1.5;">
                                        Hello <b> {{ $name1 }},</b><br>
                                        <br> Greetings from VivoCarat! Thank you for choosing Vivocarat and purchasing from us.
                                    </p>
                                    <p style="line-height:25px;">
                                        Your order details are as below:
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" style="background:white; width:100%">
                                        <tr>
                                            <td style="width:50%; padding:10px 0 10px 10px; line-height:2; font-size:11px; border-bottom:1px solid #f1f1f1; vertical-align:top; margin:0;">
                                                <div style="overflow:auto; border:1px solid #f1f1f1; height:185px;">
                                                    <h2 style="font-size:12px; padding:5px; background:#f3f3f3; text-align:center; margin:0;">
                                                        Order Details
                                                    </h2>
                                                    <b style="color:#9c9a9a; padding-left:5px;">Order ID:</b> {{ $id1 }}<br>
                                                    <div style="padding-left:5px;">
                                                        <b style="color:#9c9a9a;">Payment Mode:</b> {{ $payment_mode1 }}<br>
                                                    </div>
                                                    <b style="color:#9c9a9a; padding-left:5px;">Order Date:</b> {{ $placed_on1 }}<br>
                                                    <b style="color:#9c9a9a; padding-left:5px;">Shipping Time:</b> {{ $shipping_time1 }}<br>
                                                </div>
                                            </td>
                                            <td style="width:50%; padding:10px; line-height:2; font-size:12px; border-bottom:1px solid #f1f1f1;">
                                                <div style="overflow:auto; border:1px solid #f1f1f1; height:185px;">
                                                    <h2 style="font-size:12px; padding:5px; background:#f3f3f3; text-align:center; margin:0;">
                                                        Delivery Address
                                                    </h2>
                                                    <p style="color:#787878; margin:0; padding-left:5px;">
                                                        <b class="price-green">{{ $shipping1->name }}</b><br> {{ $shipping1->first_line }},<br> {{ $shipping1->city }} {{ $shipping1->pin }}<br> {{ $shipping1->state }}<br> {{ $shipping1->country }}<br> Contact: +91 {{ $shipping1->phone }}
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" cellspace="10px;">
                                                <h2 class="heading" style="padding:5px 0; margin:5px 10px; background:#f3f3f3; text-align:center;">Your Order</h2>
                                            </td>
                                        </tr>
                                    @php    
                                    $temp="";
                                    $items =json_decode($products1);
                                    @endphp
                                    @foreach ($items as $i)
                                        <tr>
                                        <td style="padding:10px; line-height:2; font-size:12px;" colspan="2">
                                            <div style="border-bottom:1px solid #f1f1f1; overflow:auto;">
                                                <div style="width:15%; float:left; padding: 10px 0;">
                                                    <img style="width:100%; border:1px solid #f1f1f1;" src="https://www.vivocarat.com/images/products-v2/{{$i->item->VC_SKU}}-1.jpg" alt="product" />
                                                </div>
                                                <div style="width:45%; float:left; padding:4px 10px;  box-sizing:border-box;">
                                                    <span style="color:#e62739;">{{$i->item->title}}</span><br>
                                                    <b style="color:#9c9a9a; padding-right:5px;">SKU:</b> {{$i->item->VC_SKU}}<br>
                                                    <b style="color:#9c9a9a; padding-right:5px;">Quantity:</b> {{$i->quantity}}<br>
                                                    <b style="color:#9c9a9a; padding-right:5px;">Brand:</b> {{$i->item->supplier_name}}<br>

                                                    @if($i->item->ring_size != null && $i->item->ring_size != '')
                                                        <b style="color:#9c9a9a; padding-right:5px;">Ring Size:</b>{{$i->item->ring_size}}<br>
                                                    @elseif($i->item->bangle_size != null && $i->item->bangle_size != '')
                                                        <b style="color:#9c9a9a; padding-right:5px;">Bangle Size:</b>{{$i->item->bangle_size}}<br>
                                                    @elseif($i->item->bracelet_size != null && $i->item->bracelet_size != '')
                                                        <b style="color:#9c9a9a; padding-right:5px;">Bracelet Size:</b>{{$i->item->bracelet_size}}<br>
                                                    @endif
                                                </div>
                                                <div style="width:40%; float:left; font-size:12px">
                                                    <div style="max-width:200px;">
                                                        <div style="width:50%; float:left;">
                                                            <b style="color:#9c9a9a;">
                                    Price<span style="float:right;">:</span></b>
                                                        </div>
                                                        <div style="width:50%; float:left; padding-left:5px; box-sizing:border-box;">
                                                            Rs.{{$i->item->price_before_discount}}
                                                        </div>
                                                        <br>
                                                        <div style="width:50%; float:left;">
                                                            <b style="color:#9c9a9a;">
                                     Discount<span style="float:right;">:</span></b>
                                                        </div>
                                                        <div style="width:50%; float:left; padding-left:5px; box-sizing:border-box;">
                                                            Rs.{{$i->item->discount_total}}
                                                        </div>
                                                        <hr style="background:#f1f1f1; margin-right:15px;" />
                                                        <div style="width:50%; float:left;">
                                                            <b class="price-green" style="color:#9c9a9a;">
                                     Payable<span style="float:right;">:</span></b>
                                                        </div>
                                                        <div class="price-green" style="color:#00bb27; font-weight:bold; width:50%; float:left; padding-left:5px; box-sizing:border-box;">
                                                            Rs.{{$i->item->price_after_discount}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        </tr>
                                    @endforeach   
                                    @php
                                    if($coinflag1 === 'true')
                                    {
                                        $temp=$temp.'<tr>
                                            <td style="padding:10px; line-height:2; font-size:12px;" colspan="2">
                                                <div style="border-bottom:1px solid #f1f1f1; overflow:auto;">
                                                    <div style="width:15%; float:left; padding: 10px 0;">
                                                        <img style="width:100%; border:1px solid #f1f1f1;" src="https://www.vivocarat.com/images/products-v2'.'/'.$coin_sku1.'-1.jpg" alt="product" />
                                                    </div>
                                                    <div style="width:45%; float:left; padding:4px 10px;  box-sizing:border-box;">
                                                        <span style="color:#e62739;">'.$coin_name1.'</span><br>
                                                        <b style="color:#9c9a9a; padding-right:5px;">SKU:</b> '.$coin_sku1.'<br>
                                                        <b style="color:#9c9a9a; padding-right:5px;">Brand:</b> '.$coin_jeweller1.'<br>';
                                                    $temp=$temp.'</div>
                                                    <div style="width:40%; float:left; font-size:12px">
                                                        <div style="max-width:200px;">
                                                            <div style="width:50%; float:left;">
                                                                <b style="color:#9c9a9a;">
                                                                Free</b>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td></tr>';
                                    }
                                        $temp= $temp.'<tr>
                                            <td style="padding:10px; line-height:2; font-size:12px; border-bottom:1px solid #f1f1f1;" colspan="2">
                                                <table style="width:65%" cellpadding="0" cellspacing="0" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width:60%;">
                                                                <div style="font-size:14px; color:#00bb27; font-weight:bold;">
                                                                    Subtotal<span style="float:right; font-size:14px;">:</span>
                                                                </div>
                                                            </td>
                                                            <td style="width:40%;">
                                                                <div style="padding-left:5px; box-sizing:border-box; font-size:14px; color:#00bb27; font-weight:bold;">
                                                                    Rs.'.$sub_total1.'
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:60%;">
                                                                Shipping<span style="float:right;">:</span>
                                                            </td>
                                                            <td style="width:40%;">
                                                                <div style="padding-left:5px; box-sizing:border-box;">
                                                                    Rs. 0
                                                                </div>
                                                            </td>
                                                        </tr>';
                                                        if($promo_disc1 > 0)
                                                        {
                                                            $temp=$temp.'<tr>
                                                                <td style="width:60%;">
                                                                    Promotional Discounts<span style="float:right;">:</span>
                                                                </td>
                                                                <td style="width:40%;">
                                                                    <div style="padding-left:5px; box-sizing:border-box;">
                                                                        Rs.'.$promo_disc1.'
                                                                    </div>
                                                                </td>
                                                            </tr>';
                                                        }
                                                        $temp= $temp.'<tr>
                                                            <td style="width:60%;">
                                                                <div style="font-size:14px; color:#e62739; font-weight:bold;">
                                                                    Order Total<span style="float:right; font-size:14px;">:</span>
                                                                </div>
                                                            </td>
                                                            <td style="width:40%;">
                                                                <div style="padding-left:5px; box-sizing:border-box; font-size:14px; color:#e62739; font-weight:bold;">
                                                                    Rs.'.$grand_total1.'
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>';
                                    echo $temp;
                                    @endphp                                        
                                    </table>
                                </td>
                            </tr>
                            <tr style="background:white;">
                                <td colspan="2" cellspace="10px;">
                                    <h2 class="heading" style="margin-top:15px; padding:5px 0; margin:5px 10px; background:#f3f3f3; text-align:center;">WHAT NEXT?</h2>
                                </td>
                            </tr>
                            <tr style="background:white;">
                                <td colspan="2">
                                    <ul style="line-height: 1.5; font-size: 14px; color: #797979; padding-left: 25px;">
                                        <li>We will be calling you on +91 {{ $billing1->phone }} for your confirmation.</li>
                                        <li>If you are unreachable, please contact our Customer Delight Team on +91 9503781870 to confirm your order within 24 hours of making the purchase.</li>
                                        <li>Dispatch date may vary subject to confirmation of your order.</li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style='width:100%;background-color:#f4f4f4;border-right: solid 1px #d4d4d4;border-left: solid 1px #d4d4d4;border-top: solid 1px #d4d4d4;' align='center'>
                                        <tr>
                                            <td align='center'>
                                                <a href='https://www.facebook.com/VivoCarat/' title='Facebook' target='_blank'><img src='https://www.vivocarat.com/images/emailers/facebook-icon.png' style='padding-top: 5px;padding-bottom: 5px;' alt='Facebook'></a>
                                            </td>
                                            <td align='center'>
                                                <a href='https://twitter.com/vivocarat' title='Twitter' target='_blank'><img src='https://www.vivocarat.com/images/emailers/twitter-icon.png' alt='Twitter'></a>
                                            </td>
                                            <td align='center'>
                                                <a href='https://www.pinterest.com/vivocarat/' title='Pinterest' target='_blank'><img src='https://www.vivocarat.com/images/emailers/pinterest-icon.png' alt='Pinterest'></a>
                                            </td>
                                            <td align='center'>
                                                <a href='https://www.instagram.com/vivocarat/' title='Instagram' target='_blank'>
                                                    <img src='https://www.vivocarat.com/images/emailers/instagram-icon.png' alt='Instagram'>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>

                                    <div style="background-color:#E62739; color:white; font-size:13px; overflow:auto;">
                                        <div class="contact">
                                            CALL US: +91 9167 645 314
                                        </div>
                                        <div class="contact right">
                                            EMAIL US:
                                            <a href='mailto:hello@vivocarat.com' style='text-decoration:none; color:white;'>hello@vivocarat.com</a>
                                        </div>
                                    </div>

                                    <div style='text-align: center; border: 1px solid #d4d4d4;font-weight: 500; background-color: white; width: 100%; box-sizing:border-box; padding:10px;'>
                                        <p style="font-size:12px; line-height:20px; margin:5px 0; color:#797979;">Privacy Policy | Terms & Conditions
                                            <br>&copy;&nbsp;2016 VivoCarat Retail Pvt. Ltd.All Rights Reserved.
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>    
</html>